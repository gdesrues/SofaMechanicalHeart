from Sofa import Simulation
from Sofa.Core import Node

from utils.plugins import import_plugins


def createGraph(root):
    import_plugins()

    out_path = (
        "/home/gdesrues/Documents/dev/SofaMechanicalHeart/Python/test_old_scene/out_with4e3"
    )

    # root
    root.findData("dt").value = 0.005
    root.findData("gravity").value = [0, 0, 0]

    # root.addObject(
    #     "RequiredPlugin",
    #     pluginName="SofaImplicitOdeSolver SofaGraphComponent CardiacMeshTools MechanicalHeart SofaPython",
    # )

    # root/MecaNode
    MecaNode = root.addChild("MecaNode")
    MecaNode.addObject(
        "CardiacVTKLoader",
        name="loader",
        filename=f"{out_path}/anatomy/mesh_final_meca.vtk",
        TetraTriangle="Tetra",
        unitTime="ms",
        TetraZoneName="Volume_Areas ",
        startContraction=0.12000000000000001,
        FileFacetFiber=f"{out_path}/functional/SyntheticFibers.tbb",
        FileContractionParameters=f"{out_path}/functional/Contraction.txt",
        FileTetraConductivity=f"{out_path}/functional/Conductivity.txt",
        FileStiffnessParameters=f"{out_path}/functional/Stiffness.txt",
        ElectroFile=f"{out_path}/functional/DepolTimeAndAPD.txt",
    )
    MecaNode.addObject(
        "EulerImplicitSolver",
        name="meca_solver",
        tags="meca",
        rayleighStiffness=0.02,
        rayleighMass=0.05,
        firstOrder=0,
    )
    MecaNode.addObject(
        "CGLinearSolver",
        name="linear_solver",
        tags="meca",
        iterations="200",
        tolerance="1e-12",
        threshold="1e-12",
        template="GraphScattered",
    )
    MecaNode.addObject(
        "MechanicalObject",
        name="mecaObj",
        tags="meca",
        position="@loader.position",
        restScale="1",
        scale="1e-3",
        template="Vec3d",
    )
    MecaNode.addObject(
        "TetrahedronSetTopologyContainer",
        name="ContainerTetra",
        tags="meca",
        triangles="@loader.triangles",
        tetrahedra="@loader.tetrahedra",
    )
    MecaNode.addObject("TetrahedronSetTopologyModifier", name="Modifier")
    MecaNode.addObject(
        "TetrahedronSetGeometryAlgorithms", name="GeomAlgo", template="Vec3d"
    )
    MecaNode.addObject(
        "MRForceField",
        name="MR_FF",
        tags="meca",
        ParameterSet="@loader.StiffnessParameters",
        matrixRegularization="1",
        file=f"{out_path}/others/Jacobian.txt",
        depolarisationTimes="@loader.depoTimes",
        APD="@loader.APDTimes",
        activeRelaxation=1,
        MaxPeakActiveRelaxation=1,
        template="Vec3d",
    )
    MecaNode.addObject(
        "ContractionForceField",
        name="contraction",
        tags="meca",
        viscosityParameter="1.7e5",
        heartPeriod="0.8",
        addElastometry=True,
        useCoupling=True,
        tagContraction="tagContraction",
        elasticModulus="1e7",
        fiberDirections="@loader.facetFibers",
        tetraContractivityParam="@loader.ContractionParameters",
        depolarisationTimes="@loader.depoTimes",
        APD="@loader.APDTimes",
        calculateStress=True,
        stressFile=f"{out_path}/others/CauchyStress.txt",
        SecSPK_passive="@MR_FF.MRStressPK",
        file=f"{out_path}/others/Jacobian3.txt",
        template="Vec3d",
    )
    # MecaNode.addObject(
    #     "BaseConstraintForceField",
    #     name="Apex_String",
    #     tags="meca",
    #     useForce="1",
    #     normal="0 0 1",
    #     kn="400",
    #     kp="400",
    #     Zone=["Zone17"],
    #     loadername="VTK",
    #     template="Vec3d",
    # )
    # MecaNode.addObject(
    #     "BaseConstraintForceField",
    #     name="String_Rings",
    #     tags="meca",
    #     useForce="1",
    #     normal="0 0 1",
    #     kn=250,
    #     temporaryTimes="0.05 0.15 0.3",
    #     kp=250,
    #     heartPeriod="0.8",
    #     Zone="Zone22",
    #     loadername="VTK",
    #     template="Vec3d",
    # )

    MecaNode.addObject(
        "BaseConstraintForceField",
        name="String_Atrium",
        tags="meca",
        useForce="1",
        normal="0 0 1",
        kn=4000,
        temporaryTimes="0.05 0.15 0.3",
        temporaryForce=True,
        # kp=250,
        heartPeriod="0.8",
        Zone="Zone22",
        loadername="VTK",
        template="Vec3d",
    )

    MecaNode.addObject(
        "PressureConstraintForceField",
        name="pressureforceR",
        printLog=True,
        tags="meca",
        loadername="VTK",
        loaderZoneNames="@loader.surfaceZoneNames",
        loaderZones="@loader.surfaceZones",
        pointZoneName="@loader.pointZoneNames",
        pointZones="@loader.pointZones",
        trianglesSurf="@loader.trianglesSurf",
        atriumParam="0.0001 665 997 70 70 0.05 0.15 0.3",
        aorticParam="1e-6 1995 532 0.400918297 77453890.8 3055980.31 75431.0511",
        Pv0=665,
        heartPeriod="0.8",
        file=f"{out_path}/pressureR.txt",
        Kiso="0 0",
        windkessel="4",
        SurfaceZone="SurfZone30",
        useProjection="1",
        DisableFirstAtriumContraction="false",
        ZoneType="Triangles",
        BoundaryEdgesPath=f"{out_path}/anatomy/boundary_edges.json",
        BoundaryEdgesKey="RV",
        template="Vec3d",
        tolerance=0,
        # loader_link="@loader",
        # mean_stress="@contraction.mean_stress",
    )
    MecaNode.addObject(
        "PressureConstraintForceField",
        name="pressureforceL",
        printLog=True,
        tags="meca",
        loadername="VTK",
        loaderZoneNames="@loader.surfaceZoneNames",
        loaderZones="@loader.surfaceZones",
        pointZoneName="@loader.pointZoneNames",
        pointZones="@loader.pointZones",
        trianglesSurf="@loader.trianglesSurf",
        atriumParam="0.0001 1596 2394 70 70 0.05 0.15 0.3",
        aorticParam="1e-6 10640 7980 0.400918297 77453890.8 3055980.31 75431.0511",
        Pv0=1596,
        heartPeriod="0.8",
        file=f"{out_path}/pressureL.txt",
        Kiso="0 0",
        windkessel="4",
        SurfaceZone="SurfZone31",
        useProjection="1",
        DisableFirstAtriumContraction="false",
        ZoneType="Triangles",
        BoundaryEdgesPath=f"{out_path}/anatomy/boundary_edges.json",
        BoundaryEdgesKey="LV",
        template="Vec3d",
        file2=f"{out_path}/Pressure_Phase.txt",
        tolerance=0,
        # loader_link="@loader",
        # mean_stress="@contraction.mean_stress",
    )
    MecaNode.addObject(
        "ContractionInitialization",
        name="InitializationContraction",
        tags="meca",
        template="ContractionInitialization<Vec3d,Vec3d>",
    )
    MecaNode.addObject(
        "ProjectivePressureConstraint",
        name="project",
        printLog=True,
        tags="meca",
        optimiser=False,
        FFNames="pressureforceR pressureforceL",
        template="Vec3d",
        file=f"{out_path}/others/ProjectiveConstraint.txt",
    )
    MecaNode.addObject(
        "DiagonalMass", name="DiagonalMass", tags="meca", massDensity=1070
    )
    # MecaNode.addObject(
    #     "CardiacSimulationExporter",
    #     name="VtkExporter",
    #     Filename=f"{out_path}/vtk/simu_",
    #     ExportStartStep=0,
    #     ExportFileType=".vtk",
    #     ExportEveryNSteps=1,
    #     ExportEc=False,
    #     ExportE1d=False,
    #     ExportSigmaC=False,
    #     ExportScale="1e3",
    #     Tetras="1",
    #     contraction_forcefield="@contraction",
    # )

    MecaNode.addObject(
        "VTKExporter",
        name="simu_exporter",
        filename=f"{out_path}/vtk/simu_.vtk",
        XMLformat=False,
        edges=False,
        tetras=True,
        exportEveryNumberOfSteps=1,
        listening=True,
    )

    # root/Coupling
    Coupling = root.addChild("Coupling")
    Coupling.addObject(
        "EulerImplicitSolver",
        tags="tagContraction",
        rayleighStiffness="0",
        rayleighMass="0",
        firstOrder="true",
    )
    Coupling.addObject("Gravity", tags="tagContraction", gravity="0 0 0")
    Coupling.addObject(
        "CGLinearSolver",
        name="linear-solver-coupling",
        tags="tagContraction",
        template="GraphScattered",
    )
    Coupling.addObject(
        "MechanicalObject",
        name="contractObj",
        tags="tagContraction",
        position="@../MecaNode/InitializationContraction.outputs",
        template="Vec3d",
    )
    Coupling.addObject(
        "ContractionCouplingForceField",
        tags="tagContraction",
        tetraContractivityParam="@../MecaNode/loader.ContractionParameters",
        depolarisationTimes="@../MecaNode/loader.depoTimes",
        APD="@../MecaNode/loader.APDTimes",
        n0=1,
        alpha=0.5,
        StarlingEffect=False,
        n1=0.3,
        n2=-0.4,
        useSimple=False,
        withFile=True,
        file=f"{out_path}/others/Jacobian2.txt",
    )
    Coupling.addObject("UniformMass", tags="tagContraction", vertexMass="1")


# def onBeginAnimationStep(self, dt):
#     print("Simulation time: " + str(self._total_simu_time))
#     self._total_simu_time += dt


if __name__ == "__main__":
    root = Node("root")

    createGraph(root)

    Simulation.init(root)

    for i in range(160):
        Simulation.animate(root, root.findData("dt").value)
