import logging

import numpy as np
import treefiles as tf
import pandas as pd
from matplotlib import pyplot as plt

HEADER_11 = [
    "Volume",
    "Pat",
    "Pv",
    "Par",
    "Q",
    "time",
    "PV0",
    "QV",
    "IsoPhase",
    "EjectionPhase",
    "RealPhase",
]


def main():
    d1 = "out"
    d2 = "out_temporaryForce"
    # d3 = "out_with4e3"

    root = tf.f(__file__)
    fname = "pressureL.txt"

    pr1 = np.loadtxt(root / d1 / fname)
    df1 = pd.DataFrame(pr1, columns=HEADER_11)

    pr2 = np.loadtxt(root / d2 / fname)
    df2 = pd.DataFrame(pr2, columns=HEADER_11)

    t = np.array(list(range(160))) * 5 - 100
    Pa2mmHg = 0.00750062

    with tf.SPlot():
        fig, axs = plt.subplots(nrows=2, ncols=2, sharex="all", sharey="row", figsize=(8, 6))
        axs = axs.ravel()

        ax = axs[0]
        ax.plot(t, df1["Volume"] * 1e6)
        ax.set_ylabel("Volume (mL)")

        ax = axs[2]
        ax.plot(t, df1["Pat"] * Pa2mmHg, label="Pat")
        ax.plot(t, df1["Pv"] * Pa2mmHg, label="Pv")
        ax.plot(t, df1["Par"] * Pa2mmHg, label="Par")
        ax.set_ylabel("Pressure (mmHg)")
        ax.set_xlabel("Default")

        ax = axs[1]
        ax.plot(t, df2["Volume"] * 1e6)

        ax = axs[3]
        ax.plot(t, df2["Pat"] * Pa2mmHg, label="Pat")
        ax.plot(t, df2["Pv"] * Pa2mmHg, label="Pv")
        ax.plot(t, df2["Par"] * Pa2mmHg, label="Par")
        ax.set_xlabel("With atrium force")
        ax.legend()

        tf.despine()
        fig.tight_layout()


log = logging.getLogger(__name__)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    log = tf.get_logger()

    main()
