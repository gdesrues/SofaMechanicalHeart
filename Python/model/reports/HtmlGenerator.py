import logging
from collections import defaultdict

import pandas as pd
import treefiles as tf
from MeshObject import Mesh
from jinja2 import Environment, PackageLoader, select_autoescape

from model.analyse.features.base_feature import Features
from model.labels import Labels
from model.params.ModelParams import ModelParams


class InputData:
    def __init__(self, parent):
        self.p = parent.p  # reference to model

        tf.copyfile(self.p.data_tree.clip_top, self.p.simu_tree.plots)
        tf.copyfile(self.p.data_tree.clip_long, self.p.simu_tree.plots)
        tf.copyfile(self.p.data_tree.ecg, self.p.simu_tree.plots)
        tf.copyfile(tf.f(__file__) / "templates/MyWebApp.js", self.p.simu_tree.plots)
        m = Mesh.load(self.p.simu_tree.data.mesh_final).convertToPolyData()
        m.addPointData(m.getPointDataArray("depol") * 1e3, "Activ Time (ms)")
        m.clear("Activ Time (ms)", "apd", Labels.ENDOS_EPI, Labels.VENTRICLES)
        m.write(
            self.p.simu_tree.plots.dir("mesh").dump() / "xml_mesh.vtp", type="xmlpd"
        )

        d = tf.load_json(self.p.data_tree.deform.infos)
        self.deform_params = tf.Bases(d["params"])
        self.deform_features = tf.Bases(d["features"])


class InputParams:
    def __init__(self, params):
        self.params = params
        self.key_title = {
            "ACTIVE": "Active model/Contraction",
            "PASSIVE": "Passive hyperelastic material",
            "ARTERY_LV": "Aorta",
            "ARTERY_RV": "Pulmonary artery",
            "ATRIUM_LV": "Left atrium",
            "ATRIUM_RV": "Right atrium",
            "GENERAL": "General",
            "PATHS": "Paths",
        }

        self.groups = defaultdict(list)
        for k in self.key_title.keys():
            for p in self.params.values():
                if k in p.parents:
                    self.groups[k].append(p)


class HtmlGenerator:
    def __init__(self, parent):
        self.env = Environment(
            loader=PackageLoader("model.reports.HtmlGenerator"),
            autoescape=select_autoescape(),
        )
        self.p = parent  # reference to model
        self.input_params = None
        self.input_data = None
        self.indices = None

        self.params_desc_fname = tf.f(__file__) / "params_description.ods"
        self.features_desc_fname = tf.f(__file__) / "features.ods"

    def render(self, fname: str):
        template = self.env.get_template("index.html")
        aa = template.render(
            input_params=self.input_params,
            input_data=self.input_data,
            indices=self.indices,
            enumerate=enumerate,
            round=round,
        )
        aa = aa.replace("\u2013", "-")
        tf.dump_str(fname, aa)
        log.info(f"HTML report wrote to file://{fname}")

    def set_params(self):
        params = ModelParams.from_file(self.p.simu_tree.simu_params)

        df = pd.read_excel(self.params_desc_fname, engine="odf")
        # print(df)
        # breakpoint()

        for k, v in params.items():
            ct = [""] * 3
            f = df.loc[df.Key == v.name]
            f = f.fillna("")
            if not f.empty:
                ct[0] = f.Description.values[0]
                ct[1] = f.Unit.values[0]
                ct[2] = f.Name.values[0]
            setattr(params[k], "_description", ct[0])
            setattr(params[k], "_unit", ct[1])
            setattr(params[k], "_name", ct[2])

        self.input_params = InputParams(params)

    def set_input_data(self):
        self.input_data = InputData(self)

    def set_features(self):
        self.indices = Features(tf.load_json(self.p.simu_tree.indices))

        for k in self.indices:
            self.indices[k].value = round(self.indices[k].value, 2)

            u = self.indices[k].unit
            self.indices[k].unit = u if u else ""

            u = self.indices[k].description
            self.indices[k].description = u if u else ""

            u = self.indices[k].pretty_name
            self.indices[k].pretty_name = u if u else ""


log = logging.getLogger(__name__)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    log = tf.get_logger()

    gen = HtmlGenerator()
    gen.render(
        "/user/gdesrues/home/Documents/dev/SofaMechanicalHeart/Python/elec_data/out/Case_0_1/meca_001/test.html"
    )
