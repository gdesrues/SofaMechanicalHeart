import logging

import pandas as pd
import treefiles as tf
from matplotlib import pyplot as plt


def plot_ventricle(fname: tf.TS):
    df = pd.read_csv(fname, index_col="i")
    print(df)

    t = df["t"] * 1e3  # ms
    Pv = df["Pv"] * 0.00750062  # mmHg
    Pat = df["Pat"] * 0.00750062  # mmHg
    Pat = df["Pat"] * 0.00750062  # mmHg
    Par = df["Par"] * 0.00750062  # mmHg
    Q = df["Q"]  # m^3/s
    V = df["Vol"] * 1e6  # mL

    out_name = tf.f(fname, "plots").dump() / "plots_lv.png"
    with tf.SPlot(fname=out_name):
        fig, axs = plt.subplots(nrows=2, figsize=(4, 6), sharex="all")
        axs = axs.ravel()
        fig.suptitle("LV")

        ax = axs[0]
        ax.plot(t, V)
        ax.set_ylabel("Volume")

        ax = axs[1]
        ax.plot(t, Pv, label="Pv")
        ax.plot(t, Par, label="Par")
        ax.plot(t, Pat, label="Pat")
        ax.legend()
        ax.set_xlabel("Time (ms)")
        ax.set_ylabel("Pressure (mmHg)")

        tf.despine(fig)
        fig.tight_layout()


log = logging.getLogger(__name__)

# if __name__ == "__main__":
#     logging.basicConfig(level=logging.DEBUG)
#     log = tf.get_logger()
#
#     simu_path = tf.Tree(
#         "/user/gdesrues/home/Documents/dev/SofaMechanicalHeart/Python/elec_data/out/Case_0_1/meca_001"
#     )
#
#     plot_ventricle(simu_path / "pressure_lv.txt")
