import logging

import treefiles as tf


def get_tree(root: tf.TS, key: str) -> tf.T:
    assert key in ("data", "simu")
    x = tf.Tree.from_file(tf.f(__file__) / f"{key}.tree")
    x.root = root
    return x


def get_data_tree(root: tf.TS) -> tf.T:
    return get_tree(root, "data")


def get_simu_tree(root: tf.TS) -> tf.T:
    return get_tree(root, "simu")


log = logging.getLogger(__name__)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    log = tf.get_logger()

    data = get_data_tree("/test")
    print(data)
    print(data.anatomy.abs())
    print(data.lv)
    print(data.lv.sibling("endo_lv.obj"))
