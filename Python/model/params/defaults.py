import logging

import treefiles as tf

from model.params.param import Param, DefaultParam


class MetaDefaultParams(type):
    def __new__(mcs, name, bases, dct):
        dct.update(get_atts(name, DefaultParam))
        return super().__new__(mcs, name, bases, dct)


class MetaModelParams(type):
    def __new__(mcs, name, bases, dct):
        dct.update({"__defaults__": get_atts(name, Param)})
        return super().__new__(mcs, name, bases, dct)


def flatten(d, parents=None):
    if parents is None:
        parents = []
    items = []
    for k, v in d.items():
        new_parents = parents + [k]
        if isinstance(v, dict):
            items.extend(flatten(v, new_parents))
        else:
            try:
                v = float(v)
            except (TypeError, ValueError):
                pass
            items.append(Param(new_parents[-1], v, parents=new_parents[:-1]))
    return items


def get_atts(name, cls):
    fname = tf.Str(__file__).sibling(f"{name}.pyi")
    defaults = tf.load_yaml(tf.curDirs(__file__, "default_params.yaml"))
    atts = {}
    idt = " " * 4
    s = f"# fmt: off\nfrom model.params.param import Params, {cls.__name__}\n\n\n"
    s += f"class {name}(Params[str, {cls.__name__}]):\n"
    s += f"\n{idt}def set_paths(self):\n{idt*2}pass\n\n"
    for x in flatten(defaults):
        y = f"{x.value!r}" if isinstance(x.value, str) else x.value
        s += f"{idt}{x.name} = {cls.__name__}({x.name!r}, {y}, parents={x.parents})\n"
        atts[x.name] = cls(x.name, x.value, parents=x.parents)

    with open(fname, "w") as f:
        f.write(s.strip())

    return atts


log = logging.getLogger(__name__)
