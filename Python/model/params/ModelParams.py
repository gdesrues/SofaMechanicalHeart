from typing import Union, List

import treefiles as tf

from model.params.defaults import MetaModelParams, flatten
from model.params.param import Params, Param, InvalidParam
from model.trees.trees import get_tree


class ModelParams(Params[str, Param], metaclass=MetaModelParams):
    """
    # Class that will hold all the default parameters defined in the `default_params.yaml` file.
    # Since they are known only at run time (when parsing the yaml file), we generate a stub
    # of this class and add attributes on the fly.

    pycharm stubs: https://www.jetbrains.com/help/pycharm/type-hinting-in-product.html#stub-type-hints
    or: https://mypy.readthedocs.io/en/stable/stubs.html
    """

    def __init__(
        self,
        data_path: tf.TS,
        simu_path: tf.TS,
        *items: Union[Param, List[Param], Params, dict],
    ):
        super().__init__(*items)
        innerclass = self.inner_class
        for k, v in self.__defaults__.items():
            if isinstance(v, innerclass):
                self[k] = v.copy()
        super().__init__(*items)

        self["data_path"].value = tf.Str(data_path)
        self["simu_path"].value = tf.Str(simu_path)
        self.set_paths()

    @classmethod
    def from_file(cls, yaml_path):
        d = tf.load_yaml(yaml_path)
        return cls(d["GENERAL"]["data_path"], d["GENERAL"]["simu_path"], flatten(d))

    def set_paths(self):
        data_path = self.get("data_path", InvalidParam()).value
        assert (
            data_path is not None
        ), f"You must link the data path: data_path={data_path}"
        simu_path = self.get("simu_path", InvalidParam()).value
        assert (
            simu_path is not None
        ), f"You must link the simu path: simu_path={simu_path}"

        # data = get_tree(data_path, "data")
        simu = get_tree(simu_path, "simu")

        self["PROXY_MESH_PATH"].value = simu.data.mesh_final
        self["MESH_EXPORTED"].value = simu.exported
        self["mesh_visu"].value = simu.data.visu
        self["mesh_peri"].value = simu.data.peri
        self["mesh_rings"].value = simu.data.rings
        self["mesh_endo_lv"].value = simu.data.lv
        self["mesh_endo_rv"].value = simu.data.rv
        self["pressure_lv_file"].value = simu.pressure_lv
        self["pressure_rv_file"].value = simu.pressure_rv
