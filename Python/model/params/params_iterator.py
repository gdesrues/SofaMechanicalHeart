import logging
from typing import Union, List

import numpy as np
import treefiles as tf
from smt.sampling_methods import LHS

from model.params.DefaultParams import DefaultParams
from model.params.ModelParams import ModelParams
from model.params.param import Params, Param


class ParamIter:
    """
    Class intended to provide methods to iterate over several sets
    of parameters
    """

    def __init__(
        self, *ps: Union[List[Param], Param], defaults: Params = None, n: int = None
    ):
        all_p = []
        for x in tf.get_iterable(ps):
            x = tf.get_iterable(x)
            if isinstance(x, tf.Bases):
                x = x.values()
            for y in x:
                all_p.append(y)
        self.params = all_p
        self.defaults = tf.none(defaults, ModelParams())
        self.n = n

    def __len__(self):
        return len(self.params)

    def add(self, *ps: Union[List[Param], Param]):
        self.params.append(tf.get_iterable(ps))

    @property
    def gen(self):
        for x in self.params:
            d = self.defaults.copy()
            for y in tf.get_iterable(x):
                d[y.name].value = y.value
            yield d


class LinearIter(ParamIter):
    def __len__(self):
        return self.n

    @property
    def gen(self):
        """
        Return self.n sets of parameters, each parameters has n values
        varying linearly between its bounds
        """
        bds = [np.linspace(*x.bounds, self.n) for x in self.params]
        for i in range(self.n):
            d = self.defaults.copy()
            for j, x in enumerate(self.params):
                d[x.name].value = bds[j][i]
            yield d


class LinearOneAtATimeIter(ParamIter):
    def __len__(self):
        return self.n * len(self.params)

    @property
    def gen(self):
        """
        Return self.n*len(self.params) sets of parameters, each parameters has n values
        varying linearly between its bounds while the other are set to default
        """
        for x in self.params:
            bds = x.bounds
            for val in np.linspace(*bds, self.n):
                d = self.defaults.copy()
                d[x.name].value = val
                yield d


class LHSParamIter(ParamIter):
    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        ps = Params(self.params)
        sampling = LHS(xlimits=np.array(ps.bounds), criterion="ese")
        self.params = [ps.assign(x).values() for x in sampling(self.n)]

    def __len__(self):
        return self.n

    @property
    def gen(self):
        for x in self.params:
            d = self.defaults.copy()
            for y in tf.get_iterable(x):
                d.setdefault(y.name, tf.Param(y.name))
                d[y.name].value = y.value
            yield d


log = logging.getLogger(__name__)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    log = tf.get_logger()

    TAUR = DefaultParams.TAUR
    PV0 = DefaultParams.PV0L
    print(TAUR, PV0)

    # ParamIter
    it = ParamIter([PV0(i) for i in range(4)])
    log.info(f"ParamIter ({len(it)})")
    for i, x in enumerate(it.gen):
        print(x.PV0L)

    # LinearIter
    it = LinearIter(PV0(bounds=(0, 10)), TAUR(bounds=(200, 250)), n=5)
    log.info(f"LinearIter ({len(it)})")
    for i, x in enumerate(it.gen):
        print(x.PV0L, x.TAUR)

    # LinearOneAtATimeIter
    it = LinearOneAtATimeIter(PV0(bounds=(0, 10)), TAUR(bounds=(200, 250)), n=5)
    log.info(f"LinearOneAtATimeIter ({len(it)})")
    for i, x in enumerate(it.gen):
        print(x.PV0L, x.TAUR)

    # LHSParamIter
    it = LHSParamIter(
        PV0(bounds=(0, 10)),
        TAUR(bounds=(200, 250)),
        n=8,
        defaults=ModelParams(DefaultParams.patient_id(12)),
    )
    log.info(f"LHSParamIter ({len(it)})")
    for i, x in enumerate(it.gen):
        print(x.PV0L, x.TAUR, x.patient_id)
