import json
import logging
import re
from typing import List

import treefiles as tf
from treefiles.baseio_.param import r


class Param(tf.Param):
    def __init__(self, *args, parents: List[str] = None, **kwargs):
        super().__init__(*args, **kwargs)
        self.parents = parents
        self.registered.add("parents")

    @property
    def table(self):
        return Params(self).table

    show = table

    @property
    def parentname(self) -> str:
        return f"{'.'.join(self.parents+[self.name])}"

    def __call__(self, value=None, **kwargs):
        self.value = value
        for k, v in kwargs.items():
            if k in self.registered:
                setattr(self, k, v)
        return self

    def __eq__(self, other):
        if isinstance(other, Param):
            return self.value == other.value
        return self.value == other


class InvalidParam(Param):
    def __init__(self, *args, **kwargs):
        super().__init__(None, *args, **kwargs)


class DefaultParam(Param):
    def __call__(self, value=None, **kwargs):
        obj = self.copy()
        obj.value = value
        for k, v in kwargs.items():
            if k in self.registered:
                setattr(obj, k, v)
        return obj


class Params(tf.Params[str, Param]):
    def build_table(self):
        def pts(x):
            m = re.search(r"(.+)\.", x)
            return m.group(1) if m else ""

        header = ["Parents", "Name", "Value", "Initial value", "Bounds", "Unit"]
        data = [
            [
                pts(x.parentname),
                x.name,
                r(x.value),
                x.initial_value,
                x.bounds,
                x.unit,
            ]
            for x in self.values()
        ]
        return header, data

    def __getitem__(self, item):
        cand = [
            x.copy()
            for x in self.values()
            if len(x.parents) > 0 and x.parents[0] == item
        ]
        if len(cand) > 0:
            for x in cand:
                x.parents = x.parents[1:]
            return Params(cand)
        else:
            return super().__getitem__(item)

    def to_yaml(self):
        d = {}
        for x in self.values():
            p = d
            for k in x.parents:
                p = p.setdefault(k, {})
            if isinstance(x.value, tf.Tree):
                x.value = x.value.abs()
            p.update({x.name: x.value})
        return json.loads(json.dumps(d, cls=tf.NumpyEncoder))


log = logging.getLogger(__name__)
