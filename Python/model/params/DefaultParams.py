from model.params.param import Params, DefaultParam
from model.params.defaults import MetaDefaultParams


class DefaultParams(Params[str, DefaultParam], metaclass=MetaDefaultParams):
    """
    Class that will hold all the default parameters defined in the `default_params.yaml` file.
    Since they are known only at run time (when parsing the yaml file), we generate a stub
    of this class and add attributes on the fly.

    pycharm tubs: https://www.jetbrains.com/help/pycharm/type-hinting-in-product.html#stub-type-hints
    or: https://mypy.readthedocs.io/en/stable/stubs.html
    """
