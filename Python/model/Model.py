import logging
from typing import Optional

import treefiles as tf
from Sofa import Simulation
from Sofa.Core import Node
from Sofa.Gui import GUIManager

import model.preprocess.prepare_mesh as pm
from model.analyse.Analyser import Analyser
from model.analyse.features.base_feature import Features
from model.params.ModelParams import ModelParams
from model.reports.HtmlGenerator import HtmlGenerator
from model.scene.plugins import import_plugins
from model.scene.scene import create_graph
from model.trees.trees import get_tree


class MecaModel:
    mesh_from_elec = pm.mesh_from_elec
    remesh = pm.remesh
    mesh_visu = pm.mesh_visu
    mesh_peri = pm.mesh_peri
    mesh_rings = pm.mesh_rings
    make_endos = pm.make_endos
    extract_boudary_edges = pm.extract_boudary_edges
    extract_points = pm.extract_points

    def __init__(self, params: ModelParams):
        self.params = params
        self.features: Optional[Features] = None
        self.observations = None
        self.data_tree = get_tree(params.data_path.value, "data")
        self.simu_tree = get_tree(params.simu_path.value, "simu")

        log.info(f"Output folder is file://{self.simu_tree.abs()}")

        if not self.simu_tree.isdir():
            self.simu_tree.dump()
            tf.dump_yaml(self.simu_tree.simu_params, self.params.to_yaml())

            # Copy input meshes
            tf.copyfile(self.data_tree.anatomy / "mesh_final*.vtk", self.simu_tree.data)
            tf.copyFile(self.data_tree.anatomy.geoData, self.simu_tree.data.geoData)

        self.root = None
        self.n = None
        self.dt = None
        self.center = None

    def analyse(self):
        an = Analyser(self.simu_tree)
        an.run()

    def init_scene(self):
        import_plugins()

        self.root = Node("root")
        create_graph(self.root, self.params)
        Simulation.init(self.root)

        self.n = int(
            int(self.params.HEART_PERIOD.value / self.params.DT.value)
            * self.params.NUMBER_BEATS.value
        )
        self.dt = self.params.DT.value

    def print_scene(self):
        Simulation.print(self.root)

    def run(self, gui: bool = False):
        @tf.timer
        def run_meca_model(gui):
            if gui:
                GUIManager.Init("")
                GUIManager.createGUI(self.root, __file__)
                GUIManager.SetDimension(900, 700)
                gui = GUIManager.GetGUI()
                gui.setBackgroundImage(tf.f(__file__) / "scene/SOFA_logo_white.dds"),
                GUIManager.MainLoop(self.root)
                GUIManager.closeGUI()
            else:
                # for _ in tqdm(range(self.n)):
                for _ in range(self.n):
                    Simulation.animate(self.root, self.dt)

                    V = self.root["MecaNode.LV.vol_lv"].findData("volume").value * 1e6
                    log.info(f"Volume: {round(V, 2)} mL")

        run_meca_model(gui)

    def generate_report(self):
        gen = HtmlGenerator(self)
        gen.set_params()
        gen.set_input_data()
        gen.set_features()
        gen.render(self.simu_tree.report_simu)


log = logging.getLogger(__name__)
