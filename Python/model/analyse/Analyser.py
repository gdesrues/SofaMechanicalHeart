import json
import logging

import numpy as np
import pandas as pd
import treefiles as tf

from model.analyse.features.base_feature import Feature
from model.analyse.features.extract_features import compute_indices
from model.params.ModelParams import ModelParams
from model.trees.trees import get_simu_tree


class Analyser:
    def __init__(self, path: tf.TS):
        self.features = None
        self.observations = None
        self.simu_tree = get_simu_tree(path)
        self.params = ModelParams.from_file(self.simu_tree.simu_params)

    def run(self):
        # gg = GeoExtractor(
        #     self.simu_tree.data.pts, self.simu_tree.vtk.path("simu_*.vtk")
        # )
        # data = gg.extract_geo_from_files("rotation")
        # tf.dump_json(self.simu_tree.extracted, data)
        # plot_rotation(self.simu_tree.extracted)

        # plot_ventricle(self.simu_tree.pressure_lv)

        # Observations
        self.observations = {
            "lv": parse_pressure_file(self.simu_tree.pressure_lv),
            "rv": parse_pressure_file(self.simu_tree.pressure_rv),
            "both": None,
        }

        self.features = compute_indices(
            self.observations,
            t_QRS=self.params.START_CONTRACTION.value,
            period=self.params.HEART_PERIOD.value,
        )
        link_to_ods(self.features)

        tf.dump_json(self.simu_tree.observations, self.observations, cls=AAEncoder)
        tf.dump_json(self.simu_tree.indices, self.features, cls=AAEncoder)

        log.info(
            f"Simulation results are dumped to file://{self.simu_tree.indices}"
        )


def link_to_ods(fts):
    df = pd.read_excel(tf.f(__file__) / "features/features.ods", engine="odf")
    for i, x in df.iterrows():
        y = x["Key"]
        if y in fts:
            fts[y].pretty_name = x["Name"]

            dd = x["Description_en"]
            fts[y].description = dd if isinstance(dd, str) else None
            # self.features[y].ref = x['Ref']
            # TODO: ...


def parse_pressure_file(fname):
    if not tf.isfile(fname):
        return

    df = pd.read_csv(fname, index_col="i")
    # print(df)

    df["t"] = df["t"] * 1e3  # ms
    df["Pv"] = df["Pv"] * 0.00750062  # mmHg
    df["Pat"] = df["Pat"] * 0.00750062  # mmHg
    df["Pat"] = df["Pat"] * 0.00750062  # mmHg
    df["Par"] = df["Par"] * 0.00750062  # mmHg
    df["Q"] = df["Q"]  # m^3/s
    df["Vol"] = df["Vol"] * 1e6  # mL

    if "phase3" not in df.columns:
        log.warning("[DEBUG] TODO: add phase to plugin")
        df["phase3"] = np.zeros(df.shape[0])

    return df


class AAEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, (pd.DataFrame, Feature)):
            return obj.to_dict()
        return super().default(obj)


log = logging.getLogger(__name__)
