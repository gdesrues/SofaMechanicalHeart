import numpy as np
from treefiles import load_json

from Model3D.Plot.UtilsPlot import Color


# Part of the Analyser class
def plot_thickness_one(self):
    loc_key = ["SEPTUM", "LV_FREE_WALL"]
    both_center = self.extractSeptAndFWThickness()

    fig = self.new_fig()
    ax = fig.axes[0]

    assert len(both_center[0]) == len(
        fig.time
    ), f"0: {len(both_center[0])} {len(fig.time)}"
    assert len(both_center[1]) == len(
        fig.time
    ), f"1: {len(both_center[1])} {len(fig.time)}"

    for i, (k, c) in enumerate(zip(loc_key, [Color.TRED, Color.TBLUE])):
        leg = k.replace("_", " ").title()
        ax.plot(fig.time, both_center[i], label=leg, color=c, linewidth=2)
        imax = np.argmax(both_center[i])

        ax.axvline(
            fig.time[imax], color=c, linestyle="-.", linewidth=2, label=f"max {leg}"
        )
        # ax.text(
        #     fig.time[imax] + 5,
        #     np.min(both_center[i]),
        #     f"max {leg}",
        #     color=c,
        # )

    ax.set_ylabel("Thickness ($mm$)")
    ax.set_title("Max contraction")
    ax.legend(loc="upper left", shadow=True)

    fig.add_ti()
    fig.save(self.plot_dir.path("thickness.png"))


def plot_thickness_all(self):
    zones = load_json(self.simu_dir.zones)
    data = self.extracted_mesh_infos
    thickness = [d["thickness"] for d in data]

    n_it = len(data)
    both, loc_key = [], ["SEPTUM", "LV_FREE_WALL"]
    for regions in loc_key:
        nbZonesInRegion = len(zones[regions])
        arr = np.empty((n_it, nbZonesInRegion))
        for j, zone in enumerate(zones[regions]):
            for i, d in enumerate(thickness):
                arr[i, j] = d[str(zone)]
        both.append(arr)

    SeptCenter = np.empty(n_it)
    FWCenter = np.empty(n_it)
    for i, d in enumerate(thickness):
        SeptCenter[i] = d["center_BC"]
        FWCenter[i] = d["center_GF"]
    both_center = [SeptCenter[1:-1], FWCenter[1:-1]]

    fig = self.new_fig(nrows=2)
    for i, (ax, k) in enumerate(zip(fig.axes, loc_key)):
        ax.plot(fig.time, both_center[i], label="center", marker="o", markevery=10)
        imax = np.argmax(both_center[i])

        ax.axvline(fig.time[imax], color=Color.RED, linestyle="-.", label="max")
        ax.text(
            fig.time[imax] + 5,
            np.min(both_center[i]),
            "max thickness",
            color=Color.RED,
        )

        for j, zone in enumerate(zones[k]):
            ax.plot(fig.time, both[i][1:-1, j], label=f"{zone}")

        ax.set_ylabel("Thickness ($mm$)")
        ax.set_title(k.replace("_", " ").title())
        ax.legend(loc="upper left", shadow=True)
    fig.add_ti()
    fig.save(self.plot_dir.path("thickness_all.png"))


def plot_axis(self):
    loc_key = ["long_axis", "short_axis"]
    data = self.extracted_mesh_infos
    thickness = [d["thickness"] for d in data]
    n_it = len(data)

    lenghts = np.empty((n_it, 2))
    for i, d in enumerate(thickness):
        for j, v in enumerate(loc_key):
            lenghts[i, j] = d[v]

    # TODO
    lenghts = lenghts[1:-1, :]

    fig = self.new_fig()
    ax = fig.axes[0]

    assert lenghts.shape[0] == len(
        fig.time
    ), f"In plot_axis: {lenghts.shape[0]} != {len(fig.time)}"

    for i, (k, c) in enumerate(zip(loc_key, [Color.TRED, Color.TBLUE])):
        leg = k.replace("_", " ").title()
        ax.plot(fig.time, lenghts[:, i], label=leg, color=c, linewidth=2)

    ax.set_ylabel("Lenght ($mm$)")
    ax.set_title("Axis lenght")
    ax.legend(loc="upper left", shadow=True)

    fig.add_ti()
    fig.save(self.plot_dir.path("axis_lenght.png"))


def plot_spheritisation(self):
    loc_key = ["long_axis", "short_axis"]
    data = self.extracted_mesh_infos
    thickness = [d["thickness"] for d in data]
    n_it = len(data)

    lenghts = np.empty((n_it, 2))
    for i, d in enumerate(thickness):
        for j, v in enumerate(loc_key):
            lenghts[i, j] = d[v]

    # TODO
    lenghts = lenghts[1:-1, :]

    fig = self.new_fig()
    ax = fig.axes[0]

    assert lenghts.shape[0] == len(
        fig.time
    ), f"In plot_axis: {lenghts.shape[0]} != {len(fig.time)}"


    ax.plot(fig.time, lenghts[:, 1] / lenghts[:, 0], linewidth=2)

    ax.set_title("Spheritisation")

    fig.add_ti()
    fig.save(self.plot_dir.path("spheritisation.png"))