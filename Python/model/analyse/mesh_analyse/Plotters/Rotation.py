import string

import numpy as np
from matplotlib import pyplot as plt

from model.analyse.mesh_analyse.Plotters.utils import Color
import treefiles as tf


def plot_rotation(fname):
    data = tf.load_json(fname)
    rotation = [d["rotation"] for d in data]

    apex = np.zeros((len(data), 8))
    base = np.zeros((len(data), 8))
    for i, d in enumerate(rotation):
        for j, k in enumerate(string.ascii_letters[1:9]):
            apex[i, j] = d[f"apex_{k.upper()}"]
            base[i, j] = d[f"base_{k.upper()}"]

    t = np.arange(160) * 5
    out = tf.f(fname, "plots").dump()
    with tf.SPlot(fname=out / "rotation.png"):
        fig, ax = plt.subplots()
        for i in range(8):
            k = string.ascii_letters[i + 1].upper()
            ax.plot(t, apex[:, i], label=f"Apex_{k}", lw=2, color=Color.RED)
            ax.plot(t, base[:, i], label=f"Base_{k}", lw=2, color=Color.TBLUE)

        ax.legend(loc="upper right", shadow=True)
        ax.set_ylabel("Rotation ($^o$)")
        ax.set_xlabel("Time (ms)")
        tf.despine(fig)
        fig.tight_layout()


# # Part of the Analyser class
# def plot_rotation(self):
#     data = self.extracted_mesh_infos
#     rotation = [d["rotation"] for d in data]
#
#     sept = np.empty(len(data))
#     fw = np.empty(len(data))
#     for i, d in enumerate(rotation):
#         sept[i] = d["center_B"]
#         fw[i] = d["center_F"]
#
#     fig = self.new_fig()
#     ax = fig.axes[0]
#     ax.plot(fig.time, sept[1:-1], label="Septum LV", lw=2, color=Color.RED)
#     ax.plot(fig.time, fw[1:-1], label="Free wall LV", lw=2, color=Color.TBLUE)
#
#     fig.add_ti()
#     ax.legend(loc="upper right", shadow=True)
#     ax.set_ylabel("Rotation ($^o$)")
#
#     fig.save(self.plot_dir.path("rotation.png"))
