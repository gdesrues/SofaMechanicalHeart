import numpy as np
import matplotlib.pyplot as plt


class FigWrapper:
    """
    Usage:

        an = Analyser(simu_path)
        fig = an.new_fig()

        ax = fig.axes[0]
        ax.plot([-150,100],[-10,10], label="MyData")
        ax.legend(shadow=True)
        ax.set_title("Title")

        fig.add_ti()
        fig.save("tets.png")
        pl.show()
    """

    def __init__(self, fig, times, time):
        """
        :param fig: the pyplot figure instance
        :param times: list of times where to draw the vertical line
        :param time: the vector of time steps (in ms)
        """
        self.fig = fig
        self.times = times
        self.time = time

        self.save = fig.savefig

    @property
    def axes(self):
        return self.fig.axes

    def add_ti(self, **kw):
        for ax in self.fig.axes:
            _add_ti_to_axis(ax, self.times, **kw)

    def close(self):
        plt.close(self.fig)


class Color:
    LEFT = "r"  # "tab:blue"
    RIGHT = "b"  # "tab:orange"
    TORANGE = "tab:orange"
    TBLUE = "tab:blue"
    TRED = "tab:red"
    RED = "r"

    # LARGE = {"linewidth": 2}


_voc = {
    "R": {
        "V_valve": "Tricuspid",
        "ar": "Pulm Artery",
        "A_valve": "Pulmonic",
        "name": "Right",
    },
    "L": {"V_valve": "Mitral", "ar": "Aorta", "A_valve": "Aortic", "name": "Left"},
}


def _add_ti_to_axis(ax, times, **kw):
    kw.update(color=kw.get("color", "grey"))
    kw.update(linestyle=kw.get("linestyle", "--"))

    p = 0.015
    lims = ax.get_ylim()
    yy = (1 - p) * lims[0] + p * lims[1]
    for i, t in enumerate(times):
        ax.axvline(t, **kw)
        ax.text(t + 0.005, yy, f"$t_{i+1}$", color=kw.get("color"))


def add_ti_to_axis(ax, features, tQRS):
    """
    Compute figure dimensions and print 4 phase `t_i`

    :param ax: current axe
    :param features: json dict containing the ti (in ms)
    :param tQRS: t0 in second
    """
    times = [float(features[f"t{i+1}"]["value"]) - 1e3 * tQRS for i in range(4)]
    _add_ti_to_axis(ax, times)


def set_xticks(ax, tmin, tmax, pas=None):
    """
    Set the xticks on the axe, nicely spaced by `pas` between tmin and tmax

    :param ax: current axe
    :param tmin: minimum t range (in s)
    :param tmax: maximum t range (in s)
    :param pas: step (in s)
    """
    if pas is None:
        pas = 0.1

    a = [tmin, 0, tmax]
    for (tm, i) in [(tmax, 1), (tmin, -1)]:
        t = 0
        while (tm - t) * i > 0:
            t += i * pas
            if not (tmin < t < tmax):
                break
            if abs(tm - t) > 0.8 * pas:
                a.insert(-i, t)

    a = np.array(a) * 1e3
    ax.set_xticks(a)
    labels = np.around(a, decimals=0).astype(int)
    ax.set_xticklabels(labels)
