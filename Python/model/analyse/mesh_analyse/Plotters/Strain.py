import numpy as np


# Part of the Analyser class
def plot_strain(self):
    arr_path = self.simu_dir.strain.path("res.npy")
    res = np.load(arr_path)

    markers = ["", "o", "x"]

    # Get first strain file to get the header
    with open(self.simu_dir.monitor.format(0), "r") as f:
        header = f.readline().rstrip().split(" ")

    for k, h in enumerate(header):
        fig = self.new_fig(figsize=(12, 4.8), pas=0.05)
        ax = fig.axes[0]

        for i in range(res.shape[1]):
            ax.plot(
                fig.time,
                res[:, i, k],
                label=f"{i}",
                marker=markers[i // 10],
                markevery=10,
            )
        ax.set_title(h)
        ax.legend(
            title="Zones",
            bbox_to_anchor=(1.02, 1),
            loc="upper left",
            ncol=2,
            fancybox=True,
            shadow=True,
        )
        fig.add_ti()
        fig.fig.tight_layout()
        fig.save(self.plot_dir.path(f"{h}.png"))

    print(f"Strain figures saved to {self.plot_dir.abs()}")
