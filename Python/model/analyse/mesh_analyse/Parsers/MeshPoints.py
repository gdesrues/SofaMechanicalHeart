import glob

import numpy as np
import treefiles as tf
from MeshObject import Mesh

from model.analyse.mesh_analyse.Parsers.extractor import PointExtractor
from model.analyse.mesh_analyse.Parsers.points import ZonePoints, PlanePoints


class GeoExtractor:
    def __init__(self, points_path, globs):
        self.list_files = tf.natural_sort(glob.glob(globs))
        self.points_path = points_path

    def extract_geo_from_files(self, *data, compute_rot_deriv=False):
        """
        Will loop over the n files listed in self.list_files and extract the coordinates
        of the point whose ids are
        contained in self.points_path. For each file (each time step), and for each data
        to add, some indices are
        computed and added to this per-time-step dictionary.

        :param data: list of indices to compute and add to json file.
        Choices are: "thickness", "position", "rotation"
        :param compute_rot_deriv: Choose if the derivative of the rotation should be computed
        :return: A list of n dictionaries containing extracted data
        """
        its = list()

        if len(data) == 0:
            data = ("thickness", "position", "rotation")

        for i, f in enumerate(self.list_files):
            obj = Mesh.load(f)
            zones, slices = PointExtractor.load(
                self.points_path, point_coordinates=obj.points
            )

            if i == 0:
                self.init_zones, self.init_slices = zones, slices

            plugins = dict()
            for plugin in data:
                plugins[plugin] = getattr(self, f"get_{plugin}")(zones, slices)
            its.append({"i": i, **plugins})

        if compute_rot_deriv:
            get_rotation_derivative(its)  # adds key `rot_der`
        return its

    def get_thickness(self, zones, slices):
        thicknesses = dict()
        for k, v in zones.items():
            thicknesses[k] = v.thickness
        for a, b in [
            ("F", "B"),
            ("D", "H"),
            ("G", "C"),
            ("B", "C"),
            ("G", "F"),
            ("D", "E"),
            ("H", "I"),
        ]:
            for k, v in slices.items():
                thicknesses[f"{k}_{a}{b}"] = v.lenght(a, b)

        # Add axis lenght
        thicknesses["long_axis"] = np.linalg.norm(
            slices["base"].A.coord - slices["apex"].A.coord
        )
        c = slices["center"]
        l1 = np.linalg.norm(c.B.coord - c.F.coord)
        l2 = np.linalg.norm(c.D.coord - c.H.coord)
        thicknesses["short_axis"] = 0.5 * (l1 + l2)

        return thicknesses

    def get_position(self, zones, slices):
        positions = dict()
        for a in ZonePoints.letters:
            for k, v in zones.items():
                positions[f"{k}_{a}"] = getattr(getattr(v, a), "coord").tolist()
        for a in PlanePoints.letters:
            for k, v in slices.items():
                positions[f"{k}_{a}"] = getattr(getattr(v, a), "coord").tolist()
        return positions

    def get_rotation(self, zones, slices):
        bary_rest = self.init_slices.get("center").A.coord
        bary_deformed = slices.get("center").A.coord
        rotations = dict()

        # zones
        for a in ZonePoints.letters:
            for ((k, v), (k_init, v_init)) in zip(
                zones.items(), self.init_zones.items()
            ):
                rest = (bary_rest, get_coord(v_init, a))
                deformed = (bary_deformed, get_coord(v, a))
                rotations[f"{k}_{a}"] = get_angle(rest, deformed)

        # slices
        for a in PlanePoints.letters:
            for ((k, v), (k_init, v_init)) in zip(
                slices.items(), self.init_slices.items()
            ):
                rest = (get_coord(v_init, "A"), get_coord(v_init, a))
                deformed = (get_coord(v, "A"), get_coord(v, a))
                rotations[f"{k}_{a}"] = get_angle(rest, deformed)
        return rotations


def get_rotation_derivative(data):
    """
    data: dict
    [
        {
            i: 0,
            "rotation": ...,
            "thickness": ...,
            ...
        },
        {
            i: 1,
            ...
        }
    ]
    """
    # TODO: pas
    pas = 5e-3

    get_rot = lambda x: np.array(list(x.get("rotation").values()))

    for i, d in enumerate(data):
        if i == 0 or i == len(data) - 1:
            zeros = dict(d.get("rotation"))
            for k in zeros:
                zeros[k] = 0
            d.update({"rot_der": zeros})
        else:
            last_rot = get_rot(data[i - 1])
            rot = get_rot(d)
            futu_rot = get_rot(data[i + 1])
            diff = (futu_rot - 2 * rot + last_rot) / pas  # centered differences

            rot_der = dict(d.get("rotation"))
            for i, k in enumerate(rot_der):
                rot_der[k] = diff[i]
            d.update({"rot_der": rot_der})


def get_coord(obj, le):
    return getattr(getattr(obj, le), "coord")


def get_angle(rest, deformed):
    rest, deformed = prepare(rest), prepare(deformed)
    dot = np.dot(rest, deformed)
    if abs(dot - 1) < 1e-14:
        dot = 1
    angle = np.degrees(np.arccos(dot))
    sign = 1 if np.dot(np.cross(rest, deformed), [0, 0, 1]) > 0 else -1
    return sign * angle


def prepare(v):
    v = v[1] - v[0]
    v = project_vector_on_plane(v, [0, 0, 1])
    if v.any():
        v /= np.linalg.norm(v)
    return v


def project_vector_on_vector(v1, v2):
    """
    Project v1 on v2
    """
    v1, v2 = np.array(v1), np.array(v2)
    return np.dot(v1, v2) * v2 / np.linalg.norm(v2)


def project_vector_on_plane(v, n):
    """
    Project vector v on plane with normal vector n
    """
    v, n = np.array(v), np.array(n)
    return v - project_vector_on_vector(v, n)


# def natural_sort(l):
#     convert = lambda text: int(text) if text.isdigit() else text.lower()
#     alphanum_key = lambda key: [convert(c) for c in re.split("(\d+)", key)]
#     return sorted(l, key=alphanum_key)


# if __name__ == "__main__":
#     _dir = tf.Tree(curDirs(__file__, "debug_tmp")).dump()
#
#     gg = GeoExtractor(_dir.path("test_save.json"), _dir.path("vtk/simu__*.vtk"))
#
#     data = gg.extract_geo_from_files()
#     dump_json(_dir.path("extracted.json"), data)
