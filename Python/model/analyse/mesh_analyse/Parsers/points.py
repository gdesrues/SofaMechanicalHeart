import string
import numpy as np


class BasePoints:
    letters = string.ascii_uppercase

    def __init__(self, points):
        self.points = [Point(p) for p in points]
        assert len(points) == len(self.letters), f"{len(points)} != {len(self.letters)}"

    def __repr__(self):
        s = "\n"
        for i in range(len(self.points)):
            s += f"  {self.letters[i]}: {getattr(self, self.letters[i])}\n"
        return s

    def __getattr__(self, item):
        arg = self.letters.find(item)
        if arg != -1:
            return self.points[arg]
        return self.__getattribute__(item)

    def as_np(self):
        return np.array([p.coord for p in self.points])

    def ids_as_list(self):
        return [p.id for p in self.points]

    # def write(self, fname):
    #     mm = Object.from_elements(self.as_np())
    #     mm.write(fname)

    def lenght(self, p1: str, p2: str):
        """
        Return the lenght between two points described by their letter
        """
        p1, p2 = getattr(self, p1).coord, getattr(self, p2).coord
        return np.linalg.norm(p2 - p1)


class Point:
    def __init__(self, item):
        self.id, self.coord = item
        self.coord = np.array(self.coord)

    def __repr__(self):
        return f"{self.id}: {self.coord}"


class ZonePoints(BasePoints):
    #
    # points: Epicardium, Mid-myocardium, Epicardium
    #
    #   endo    mid    epi
    #     A      B      C
    #
    letters = BasePoints.letters[:3]

    def __init__(self, zone, points):
        super().__init__(points)
        self.zone = str(zone)

    @property
    def thickness(self):
        return np.linalg.norm(self.points[2].coord - self.points[0].coord)


class PlanePoints(BasePoints):
    #
    # points: Est, North, West, South
    #
    # Labelling of points in slice in small axis viewed from base to apex:
    # ex: get septal endocardial point: planePoints.B
    #           E
    #           D
    #     G  F  A  B  C          right ventricle
    #           H
    #           I
    # B, D, F, H on endocardium
    # C, E, G, I on epicardium
    #
    letters = BasePoints.letters[:9]

    def __init__(self, origin, points):
        points.insert(0, (None, origin))
        super().__init__(points)
