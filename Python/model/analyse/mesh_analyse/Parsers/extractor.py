import numpy as np
from MeshObject import Object
import treefiles as tf

from model.analyse.mesh_analyse.Parsers.points import PlanePoints, ZonePoints
from model.labels import Labels
import pyvista as pv


class PointExtractor:
    @tf.timer
    def __init__(self, mesh_path, edges_path):
        self.mesh = Object.load(mesh_path)
        self.mesh.filterCellsTo(Object.types.TETRA)
        self.points = self.mesh.points.as_np()
        self.cells = self.mesh.cells.as_np()

        self.mesh_pd = self.mesh.copy()
        self.mesh_pd.convertToPolyData()

        # self.zones = load_json(zones_path)
        self.lv_zones = list(range(1, 18))  # self.zones["lv"]
        self.aha = self.mesh.getCellDataArray(
            "Volume_Areas"
        )  # self.zones["ZONES_NAME"])

        # self.geo = load_json(geo_path)
        def get_bary(m, k):
            m.convertToPolyData().threshold(Labels.Endos_Epi.th(k), Labels.ENDOS_EPI)
            return m.getCenter()

        self.lv_barycenter = get_bary(
            self.mesh.copy(), "lv"
        )  # np.array(self.geo.get("LV").get("center"))
        self.rv_barycenter = get_bary(
            self.mesh.copy(), "rv"
        )  # np.array(self.geo.get("RV").get("center"))
        self.edges = tf.load_json(edges_path)

        self.points_in_zones = dict()
        for zone in self.lv_zones:
            ids, coords = self.get_points_in_zone(zone)
            self.points_in_zones[zone] = (ids, coords)

    def get_points_in_zone(self, n):
        arr = np.where(self.aha == n)
        pts = np.zeros(self.mesh.nbPoints, dtype=bool)

        for t in self.cells[arr]:
            pts[t] = True
        ids = np.where(pts)[0]
        coords = self.points[ids]
        return ids, coords

    def get_bary_zone(self, n):
        _, coords = self.points_in_zones.get(n)
        bary_coord = np.mean(coords, axis=0)
        return bary_coord

    def get_3_points(self, n):
        bary_coord = self.get_bary_zone(n)
        p1, p3 = tuple(self.get_intersection(self.lv_barycenter, point2=bary_coord)[:2])
        p2 = self.mesh.closestPoint((np.array(p1[1]) + np.array(p3[1])) * 0.5)
        return p1, p2, p3

    def get_intersection(self, point_coords, axis=None, point2=None):
        if axis is None and point2 is not None:
            axis = point2 - point_coords
        point3 = point_coords + self.mesh.BBDiag * axis / np.linalg.norm(axis)

        # with tf.PvPlot() as p:
        #     p.add_mesh(self.mesh_pd, style="wireframe")
        #     p.label_point(p, point_coords, "Ventricle center", point_color="red")
        #     p.label_point(p, point2, "AHA center", point_color="black")
        #     p.label_point(p, point3, "intersection point", point_color="green")
        #     p.vector(p, point_coords, axis)

        int_points = self.mesh_pd.intersect(point_coords, point3)
        return [self.mesh.closestPoint(p) for p in int_points]

    def get_points_plane(self, point_coord):
        point_coord = np.array(point_coord)
        lv2rv_axis = self.rv_barycenter - point_coord
        lv2rv_axis[2] = 0
        radial = np.cross(np.array([0, 0, 1]), lv2rv_axis)

        inter = []
        for i in [1, -1]:
            for ax in [lv2rv_axis, radial]:
                inter.extend(self.get_intersection(point_coord, axis=i * ax)[:2])

        return PlanePoints(point_coord, inter)

    @property
    def point_valve_min_height(self):
        lv_edges = np.array(self.edges.get("lv"))
        min_height, ind = np.inf, -1
        for hole in lv_edges:
            for e in hole:
                for p in e:
                    if self.points[p][2] < min_height:
                        min_height = self.points[p][2]
                        ind = p
        return self.points[ind]

    @property
    def point_valve_centroid(self):
        lv_edges = self.edges.get("lv")[0]
        points = set()
        for e in lv_edges:
            for p in e:
                points.add(p)
        points = np.array(list(map(lambda x: self.points[x], points)))
        return np.mean(points, axis=0)

    @property
    def valve_centroid_min(self):
        min_height_valve = self.point_valve_min_height[2]
        coords = self.point_valve_centroid
        coords[2] = min_height_valve
        return coords

    @property
    def apex_physio(self):
        points_apex = self._zones_.get("17")  # str(self.zones.get("APEX_LV")[0])
        c = list(points_apex.A.coord)
        c[2] += 0.01
        return c

    def build_plane_points(self):
        apex_slice = self.get_points_plane(self.apex_physio)
        center_slice = self.get_points_plane(self.lv_barycenter)
        valve_slice = self.get_points_plane(self.valve_centroid_min)
        self._slices_ = dict(apex=apex_slice, center=center_slice, base=valve_slice)

    def build_zones_points(self):
        self._zones_ = dict()
        for i in self.lv_zones:
            self._zones_[str(i)] = ZonePoints(i, self.get_3_points(i))

    def save_to_file(self, fname):
        data = dict()

        for k, v in self._zones_.items():
            data[f"Zone{k}"] = v.ids_as_list()

        for k, v in self._slices_.items():
            data[k] = v.ids_as_list()

        tf.dump_json(fname, data)

    def load_from_file(self, fname, point_coordinates=None):
        if point_coordinates is None:
            point_coordinates = self.points
        self._zones_, self._slices_ = PointExtractor.load(fname, point_coordinates)

    @staticmethod
    def load(fname, point_coordinates):
        """
        Load the file containing the point ids `fname` and
        generate coordinates from the mesh points `point_coordinates`

        zones: {1: (A, B, C), 2: ... }
            keys: zone number
            values: tuple of the endo, mid and epi points

        slices: {"apex": [A..I], "center": ...}
            keys: `apex`, `center` or `base`
            values: list of the coordinates of the 9 points in the plane
        """
        data = tf.load_json(fname)

        _zones_, _slices_ = dict(), dict()
        for k, v in data.items():
            if "Zone" in k:
                z = k.replace("Zone", "")
                vc = [point_coordinates[p] for p in v]
                _zones_[z] = ZonePoints(int(z), [(v[i], vc[i]) for i in range(len(v))])
            else:
                _ = v.pop(0)
                vc = [point_coordinates[p] for p in v]
                origin = np.mean(vc, axis=0)  # New point A (coords only)
                _slices_[k] = PlanePoints(
                    origin, [(v[i], vc[i]) for i in range(len(v))]
                )

        return _zones_, _slices_


if __name__ == "__main__":
    _dir = tf.Tree(tf.curDirs(__file__, "debug_tmp")).dump()
    tt = tf.Tree(
        "/home/gaetan/Documents/Dev/Python/gaetools/usage/Meca/Meca_Out/Case_1/data"
    )
    tt.dir("anatomy", "others")
    pp = PointExtractor(
        tt.anatomy.path("mesh_final.vtk"),
        tt.anatomy.path("aha_zones.json"),
        tt.anatomy.path("geoData.json"),
        tt.anatomy.path("boundary_edges.json"),
    )

    # pp.build_zones_points()
    # pp.build_plane_points()

    # pp.save_to_file(_dir.path("test_save.json"))
    pp.load_from_file(_dir.path("test_save.json"))

    # print(pp._zones_)
    # print(pp._slices_)

    # pp._slices_.get("apex").write(_dir.path("apex.vtk"))
    # pp._slices_.get("center").write(_dir.path("center.vtk"))
    # pp._slices_.get("base").write(_dir.path("valve.vtk"))
