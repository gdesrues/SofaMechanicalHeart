import os
import numpy as np
from matplotlib import pyplot as pl
from tqdm import tqdm
from treefiles import load_json, isfile

CLUSTER = os.environ["CARDIAC_CLUSTER"] == "1"

if CLUSTER:
    import matplotlib as mpl

    mpl.use("Agg")


def get_by_zone(simu_dir, params, features, force_compute=False):
    from MeshObject import Object
    from multiprocessing import Pool

    nbFiles = int(float(params["NUMBER_STEPS"])) - 1
    dt = float(params["DT"])
    tQRS = float(params["START_CONTRACTION"])

    # Get first strain file to get the header
    with open(simu_dir.monitor.format(0), "r") as f:
        header = f.readline().rstrip().split(" ")

    if isfile(simu_dir.strain.path("res.npy")) and not force_compute:
        res = np.load(simu_dir.strain.path("res.npy"), allow_pickle=True)
    else:
        # Get the tetra ids by zone
        zones_array_name = load_json(simu_dir.zones)["ZONES_NAME"]
        mm = Object.load(simu_dir.mesh_final)
        mm.filterCellsTo(Object.types.TETRA)
        zones = mm.getCellDataArray(zones_array_name)

        args = zip(range(nbFiles), [zones] * nbFiles, [simu_dir.monitor] * nbFiles)
        with Pool() as p:
            res = list(tqdm(p.imap(parse_strain_file, args), total=nbFiles))

        # if None in res means that simu has crashed (eg. job killed) at some time step
        while res[-1] is None:
            res.pop()

        nbFiles = len(res)
        res = np.array(res)
        # print("res", res.shape, res)

        arr_path = simu_dir.strain.path("res.npy")
        np.save(arr_path, res)
        print(f"Array saved to {arr_path}")


def print_texte(ft, shiftt: float = 0):
    # Compute dimensions and print phases texte
    p = 0.015
    lims = pl.gca().get_ylim()
    yy = (1 - p) * lims[0] + p * lims[1]
    for i in range(1, 5):
        tt = 1e3 * (1e-3 * float(ft[f"t{i}"]["value"]) - shiftt)
        pl.axvline(tt, color="grey", linestyle="--")
        pl.text(tt + 0.005, yy, f"$t_{i}$", color="grey")


def parse_strain_file(args):
    """
    Based on strain file at time (i+1)*dt, return a 2d table, columns being
    the n indices (monitored values, eg. strain, stress, ...) and the
    rows being the k aha zones
    """
    # Unzip arguments
    i, zones, fpath = args

    if not isfile(fpath.format(i)):
        return None
    else:
        # Read file
        data = np.loadtxt(fpath.format(i), skiprows=1)

    # Get context
    nbMonitored = data.shape[1]
    nbZones = np.unique(zones).shape[0]
    nbTetras = zones.shape[0]
    assert data.shape[0] == nbTetras

    # Stack values for each zone
    table = np.zeros((nbZones, nbMonitored))
    nbTetrasByZone = np.zeros(nbZones)

    for k in range(nbTetras):
        table[zones[k] - 1, :] += data[k, :]
        nbTetrasByZone[zones[k] - 1] += 1

    # Return the mean by zone for each index
    return table / nbTetrasByZone[:, None]
