import logging
from typing import Dict

import numpy as np
import treefiles as tf


class Feature(tf.BaseIO):
    def __init__(self, *args, ref=None, uncertainty=None, description=None, **kwargs):
        super().__init__(*args, **kwargs)

        self.ref = ref
        self.uncertainty = uncertainty
        self.description = description

        self.registered.add("ref")
        self.registered.add("uncertainty")
        self.registered.add("description")

    @property
    def error(self):
        return np.square(self.value - self.ref)


class Features(tf.Bases[str, Feature]):
    def build_table(self):
        pass


log = logging.getLogger(__name__)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    log = tf.get_logger()

    x = Feature("ef", 50, ref=65)
    print(x)
    print(x.ref)
    print(x.error)

    y = Features(x)
    print(y)
