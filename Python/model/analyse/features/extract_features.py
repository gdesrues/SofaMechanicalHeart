import logging

import numpy as np

from model.analyse.features.base_feature import Features, Feature


def compute_indices(fts: dict, t_QRS, period) -> Features:
    if fts["lv"] is None:
        raise RuntimeError("Cannot compute indices without observations")

    Volume = fts["lv"]["Vol"].values
    Q = fts["lv"]["Q"].values
    Par = fts["lv"]["Par"].values
    Pat = fts["lv"]["Pat"].values
    Pv = fts["lv"]["Pv"].values
    phase = fts["lv"]["phase3"].values
    t = fts["lv"]["t"].values

    # tmin, tmax = np.min(t), np.max(t)
    t_QRS = float(t_QRS)
    period = float(period)

    def get_safe_time(fun, v):
        """
        If simu fails, phase may not reach all 4 cardiac phases
        Return time=0 for a not-defined phase
        """
        if len(np.where(phase == v)[0]) > 0:
            return fun(np.where(phase == v))
        return 0

    # Debut isvol contraction
    i_t1 = get_safe_time(np.min, 1)
    _t1 = t[i_t1]
    # t1 = Feature(_t1, name=f"Delai t0-debut montée pression {'l'}V")

    # Fermeture valve mitrale
    QRSA = Feature("QRS_A", _t1)

    # Debut ejection
    i_t2 = get_safe_time(np.min, 2)
    _t2 = t[i_t2]
    t2 = Feature("LPEI", _t2)

    # Debut isovol relaxation
    i_t3 = get_safe_time(np.min, 3)
    _t3 = t[i_t3]
    t3 = Feature("TSD", _t3)

    # Debug filling
    i_t4 = get_safe_time(np.max, 3)
    _t4 = t[i_t4]
    t4 = Feature("QSR_E", _t4)

    # Peak pressure in the middle of the ejection phase (symetric pressure curve)
    t_pv_max = t[np.argmax(Pv)]
    PeakPressure = Feature("PeakPressure", t_pv_max)

    # DFT(%) Regarde le papier, page 2 : measured between onset of E wave and the end of A wave.
    # formula from serge: (duree de reemplisage)/(frequence cardiaque)
    # period - QRS-E + QRSA
    # breakpoint()
    DFT = Feature("DFT", 100 * (period - _t4 + 1e-3 * QRSA.value))

    # Ejection time (LVET, RVET)
    EjectionTime = Feature("LVET", _t3 - _t2)

    # PEI / VET
    et = EjectionTime.value
    LPEI_LVET = Feature("LPEI_LVET", 1e3 * (_t2 - t_QRS) / et if et != 0 else 0)

    # # Systole duration (SD)
    # SystoleDuration = Feature_DT(
    #     t3 - t1,
    #     name="Systole duration",
    #     long_name=f"{period} / 3",
    #     ref=roundx(1e3 * (tmax - tmin) / 3),
    # )

    # Isovolumetric contraction time
    IsovolCT = Feature("IsoC", _t2 - _t1)

    # Isovolumetric relaxation time
    IsovolRT = Feature("IsoR", _t4 - _t3)

    # End Systolic Pressure (Pv en fin systole (t3))
    EndSystolicPressure = Feature("ESP", Pv[i_t3])

    # End Systolic Volume (Volume en fin systole (t3))
    EndSystolicVolume = Feature("ESV", Volume[i_t3])

    # End Diastolic Volume (Volume en fin diastole (t0))
    i_t0 = np.where(t == 0)[0][0]
    EndDiastolicVolume = Feature("EDV", Volume[i_t0])

    # End Diastolic Pressure (Pv en fin de diastole (t1))
    EndDiastolicPressure = Feature("EDP", Pv[i_t1])

    # Pression aortique (Par en t3)
    AorticPressureEndEjection = Feature("ArPL", Par[i_t3])

    # Max ventricular pressure
    PvMax = Feature("PVm", np.max(Pv))

    # Volumes
    Vmax = np.max(Volume)
    Vmin = np.min(Volume)

    # Systolic volume
    SystolicVolume = Feature("SV", Vmax - Vmin)

    # Ejection fraction
    EjectionFraction = Feature("EF", 100 * SystolicVolume.value / Vmax)

    # # Pression atriale en fin de diastole (Pat en t1)
    # PAT = Pat[i_t1]

    # Heart rate
    HeartRate = Feature("HR", 60 / period)

    # Cardiac output
    CardiacOutput = Feature("CO", SystolicVolume.value / period)

    # Cardiac output Index (Body Surface Area NORMAL 20–79 years 2.060 	m2 	22.173 	ft2 )
    IndexCardiacOutput = Feature("COI", SystolicVolume.value / period / 2.060)

    return Features(
        # t1,
        QRSA,
        t2,
        t3,
        t4,
        PeakPressure,
        DFT,
        EjectionTime,
        LPEI_LVET,
        IsovolCT,
        IsovolRT,
        EndSystolicPressure,
        EndSystolicVolume,
        EndDiastolicVolume,
        EndDiastolicPressure,
        AorticPressureEndEjection,
        PvMax,
        SystolicVolume,
        EjectionFraction,
        HeartRate,
        CardiacOutput,
        IndexCardiacOutput,
    )


log = logging.getLogger(__name__)
