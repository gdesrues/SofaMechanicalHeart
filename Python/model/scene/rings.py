from model.scene.node import NodeHelper as Nh


def add_meca_rings(fa, node):

    # Imaginary points
    img = node.addChild("img")
    Nh(img).add(
        fa.MeshObjLoader(
            name="l13", filename=fa.cfg("mesh_rings"), translation=[0, 0, 0.02]
        ),
        fa.TriangleSetTopologyContainer(src="@l13"),
        fa.MechanicalObject(name='ms3'),
    )
    Nh(img.addChild("img_visu")).add(
        fa.OglModel(src="@../l13", color="green"),
        fa.VisualStyle(displayFlags="showWireframe"),
        fa.IdentityMapping(),
    )

    # Real rings
    # ns = dict(simulated=False, moving=False)
    realnodes = node.addChild("real_rings")
    Nh(realnodes).add(
        fa.MeshObjLoader(name="l12", filename=fa.cfg("mesh_rings")),
        fa.TriangleSetTopologyContainer(src="@l12"),
        fa.MechanicalObject(),
        fa.RestShapeSpringsForceField(
            name="Atrium", stiffness=400, external_rest_shape="@../img/ms3"
        ),
        fa.SubsetMapping(),
    )

    Nh(realnodes.addChild("rings_visu")).add(
        fa.OglModel(src="@../l12", color="black"),
        fa.VisualStyle(displayFlags="showWireframe"),
        fa.IdentityMapping(),
    )
