from model.scene.node import NodeHelper as Nh


def add_LV(fa, node):
    Nh(node).add(
        fa.MeshObjLoader(name="loader_endo_lv", filename=fa.cfg("mesh_endo_lv")),
        fa.TriangleSetTopologyContainer(name="topo_lv", src="@loader_endo_lv"),
        fa.MechanicalObject(name="obj_lv", src="@topo_lv"),
        fa.VolumeEngine(
            name="vol_lv",
            topology="@topo_lv",
            printLog=True,
            position="@obj_lv.position",
            # show_boundary_edges=True,
        ),
        fa.Pat(
            name="pat_lv",
            printLog=True,
            pat_0=fa.cfg("Pat1L"),
            pat_max=fa.cfg("Pat2L"),
            heart_period=fa.cfg("HEART_PERIOD"),
            t1=fa.cfg("T_0L"),
            t2=fa.cfg("T_1L"),
            t3=fa.cfg("T_2L"),
            alpha1=fa.cfg("V1L"),
            alpha2=fa.cfg("V2L"),
        ),
        fa.Par(
            name="par_lv",
            printLog=True,
            phase="@phase_lv.phase",
            ff="@ff_lv",
            par_0=fa.cfg("Par1L"),
            par_inf=fa.cfg("Par2L"),
        ),
        fa.PhaseManager(
            name="phase_lv",
            Pv="@ff_lv.Pv",
            Par="@par_lv.pressure",
            Pat="@pat_lv.pressure",
        ),
        fa.PressureFF(
            name="ff_lv",
            mstate="@obj_lv",
            topology="@topo_lv",
            edges_on_border="@vol_lv.edges_on_border",
            dV="@vol_lv.dV",
            pv_0=fa.cfg("PV0L"),
            Par="@par_lv.pressure",
            par_inf="@par_lv.par_inf",
            Pat="@pat_lv.pressure",
            printLog=True,
            phase="@phase_lv.phase",
            Kat=fa.cfg("KatL"),
            Kar=fa.cfg("KatL"),
            KisoC=fa.cfg("KISOL"),
            KisoR=fa.cfg("KISOL"),
            Tau=fa.cfg("TAUL"),
            Rp=fa.cfg("RpL"),
            Zc=fa.cfg("ZcL"),
            L=fa.cfg("LLL"),
        ),
        fa.TxtExporter(
            filename=fa.cfg("pressure_lv_file"),
            exportEveryNumberOfSteps=1,
            exportAtBegin=True,
            ff="@ff_lv",
            volume="@vol_lv.volume",
        ),
        fa.SubsetMapping(name="subsetmapping_lv"),
    )

    Nh(node.addChild("endo_visu_lv")).add(
        fa.OglModel(src="@../loader_endo_lv", color="#bb0a1e"),
        fa.IdentityMapping(),
    )
