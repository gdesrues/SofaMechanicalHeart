from model.scene.node import NodeHelper as Nh


def add_RV(fa, node):
    Nh(node).add(
        fa.MeshObjLoader(name="loader_endo_rv", filename=fa.cfg("mesh_endo_rv")),
        fa.TriangleSetTopologyContainer(name="topo_rv", src="@loader_endo_rv"),
        fa.MechanicalObject(name="obj_rv", src="@topo_rv"),
        fa.VolumeEngine(
            name="vol_rv",
            topology="@topo_rv",
            printLog=True,
            position="@obj_rv.position",
            # show_boundary_edges=True,
        ),
        fa.Pat(
            name="pat_rv",
            printLog=True,
            pat_0=fa.cfg("Pat1L"),
            pat_max=fa.cfg("Pat2L"),
            heart_period=fa.cfg("HEART_PERIOD"),
            t1=fa.cfg("T_0L"),
            t2=fa.cfg("T_1L"),
            t3=fa.cfg("T_2L"),
            alpha1=fa.cfg("V1L"),
            alpha2=fa.cfg("V2L"),
        ),
        fa.Par(
            name="par_rv",
            printLog=True,
            phase="@phase_rv.phase",
            ff="@ff_rv",
            par_0=fa.cfg("Par1L"),
            par_inf=fa.cfg("Par2L"),
        ),
        fa.PhaseManager(
            name="phase_rv",
            Pv="@ff_rv.Pv",
            Par="@par_rv.pressure",
            Pat="@pat_rv.pressure",
        ),
        fa.PressureFF(
            name="ff_rv",
            mstate="@obj_rv",
            topology="@topo_rv",
            edges_on_border="@vol_rv.edges_on_border",
            dV="@vol_rv.dV",
            pv_0=fa.cfg("PV0L"),
            Par="@par_rv.pressure",
            par_inf="@par_rv.par_inf",
            Pat="@pat_rv.pressure",
            printLog=True,
            phase="@phase_rv.phase",
            Kat=fa.cfg("KatL"),
            Kar=fa.cfg("KatL"),
            KisoC=fa.cfg("KISOL"),
            KisoR=fa.cfg("KISOL"),
            Tau=fa.cfg("TAUL"),
            Rp=fa.cfg("RpL"),
            Zc=fa.cfg("ZcL"),
            L=fa.cfg("LLL"),
        ),
        fa.TxtExporter(
            filename=fa.cfg("pressure_rv_file"),
            exportEveryNumberOfSteps=1,
            exportAtBegin=True,
            ff="@ff_rv",
            volume="@vol_rv.volume",
        ),
        fa.SubsetMapping(name="subsetmapping_rv"),
    )

    Nh(node.addChild("endo_visu_rv")).add(
        fa.OglModel(src="@../loader_endo_rv", color="#bb0a1e"),
        fa.IdentityMapping(),
    )
