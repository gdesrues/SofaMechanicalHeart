from SofaRuntime.SofaRuntime import importPlugin

PLUGINS = {
    "SofaSimpleFem",
    "SofaImplicitOdeSolver",
    "SofaExporter",
    "SofaOpenglVisual",
    "SofaDeformable",
    "SofaEngine",
    "SofaGeneralLoader",
    "SofaMeshCollision",
    "SofaDenseSolver",
    "SofaPreconditioner",
    "SofaBoundaryCondition",
    "SofaConstraint",
    "SofaGraphComponent",
    "SofaOpenglVisual",
    "SofaMechanicalHeart",
}


def import_plugins(*args):
    pl = set(PLUGINS)
    for x in args:
        pl.add(x)
    for x in pl:
        importPlugin(x)
