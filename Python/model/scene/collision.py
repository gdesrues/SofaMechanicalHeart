from model.scene.node import NodeHelper as Nh


# Add in the meca node:
# fa.GenericConstraintSolver(tolerance=1e-3, maxIterations=1e3),
# fa.UncoupledConstraintCorrection(),
# fa.BilateralInteractionConstraint(),
# + add_meca_collision as meca child
# + add_pericardium node at same level as meca node


def add_meca_collision(fa, node):
    Nh(node).add(
        fa.MeshObjLoader(name="l11", filename=fa.cfg("mesh_visu")),
        fa.TriangleSetTopologyContainer(src="@l11"),
        fa.MechanicalObject(),
        fa.TriangleCollisionModel(),
        fa.LineCollisionModel(),
        fa.PointCollisionModel(),
        fa.SubsetMapping(),
    )


def add_pericardium(fa, node):
    ns = dict(simulated=False, moving=False)
    Nh(node).add(
        fa.MeshObjLoader(name="loader_peri", filename=fa.cfg("mesh_peri")),
        fa.MeshTopology(src="@loader_peri"),
        fa.MechanicalObject(src="@loader_peri"),
        fa.TriangleCollisionModel(**ns),
        fa.LineCollisionModel(**ns),
        fa.PointCollisionModel(**ns),
        fa.VisualStyle(displayFlags="showWireframe"),
        fa.OglModel(src="@loader_peri", color="black"),
    )
