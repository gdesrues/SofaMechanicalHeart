from Sofa.Core import Node


class NodeHelper:
    def __init__(self, node: Node):
        self.node = node

    def add(self, *objs):
        for obj in objs:
            self.node.addObject(obj[0], **obj[1])
        return self.node
