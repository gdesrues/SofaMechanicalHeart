import logging

import treefiles as tf
from Sofa.Core import Node
from SofaRuntime.SofaRuntime import importPlugin

from model.params.ModelParams import ModelParams
from model.scene.collision import add_meca_collision, add_pericardium
from model.scene.factory import Factory
from model.scene.node import NodeHelper as Nh
from model.scene.rings import add_meca_rings
from model.scene.scene_LV import add_LV
from model.scene.scene_RV import add_RV
from model.scene.visual import VisualStyle


def create_graph(root: Node, params: ModelParams):
    fa = Factory(params)

    root.findData("dt").value = params.DT.value
    root.findData("gravity").value = [0, 0, 0]

    importPlugin("SofaMiscFem")

    v = (
        VisualStyle.showCollisionModels,
        VisualStyle.showInteractionForceFields,
        # VisualStyle.showVisualModels,
    )
    lts = dict(color="1 1 0.6 1", attenuation=0, fixed=True)

    Nh(root).add(
        fa.DefaultAnimationLoop(),
        fa.DefaultVisualManagerLoop(),
        fa.VisualStyle(displayFlags=" ".join(v)),
        fa.LightManager(),
        fa.PositionalLight(position=[0, 0, 300], name=f"light_0", **lts),
        fa.PositionalLight(position=[0, 0, -600], name=f"light_1", **lts),
        # fa.BackgroundSetting(image="textures/SOFA_logo_white.bmp"),  # seems to be ignored

        # Collision
        # fa.FreeMotionAnimationLoop(),
        # fa.DefaultPipeline(),
        # fa.BruteForceBroadPhase(),
        # fa.BVHNarrowPhase(),
        # # fa.MinProximityIntersection(alarmDistance=1e-3, contactDistance=5e-4),
        # fa.MinProximityIntersection(alarmDistance=3e-4, contactDistance=1e-4),
        # fa.DefaultContactManager(response="PenalityContactForceField"),
    )

    meca = root.addChild("MecaNode")
    Nh(meca).add(
        fa.HeartMeshLoader(),
        fa.MecaEulerImplicitSolver(),
        fa.CGLinearSolver(),
        fa.MecaMechanicalObject(),
        fa.TetrahedronSetTopologyContainer(),
        fa.TetrahedronSetTopologyModifier(),
        fa.TetrahedronSetGeometryAlgorithms(),
        # fa.MooneyRivlin(),
        fa.TetrahedronHyperelasticityFEMForceField(
            name="FEM", ParameterSet="1e5 1e5 80e5", materialName="MooneyRivlin"
        ),
        # fa.ContractionForceField(),
        # fa.ContractionInitialization(),
        # fa.ApexFF(),
        fa.BaseFF(),
        #
        # fa.HemoConstraint(
        #     printLog=True,
        #     ff="@LV/ff_lv ",
        #     solver="@linear_solver",
        #     mapping="@LV/subsetmapping_lv",
        # ),
        #
        fa.DiagonalMass(massDensity=fa.cfg("MYO_DENSITY")),
        fa.VTKExporter(),
        #
        # Collision
        fa.GenericConstraintSolver(tolerance=1e-3, maxIterations=1e3),
        fa.UncoupledConstraintCorrection(),
        fa.BilateralInteractionConstraint(),
    )

    visu_meca = meca.addChild("visu_meca")
    Nh(visu_meca).add(
        fa.MeshObjLoader(name="visu_loader", filename=fa.cfg("mesh_visu")),
        fa.OglModel(src="@visu_loader", color="#800000"),
        fa.SubsetMapping(),
    )

    # Collision
    coll = meca.addChild("meca_collision")
    add_meca_collision(fa, coll)

    # Rings boundary conditions
    # rings = meca.addChild("meca_rings")
    # add_meca_rings(fa, rings)

    endo_lv = meca.addChild("LV")
    add_LV(fa, endo_lv)

    endo_rv = meca.addChild("RV")
    add_RV(fa, endo_rv)

    # Nh(root.addChild("Coupling")).add(
    #     fa.CouplingEulerImplicitSolver(),
    #     fa.Gravity(),
    #     fa.ContractionCGLinearSolver(),
    #     fa.ContractionMechanicalObject(),
    #     fa.ContractionCouplingForceField(),
    #     fa.UniformMass(),
    # )

    # Collision
    peri = meca.addChild("Pericardium")
    add_pericardium(fa, peri)


log = logging.getLogger(__name__)
