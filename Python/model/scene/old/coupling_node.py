from Sofa.Core import Node


def add_coupling_node(root: Node):
    root.addObject(
        "EulerImplicitSolver",
        firstOrder="true",
        tags="tagContraction",
        rayleighStiffness="0",
        rayleighMass="0",
    )
    root.addObject("Gravity", gravity="0 0 0", tags="tagContraction"),
    root.addObject(
        "CGLinearSolver",
        name="linear-solver-coupling",
        template="GraphScattered",
        tags="tagContraction",
    ),
    root.addObject(
        "MechanicalObject",
        position="@../MecaNode/InitializationContraction.outputs",
        name="contractObj",
        template="Vec3d",
        tags="tagContraction",
    ),
    root.addObject(
        "ContractionCouplingForceField",
        StarlingEffect=False,  # "if used with verdandi"
        withFile=True,
        tetraContractivityParam="@../MecaNode/loader.ContractionParameters",
        tags="tagContraction",
        useSimple=False,  # If the model should be simplified
        depolarisationTimes="@../MecaNode/loader.depoTimes",
        APD="@../MecaNode/loader.APDTimes",
        alpha=self.cfg["ALPHA"],
        n0=self.cfg["F_N0"],
        n1=self.cfg["F_N1"],
        n2=self.cfg["F_N2"],
        file=self.cfg["JACFILE2"],
    ),
    root.addObject(
        "UniformMass",
        vertexMass="1",
        tags="tagContraction",
    ),
