import logging

from model.params.ModelParams import ModelParams


class BaseFactory:
    def __getattr__(self, item):
        if item[0] == item[0].upper():

            def w_(**kw):
                return item, kw

            return w_


class Factory(BaseFactory):
    def __init__(self, params: ModelParams):
        super().__init__()
        self.cfg = lambda x: params[x].value

    def assoc(self, *args):
        """Helper to concatenate params as x[args1]+' '+x[args2]+' '+..."""
        return " ".join(str(s) for s in map(self.cfg, args))

    def HeartMeshLoader(self, **kwargs):
        d = dict(
            name="loader",
            filename=self.cfg("PROXY_MESH_PATH"),
            createSubelements=True,
        )
        d.update(kwargs)
        return "HeartMeshLoader", d

    # def CardiacVTKLoader(self, **kwargs):
    #     d = dict(
    #         name="loader",
    #         TetraTriangle="Tetra",
    #         unitTime="ms",
    #         filename=self.cfg("MESH_PATH"),
    #         FileFacetFiber=self.cfg("FIBERS_PATH"),
    #         startContraction=float(self.cfg("EM_DELAY"))
    #         + float(self.cfg("START_CONTRACTION")),
    #         FileStiffnessParameters=self.cfg("STIFFNESS_FILE"),
    #         FileContractionParameters=self.cfg("CONTRACTION_FILE"),
    #         ElectroFile=self.cfg("ELECTRO_FILE"),
    #         FileTetraConductivity=self.cfg("CONDUCTIVITY_FILE"),
    #         TetraZoneName=self.cfg("TETRA_ZONE_NAME"),
    #     )
    #     d.update(kwargs)
    #     return "CardiacVTKLoader", d

    # def StrainMonitor(self, **kwargs):
    #     params = dict(
    #         name="strainMonitor",
    #         contraction_forcefield="@contraction",
    #         vtk_loader="@loader",
    #         topology="@ContainerTetra",
    #         filename=self.cfg("STRAIN_MONITOR_FILENAME"),
    #     )
    #     return "StrainMonitor", params

    def CGLinearSolver(self, **kwargs):
        d = dict(
            name="linear_solver",
            iterations="200",
            tags="meca",
            threshold="1e-12",
            tolerance="1e-12",
            template="GraphScattered",
        )
        d.update(kwargs)
        return "CGLinearSolver", d

    # def ProjectiveLinearSolver(self, **kwargs):
    #     d = dict(
    #         name="projective_linear_solver",
    #         iterations="1000",
    #         threshold="1e-12",
    #         tolerance="1e-12",
    #         template="GraphScattered",
    #         warmStart=True,
    #     )
    #     d.update(kwargs)
    #     return "CGLinearSolver", d

    # def CollisionDefaultPipeline(self, **kwargs):
    #     d = dict(draw="0", depth="10", verbose="0")
    #     d.update(kwargs)
    #     return "DefaultPipeline", d
    #
    # def BruteForceDetection(self, **kwargs):
    #     d = dict(name="N2")
    #     d.update(kwargs)
    #     return "BruteForceDetection", d
    #
    # def MinProximityIntersection(self, **kwargs):
    #     d = dict(
    #         contactDistance="0.001",
    #         alarmDistance="0.005",
    #         name="Proximity",
    #     )
    #     d.update(kwargs)
    #     return "MinProximityIntersection", d

    # def CollisionDefaultResponse(self, **kwargs):
    #     d = dict(name="Response", response="default")
    #     d.update(kwargs)
    #     return "DefaultContactManager", d

    def MecaEulerImplicitSolver(self, **kwargs):
        d = dict(
            name="meca_solver",
            tags="meca",
            rayleighStiffness=self.cfg("RAY_STIFF"),
            rayleighMass=self.cfg("RAY_MASS"),
            firstOrder=self.cfg("EULER_FIRST_ORDER"),
        )
        d.update(kwargs)
        return "EulerImplicitSolver", d

    def MecaMechanicalObject(self, **kwargs):
        d = dict(
            # scale="1e-3",
            name="mecaObj",
            template="Vec3d",
            tags="meca",
            position="@loader.position",
            restScale="1",
        )
        d.update(kwargs)
        return "MechanicalObject", d

    # def TopologyComponents(self):
    #     return (
    #         self.TetrahedronSetTopologyContainer(),
    #         self.TetrahedronSetTopologyModifier(),
    #         # self.TetrahedronSetTopologyAlgorithms(),
    #         self.TetrahedronSetGeometryAlgorithms(),
    #     )

    def TetrahedronSetTopologyContainer(self, **kwargs):
        d = dict(
            name="ContainerTetra",
            tetrahedra="@loader.tetrahedra",
            triangles="@loader.triangles",
            tags="meca",
        )
        d.update(kwargs)
        return "TetrahedronSetTopologyContainer", d

    # def TriangleSetTopologyContainer(self, **kwargs):
    #     d = dict(
    #         name="ContainerTri",
    #         triangles="@loader.triangles",
    #         tags="meca",
    #     )
    #     d.update(kwargs)
    #     return "TriangleSetTopologyContainer", d

    def TetrahedronSetTopologyModifier(self, **kwargs):
        d = dict(name="Modifier")
        d.update(kwargs)
        return "TetrahedronSetTopologyModifier", d

    # def TriangleSetTopologyModifier(self, **kwargs):
    #     d = dict(name="Modifier")
    #     d.update(kwargs)
    #     return "TriangleSetTopologyModifier", d

    def TetrahedronSetTopologyAlgorithms(self, **kwargs):
        d = dict(name="TopoAlgo", template="Vec3d")
        d.update(kwargs)
        return "TetrahedronSetTopologyAlgorithms", d

    # def TriangleSetTopologyAlgorithms(self, **kwargs):
    #     d = dict(name="TopoAlgo", template="Vec3d")
    #     d.update(kwargs)
    #     return "TriangleSetTopologyAlgorithms", d

    def TetrahedronSetGeometryAlgorithms(self, **kwargs):
        d = dict(name="GeomAlgo", template="Vec3d")
        d.update(kwargs)
        return "TetrahedronSetGeometryAlgorithms", d

    def TriangleSetGeometryAlgorithms(self, **kwargs):
        d = dict(name="GeomAlgo", template="Vec3d")
        d.update(kwargs)
        return "TriangleSetGeometryAlgorithms", d

    def RightPressure(self, **kwargs):
        d = dict(
            DisableFirstAtriumContraction="false",
            template="Vec3d",
            name="pressureforceR",
            tags="meca",
            loadername="VTK",
            windkessel="4",
            useProjection="1",
            Kiso=self.assoc("KISOR", "KISOR"),
            trianglesSurf="@loader.trianglesSurf",
            pointZoneName="@loader.pointZoneNames",
            loaderZoneNames="@loader.surfaceZoneNames",
            loaderZones="@loader.surfaceZones",
            pointZones="@loader.pointZones",
            SurfaceZone=self.cfg("RV_ENDO_ZONE"),
            ZoneType=self.cfg("ZONE_TYPE_PRESSURE"),  # Triangles or Points
            Pv0=self.cfg("PV0R"),
            # file=self.cfg("RIGHT_PRESSURE_FILE"),
            heartPeriod=self.cfg("HEART_PERIOD"),
            atriumParam=self.assoc(
                "KatR", "Pat1R", "Pat2R", "V1R", "V2R", "T_0R", "T_1R", "T_2R"
            ),
            aorticParam="1e-6 "
            + self.assoc("Par1R", "Par2R", "TAUR", "RpR", "ZcR", "LLR"),
            # EdgesOnBorder=edges["RV"],
            BoundaryEdgesPath=self.cfg("BOUNDARY_EDGES_JSON"),
            BoundaryEdgesKey="RV",
            # tolerance=self.cfg("TOLERANCE_ISO"),
            loader_link="@loader",
            # contraction_forcefield="@contraction",
            # tetra_topology="@ContainerTetra",
            # strain="@strainMonitor",
            # escalier_pat=self.cfg("ESCALIER"),
            # init_phase=0,
        )

        d.update(kwargs)
        return "PressureConstraintForceField", d

    # Pressure forces left ventricle
    def LeftPressure(self, **kwargs):
        d = dict(
            DisableFirstAtriumContraction="false",
            template="Vec3d",
            name="pressureforceL",
            tags="meca",
            loadername="VTK",
            windkessel="4",
            useProjection="1",
            Kiso=self.assoc("KISOL", "KISOL"),
            trianglesSurf="@loader.trianglesSurf",
            pointZoneName="@loader.pointZoneNames",
            pointZones="@loader.pointZones",
            loaderZoneNames="@loader.surfaceZoneNames",
            loaderZones="@loader.surfaceZones",
            SurfaceZone=self.cfg("LV_ENDO_ZONE"),
            ZoneType=self.cfg("ZONE_TYPE_PRESSURE"),  # Triangles or Points
            Pv0=self.cfg("PV0L"),
            # file=self.cfg("LEFT_PRESSURE_FILE"),
            # file2=self.cfg("LEFT_PRESSURE_FILE_2"),
            heartPeriod=self.cfg("HEART_PERIOD"),
            atriumParam=self.assoc(
                "KatL", "Pat1L", "Pat2L", "V1L", "V2L", "T_0L", "T_1L", "T_2L"
            ),
            # Kat, Pat0, Patm, alpha1, alpha2, tof, tm, tc
            aorticParam="1e-6 "
            + self.assoc("Par1L", "Par2L", "TAUL", "RpL", "ZcL", "LLL"),
            # Kar, Par0, Pve, tau, Rp, Zc, L
            # EdgesOnBorder=edges["LV"],
            BoundaryEdgesPath=self.cfg("BOUNDARY_EDGES_JSON"),
            BoundaryEdgesKey="LV",
            # tolerance=self.cfg("TOLERANCE_ISO"),
            loader_link="@loader",
            # contraction_forcefield="@contraction",
            # tetra_topology="@ContainerTetra",
            # strain="@strainMonitor.strain",
            # escalier_pat=self.cfg("ESCALIER"),
            # init_phase=0,
        )
        d.update(kwargs)
        return "PressureConstraintForceField", d

    def ContractionInitialization(self, **kwargs):
        d = dict(
            name="InitializationContraction",
            template="ContractionInitialization<Vec3d,Vec3d>",
            tags="meca",
        )
        d.update(kwargs)
        return "ContractionInitialization", d

    def ProjectivePressureConstraint(self, **kwargs):
        d = dict(
            FFNames="pressureforceR pressureforceL",
            # forcefields="@pressureforceR @pressureforceL",
            # linear_solver="@linear_solver",
            name="project",
            template="Vec3d",
            tags="meca",
            # file=self.cfg("PROJECTIVE_PRESSURE_FILE"),
            optimiser=False,
            # vents_file=self.cfg("VENTS_FILE"),
        )
        d.update(kwargs)
        return "ProjectivePressureConstraint", d

    def MooneyRivlin(self, **kwargs):
        d = dict(
            ParameterSet="@loader.StiffnessParameters",
            name="MR_FF",
            tags="meca",
            matrixRegularization="1",
            depolarisationTimes="@loader.p_depol",
            APD="@loader.p_apd",
            template="Vec3d",
            activeRelaxation=self.cfg("ACTIVE_RELAXATION"),
            MaxPeakActiveRelaxation=self.cfg("MAX_PEAK"),
            # file=self.cfg("JACFILE"),
        )

        d.update(kwargs)
        return "MRForceField", d

    #  Contraction forces, along the fibre direction
    def ContractionForceField(self, **kwargs):
        d = dict(
            fiberDirections="@loader.facetFibers",
            addElastometry=True,
            name="contraction",
            tags="meca",
            useCoupling=True,
            tagContraction="tagContraction",
            APD="@loader.p_apd",
            template="Vec3d",
            tetraContractivityParam="@loader.ContractionParameters",
            depolarisationTimes="@loader.p_depol",
            viscosityParameter=self.cfg("MU"),
            elasticModulus=self.cfg("EEE"),
            # file=self.cfg("JACFILE3"),
            heartPeriod=self.cfg("HEART_PERIOD"),
            # calculateStress=True,
            # SecSPK_passive="@MR_FF.MRStressPK",
            # stressFile=self.cfg("stressFile_ContractionForceField"),
        )

        d.update(kwargs)
        return "ContractionForceField", d

    # Spring at the apex
    def ApexFF(self, **kwargs):
        d = dict(
            name="Apex_String",
            normal="0 0 1",
            tags="meca",
            useForce="1",
            template="Vec3d",
            loadername="VTK",
            kn="400",
            kp="400",
            Zone="Zone17 ",  # self.cfg("APEX_ZONE"),
        )

        d.update(kwargs)
        return "BaseConstraintForceField", d

    # Spring at the rings / Load heart axis file data
    def BaseFF(self, **kwargs):
        d = dict(
            name="String_Rings",
            # temporaryForce="True",   # False is default
            loadername="VTK",
            useForce="1",
            template="Vec3d",
            tags="meca",
            Zone=self.cfg("RINGS_ZONE"),
            kp=100, # self.cfg("ATRIUM_FORCE"),
            kn=100, # self.cfg("ATRIUM_FORCE"),
            heartPeriod=self.cfg("HEART_PERIOD"),
            normal="0 0 1",  # self.geoData["AxisHeart"],
            temporaryTimes=self.assoc("T_0L", "T_1L", "T_2L"),
        )
        d.update(kwargs)
        return "BaseConstraintForceField", d

    # def DiagonalMass(self, **kwargs):
    #     d = dict(
    #         name="DiagonalMass",
    #         # template="Vec3d",
    #         tags="meca",
    #         massDensity=self.cfg("MYO_DENSITY"),
    #     )
    #     d.update(kwargs)
    #     return "DiagonalMass", d

    def VTKExporter(self, **kwargs):
        d = dict(
            name="simu_exporter",
            filename=self.cfg("MESH_EXPORTED"),
            XMLformat=False,
            edges=False,
            tetras=True,
            exportEveryNumberOfSteps=1,
            listening=True,
            printLog=False,
        )
        d.update(kwargs)
        return "VTKExporter", d

    # def CardiacSimulationExporter(self, **kwargs):
    #     d = dict(
    #         Tetras="1",
    #         ExportScale="1e3",
    #         name="VtkExporter",
    #         ExportFileType=".vtk",
    #         ExportEveryNSteps=self.cfg("EXPORT_STEP"),
    #         Filename=self.cfg("MESH_EXPORTED"),
    #         ExportStartStep=self.cfg("START_EXPORT_STEP"),
    #         ExportSigmaC=False,
    #         ExportEc=False,
    #         ExportE1d=False,
    #         # exportVelocity=1,
    #         # exportAcceleration=1,
    #         contraction_forcefield="@contraction",
    #     )
    #     d.update(kwargs)
    #     return "CardiacSimulationExporter", d

    # def GeoController(self, **kwargs):
    #     d = dict(
    #         meca="@mecaObj",
    #         loader="@loader",
    #         geo="@GeomAlgo",
    #         filename=self.cfg("EXPORT_GEO_FILE"),
    #         BarycentreMesh=self.geoData["BarycentreMesh"],
    #         AxisHeart=self.geoData["AxisHeart"],
    #         PointApexLV=self.geoData["PointApexLV"],
    #         BarycentreTwoRings=self.geoData["BarycentreTwoRings"],
    #         BarycentreLVendo=self.geoData["BarycentreLVendo"],
    #         PointTricuspidExtremal=self.geoData["PointTricuspidExtremal"],
    #         PointApexRV=self.geoData["PointApexRV"],
    #         BarycentreRingMitral=self.geoData["BarycentreRingMitral"],
    #         BarycentreRingTricuspid=self.geoData["BarycentreRingTricuspid"],
    #         AxisLV=self.geoData["AxisLV"],
    #         PointMitralExtremal=self.geoData["PointMitralExtremal"],
    #         AxisRV=self.geoData["AxisRV"],
    #         PointTopSeptum=self.geoData["PointTopSeptum"],
    #         AxisRings=self.geoData["AxisRings"],
    #     )
    #     d.update(kwargs)
    #     return "GeoController", d

    def CouplingEulerImplicitSolver(self, **kwargs):
        d = dict(
            firstOrder="true",
            tags="tagContraction",
            rayleighStiffness="0",
            # template="Vec3d",
            rayleighMass="0",
        )
        d.update(kwargs)
        return "EulerImplicitSolver", d

    def Gravity(self, **kwargs):
        d = dict(gravity="0 0 0", tags="tagContraction")
        d.update(kwargs)
        return "Gravity", d

    def ContractionCGLinearSolver(self, **kwargs):
        d = dict(
            name="linear-solver-coupling",
            template="GraphScattered",
            tags="tagContraction",
            iterations=25,
            tolerance=1e-5,
            threshold=1e-5,
        )
        d.update(kwargs)
        return "CGLinearSolver", d

    def ContractionMechanicalObject(self, **kwargs):
        d = dict(
            position="@../MecaNode/InitializationContraction.outputs",
            name="contractObj",
            template="Vec3d",
            tags="tagContraction",
        )
        d.update(kwargs)
        return "MechanicalObject", d

    def ContractionCouplingForceField(self, **kwargs):
        d = dict(
            StarlingEffect=False,  # "if used with verdandi"
            withFile=True,
            tetraContractivityParam="@../MecaNode/loader.ContractionParameters",
            tags="tagContraction",
            useSimple=False,  # If the model should be simplified
            depolarisationTimes="@../MecaNode/loader.p_depol",
            APD="@../MecaNode/loader.p_apd",
            alpha=self.cfg("ALPHA"),
            n0=self.cfg("F_N0"),
            n1=self.cfg("F_N1"),
            n2=self.cfg("F_N2"),
            loaderzones="@../MecaNode/loader.zones",
            # file=self.cfg("JACFILE2"),
        )
        d.update(kwargs)
        return "ContractionCouplingForceField", d

    def UniformMass(self, **kwargs):
        d = dict(
            vertexMass="1",
            # template="Vec3d",
            tags="tagContraction",
        )
        return "UniformMass", d


log = logging.getLogger(__name__)
