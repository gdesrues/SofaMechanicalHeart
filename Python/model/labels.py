class BLabel:
    @classmethod
    def th(cls, k: str):
        return getattr(cls, f"TH_{k.upper()}")

    @classmethod
    def attr(cls, k: str):
        return getattr(cls, k.upper())


class Labels:
    # Name of vtk data arrays
    ENDOS_EPI = "Endos_Epi"
    VENTRICLES = "Ventricles"
    ZONES = "Volume_Areas"
    ACTIV = "Activ_Time"
    ACTIV_MOVIE = "Depolarisation"

    class Endos_Epi(BLabel):
        """
        The `ENDOS_EPI` data arrays will have the following definition:
            - Right ventricle elements as tagged as 0
            - Left ventricle elements as tagged as 1
            - Epicardium elements as tagged as 2
            - Myocardium elements as tagged as 3
        """

        TH_ENDOS = (-0.5, 1.5)  # for thresholds
        TH_RV = (-0.5, 0.5)
        RV = 0
        TH_LV = (0.5, 1.5)
        TH_EPI = (1.5, 2.5)
        LV = 1
        EPI = 2
        MYO = 3

        # @staticmethod
        # def th(k: str):
        #     return getattr(Labels.Endos_Epi, f"TH_{k.upper()}")

    class Ventricles(BLabel):
        LV = 0
        RV = 1
        TH_LV = (-0.5, 0.5)
        TH_RV = (0.5, 1.5)

    class Zones(BLabel):
        AHA_LV = (0.5, 17.5)
        APEX = 17
        TH_RV = (29.5, 30.5)
        TH_LV = (30.5, 31.5)
        TH_RINGS = (21.5, 22.5)
        RV = 30
        LV = 31
        EPI = 32
        MYO = 33
        RINGS = 22  # because no zone 22 with buildZones method
        SEPTUM = [2, 8, 9, 14]
        LV_FREE_WALL = [5, 6, 11, 12, 16]

    class Image:
        BLOOD_LV = 15
        BLOOD_RV = 16
        WALL_LV = 17
        WALL_RV = 18

    class Fascicle:
        ANTERIOR = 2
        POSTERIOR = 1
        TH_LV = (0.5, 2.5)
        RV = 3
        HIS = 4
