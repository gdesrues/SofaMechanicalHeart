import logging

import numpy as np
import treefiles as tf
from MeshObject import Mesh

from elec_data.prepare_meca_mesh import MecaMeshData
from model.analyse.mesh_analyse.Parsers.extractor import PointExtractor
from model.labels import Labels


__all__ = [
    "mesh_from_elec",
    "remesh",
    "mesh_visu",
    "mesh_peri",
    "mesh_rings",
    "make_endos",
    "extract_boudary_edges",
    "extract_points",
]


def mesh_from_elec(self):
    """
    Convert elec simu output into mechanical simu input
    Point and cell data are built from parameters
    """
    gen = MecaMeshData(fname=self.data_tree.mesh_final, params=self.params)
    gen.build_rings()
    gen.add_fibers(self.data_tree.fibers)
    gen.add_depol_apd(self.data_tree.depol)
    gen.add_passive()
    gen.add_active()
    gen.convert_unit()
    gen.write()


def remesh(self):
    gen = MecaMeshData(fname=self.data_tree.mesh_final, params=self.params)
    # gen.build_rings()
    gen.add_fibers(self.data_tree.fibers)
    gen.add_depol_apd(self.data_tree.depol)
    gen.add_passive()
    gen.add_active()
    gen.convert_unit()
    # gen.write()

    m = gen.m.copy()
    m.CellDataToPointData()
    m1 = m.copy().convertToPolyData()
    m1.tetrahedralize(m.mmg_options(hmin=0.01, hmax=0.01, nr=True))
    m1.interpolate(
        m,
        kernel=dict(
            name="vtkGaussianKernel",
            SetKernelFootprintToNClosest=[],
            SetNumberOfPoints=1,
        ),
    )
    m1.CustomPointDataToCellData(Labels.ZONES)
    m1.write(self.simu_tree.data / "test.vtk")

    gen2 = MecaMeshData(fname=self.simu_tree.data / "test.vtk", params=self.params)
    gen2.build_rings()
    # gen2.m.write(self.simu_tree.data / "test2.vtk")

    m = gen2.m.copy()
    for x in ["bulk", "c1", "c2", "K0", "sigma", "Katp", "Krs"]:
        m.addCellData(np.ones(m.nbCells), x)
    m.addCellData(np.ones((m.nbCells, 3)), "fibers")
    for x in ["depol", "apd"]:
        m.addPointData(np.ones(m.nbPoints), x)
    m.write(self.simu_tree.data.mesh_final, support_new_type=False)


def mesh_visu(self):
    m = Mesh.load(self.simu_tree.data.mesh_final)
    m.convertToPolyData()
    m.filterCellsTo(Mesh.TRIANGLE)
    m.write(self.simu_tree.data.visu)


def mesh_peri(self, scale=1.03, height=0.75):
    m = Mesh.load(self.simu_tree.data.mesh_final)
    m.convertToPolyData()
    m.filterCellsTo(Mesh.TRIANGLE)
    m.threshold(Labels.Endos_Epi.TH_EPI, Labels.ENDOS_EPI)
    m.clip_at_height(height)
    m.scale(scale)
    m.triangulate(m.mmg_options(hmin=5e-3, hmax=5e-3, nr=True))
    m.write(self.simu_tree.data.peri)


def mesh_rings(self, height=0.75):
    m = Mesh.load(self.simu_tree.data.mesh_final)
    m.convertToPolyData()
    m.filterCellsTo(Mesh.TRIANGLE)
    m.threshold(Labels.Zones.TH_RINGS, Labels.ZONES, method="cell")
    # m.transform(translate=[0, 0, 0.01])
    m.write(self.simu_tree.data.rings)


def make_endos(self):
    fname = self.simu_tree.data.mesh_final
    m = Mesh.load(fname).copy().convertToPolyData()
    for k in ["lv", "rv"]:
        endo = m.copy().threshold(Labels.Endos_Epi.th(k), Labels.ENDOS_EPI)
        endo.clear()
        endo.write(getattr(self.simu_tree.data, k))


def extract_boudary_edges(self):
    if not tf.isfile(self.simu_tree.data.lv):
        self.make_endos()
    edges, m1 = {}, Mesh.load(self.simu_tree.data.mesh_final)
    for k in ["lv", "rv"]:
        m = Mesh.load(getattr(self.simu_tree.data, k))
        edges[k] = m.get_boundary_edges(m1)
    tf.dump_json(self.simu_tree.data.edges, edges)


def extract_points(self):
    d = self.simu_tree.data

    if not tf.isfile(d.edges):
        self.extract_boudary_edges()

    pp = PointExtractor(d.mesh_final, d.edges)
    pp.build_zones_points()
    pp.build_plane_points()
    pp.save_to_file(d.pts)
    # pp.load_from_file(d.pts)
    # pp = PointExtractor.load(d.pts, Mesh.load(d.mesh_final).pts)

    # print(pp._zones_)
    # print(pp._slices_)

    # m = Mesh.load(self.simu_tree.data.mesh_final)
    # with tf.PvPlot() as p:
    #     p.add_mesh(m, style="wireframe")
    #     p.label_point(pp.lv_barycenter, "LV", point_color="red")
    #     p.label_point(pp.rv_barycenter, "RV", point_color="red")
    #
    #     for k, v in pp._zones_.items():
    #         p.label_point(v.A.coord, k, point_color="black")
    #         p.label_point(v.B.coord, k, point_color="black")
    #         p.label_point(v.C.coord, k, point_color="black")
    #
    #     for k, v in pp._slices_.items():
    #         p.label_point(v.A.coord, k, point_color="black")
    #         p.label_point(v.B.coord, k, point_color="black")
    #         p.label_point(v.C.coord, k, point_color="black")
    #         p.label_point(v.D.coord, k, point_color="black")
    #         p.label_point(v.E.coord, k, point_color="black")
    #         p.label_point(v.F.coord, k, point_color="black")
    #         p.label_point(v.G.coord, k, point_color="black")
    #         p.label_point(v.H.coord, k, point_color="black")
    #         p.label_point(v.I.coord, k, point_color="black")


log = logging.getLogger(__name__)
