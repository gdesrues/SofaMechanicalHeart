import logging

import treefiles as tf

from BasePackage.Pipeline import ModelParams, ElecModel
from usage.base_data.proxy import BaseDataProxy


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    log = tf.get_logger()

    geometry_id = 0
    purk_id = 1

    proxy = BaseDataProxy("3_12")
    geo_path, purk_path = proxy.get(geometry_id, purk_id)

    out = tf.fTree(__file__, "out").dump()

    params = ModelParams()
    params.attach_data(geo_path)
    params.attach_purkinje(purk_path)
    params.myocardium_conductivity_LV(0.8)
    params.myocardium_conductivity_RV(0.8)
    params.endocardium_conductivity_LV(1.5)
    params.endocardium_conductivity_RV(1.5)
    params.out_dir(out / f'Case_{geometry_id}_{purk_id}')

    ElecModel["electro"](params).start()
