import logging

import numpy as np
import treefiles as tf
from MeshObject import Mesh, TMeshLoadable

from model.labels import Labels
from model.params.ModelParams import ModelParams


class MecaMeshData:
    def __init__(self, fname: TMeshLoadable, params: ModelParams):
        self.params = params
        self.m = Mesh.load(fname)
        self.meca_fname = tf.insert_before(self.m.filename, ".vtk", "_meca")
        self.clear_mesh()

    def clear_mesh(self):
        aha = self.m.getCellDataArray("Volume_Areas")
        ee = self.m.getPointDataArray("Endos_Epi")
        ve = self.m.getPointDataArray("Ventricles")
        self.m.clear()

        # Weird error in sofa vtk reader: workaround
        def weird(y):
            a = np.zeros(y.shape[0])
            for i, x in enumerate(y.astype(int)):
                a[i] = x
            return a

        self.m.addCellData(weird(aha), "Volume_Areas")
        self.m.addPointData(weird(ee), "Endos_Epi")
        self.m.addPointData(weird(ve), "Ventricles")

    def add_fibers(self, fname):
        fibers = np.loadtxt(fname, skiprows=1)
        assert fibers.shape[0] == self.m.nbCells
        self.m.addCellData(fibers, "fibers")

    def add_depol_apd(self, fname):
        depol_apd = np.loadtxt(fname, skiprows=1)
        assert depol_apd.shape[0] == self.m.nbPoints
        self.m.addPointData(depol_apd[:, 0] * 1e-3, "depol")
        self.m.addPointData(depol_apd[:, 1] * 1e-3, "apd")

    def add_passive(self):
        arr = np.ones(self.m.nbCells)
        self.m.addCellData(arr * self.params.c1_default.value, "c1")
        self.m.addCellData(arr * self.params.c2_default.value, "c2")
        self.m.addCellData(arr * self.params.K_default.value, "bulk")

    def add_active(self):
        arr = np.ones(self.m.nbCells)
        self.m.addCellData(arr * self.params.KO_default.value, "K0")
        self.m.addCellData(arr * self.params.sigma_contractility_default.value, "sigma")
        self.m.addCellData(arr * self.params.Katp_default.value, "Katp")
        self.m.addCellData(arr * self.params.Krs_default.value, "Krs")

    def build_rings(self, k=1):
        log.info("Adding rings AHA region to mesh...")

        # Get point indices near valves
        idx = set()
        m = self.m.copy().convertToPolyData()
        for i in range(2):
            vent = m.copy().threshold((i - 0.5, i + 0.5), "Endos_Epi")
            for hole in vent.get_boundary_edges(self.m):
                for e in hole:
                    idx.add(e[0])

        # Get tetras sharing these points and store vertices
        self.m.buildCellsAroundPoints()
        cells = self.m.cells
        for j in range(k):
            for i in idx.copy():
                for t in self.m.cellsAroundPoints[i]:
                    for n in cells[t].ids.as_np():
                        idx.add(n)

        # Finally, get tetras to get rings aha zone
        tidx = set()
        for i in idx:
            for t in self.m.cellsAroundPoints[i]:
                tidx.add(t)

        aha = self.m.getCellDataArray("Volume_Areas")
        aha[np.array(list(tidx)).astype(int)] = 22
        self.m.addCellData(aha, "Volume_Areas")
        # self.m.plot(scalars="Volume_Areas")

    def convert_unit(self):
        self.m.scale(1e-3)
        self.m.transform(translate=-self.m.BBCenter)


    def write(self):
        self.m.write(self.meca_fname, support_new_type=False)
        log.info(
            f"Wrote mesh to file://{self.meca_fname.parent.abs()} ({self.meca_fname.basename})"
        )


log = logging.getLogger(__name__)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    log = tf.get_logger()

    data_path = tf.f(__file__) / "out/Case_0_1"
    pars = ModelParams()

    gen = MecaMeshData(fname=data_path / "data/anatomy/mesh_final.vtk", params=pars)
    gen.add_fibers(data_path / "data/functional/SyntheticFibers.tbb")
    gen.add_depol_apd(data_path / "data/functional/DepolTimeAndAPD.txt")
    gen.add_passive()
    gen.add_active()
    gen.write()
