import logging

import treefiles as tf

from elec_data.prepare_meca_mesh import MecaMeshData
from model.Model import MecaModel
from model.params.ModelParams import ModelParams

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    log = tf.get_logger()

    data_path = tf.f(__file__) / "elec_data/out/Case_0_1"
    simu_path = data_path / "meca_009"

    # Create model default parameters (internally create in/out paths)
    pars = ModelParams(data_path, simu_path)
    # pars.sigma_contractility_default( ... )  # modify parameters value
    pars.PV0L(10e3)
    pars.PV0R(2e3)

    # Create mechanical model instance
    model = MecaModel(pars)

    # # If you need to prepare elec data
    # model.mesh_from_elec()
    # # model.remesh()
    # model.mesh_visu()
    # model.mesh_peri()
    # model.make_endos()
    # model.mesh_rings()
    # model.extract_points()

    # Sofa simulation
    model.init_scene()
    model.run(gui=True)

    # Compute observations and indices
    # model.analyse()

    # Generate html report
    # model.generate_report()
