#include <sofa/core/ObjectFactory.h>
//#include <sofa/type/RigidTypes.h>
#include "ContractionInitialization.inl"

namespace sofa
{

namespace component
{

namespace engine
{

SOFA_DECL_CLASS(ContractionInitialization)

using namespace sofa::defaulttype;

int ContractionInitializationClass = core::RegisterObject("Create potentiel array for initial conditions")
#ifndef SOFA_FLOAT

  .add< ContractionInitialization <Vec1dTypes, Vec3dTypes> >()
  .add< ContractionInitialization <Vec2dTypes, Vec3dTypes> >()
  .add< ContractionInitialization <Vec3dTypes, Vec3dTypes> >()
#endif //SOFA_FLOAT
#ifndef SOFA_DOUBLE


  .add< ContractionInitialization <Vec1fTypes, Vec3fTypes> >()
  .add< ContractionInitialization <Vec2fTypes, Vec3fTypes> >()
  .add< ContractionInitialization <Vec3fTypes, Vec3fTypes> >()
#endif //SOFA_DOUBLE
;

#ifndef SOFA_FLOAT


  template class ContractionInitialization <Vec1dTypes, Vec3dTypes>;
  template class ContractionInitialization <Vec2dTypes, Vec3dTypes>;
  template class ContractionInitialization <Vec3dTypes, Vec3dTypes>;

#endif //SOFA_FLOAT
#ifndef SOFA_DOUBLE


  template class ContractionInitialization <Vec1fTypes, Vec3fTypes>;
  template class ContractionInitialization <Vec2fTypes, Vec3fTypes>;
  template class ContractionInitialization <Vec3fTypes, Vec3fTypes>;
#endif //SOFA_DOUBLE


} // namespace constraint

} // namespace component

} // namespace sofa
