#include "ContractionCouplingForceField.h"
#include <fstream>
#include <iostream>
#include <SofaBaseTopology/TopologyData.inl>
#include <sofa/core/behavior/ForceField.inl>
#include "ContractionForceField.h"
#include <sofa/core/visual/VisualParams.h> 
#include <sofa/helper/AdvancedTimer.h>

namespace sofa
{

namespace component
{

namespace forcefield
{

using namespace sofa::type;
using namespace	sofa::component::topology;
using namespace core::topology;
using namespace core::objectmodel;



using core::topology::BaseMeshTopology;

typedef BaseMeshTopology::Tetra				Tetra;
typedef BaseMeshTopology::EdgesInTetrahedron		EdgesInTetrahedron;
typedef BaseMeshTopology::PointID				NodesId;

typedef Tetra			Tetrahedron;
typedef EdgesInTetrahedron		EdgesInTetrahedron;
typedef NodesId			Vertex;

template< class DataTypes>
void ContractionCouplingForceField<DataTypes>::CCTetrahedronHandler::applyCreateFunction(unsigned int tetrahedronIndex, TetrahedronRestInformation &tinfo,const Tetrahedron& , const type::vector< unsigned int > &, const type::vector< double >&)
{
	if (ff) {
		Vec<4,double> param=ff->getContractivityParameters(tetrahedronIndex);
		tinfo.maxstiffness=param[1];
		tinfo.contractivity=param[0];
		tinfo.relaxationRate=param[3];
		tinfo.contractionRate=param[2];
		Real so=tinfo.contractivity/tinfo.maxstiffness-1.0/2.0;
		tinfo.Uo=tinfo.maxstiffness*(so*so+so+1.0/3.0)/2.0;

		const vector< Tetrahedron > &tetrahedronArray=ff->_topology->getTetrahedra() ;
		const Tetrahedron &t = tetrahedronArray[tetrahedronIndex];
		tinfo.tetraIndices[0] = (Real) t[0];
		tinfo.tetraIndices[1] = (Real) t[1];
		tinfo.tetraIndices[2] = (Real) t[2];
		tinfo.tetraIndices[3] = (Real) t[3];

		tinfo.f_APD[0] = ff->f_APD.getValue()[t[0]];
		tinfo.f_APD[1] = ff->f_APD.getValue()[t[1]];
		tinfo.f_APD[2] = ff->f_APD.getValue()[t[2]];
		tinfo.f_APD[3] = ff->f_APD.getValue()[t[3]];

		tinfo.f_depolarisationTimes[0] = ff->f_depolarisationTimes.getValue()[t[0]];
		tinfo.f_depolarisationTimes[1] = ff->f_depolarisationTimes.getValue()[t[1]];
		tinfo.f_depolarisationTimes[2] = ff->f_depolarisationTimes.getValue()[t[2]];
		tinfo.f_depolarisationTimes[3] = ff->f_depolarisationTimes.getValue()[t[3]];

		tinfo.dEpsilonC = ff->forcefield->getdEpsilonC(tetrahedronIndex);
	}

}
template<class DataTypes>
Vec<4,double> ContractionCouplingForceField<DataTypes>::getContractivityParameters(const unsigned int tetraIndex)
{
Vec<4,double> param;
unsigned int si=(f_tetraContractivityParam.getValue()).size();

if(si==1){
   param = (f_tetraContractivityParam.getValue())[0];

}

else if ((si>1)&&(tetraIndex < si)){

    param = (f_tetraContractivityParam.getValue())[tetraIndex];
}
else
std::cout << "Error, index of tetrahedron is out of bound of Contractivity container. returning null value." << std::endl;
return param;
}

template <class DataTypes> ContractionCouplingForceField<DataTypes>::ContractionCouplingForceField()
: tetraInfo(initData(&tetraInfo,"tetraInfo","Data to handle topology on tetrahedra"))
, f_tetraContractivityParam(initData(&f_tetraContractivityParam,"tetraContractivityParam","<Contractivity,stiffness,contractionRate,relaxationrate> by tetra"))
, f_depolarisationTimes(initData(&f_depolarisationTimes,"depolarisationTimes","depolarisationTimes at each node"))
, f_uniquedepolarisationTimes(initData(&f_uniquedepolarisationTimes,"uniquedepolarisationTimes","depolarisationTimes at each node"))
, f_APD(initData(&f_APD,"APD","APD at each node"))

, f_n0(initData(&f_n0,"n0","reduction parameter between 0 and 1"))
, f_n1(initData(&f_n1,"n1","reduction parameter between 0 and 1"))
, f_n2(initData(&f_n2,"n2","reduction parameter between 0 and 1"))
, fileName(initData(&fileName,"file","File where to store the Monitoring"))
, StarlingEffect(initData(&StarlingEffect,(bool)0,"StarlingEffect","if used with verdandi"))
, m_loaderzones(initData(&m_loaderzones, "loaderzones", "link from HeartMeshLoader"))

, f_alpha(initData(&f_alpha,"alpha","alpha"))
, _initialPoints(0)
, useSimple(initData(&useSimple,"useSimple","If the model should be simplified"))
, withFile(initData(&withFile,"withFile","if the electrophysiology is precomputed"))
, m_tagMeshMechanics(initData(&m_tagMeshMechanics,std::string("meca"),"tagMechanics","Tag of the Mechanical Object"))
, withVT(initData(&withVT,(bool)0,"withVT","if VT TD are used"))
, f_APDVT(initData(&f_APDVT,"APDVT","APDVT"))
, f_TdVT(initData(&f_TdVT,"TdVT","TdVT"))
, useVerdandi(initData(&useVerdandi,(bool)0,"useVerdandi","if used with verdandi"))
{
    tetrahedronHandler = new CCTetrahedronHandler(this, &tetraInfo);
}

template <class DataTypes> void ContractionCouplingForceField<DataTypes>::handleTopologyChange()
{

}

template <class DataTypes> ContractionCouplingForceField<DataTypes>::~ContractionCouplingForceField()
{
    Jac2.close();

    if(tetrahedronHandler) delete tetrahedronHandler;
}

template <class DataTypes> void ContractionCouplingForceField<DataTypes>::init()
{

    cerr << "initializing ContractionCouplingForceField" <<endl;

        this->Inherited::init();




        /// get the mechanical object containing the mesh position

        Tag mechanicalTag(m_tagMeshMechanics.getValue());
        this->getContext()->get(forcefield, mechanicalTag,sofa::core::objectmodel::BaseContext::SearchRoot);

        if (forcefield==NULL) {
                serr << "ERROR(ContractionCouplingForceField): cannot find the forcefield ."<<sendl;
                return;
        }
        _topology=forcefield->getContext()->getMeshTopology();
        if (_topology->getNbTetrahedra()==0)
        {
                serr << "ERROR(ContractionCouplingForceField): object must have a Tetrahedral Set Topology."<<sendl;
                return;
        }

        // get restPosition
        if (_initialPoints.size() == 0)
        {
            const VecCoord& p = this->mstate->read(core::ConstVecCoordId::restPosition())->getValue();
            _initialPoints=p;
        }


        /// prepare to store info in the triangle array
        tetraInfo.createTopologyHandler(_topology);
//         tetraInfo.registerTopologicalData();

        tetrahedronRestInfoVector& tetrahedronInf = *(tetraInfo.beginEdit());
        tetrahedronInf.resize(_topology->getNbTetrahedra());

        int i;
        for (i=0;i<_topology->getNbTetrahedra();++i) {
                tetrahedronHandler->applyCreateFunction(i, tetrahedronInf[i],
                        _topology->getTetrahedron(i),  (const vector< unsigned int > )0,
                        (const vector< double >)0);

        }


        if(StarlingEffect.getValue()){
        /////////// THIS is to monitor the evolution of EC and Starling Effect
        //get loaderzone
//         this->getContext()->get(_meshLoader, sofa::core::objectmodel::BaseContext::SearchRoot);
//         if (!_meshLoader)
//             std::cout << "Error: ContractionCouplingForceField is not able to acces mesh loader!" << std::endl;

        sofa::type::vector<sofa::type::vector<unsigned int>> loaderzones = m_loaderzones.getValue();
// loaderzones.clear(); msg_error() << "GAETAN: single link to heartmeshloader";


        MeanEc.resize(loaderzones.size());
        MeanStar.resize(loaderzones.size());
        zonetbl.resize(_topology->getNbTetrahedra());

        for (unsigned int v = 0; v<loaderzones.size(); v++ ){
            MeanEc[v]=0;
            MeanStar[v]=0;

            for (unsigned int w = 0; w<loaderzones[v].size(); w++ ){
             zonetbl[loaderzones[v][w]]=v;
            }
        }

        if(fileName.getValue()!=""){
            Jac2.open((fileName.getValue()).c_str(),std::ios::out);}
        else{
            Jac2.open("Log.txt",std::ios::out);}
        ///////////
        }

        tetraInfo.endEdit();
        numberCycles=0;
}




template <class DataTypes>
    SReal ContractionCouplingForceField<DataTypes>::getPotentialEnergy(const core::MechanicalParams*, const DataVecCoord& d_x) const
{
    const VecCoord& x = d_x.getValue();

    unsigned int nbTetrahedra=_topology->getNbTetrahedra();
    unsigned int i=0,j=0;
    const tetrahedronRestInfoVector& tetrahedronInf = tetraInfo.getValue();
    const TetrahedronRestInformation *tetInfo;

    Real vv=0;
    double power=0;
    double dissip=0;
       for (i=0; i<nbTetrahedra; ++i){
             tetInfo=&tetrahedronInf[i];
             Real volume=forcefield->getVolume(i);
             vv+=volume;

             power+=f_n0.getValue()*tetInfo->abspot*tetInfo->Uo*volume;

             Vec3 sol=x[i];
             Real UC=sol[2];
             Real num=tetInfo->num;
             dissip+=num*UC*volume;
         }

    return 0;

}


    template <class DataTypes>
    sofa::type::vector<double> ContractionCouplingForceField<DataTypes>::get_SarcomerePower(sofa::type::vector <unsigned int> TetrasIndices){

        sofa::type::vector<double> powers;
        powers.resize(4);
        powers[0]=0;powers[1]=0;powers[2]=0;powers[3]=0;


        const VecDeriv& x = this->mstate->read(core::ConstVecCoordId::position())->getValue();
        const tetrahedronRestInfoVector& tetrahedronInf = tetraInfo.getValue();
        const TetrahedronRestInformation *tetInfo;

        for (unsigned int v = 0; v<TetrasIndices.size(); v++ ){

            tetInfo=&tetrahedronInf[TetrasIndices[v]];
            Real volume=forcefield->getVolume(TetrasIndices[v])/6.0;

            //Binding power
            powers[0]+=volume*f_n0.getValue()*tetInfo->abspot*tetInfo->Uo;

            //Unbinding Power
            powers[1]+=volume*tetInfo->num*x[TetrasIndices[v]][2];

            //Sarcomere out
            powers[2]+=-volume*x[TetrasIndices[v]][0]*forcefield->getdEpsilonC(TetrasIndices[v]);

            //Energy Actine Myosine
            powers[3]+=volume*x[TetrasIndices[v]][2];
        }

        return powers;
    }


template <class DataTypes>
void ContractionCouplingForceField<DataTypes>::addForce(const core::MechanicalParams* /* PARAMS FIRST */, DataVecDeriv& d_f, const DataVecCoord& d_x, const DataVecDeriv& )
{
 	sofa::helper::AdvancedTimer::stepBegin("addForceContractionCouplingFF");

    unsigned int i=0;

    unsigned int nbTetrahedra=_topology->getNbTetrahedra();
    tetrahedronRestInfoVector& tetrahedronInf = *(tetraInfo.beginEdit());
    TetrahedronRestInformation *tetInfo;

    assert(this->mstate);
    double simuTime = this->getContext()->getTime();
    VecDeriv &f=*(d_f.beginEdit());
    VecCoord x=d_x.getValue();
    int HP=forcefield->heartPeriod/this->getContext()->getDt();
    int steps=simuTime/this->getContext()->getDt();
    if (withFile.getValue()){
        if ((steps%HP==0)&&(simuTime>0)) {
        numberCycles ++;
        std::cout<<"numberCycles = "<<numberCycles<<std::endl;
        }

   // else numberCycles=0;
    }


    if(StarlingEffect.getValue()){
    /////////// THIS is to monitor the evolution of EC and Starling Effect
    for (unsigned int v = 0; v<MeanEc.size(); v++ ){
        MeanEc[v]=0;
        MeanStar[v]=0;
    }
    ///////////
    }

    for (i=0; i<nbTetrahedra; ++i){

            tetInfo=&tetrahedronInf[i];

            if(useVerdandi.getValue()){
                    Vec<4,double> param=getContractivityParameters(i);
                    tetInfo->maxstiffness=param[1];
                    tetInfo->contractivity=param[0];
                    tetInfo->relaxationRate=param[3];
                    tetInfo->contractionRate=param[2];
                }

            const Tetrahedron &ta= _topology->getTetrahedron(i);
            tetInfo->abspot=0;
            Real potential=0;
            double APD=10;
            double Td=10;

if(!withVT.getValue()){
            ///value at the barycenter
            APD=((f_APD.getValue())[ta[0]]+(f_APD.getValue())[ta[1]]+(f_APD.getValue())[ta[2]]+(f_APD.getValue())[ta[3]])/4.0;

            if(f_uniquedepolarisationTimes.getValue()==0){
            Td=((f_depolarisationTimes.getValue())[ta[0]]+(f_depolarisationTimes.getValue())[ta[1]]+
                      (f_depolarisationTimes.getValue())[ta[2]]+(f_depolarisationTimes.getValue())[ta[3]])/4.0;
            }
            else{
            Td=f_uniquedepolarisationTimes.getValue();
            }
        }
else{
      int num=(f_TdVT.getValue()).size();


                std::vector< std::vector<double> > APDv0;
                std::vector<std::vector<double> > Tdv0;
                APDv0.resize(4);
                Tdv0.resize(4);
                std::vector<double> td0(4,10);
                std::vector<double> Apd0(4,10);


                for (int j=0; j<num;j++){

                    for(int v=0;v<4;v++){
                        APDv0[v].push_back(f_APDVT.getValue()[j][ta[v]]);
                        Tdv0[v].push_back(f_TdVT.getValue()[j][ta[v]]);
                    }
                }


                for(int v=0;v<4;v++){
                    for (int j=0; j<num-1;j++){

                    if ((simuTime+this->getContext()->getDt()>Tdv0[v][j])&&(simuTime+this->getContext()->getDt()<Tdv0[v][j+1])){
                        td0[v]=Tdv0[v][j];

                    }
                    if ((simuTime+this->getContext()->getDt()>APDv0[v][j]+Tdv0[v][j])&&(simuTime+this->getContext()->getDt()<APDv0[v][j+1]+Tdv0[v][j+1])){
                        Apd0[v]=APDv0[v][j];

                    }
                }

                }

                Td=(td0[0]+td0[1]+td0[2]+td0[3])/4.0;
                APD=(Apd0[0]+Apd0[1]+Apd0[2]+Apd0[3])/4.0;
                if (i==0)std::cout<<Apd0[0]<<" Td "<<td0[0]<<std::endl;
     }

             double Tr=Td+APD;

            if((simuTime>=Td+numberCycles*forcefield->heartPeriod)&&(simuTime<=Tr+numberCycles*forcefield->heartPeriod)){
               potential=tetInfo->contractionRate;

               tetInfo->abspot=tetInfo->contractionRate;

            }
           else if((simuTime>Tr+numberCycles*forcefield->heartPeriod)&&(simuTime<Td+forcefield->heartPeriod+numberCycles*forcefield->heartPeriod)){
                potential=tetInfo->relaxationRate;

                tetInfo->abspot=0;
            }



           tetInfo->num=-(potential+f_alpha.getValue()*fabs(forcefield->getdEpsilonC(i)));

           tetInfo->A.clear();
           tetInfo->A[0][0]=tetInfo->A[1][1]=tetInfo->A[2][2]=tetInfo->num;
           if (!useSimple.getValue()){
               tetInfo->A[0][1]=tetInfo->A[2][0]=forcefield->getdEpsilonC(i);
           }

                                double starlingcoeff=f_n0.getValue();

                                if(StarlingEffect.getValue()){
                                starlingcoeff=f_n0.getValue()*std::max(std::min(std::min(0.5*(forcefield->getEC(i)-f_n2.getValue())/f_n1.getValue()+0.55,1.0),2.25-(forcefield->getEC(i)-f_n2.getValue())/f_n1.getValue()),0.1);
                                /* Code matlab pour visualizer l'évolution
                                figure(1);n0=1;n1=0.3;n2=-0.5;V=@(x)max(min(min(0.25+0.75*(x-n2)/n1,1),2.25-(x-n2)/n1),0.1)*n0;A=-1:0.001:1;plot(A,arrayfun(V,A));axis([-1 1 0 5]);
                                */
                                /////////// THIS is to monitor the evolution of EC and Starling Effect
                                MeanEc[zonetbl[i]]+=forcefield->getEC(i);
                                MeanStar[zonetbl[i]]+=starlingcoeff;
                                ///////////
                                }

            Vec3 force;
            force.clear();
            force[0]=tetInfo->contractivity*starlingcoeff*tetInfo->abspot;
            force[1]=tetInfo->maxstiffness*starlingcoeff*tetInfo->abspot;
            force[2]=tetInfo->Uo*starlingcoeff*tetInfo->abspot;


            f[i]+=(force+tetInfo->A*x[i]);

        }

    if(StarlingEffect.getValue()){
    /////////// THIS is to monitor the evolution of EC and Starling Effect

	sofa::type::vector<sofa::type::vector<unsigned int>> loaderzones = m_loaderzones.getValue();

    for (unsigned int v = 0; v<MeanEc.size(); v++ ){
        MeanEc[v]=MeanEc[v]/loaderzones[v].size();
        MeanStar[v]=MeanStar[v]/loaderzones[v].size();
    }
    cout << MeanEc << " " << MeanStar << endl;
    Jac2 << MeanEc << " " << MeanStar << endl;
    ///////////
    }

    tetraInfo.endEdit();
    d_f.endEdit();

    sofa::helper::AdvancedTimer::stepEnd("addForceContractionCouplingFF");

}


template<class DataTypes>
void ContractionCouplingForceField<DataTypes>::addDForce (const core::MechanicalParams* mparams /* PARAMS FIRST */, DataVecDeriv& d_df, const DataVecDeriv& d_dx){

    sofa::helper::AdvancedTimer::stepBegin("addDForceContractionCouplingFF");

    tetrahedronRestInfoVector& tetrahedronInf = *(tetraInfo.beginEdit());

            TetrahedronRestInformation *tetInfo;
            unsigned int nbTetrahedra=_topology->getNbTetrahedra();
            unsigned int i=0;
            Deriv dv,deltadx;
            const VecDeriv dx=d_dx.getValue();
            VecDeriv& df=*(d_df.beginEdit());
            double kfactor = mparams->kFactor();
            for(i=0; i<nbTetrahedra; i++ )
            {
                tetInfo=&tetrahedronInf[i];

                Mat<3,3,Real> M;
                M=tetInfo->A;


                deltadx= dx[i];

                dv[2] = (Real)(deltadx[0]*M[0][0] + deltadx[1]*M[1][0]+deltadx[2]*M[2][0]) ;
                dv[1] = (Real)(deltadx[0]*M[0][1] + deltadx[1]*M[1][1]+deltadx[2]*M[2][1]) ;
                dv[0] = (Real)(deltadx[0]*M[0][2] + deltadx[1]*M[1][2]+deltadx[2]*M[2][2]) ;
                df[i] += dv*kfactor;
            

            }
            d_df.endEdit();
            tetraInfo.endEdit();

            sofa::helper::AdvancedTimer::stepEnd("addDForceContractionCouplingFF");

}


template<class DataTypes>
void ContractionCouplingForceField<DataTypes>::draw(const core::visual::VisualParams* vparams)
{
        //	unsigned int i;
        if (!vparams->displayFlags().getShowForceFields()) return;
        if (!this->mstate) return;

        if (vparams->displayFlags().getShowWireFrame())
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

        if (vparams->displayFlags().getShowWireFrame())
                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

}

} // namespace forcefield

} // namespace Components

} // namespace Sofa

