#pragma once

#include "ContractionForceField.h"

#include <SofaBaseMechanics/MechanicalObject.h>
#include <sofa/core/ObjectFactory.h>
#include <fstream>
#include <iostream>
#include <sofa/core/behavior/ForceField.inl>
#include <SofaBaseTopology/TopologyData.inl>
#include <algorithm>
#include <iterator>
#include <sofa/helper/AdvancedTimer.h>
#include <sofa/simulation/Node.h>
#include <sofa/simulation/Simulation.h>
#include <sofa/core/visual/VisualParams.h>


namespace sofa
{
	namespace component
	{
		namespace forcefield
		{
			using namespace sofa::type;
			using namespace	sofa::component::topology;
			using namespace core::topology;


			template< class DataTypes>
                        void ContractionForceField<DataTypes>::ContractionTetrahedronHandler::applyCreateFunction(unsigned int tetrahedronIndex,
				TetrahedronRestInformation &tinfo,const Tetrahedron& , const type::vector< unsigned int > &, 
				const type::vector< double >&)
			{
				if (ff) {
					const vector< Tetrahedron > &tetrahedronArray=ff->_topology->getTetrahedra() ;
                    unsigned int j,k,l;

                    const typename DataTypes::VecCoord restPosition=ff->mstate->read(core::ConstVecCoordId::restPosition())->getValue();

					///describe the indices of the 4 tetrahedron vertices  
					const Tetrahedron &t= tetrahedronArray[tetrahedronIndex];
					BaseMeshTopology::EdgesInTetrahedron te=ff->_topology->getEdgesInTetrahedron(tetrahedronIndex);

                    //store point indices
                    tinfo.tetraIndices[0] = (float)t[0];
                    tinfo.tetraIndices[1] = (float)t[1];
                    tinfo.tetraIndices[2] = (float)t[2];
                    tinfo.tetraIndices[3] = (float)t[3];

                    //store parameters
                    tinfo.f_contractivityParams = ff->getContractivityParameters(tetrahedronIndex);

					if( !ff->useCoupling.getValue() )
					{
						tinfo.f_depolarisationTimes[0] = ff->f_depolarisationTimes.getValue()[t[0]];
						tinfo.f_depolarisationTimes[1] = ff->f_depolarisationTimes.getValue()[t[1]];
						tinfo.f_depolarisationTimes[2] = ff->f_depolarisationTimes.getValue()[t[2]];
						tinfo.f_depolarisationTimes[3] = ff->f_depolarisationTimes.getValue()[t[3]];

						tinfo.f_APD[0] = ff->f_APD.getValue()[t[0]];
						tinfo.f_APD[1] = ff->f_APD.getValue()[t[1]];
						tinfo.f_APD[2] = ff->f_APD.getValue()[t[2]];
						tinfo.f_APD[3] = ff->f_APD.getValue()[t[3]];

					}
					// store the point position
					Coord pointP[4];
					
					for(j=0;j<4;++j)
                        pointP[j]=(restPosition)[t[j]];
			
					tinfo.volume=dot(cross(pointP[2]-pointP[0],pointP[3]-pointP[0]),pointP[1]-pointP[0]);
                    tinfo.SigmaC=0;
                    tinfo.oldEc=0;

                    //stess calc
                    tinfo.CauchyStress_n=0;
                    tinfo.J=0;
                    for (k=0;k<3;++k) {
                        for (l=k;l<3;++l) {
                            tinfo.SecSPK_p(k,l)=0;
                        }
                    }

					for(j=0;j<4;++j) {

						if (!(j%2))
							tinfo.shapeVector[j]=(-cross(pointP[(j+2)%4] - pointP[(j+1)%4],pointP[(j+3)%4] -pointP[(j+1)%4]))/tinfo.volume;
						else 
							tinfo.shapeVector[j]=(cross(pointP[(j+2)%4] - pointP[(j+1)%4],pointP[(j+3)%4] - pointP[(j+1)%4]))/tinfo.volume;
					}

					tinfo.fiberDirections=ff->getFiberDirections(tetrahedronIndex);
                                        tinfo.contractivity=(ff->getContractivityParameters(tetrahedronIndex))[0];
                                        tinfo.relaxationRate=(ff->getContractivityParameters(tetrahedronIndex))[2];
                                        tinfo.contractionRate=(ff->getContractivityParameters(tetrahedronIndex))[1];
                                        tinfo.tauC=0;
                                        tinfo.tauCana=0;
                                        tinfo.Ec=0;

										
				}//end if(ff)

			}

			template<class DataTypes>
            Vec<3,double> ContractionForceField<DataTypes>::getFiberDirections(const unsigned int tetraIndex)
			{
                        Vec<3,double> fibre;
                        unsigned int fi=(f_fiberDirections.getValue()).size();


                        if(fi==1){
                            fibre=(f_fiberDirections.getValue())[0];

                        }
                        else if (tetraIndex < fi)
                        fibre = (f_fiberDirections.getValue())[tetraIndex];
			else
                        std::cout << "Error, index of tetrahedron is out of bound of fibers container. returning null vector." << std::endl;

                        return fibre;
			}


			template<class DataTypes>
                        Vec<3,double> ContractionForceField<DataTypes>::getContractivityParameters(const unsigned int tetraIndex)
			{
                        Vec<3,double> param;
                        unsigned int si=(f_tetraContractivityParam.getValue()).size();


			if(si==1){
                           param = (f_tetraContractivityParam.getValue())[0];

			}

			else if (tetraIndex < si)

                            param = (f_tetraContractivityParam.getValue())[tetraIndex];

			else
			std::cout << "Error, index of tetrahedron is out of bound of Contractivity container. returning null value." << std::endl;

                        return param;
			}


            template <class DataTypes> ContractionForceField<DataTypes>::ContractionForceField()
				: _topology(0)
				, _initialPoints(0)
				, updateMatrix(true)
                , m_tagMeshCoupling(initData(&m_tagMeshCoupling,std::string("tagContraction"),"tagContraction","Tag of the contraction node"))
                , m_tagMeshSolver(initData(&m_tagMeshSolver, std::string("solver"),"tagSolver","Tag of the Solver Object"))
                , DesactivateElastoIfNoElec(initData(&DesactivateElastoIfNoElec, (bool)0, "DesactivateElastoIfNoElec", "set to 1 for calculating the cauchy stress"))
                , useCoupling(initData(&useCoupling,"useCoupling","if the contraction is couple"))
				, f_heartPeriod(initData(&f_heartPeriod,"heartPeriod","heart Period"))
                , f_tetraContractivityParam(initData(&f_tetraContractivityParam,"tetraContractivityParam","<Contractivity,contractionRate,relaxationrate> by tetra"))
				, f_fiberDirections(initData(&f_fiberDirections,"fiberDirections", " file with fibers at each tetra"))
                , f_addElastometry(initData(&f_addElastometry,"addElastometry","If we want the elastic component in series"))
                , f_elasticModulus(initData(&f_elasticModulus,"elasticModulus","modulus for the elastic component in series"))
                , f_viscosityParam(initData(&f_viscosityParam,"viscosityParameter","for passive relaxation"))
                , _tetraPloted (initData(&_tetraPloted, (unsigned int)0, "tetraPloted", "tetra index of values display in graph for each iteration."))
                , f_graph( initData(&f_graph,"graph","Vertex state value per iteration") )
                , f_graph2( initData(&f_graph2,"graph2","Vertex state value per iteration") )
                , f_depolarisationTimes(initData(&f_depolarisationTimes,"depolarisationTimes","depolarisationTimes at each node"))
                , f_APD(initData(&f_APD,"APD","APD at each node"))
                , fileName2(initData(&fileName2,"file","File where to store the Monitoring"))
                , useVerdandi(initData(&useVerdandi,(bool)0,"useVerdandi","useVerdandi"))
                , f_calculateStress(initData(&f_calculateStress, (bool)0, "calculateStress", "set to 1 for calculating the cauchy stress"))
                , f_stressFile(initData(&f_stressFile,  std::string("CauchyStress.txt"),"stressFile", "name of the output cauchy stress file"))
                , f_2ndSPK_p(initData(&f_2ndSPK_p,  "SecSPK_passive", "second piola kirshoff tensor of passive part"))
				, tetrahedronInfo(initData(&tetrahedronInfo,"tetrahedronInfo","Data to handle topology on tetrahedra"))
				, edgeInfo(initData(&edgeInfo,"edgeInfo","Data to handle topology on edges"))
                , m_mean_stress(initData(&m_mean_stress, "mean_stress", "Mean active stress to use in phase computation"))
                {
                    f_graph.setWidget("graph");
                    f_graph2.setWidget("graph");
                    tetrahedronHandler = new ContractionTetrahedronHandler(this, &tetrahedronInfo);

                    // Activates the "listening" capabilities of handle events
                    this->f_listening.setValue(true);
                }

            template <class DataTypes> void ContractionForceField<DataTypes>::handleTopologyChange()
			{

			}

            template <class DataTypes> ContractionForceField<DataTypes>::~ContractionForceField()
            {
                Jac1.close();
                Jac2.close();
                Jac3.close();
                Jac4.close();
                Jac5.close();
                Jac6.close();
                if(tetrahedronHandler) delete tetrahedronHandler;

                /// stress calc
                if(f_calculateStress.getValue()){
                    StressStream.close();
                }
			}


            template <class DataTypes> void ContractionForceField<DataTypes>::init()
            {
                cerr << "initializing ContractionForceField" <<endl;

                /// stress calc
                if(f_calculateStress.getValue()){
                    if(StressStream.is_open()){
                        StressStream.close();
                    }
                    StressStream.open((f_stressFile.getValue()).c_str(),std::ios::out);
                }
                this->Inherited::init();
                _topology = this->getContext()->getMeshTopology();

                if (!_topology->getNbTetrahedra())
                {
                    cerr << "ERROR(ContractionForceField): object must have a Tetrahedral Set Topology.\n";
                    return;
                }

                /// prepare to store info in the triangle array
                edgeInfo.createTopologyHandler(_topology);
//                 edgeInfo.registerTopologicalData();

                tetrahedronInfo.createTopologyHandler(_topology);
//                 tetrahedronInfo.registerTopologicalData();

                tetrahedronRestInfoVector& tetrahedronInf = *(tetrahedronInfo.beginEdit());
                tetrahedronInf.resize(_topology->getNbTetrahedra());
                edgeInfoVector& edgeInf = *(edgeInfo.beginEdit());

                edgeInf.resize(_topology->getNbEdges());
                edgeInfo.endEdit();

                // get restPosition
                if (_initialPoints.size() == 0)
                {
                    const VecCoord& p = this->mstate->read(core::ConstVecCoordId::restPosition())->getValue();
                    _initialPoints=p;
                }
                int i;

                /// initialize the data structure associate with each tetrahedron
                for (int i=0; i<_topology->getNbEdges(); i++)
                {
                    edgeInf[i].vertices[0] = (float) _topology->getEdge(i)[0];
                    edgeInf[i].vertices[1] = (float) _topology->getEdge(i)[1];
                }
                /// initialize the data structure associated with each tetrahedron
                for (i=0;i<_topology->getNbTetrahedra();++i)
                {
                    tetrahedronHandler->applyCreateFunction(i, tetrahedronInf[i],
                        _topology->getTetrahedron(i),  (const vector< unsigned int > )0,
                        (const vector< double >)0);	

                }


                if(useCoupling.getValue()){
                    const std::string u=m_tagMeshCoupling.getValue();
                    sofa::core::objectmodel::Tag couplingTag(u);
                    this->getContext()->get(mechanicalObject, couplingTag,sofa::core::objectmodel::BaseContext::SearchRoot);
                    if (mechanicalObject==NULL) {
                        serr << "ERROR(ContractionForceField): cannot find the mechanicalObject ."<<sendl;
                        return;
                    }
                }

                heartPeriod=f_heartPeriod.getValue();
                if(f_addElastometry.getValue()) elasticMod=f_elasticModulus.getValue();
                else elasticMod=1e20;

                if(f_calculateStress.getValue()){
                    std::cout << "(ContractionForceField): calculating Cauchy stress." <<endl;
                    unsigned int nbStress=(f_2ndSPK_p.getValue()).size();
                    if(nbStress<2){
                        serr << "ERROR(ContractionForceField): cannot find second Piola Kirshoff stress from MRForceField ."<<sendl;
                        return;
                    }
                }


                tetrahedronInfo.endEdit();

                if(fileName2.getValue()!=""){
                    Jac1.open((fileName2.getValue()+"A").c_str(),std::ios::out);
                    Jac2.open((fileName2.getValue()+"B").c_str(),std::ios::out);
                    Jac3.open((fileName2.getValue()+"C").c_str(),std::ios::out);
                    Jac4.open((fileName2.getValue()+"D").c_str(),std::ios::out);
                    Jac5.open((fileName2.getValue()+"E").c_str(),std::ios::out);
                    Jac6.open((fileName2.getValue()+"F").c_str(),std::ios::out);}
                else{
                    Jac1.open("Log11.txt",std::ios::out);
                    Jac2.open("Log22.txt",std::ios::out);
                    Jac3.open("Log33.txt",std::ios::out);
                    Jac4.open("Log44.txt",std::ios::out);
                    Jac5.open("Log55.txt",std::ios::out);
                    Jac6.open("Log66.txt",std::ios::out);
                }

                //getVolume(0);
             //   getdEpsilonC(0);

                /// FOR CUDA
                /// Save the neighbourhood for points (in case of CudaTypes and non atomic)
                this->initTetraNeighbourhoodPoints();
                this->initEdgeNeighbourhoodPoints();
                this->initNeighbourhoodEdges();


            }

            template <class DataTypes>
            void ContractionForceField<DataTypes>::initTetraNeighbourhoodPoints(){}

            template <class DataTypes>
            void ContractionForceField<DataTypes>::initEdgeNeighbourhoodPoints(){}

            template <class DataTypes>
            void ContractionForceField<DataTypes>::initNeighbourhoodEdges(){}

            template <class DataTypes>
            void ContractionForceField<DataTypes>::reinit()
            {
                if (_tetraPloted.isDirty())
                    this->resetGraph();

                // stress calc
                if(f_calculateStress.getValue()){
                    if(StressStream.is_open()){
                        StressStream.close();
                    }
                    StressStream.open((f_stressFile.getValue()).c_str(),std::ios::out);
                }

            }

			template <class DataTypes> 
            void ContractionForceField<DataTypes>::reset()
            {
				init();
			}


			template <class DataTypes> 
            SReal ContractionForceField<DataTypes>::getPotentialEnergy(const core::MechanicalParams*, const DataVecCoord&) const
			{
                   unsigned int i=0/*,k=0,l=0*/;

                   unsigned int nbTetrahedra=_topology->getNbTetrahedra();
                   // type::vector<TetrahedronRestInformation>& tetrahedronInf = *(tetrahedronInfo.beginEdit());
                   double simuTime = this->getContext()->getTime();
                   double dt=this->getContext()->getDt();
                   const tetrahedronRestInfoVector& tetrahedronInf = tetrahedronInfo.getValue();
                   const TetrahedronRestInformation *tetInfo;
                   double EnergyElastic=0;
                   double UC=0;
                   double viscous=0;
                   double scec=0;
                   double maxes=0;double ecc=0;
                   int tet=0;
                   //const Tetrahedron &ta= _topology->getTetrahedron(i);
                   for (i=0; i<nbTetrahedra; ++i){
                       tetInfo=&tetrahedronInf[i];
                       Real E1d=tetInfo->oldEff;

                       //if(maxes<(E1d)){maxes=(E1d);tet=i;}
                       double dec=(tetInfo->Ec-tetInfo->oldEc)/dt;
                       if(maxes<(E1d)){maxes=(E1d);tet=i;ecc=dec;}
                       if(useCoupling.getValue())
                       {
                           UC+=tetInfo->Uc*tetInfo->volume/6.0;
                            scec+=tetInfo->SigmaC*dec*tetInfo->volume/6.0;

                       }


                       if(f_addElastometry.getValue()){
                           viscous-=f_viscosityParam.getValue()*(dec*dec)*tetInfo->volume/6.0;
                           EnergyElastic+=elasticMod*(E1d-tetInfo->Ec)*(E1d-tetInfo->Ec)/12.0*tetInfo->volume;


                       }

                       if(!useCoupling.getValue())
                       {

                           double uo=0;
                           double energy=0;
                            double Tr=tetInfo->Td+tetInfo->APD;
                           if((simuTime-dt>=tetInfo->Td)&&(simuTime-dt<Tr)){
                               //sig=tetInfo->contractivity*(1-exp(tetInfo->contractionRate*(Td-simuTime)));
                               uo=tetInfo->contractionRate*tetInfo->contractivity*tetInfo->volume/6.0;
                               energy=-tetInfo->SigmaC*tetInfo->contractionRate*tetInfo->volume/6.0;

                           }
                           else if ((simuTime-dt>=Tr)){
                              // sig=tetInfo->contractivity*(1-exp(tetInfo->contractionRate*(-APD)))*exp(tetInfo->relaxationRate*(Tr-simuTime));
                               energy=-tetInfo->SigmaC*tetInfo->relaxationRate*tetInfo->volume/6.0;

                           }

                           EnergyElastic+=energy*E1d;
                           UC+=tetInfo->SigmaC*E1d*tetInfo->volume/6.0;
                           scec+=tetInfo->SigmaC*dec*tetInfo->volume/6.0;
                           viscous+=uo*E1d;
                       }
                   }


                //    return EnergyElastic+UC+viscous;
				return 0;
			}


            template <class DataTypes>
            sofa::type::vector<double> ContractionForceField<DataTypes>::get_Energies(sofa::type::vector <unsigned int> TetrasIndices){

                sofa::type::vector<double> powers;
                powers.resize(2);
                powers[0]=0;//Energy Es
                powers[1]=0;

                const tetrahedronRestInfoVector& tetrahedronInf = tetrahedronInfo.getValue();
                const TetrahedronRestInformation *tetInfo;


                for (unsigned int v = 0; v<TetrasIndices.size(); v++ ){

                    tetInfo=&tetrahedronInf[TetrasIndices[v]];
                    Real volume=tetInfo->volume/6.0;
                    Real E1d=tetInfo->oldEff;
                    double dec=(tetInfo->Ec-tetInfo->oldEc)/dt;

                    powers[1]-=f_viscosityParam.getValue()*(dec*dec)*volume;
                    powers[0]+=elasticMod*(E1d-tetInfo->Ec)*(E1d-tetInfo->Ec)/2.0*volume;
                }

                return powers;
            }



			template <class DataTypes> 
            void ContractionForceField<DataTypes>::addForce(const core::MechanicalParams* /* PARAMS FIRST */, DataVecDeriv& d_f, const DataVecCoord& d_x, const DataVecDeriv& )
			{
                sofa::helper::AdvancedTimer::stepBegin("addForceContractionFF");
                dt = this->getContext()->getDt();
                unsigned int i=0, j=0, l=0, k=0;
                Coord dp[3], dq[3], x0, sv;

                unsigned int nbTetrahedra = _topology->getNbTetrahedra();
                tetrahedronRestInfoVector& tetrahedronInf = *(tetrahedronInfo.beginEdit());
                TetrahedronRestInformation *tetInfo;
                assert(this->mstate);
                double simuTime = this->getContext()->getTime();
                VecDeriv &f = *(d_f.beginEdit());
                VecCoord x = d_x.getValue();

                VecCoord tauk;
                if(useCoupling.getValue())
                {
                    tauk = mechanicalObject->read(core::ConstVecCoordId::position())->getValue();
                }


                for (i=0; i<nbTetrahedra; ++i){
                    tetInfo=&tetrahedronInf[i];
                    const Tetrahedron &ta= _topology->getTetrahedron(i);
                    x0=x[ta[0]];

                    if(useVerdandi.getValue()){
                        tetInfo->contractivity=(getContractivityParameters(i))[0];
                        tetInfo->relaxationRate=(getContractivityParameters(i))[2];
                        tetInfo->contractionRate=(getContractivityParameters(i))[1];
                    }

                    dp[0]=x[ta[1]]-x0;

                    sv=tetInfo->shapeVector[1];
                    tetInfo->SPK.clear();
                    for (k=0;k<3;++k)
                        for (l=0;l<3;++l)
                            tetInfo->deformationGradient[k][l]=dp[0][k]*sv[l];

                    for (j=1;j<3;++j)
                    {
                        dp[j]=x[ta[j+1]]-x0;
                        sv=tetInfo->shapeVector[j+1];
                        for (k=0;k<3;++k)
                            for (l=0;l<3;++l)
                                tetInfo->deformationGradient[k][l]+=dp[j][k]*sv[l];
                    }

                    // stress calc
                    Coord areaVec = cross( dp[1], dp[2] );
                    tetInfo->J = dot( areaVec, dp[0] ) * (1.0/tetInfo->volume);

                    /// compute the Green-Lagrange strain rate
                    MatrixSym StrainMat;
                    for (k=0; k<3; ++k) {
                        for (l=k; l<3; ++l) {
                            StrainMat(k,l) = (tetInfo->deformationGradient(0,k)*tetInfo->deformationGradient(0,l)+
                                                tetInfo->deformationGradient(1,k)*tetInfo->deformationGradient(1,l)+
                                                tetInfo->deformationGradient(2,k)*tetInfo->deformationGradient(2,l))/2;
                            if (l==k) StrainMat(k,l) -= 0.5;
                        }
                    }

                    /// Projection of the Green-Lagrange deformation tensor E (StrainMat) on the fibre direction
                    Real E1d = dot(tetInfo->fiberDirections, StrainMat*tetInfo->fiberDirections);

                    if(simuTime == 0){
                        tetInfo->Ec=E1d;
                        tetInfo->oldEc=E1d;
                    }


                    for(k=0; k<3; k++)
                        for(j=0; j<3; j++)
                            tetInfo->contractionTensor[k][j] = tetInfo->fiberDirections[k]*tetInfo->fiberDirections[j];

                    Real tau=0, kk=0, uu=0;
                    if(useCoupling.getValue()){
                        Vec3 tt=tauk[i];
                        tau=tt[0];
                        kk=tt[1];
                        uu=tt[2];
                    }


                    tetInfo->tauCana = 0;
                    tetInfo->tauC = 0;
                    tetInfo->APD = 0;
                    tetInfo->Td = 0;

                    if(!useCoupling.getValue()){
                        for (int u=0; u<4; u++){
                            ///value at the barycenter
                            tetInfo->APD += (f_APD.getValue())[ta[u]]/4.0;
                            tetInfo->Td += (f_depolarisationTimes.getValue())[ta[u]]/4.0;
                        }
                        double Tr=tetInfo->Td+tetInfo->APD;

                        if((simuTime >= tetInfo->Td) && (simuTime < Tr)){
                            tetInfo->tauCana += tetInfo->contractivity*(1-exp(tetInfo->contractionRate*(tetInfo->Td-simuTime)));

                        }
                        else if((simuTime >= Tr) && (simuTime < tetInfo->Td + heartPeriod)){
                            tetInfo->tauCana += tetInfo->contractivity*(1-exp(tetInfo->contractionRate*(-tetInfo->APD)))*exp(tetInfo->relaxationRate*(Tr-simuTime));
                        }
                        else tetInfo->tauCana += 0;

                        tetInfo->tauC = tetInfo->tauCana;
                    }

                    if(useCoupling.getValue()) {
                        tetInfo->tauC = tau;
                        tetInfo->k = kk;
                        tetInfo->Uc = uu;
                    }

                    Real newEc;
                    if (!f_addElastometry.getValue()) {
                        newEc = E1d;
                        tetInfo->SigmaC = tetInfo->tauC;
                    }
                    else {
                        // Contraction strain
                        newEc = tetInfo->Ec*(1.0/dt-f_elasticModulus.getValue()/f_viscosityParam.getValue())*dt + f_elasticModulus.getValue()*dt/f_viscosityParam.getValue()*E1d - tetInfo->tauC*dt/f_viscosityParam.getValue();
                        // Contraction stress
                        tetInfo->SigmaC = f_elasticModulus.getValue()*(E1d - tetInfo->Ec);
                    }


                    if(i==5000)
                        Jac1 << tetInfo->SigmaC << " " << E1d << " " << tetInfo->Ec << " " << tetInfo->tauC << " " << tetInfo->Uc << " " <<  endl;
                    if(i==15000)
                        Jac2 << tetInfo->SigmaC << " " << E1d << " " << tetInfo->Ec << " " << tetInfo->tauC << " " << tetInfo->Uc << " " <<  endl;
                    if(i==25000)
                        Jac3 << tetInfo->SigmaC << " " << E1d << " " << tetInfo->Ec << " " << tetInfo->tauC << " " << tetInfo->Uc << " " <<  endl;
                    if(i==35000)
                        Jac4 << tetInfo->SigmaC << " " << E1d << " " << tetInfo->Ec << " " << tetInfo->tauC << " " << tetInfo->Uc << " " <<  endl;
                    if(i==45000)
                        Jac5 << tetInfo->SigmaC << " " << E1d << " " << tetInfo->Ec << " " << tetInfo->tauC << " " << tetInfo->Uc << " " <<  endl;
                    if(i==55000)
                        Jac6 << tetInfo->SigmaC << " " << E1d << " " << tetInfo->Ec << " " << tetInfo->tauC << " " << tetInfo->Uc << " " <<  endl;


                    tetInfo->oldEc = tetInfo->Ec;
                    tetInfo->Ec = newEc;
                    tetInfo->oldEff = E1d;
                    tetInfo->SPK = tetInfo->contractionTensor*(tetInfo->SigmaC)/4.0;  // for each node of the tetra
                    for(l=0; l<4; ++l)
                        f[ta[l]] -= tetInfo->deformationGradient*tetInfo->SPK*tetInfo->shapeVector[l]*tetInfo->volume/6.0;




                }//end of i

              //  this->updateGraph(_tetraPloted.getValue());
                updateMatrix=true;
                tetrahedronInfo.endEdit();
                d_f.endEdit();
                sofa::helper::AdvancedTimer::stepEnd("addForceContractionFF");
			}




            template <class DataTypes>
            double ContractionForceField<DataTypes>::getdEpsilonC(const unsigned int tetraIndex)
            {
                tetrahedronRestInfoVector& tetrahedronInf = *(tetrahedronInfo.beginEdit());
                TetrahedronRestInformation *tetInfo;
                tetInfo=&tetrahedronInf[tetraIndex];
                return (tetInfo->Ec-tetInfo->oldEc)/dt;
                tetrahedronInfo.endEdit();

            }

            template <class DataTypes>
            void ContractionForceField<DataTypes>::setECold(const unsigned int tetraIndex,double value)
            {
                tetrahedronRestInfoVector& tetrahedronInf = *(tetrahedronInfo.beginEdit());
                TetrahedronRestInformation *tetInfo;
                tetInfo=&tetrahedronInf[tetraIndex];
                tetInfo->oldEc=value;
                tetrahedronInfo.endEdit();

            }
            template <class DataTypes>
            double ContractionForceField<DataTypes>::getECold(const unsigned int tetraIndex)
            {
                tetrahedronRestInfoVector& tetrahedronInf = *(tetrahedronInfo.beginEdit());
                TetrahedronRestInformation *tetInfo;
                tetInfo=&tetrahedronInf[tetraIndex];
                return tetInfo->oldEc;
                tetrahedronInfo.endEdit();

            }

            template <class DataTypes>
            double ContractionForceField<DataTypes>::getVolume(const unsigned int tetraIndex)
            {
                tetrahedronRestInfoVector& tetrahedronInf = *(tetrahedronInfo.beginEdit());
                TetrahedronRestInformation *tetInfo;
                tetInfo=&tetrahedronInf[tetraIndex];
                return tetInfo->volume;
                tetrahedronInfo.endEdit();

            }

            template <class DataTypes>
            double ContractionForceField<DataTypes>::getEC(const unsigned int tetraIndex)
            {
                tetrahedronRestInfoVector& tetrahedronInf = *(tetrahedronInfo.beginEdit());
                TetrahedronRestInformation *tetInfo;
                tetInfo=&tetrahedronInf[tetraIndex];
                return tetInfo->Ec;

                tetrahedronInfo.endEdit();
            }

            template <class DataTypes>
            double ContractionForceField<DataTypes>::getSigmaC(const unsigned int tetraIndex)
            {
                tetrahedronRestInfoVector& tetrahedronInf = *(tetrahedronInfo.beginEdit());
                TetrahedronRestInformation *tetInfo;
                tetInfo=&tetrahedronInf[tetraIndex];
                return tetInfo->SigmaC;

                tetrahedronInfo.endEdit();
            }

            template <class DataTypes>
            double ContractionForceField<DataTypes>::getE1d(const unsigned int tetraIndex)
            {
                tetrahedronRestInfoVector& tetrahedronInf = *(tetrahedronInfo.beginEdit());
                TetrahedronRestInformation *tetInfo;
                tetInfo=&tetrahedronInf[tetraIndex];
                return tetInfo->oldEff;
                tetrahedronInfo.endEdit();

            }

            template <class DataTypes>
            void ContractionForceField<DataTypes>::setEC(const unsigned int tetraIndex,double value)
            {
                tetrahedronRestInfoVector& tetrahedronInf = *(tetrahedronInfo.beginEdit());
                TetrahedronRestInformation *tetInfo;
                tetInfo=&tetrahedronInf[tetraIndex];
                tetInfo->Ec=value;
                tetrahedronInfo.endEdit();

            }


            template <class DataTypes>
            void ContractionForceField<DataTypes>::updateGraph(int tetraIndex)
            {

                tetrahedronRestInfoVector& tetrahedronInf = *(tetrahedronInfo.beginEdit());
                                TetrahedronRestInformation *tetInfo;
                                tetInfo=&tetrahedronInf[tetraIndex];

                                std::map < std::string, sofa::type::vector<double> >& graph = *(f_graph.beginEdit());
                                sofa::type::vector<double>& graph_e1d = graph["E1d"];
                                sofa::type::vector<double>& graph_ec = graph["Ec"];
                                sofa::type::vector<double>& graph_es = graph["Es"];

                                graph_e1d.push_back(tetInfo->oldEff+1);//+1 pour un meilleur affichage
                                graph_es.push_back(tetInfo->SigmaC/f_elasticModulus.getValue() +1);//+1 pour un meilleur affichage
                                graph_ec.push_back(tetInfo->oldEc+1);

                                f_graph.endEdit();
                                std::map < std::string, sofa::type::vector<double> >& graph2 = *(f_graph2.beginEdit());
                                if (useCoupling.getValue()) {
                                    sofa::type::vector<double>& graph_tau = graph2["tausol"];

                                    sofa::type::vector<double>& graph_sig = graph2["sigma"];
                                    sofa::type::vector<double>& graph_k = graph2["kc"];
                                    sofa::type::vector<double>& graph_U = graph2["Uc"];
                                    graph_tau.push_back(tetInfo->tauC/tetInfo->contractivity+1);
                                    //graph_sig.push_back(tetInfo->SigmaC/tetInfo->contractivity+1);
                                    graph_sig.push_back(((tetInfo->tauC*tetInfo->tauC)/(2.0*tetInfo->k))/tetInfo->contractivity+1);
                                    graph_k.push_back(tetInfo->k/tetInfo->contractivity+1);
                                    graph_U.push_back(tetInfo->Uc/tetInfo->contractivity+1);
                                }
                                else {

                                sofa::type::vector<double>& graph_sig = graph2["sigma"];
                                graph_sig.push_back(tetInfo->SigmaC/tetInfo->contractivity+1);
                                sofa::type::vector<double>& graph_ana = graph2["tauana"];
                                graph_ana.push_back(tetInfo->tauCana/tetInfo->contractivity+1);
                            }
                                f_graph2.endEdit();
                                tetrahedronInfo.endEdit();
                                /*std::map < std::string, sofa::type::vector<double> >& graph3 = *(f_graph3.beginEdit());
                                            sofa::type::vector<double>& graph_Energy = graph3["Energy"];
                                            graph_Energy.push_back(EnergyTotale+1);
                                                f_graph3.endEdit();*/
                                //  std::cout<<EnergyTotale<<std::endl;

            }

            template <class DataTypes>
            void ContractionForceField<DataTypes>::resetGraph()
            {
               std::map < std::string, sofa::type::vector<double> >& graph = *f_graph.beginEdit();
               sofa::type::vector<double> graph_e1d = graph[std::string("E1d")];
               sofa::type::vector<double> graph_ec = graph[std::string("Ec")];

               graph_e1d.clear();
              graph_ec.clear();


              f_graph.endEdit();
            }

			template <class DataTypes> 
                                void ContractionForceField<DataTypes>::updateMatrixData()
			{
							sofa::helper::AdvancedTimer::stepBegin("updateMatrixDataContractionFF");
                            unsigned int i=0,j=0,k=0,l=0;
                                            unsigned int nbEdges=_topology->getNbEdges();
                                            const vector< Edge> &edgeArray=_topology->getEdges() ;

                                            edgeInfoVector& edgeInf = *(edgeInfo.beginEdit());
                                            tetrahedronRestInfoVector& tetrahedronInf = *(tetrahedronInfo.beginEdit());

                                            EdgeInformation *einfo;


                                            /// if the  matrix needs to be updated
                                            if (updateMatrix) {

                                                TetrahedronRestInformation *tetInfo;
                                                unsigned int nbTetrahedra=_topology->getNbTetrahedra();
                                                const std::vector< Tetrahedron> &tetrahedronArray=_topology->getTetrahedra() ;



                                                Matrix3 M,N;
                                                N.clear();

                                                for(l=0; l<nbEdges; l++ ) {
                                                    edgeInf[l].DfDx.clear();

                                                }
                                                for(i=0; i<nbTetrahedra; i++ )
                                                {

                                                    tetInfo=&tetrahedronInf[i];
                                                    BaseMeshTopology::EdgesInTetrahedron te=_topology->getEdgesInTetrahedron(i);

                                                if(useVerdandi.getValue()){
                                                    tetInfo->contractivity=(getContractivityParameters(i))[0];
                                                    tetInfo->relaxationRate=(getContractivityParameters(i))[2];
                                                    tetInfo->contractionRate=(getContractivityParameters(i))[1];
                                                }

                                                    /// describe the jth vertex index of triangle no i
                                                    const Tetrahedron &ta= tetrahedronArray[i];

                                                    for(j=0;j<6;j++) {
                                                        einfo= &edgeInf[te[j]];
                                                        Edge e=_topology->getLocalEdgesInTetrahedron(j);

                                                        k=e[0];
                                                        l=e[1];
                                                        if (edgeArray[te[j]][0]!=ta[k]) {
                                                            k=e[1];
                                                            l=e[0];
                                                        }
                                                        Matrix3 &edgeDfDx = einfo->DfDx;

                                                        Coord svl=tetInfo->shapeVector[l];
                                                        Coord svk=tetInfo->shapeVector[k];


                                                        Real productDSD;
                                                        Coord vectSD=tetInfo->SPK*svk;
                                                        productDSD=dot(vectSD,svl);
                                                        M[0][0]=M[1][1]=M[2][2]=(Real)productDSD;
                                                        Real Djf=dot(svl,tetInfo->fiberDirections)*f_elasticModulus.getValue()/4.0;

                                                        if(DesactivateElastoIfNoElec.getValue()){
                                                            if(tetInfo->Uc<1e4){
                                                                Djf=0;
                                                            }
                                                        }

                                                        if(f_addElastometry.getValue()){

                                                            Matrix3 fDi;
                                                            Coord phif;
                                                            phif=tetInfo->deformationGradient*tetInfo->fiberDirections;

                                                            for(int a=0;a<3;a++){
                                                                for(int b=0;b<3;b++){
                                                                    fDi[a][b]=phif[a]*svk[b];
                                                                }
                                                            }


                                                            N=(fDi*tetInfo->contractionTensor);
                                                            N=N*(tetInfo->deformationGradient.transposed());
                                                        }

                                                        edgeDfDx+= (N*Djf+M)*tetInfo->volume/6.0;

                                                    }

                                                }//end of for i
                                                updateMatrix=false;
                                            }// end of if

							sofa::helper::AdvancedTimer::stepEnd("updateMatrixDataContractionFF");

                            }

                        template<class DataTypes>
                                    void ContractionForceField<DataTypes>::addDForce (const core::MechanicalParams* mparams /* PARAMS FIRST */, DataVecDeriv& d_df, const DataVecDeriv& d_dx){
							sofa::helper::AdvancedTimer::stepBegin("addDForceContractionFF");

                                        unsigned int l=0;
                                        unsigned int nbEdges=_topology->getNbEdges();
                                        const vector< Edge> &edgeArray=_topology->getEdges() ;

                                        edgeInfoVector& edgeInf = *(edgeInfo.beginEdit());

                                        EdgeInformation *einfo;
                                        this->updateMatrixData();
                                        /// performs matrix vector computation
                                        unsigned int v0,v1;
                                        Deriv deltax;	Deriv dv0,dv1;
                                        const VecDeriv dx=d_dx.getValue();
                                        VecDeriv& df=*(d_df.beginEdit());
                                        double kfactor = mparams->kFactor();

                                        for(l=0; l<nbEdges; l++ )
                                        {
                                            einfo=&edgeInf[l];
                                            v0=edgeArray[l][0];
                                            v1=edgeArray[l][1];

                                            deltax= dx[v0] - dx[v1];
                                            dv0 = einfo->DfDx * deltax*kfactor;
                                               dv1[0] = (Real)(deltax[0]*einfo->DfDx[0][0] + deltax[1]*einfo->DfDx[1][0] + deltax[2]*einfo->DfDx[2][0])*kfactor;
                                            dv1[1] = (Real)(deltax[0]*einfo->DfDx[0][1] + deltax[1]*einfo->DfDx[1][1] + deltax[2]*einfo->DfDx[2][1])*kfactor;
                                            dv1[2] = (Real)(deltax[0]*einfo->DfDx[0][2] + deltax[1]*einfo->DfDx[1][2] + deltax[2]*einfo->DfDx[2][2])*kfactor;
                                            // add forces
                                            df[v0] += dv1;
                                            df[v1] -= dv0;

                                        }
                                        d_df.endEdit();
                                        edgeInfo.endEdit();
                                        //  tetrahedronInfo.endEdit();

							sofa::helper::AdvancedTimer::stepEnd("addDForceContractionFF");


                                    }

                                    template <class DataTypes>
                                            void ContractionForceField<DataTypes>::addKToMatrix(linearalgebra::BaseMatrix *m, SReal kFactor, unsigned int &offset) {
                                        unsigned int l=0;
                                        unsigned int nbEdges=_topology->getNbEdges();
                                        const vector< Edge> &edgeArray=_topology->getEdges();

                                        edgeInfoVector& edgeInf = *(edgeInfo.beginEdit());
                                        EdgeInformation *einfo;

                                        this->updateMatrixData();

                                        /// performs matrix vector computation
                                        unsigned int v0,v1;

                                        for(l=0; l<nbEdges; l++ )
                                        {
                                            einfo=&edgeInf[l];
                                            v0=offset + edgeArray[l][0]*3;
                                            v1=offset + edgeArray[l][1]*3;

                                            for (int L=0;L<3;L++) {
                                                for (int C=0;C<3;C++) {
                                                    double v = einfo->DfDx[L][C] * kFactor;
                                                    m->add(v0+C,v0+L, v);
                                                    m->add(v0+C,v1+L,-v);
                                                    m->add(v1+L,v0+C,-v);
                                                    m->add(v1+L,v1+C, v);
                                                }
                                            }
                                        }

                                        edgeInfo.endEdit();
                                    }


                                    template<class DataTypes>
                                    void ContractionForceField<DataTypes>::draw(const core::visual::VisualParams* vparams)
                                    {
                                            //	unsigned int i;
                                            if (!vparams->displayFlags().getShowForceFields()) return;
                                            if (!this->mstate) return;

                                            if (vparams->displayFlags().getShowWireFrame())
                                                    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

                                            if (vparams->displayFlags().getShowWireFrame())
                                                    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

                                    }


                                    // stress calc
                                    template <class DataTypes>
                                    void ContractionForceField<DataTypes>::handleEvent(sofa::core::objectmodel::Event* event)
                                    {

                                        if (dynamic_cast<sofa::simulation::IntegrateEndEvent *>(event))
                                        {
                                            if(f_calculateStress.getValue())
                                            {
                                                Real mean_stress = 0;

                                                tetrahedronRestInfoVector& tetrahedronInf = *(tetrahedronInfo.beginEdit());
                                                TetrahedronRestInformation *tetInfo;
                                                unsigned int nbTetrahedra=_topology->getNbTetrahedra();
                                                unsigned int i=0,k=0,l=0;
                                                Real SecSPK_p_proj;
                                                for(i=0; i<nbTetrahedra; i++ )
                                                {

                                                    tetInfo=&tetrahedronInf[i];
                                                    // get passive 2ndSPK - second piola kirshoff stress (tetInfo->SecSPK_p)
                                                    for (k=0;k<3;++k) {
                                                        for (l=k;l<3;++l) {
                                                            tetInfo->SecSPK_p(k,l)=f_2ndSPK_p.getValue()[i][k*3+l];
                                                        }
                                                    }
                                                    // take only the projection on the fiber direction
                                                    SecSPK_p_proj=dot(tetInfo->fiberDirections,tetInfo->SecSPK_p*tetInfo->fiberDirections);

                                                    // sum of Second Piola Kirshoff stresses
                                                    tetInfo->CauchyStress_n=tetInfo->SigmaC+SecSPK_p_proj;
                                                    //std::cout << tetInfo->SigmaC <<" + " << SecSPK_p_proj <<" = " << tetInfo->CauchyStress_n;

                                                    // Cauchy : multiply SPK by J=det(F)
                                                    tetInfo->CauchyStress_n=tetInfo->J*tetInfo->CauchyStress_n;

                                                    // print in CauchyStress file :
                                                    // std::cout   << " and Cauchy : " << tetInfo->CauchyStress_n << std::endl;
                                                   // std::cout << tetInfo->CauchyStress_n << " ";
                                                    StressStream << double(tetInfo->CauchyStress_n) << " ";
                                                    mean_stress += double(tetInfo->CauchyStress_n);

                                                }
                                                m_mean_stress.setValue(mean_stress / nbTetrahedra);

                                                StressStream << "" << std::endl;
                                             tetrahedronInfo.endEdit();
                                            }
                                        }

                                    }
        } // namespace forcefield
    } // namespace Components
} // namespace Sofa

