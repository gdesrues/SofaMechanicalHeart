#pragma once

#include <sofa/type/Vec.h>
#include <sofa/core/DataEngine.h>
#include <sofa/core/behavior/MechanicalState.h>
#include <SofaBaseMechanics/MechanicalObject.h>

namespace sofa
{

namespace component
{

namespace engine
{

  using namespace sofa::core::topology;
  using namespace sofa::type;
  using namespace sofa::defaulttype;
  using namespace sofa::component::container;
  using namespace sofa::core::behavior;


/**
 * This class do.....
 */
 template <class DataTypes, class DataTypes2>
   class ContractionInitialization : public virtual core::DataEngine
 {
 public:
   SOFA_CLASS(SOFA_TEMPLATE2(ContractionInitialization, DataTypes, DataTypes2), core::DataEngine);

   typedef DataTypes DataTypes_1;
   typedef DataTypes2 DataTypes_2;
   typedef typename DataTypes::Coord         Coord;
   typedef typename DataTypes::VecCoord      VecCoord;
   typedef typename DataTypes::Real          Real;
   typedef Vec<3,Real>                      Coord3D;
   typedef Vec<2,Real>                      Coord2D;
   typedef sofa::type::vector <Coord3D>    VecCoord3D;
   typedef sofa::type::vector <Coord2D>    VecCoord2D;
   typedef StdVectorTypes<Coord3D, Coord3D, Real >     MechanicalTypes ;
   typedef component::container::MechanicalObject<MechanicalTypes>      MechObject;

   typedef typename DataTypes2::Coord         Coord_out;

 public:

   ContractionInitialization();

   ~ContractionInitialization(){}

   virtual void init();

   virtual void reinit();

   virtual void reset();

   virtual void doUpdate();

   virtual void draw(const sofa::core::visual::VisualParams*);



    /// Pre-construction check method called by ObjectFactory.
    /// Check that DataTypes matches the MechanicalState.
    template<class T>
    static bool canCreate(T*& obj, core::objectmodel::BaseContext* context, core::objectmodel::BaseObjectDescription* arg)
    {
        if (dynamic_cast<MechanicalState<DataTypes>*>(context->getMechanicalState()) == NULL)
            return false;
        return BaseObject::canCreate(obj, context, arg);
    }

    /// Construction method called by ObjectFactory.
    template<class T>
    static typename T::SPtr create(T*  /*obj*/, core::objectmodel::BaseContext* context, core::objectmodel::BaseObjectDescription* arg)
    {
        typename T::SPtr obj = sofa::core::objectmodel::New<T>();
        if (context) context->addObject(obj);
        if (arg) obj->parse(arg);
        if (context)
        {
            obj->mstate = dynamic_cast<MechanicalState<DataTypes>*>(context->getMechanicalState());
        }
        return obj;
    }

   virtual std::string getTemplateName() const
   {
     return templateName(this);
   }

   static std::string templateName(const ContractionInitialization<DataTypes_1, DataTypes_2>* = NULL)
   {
      return std::string("ContractionInitialization<")+DataTypes_1::Name() + std::string(",") + DataTypes_2::Name() + std::string(">");
   }


 protected:
core::topology::BaseMeshTopology* _topology;
   void initialiseValues();
public:
   Data <VecCoord3D> _inputValues;             // Array of manual input values <index, contractivity, stiffness>.
   Data <VecCoord3D> _outputParam;        // Output field of potentiel values.
   Data <char*> _filename;
   Data <double> _secondaryValues;
   Data<std::string> tagTopo;
   MechanicalState<DataTypes>* mstate;
   MechObject *mechanicalObject;

 };

} // namespace engine

} // namespace component

} // namespace sofa


