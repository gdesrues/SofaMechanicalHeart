#include "ContractionForceField.inl"

#include <SofaBaseMechanics/MechanicalObject.h>
#include <sofa/core/ObjectFactory.h>
#include <sofa/core/behavior/ForceField.inl>
#include <SofaBaseTopology/TopologyData.inl>
#include <string.h>
#include <iostream>

namespace sofa
{

namespace component
{

namespace forcefield
{

using namespace sofa::type;
using namespace sofa::defaulttype;
using namespace	sofa::component::topology;
using namespace core::topology;

using std::cerr;
using std::cout;
using std::endl;
using std::string;


//////////****************To register in the factory******************

SOFA_DECL_CLASS(ContractionForceField)

// Register in the Factory
int ContractionForceFieldClass = core::RegisterObject("Contraction Force for heart beating")
#ifndef SOFA_FLOAT
	.add< ContractionForceField<Vec3dTypes> >()
#endif
#ifndef SOFA_DOUBLE
	.add< ContractionForceField<Vec3fTypes> >()
#endif
;

#ifndef SOFA_FLOAT
	template class ContractionForceField<Vec3dTypes>;
#endif
#ifndef SOFA_DOUBLE
	template class ContractionForceField<Vec3fTypes>;
#endif

} // namespace forcefield

} // namespace component

} // namespace sofa

