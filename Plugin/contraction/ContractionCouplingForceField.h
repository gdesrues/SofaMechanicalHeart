#pragma once


#include <sofa/core/behavior/ForceField.h>
#include <SofaBaseMechanics/MechanicalObject.h>
#include <sofa/type/fixed_array.h>
#include <sofa/type/vector.h>
#include <sofa/type/Vec.h>
#include <sofa/type/Mat.h>

#include <SofaBaseTopology/TopologyData.h>
//#include "../../CardiacMeshTools/src/MeshTetrahedrisationLoader.h"
#include "../io_components/HeartMeshLoader.h"

#include <sofa/core/topology/BaseMeshTopology.h>
#include "ContractionForceField.h"



namespace sofa
{

namespace component
{


namespace forcefield
{

using namespace sofa::type;
using namespace sofa::component::topology;
using namespace sofa::component::container;

//template <class DataTypes>
// class ContractionForceField;

template<class DataTypes>
class ContractionCouplingForceField : public sofa::core::behavior::ForceField<DataTypes>
{
 public:
  SOFA_CLASS(SOFA_TEMPLATE(ContractionCouplingForceField, DataTypes), SOFA_TEMPLATE(core::behavior::ForceField, DataTypes));




  typedef core::behavior::ForceField<DataTypes> Inherited;
  typedef typename DataTypes::VecCoord VecCoord;
  typedef typename DataTypes::VecDeriv VecDeriv;
  typedef typename DataTypes::Coord    Coord   ;
  typedef typename DataTypes::Deriv    Deriv   ;
  typedef typename Coord::value_type   Real    ;
  typedef Vec<3,Real>                  Vec3;
  typedef Vec<2,Real>                  Vec2;
  typedef Vec<4,Real>                  Vec4;

  typedef vector<Vec3>		VecCoord3d;

  typedef DataTypes MechanicalTypes;
  //typedef StdVectorTypes< Vec3, Vec3, Real >     MechanicalTypes ; /// assumes the mechanical object type
  typedef core::objectmodel::Data<VecDeriv>    DataVecDeriv;
  typedef core::objectmodel::Data<VecCoord>    DataVecCoord;
  typedef sofa::core::topology::Topology::Tetrahedron Tetrahedron;
  typedef sofa::core::topology::Topology::Triangle Triangle;
  typedef sofa::core::topology::Topology::TetraID TetraID;
  typedef sofa::core::topology::Topology::Tetra Tetra;
  typedef sofa::core::topology::Topology::Edge Edge;
  typedef sofa::core::topology::BaseMeshTopology::EdgesInTriangle EdgesInTriangle;
  typedef sofa::core::topology::BaseMeshTopology::EdgesInTetrahedron EdgesInTetrahedron;
  typedef sofa::core::topology::BaseMeshTopology::TrianglesInTetrahedron TrianglesInTetrahedron;

protected:


  class TetrahedronRestInformation
        {
        public:

      Mat<3,3,Real> A;
      //Vec3 force;
      Real contractivity;
      Real relaxationRate;
      Real contractionRate;
      Real maxstiffness;
      Real Uo;
      Real abspot;
      Real num;

	  //Tetrahedron Point Indices for CUDA
	  Real tetraIndices[4];
	  //f_APD for CUDA
	  Vec<4,Real> f_APD;
	  //f_depolarisationTimes for CUDA
	  Vec<4,Real> f_depolarisationTimes;
	  //dEpsilonC from ContractionFF for CUDA
	  Real dEpsilonC;

                TetrahedronRestInformation() {
                }
                /// Output stream
                inline friend std::ostream& operator<< ( std::ostream& os, const TetrahedronRestInformation& /*eri*/ )
                {
                return os;
                }

                /// Input stream
                inline friend std::istream& operator>> ( std::istream& in, TetrahedronRestInformation& /*eri*/ )
                {
                return in;
                }
        };
       typedef typename VecCoord::template rebind<TetrahedronRestInformation>::other tetrahedronRestInfoVector;

          //Tetrahedron Handler
          class CCTetrahedronHandler : public TopologyDataHandler<Tetrahedron,tetrahedronRestInfoVector >
          {
          public:
              typedef typename ContractionCouplingForceField<DataTypes>::TetrahedronRestInformation TetrahedronRestInformation;
              CCTetrahedronHandler(ContractionCouplingForceField<DataTypes>* _ff, TetrahedronData<tetrahedronRestInfoVector>* _data) : TopologyDataHandler<Tetrahedron, tetrahedronRestInfoVector >(_data), ff(_ff) {}

              void applyCreateFunction(unsigned int tetraIndex, TetrahedronRestInformation &teti, const Tetrahedron& ,  const sofa::type::vector< unsigned int > &, const sofa::type::vector< double >&);

          protected:
              ContractionCouplingForceField<DataTypes>* ff;
          };

        TetrahedronData<tetrahedronRestInfoVector > tetraInfo;
        //MechanicalObject<MechanicalTypes> *mechanicalObject;

public:
        TetrahedronData<type::vector<Vec4> > f_tetraContractivityParam; //list of sigma0,k0, katp and krs
        PointData<type::vector<Real> >f_depolarisationTimes;
        Data<double> f_uniquedepolarisationTimes;
        PointData<type::vector<Real> > f_APD;
        PointData<type::vector<type::vector<Real> > > f_APDVT;
        PointData<type::vector<type::vector<Real> > > f_TdVT;
        Data<bool> withVT;
        Data<bool> useVerdandi;

        Data<Real> f_n0; //reduction factor between 0 and 1
        Data<Real> f_alpha; //parameter alpha
        Data<bool> StarlingEffect;
        Data<Real> f_n1; //Starling factor between 0 and 1
        Data<Real> f_n2; //Starling factor between 0 and 1

        sofa::core::topology::BaseMeshTopology* _topology;
        VecCoord  _initialPoints;///< the intial positions of the points

        Data<std::string> m_tagMeshMechanics;
        Data<bool> useSimple;
        Data<bool> withFile;
        int numberCycles;


public:

        ContractionCouplingForceField();

        virtual ~ContractionCouplingForceField();

        virtual SReal getPotentialEnergy(const core::MechanicalParams*, const DataVecCoord&) const;
        Vec<4,double> getContractivityParameters(const unsigned int tetraIndex);
        virtual void init();


        virtual sofa::type::vector<double> get_SarcomerePower(sofa::type::vector <unsigned int> TetrasIndices);


        //virtual void reinit();
        virtual void addForce(const core::MechanicalParams* mparams /* PARAMS FIRST */, DataVecDeriv& d_f, const DataVecCoord& d_x, const DataVecDeriv& d_v);
        virtual void addDForce(const core::MechanicalParams* mparams /* PARAMS FIRST */, DataVecDeriv& d_df, const DataVecDeriv& d_dx);

        // handle topological changes
        virtual void handleTopologyChange();

         virtual void draw(const core::visual::VisualParams*);
        /// compute lambda and mu based on the Young modulus and Poisson ratio



protected :



        ContractionForceField<MechanicalTypes>     *forcefield;
        Data<std::string> fileName;
        TetrahedronData<tetrahedronRestInfoVector > &gettetInfo() {return tetraInfo;}

public:
        CCTetrahedronHandler* tetrahedronHandler;
        /////////// THIS is to monitor the evolution of EC and Starling Effect
        sofa::component::loader::HeartMeshLoader* _meshLoader;
        sofa::type::vector<double> MeanStar;
        sofa::type::vector<double> MeanEc;
        sofa::type::vector <unsigned int> zonetbl;
        Data<sofa::type::vector<sofa::type::vector<unsigned int>>> m_loaderzones;
        std::ofstream Jac2;
        ///////////
};

} //namespace forcefield

} // namespace Components

} // namespace Sofa

