#pragma once

#include "ContractionInitialization.h"
#include <iostream>
#include <fstream>
#include <cstdlib> // for exit function




namespace sofa
{

namespace component
{

namespace engine
{

  //using namespace sofa::helper;
  //using namespace sofa::type;
  //using namespace core::objectmodel;

  template <class DataTypes, class DataTypes2>
  ContractionInitialization<DataTypes, DataTypes2>::ContractionInitialization()
    : _inputValues (initData (&_inputValues, "input_values", "input array of manual potential values <index of tetrahedra ,contraction,stiffness>."))
    ,tagTopo(initData(&tagTopo,std::string("tagContraction"),"tagContraction","Tag of the contraction node"))
    , _outputParam (initData (&_outputParam, "outputs", "output array of initial condition contraction and stiffness values."))
    , _filename (initData (&_filename, "filename", "name of file where to write initial condition potential values."))
    , _secondaryValues (initData (&_secondaryValues, (double)0.0, "secondaryValues", "default value given to other dimension fields."))

  {
  }


  template <class DataTypes, class DataTypes2>
  void ContractionInitialization<DataTypes, DataTypes2>::init()
  {

    _outputParam.setPersistent (false);

    std::cout<<"Contraction  initialization"<<std::endl;
    addInput(&_inputValues);
    addOutput(&_outputParam);
    setDirtyValue();
  }


  template <class DataTypes, class DataTypes2>
  void ContractionInitialization<DataTypes, DataTypes2>::reinit()
  {
    update();
    _outputParam.setPersistent (false);
    delOutput (&_outputParam);



    // setDirtyValue();
    // setDirtyOutputs();


  }

  template <class DataTypes, class DataTypes2>
  void ContractionInitialization<DataTypes, DataTypes2>::reset()
  {
	  update();
	  _outputParam.setPersistent (false);
	  delOutput (&_outputParam);
  }


  template <class DataTypes, class DataTypes2>
  void ContractionInitialization<DataTypes, DataTypes2>::doUpdate()
  {
        _topology = this->getContext()->getMeshTopology();
    cleanDirty();

    if (!mstate)
    {
       std::cerr << "Error in ContractionInitialization, no mechanichal State found." << std::endl;
       return;
    }

    unsigned int nbrDOF = _topology->getNbTetrahedra();

    if (nbrDOF == 0)
    {
       std::cerr << "Error in ContractionInitialization, no dof found in mechanichal State." << std::endl;
       return;
    }

    const VecCoord3D& values = _inputValues.getValue();

    VecCoord3D& potentiels = *(_outputParam.beginEdit());
    potentiels.resize(nbrDOF);


    // Fill first dimension potentiels only with 0 and other with _secondaryValues:
    Real m_secondaryValues = (Real)_secondaryValues.getValue();

    for (unsigned int i = 0; i<nbrDOF; ++i)
    {
       potentiels[i][0] = 0.0;

       for (unsigned int j = 1; j<potentiels[0].size(); ++j)
          potentiels[i][j] = m_secondaryValues;
    }


    // Fill specific values:
    for (unsigned int i = 0; i<values.size(); ++i)
    {
      unsigned int index = (unsigned int)values[i][0];

      if (index<potentiels.size()-1)
      {
         //for (unsigned int j = 0; j<potentiels[0].size(); ++j)
            potentiels[index][0] = values[i][1];
      }
      else
        std::cerr << "Error: in ContractionInitialization, index: " << index << " is out of bound" << std::endl;
    }


    _outputParam.endEdit();

  }






  template <class DataTypes, class DataTypes2>
  void ContractionInitialization<DataTypes, DataTypes2>::initialiseValues()
  {
     sofa::component::container::MechanicalObject <DataTypes2>* PotentialObjectContainer;

 sofa::core::objectmodel::Tag couplingTag(tagTopo.getValue());
     this->getContext()->get(PotentialObjectContainer,couplingTag,sofa::core::objectmodel::BaseContext::SearchRoot);

     if (!PotentialObjectContainer)
     {
        std::cout << "Error: no PotentialObjectContainer found." << std::endl;
        return;
     }

     Data<typename DataTypes2::VecCoord  >* ccoords = PotentialObjectContainer->write(core::VecCoordId::position());
//        Data<VecCoord3D  >* ccoords = PotentialObjectContainer->write(core::VecCoordId::position());
       typename DataTypes2::VecCoord & listPosition= *((*ccoords).beginEdit());
//        VecCoord3D & listPosition= *((*ccoords).beginEdit());

//     const VecCoord_out& listPosition = (PotentialObjectContainer->read(core::ConstVecCoordId::position())->getValue());
     const VecCoord3D& values = _inputValues.getValue();

//     const VecCoord_out& listPosition = (*PotentialObjectContainer->read(core::ConstVecCoordId::position())->getValue());
//const VecCoord2D& values = _inputValues.getValue();

     for (unsigned int i = 0; i<values.size(); ++i)
     {
        unsigned int index = (unsigned int)values[i][0];
        listPosition[ index ][0] = values[i][1];
     }

  }


  template <class DataTypes, class DataTypes2>
  void ContractionInitialization<DataTypes, DataTypes2>::draw(const sofa::core::visual::VisualParams* vparams)
  {


       this->initialiseValues();

    //  _outputPotentiels.getValue();

  }


} // namespace engine

} // namespace component

} // namespace sofa


