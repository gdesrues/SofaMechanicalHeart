#include "ContractionCouplingForceField.inl"
#include <sofa/core/ObjectFactory.h>
#include <sofa/core/topology/BaseMeshTopology.h>
#include <fstream>
#include <iostream>
#include <vector>


namespace sofa
{

namespace component
{

namespace forcefield
{

using namespace sofa::type;
using namespace sofa::defaulttype;


SOFA_DECL_CLASS(ContractionCouplingForceField)

using namespace sofa::type;


// Register in the Factory
int ContractionCouplingForceFieldClass = core::RegisterObject("Coupling for contraction on heart")
#ifndef SOFA_FLOAT
 // .add< ContractionCouplingForceField<Vec1dTypes> >()
  .add< ContractionCouplingForceField<Vec3dTypes> >()
//  .add< ContractionCouplingForceField<Vec3dTypes> >()
#endif
#ifndef SOFA_DOUBLE
 // .add< ContractionCouplingForceField<Vec1fTypes> >()
  .add< ContractionCouplingForceField<Vec3fTypes> >()
//  .add< ContractionCouplingForceField<Vec3fTypes> >()
#endif
;

#ifndef SOFA_FLOAT
 // template class ContractionCouplingForceField<Vec1dTypes>;
  template class ContractionCouplingForceField<Vec3dTypes>;
 // template class ContractionCouplingForceField<Vec3dTypes>;
#endif
#ifndef SOFA_DOUBLE
  //  template class ContractionCouplingForceField<Vec1fTypes>;
  template class ContractionCouplingForceField<Vec3fTypes>;
 // template class ContractionCouplingForceField<Vec3fTypes>;
#endif



} // namespace forcefield

} // namespace Components

}// namespace Sofa
