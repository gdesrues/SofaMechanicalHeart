#pragma once

#include <sofa/core/behavior/ForceField.h>
#include <SofaBaseMechanics/MechanicalObject.h>
#include <sofa/core/behavior/OdeSolver.h>
#include <GL/gl.h>
#include <sofa/core/visual/VisualModel.h>
#include <sofa/type/Vec.h>
#include <sofa/type/Mat.h>
#include <sofa/type/MatSym.h>
#include <sofa/core/topology/BaseMeshTopology.h>
#include <SofaBaseTopology/TopologyData.h>
#include <sofa/core/objectmodel/BaseData.h>

#include <sofa/simulation/IntegrateEndEvent.h>
#include <sofa/core/objectmodel/Event.h>

#include <string>
#include <map>
#include <sofa/helper/map.h>

namespace sofa
{

namespace component
{

namespace forcefield
{
using namespace std;
using namespace sofa::type;
using namespace sofa::component::topology;


/** Compute Finite Element forces based on tetrahedral elements.
*/
template<class DataTypes>
class ContractionForceField : public core::behavior::ForceField<DataTypes>
{
  public:
    SOFA_CLASS(SOFA_TEMPLATE(ContractionForceField, DataTypes), SOFA_TEMPLATE(core::behavior::ForceField, DataTypes));

    typedef core::behavior::ForceField<DataTypes> Inherited;
    typedef typename DataTypes::VecCoord VecCoord;
    typedef typename DataTypes::VecDeriv VecDeriv;
    typedef typename DataTypes::Coord Coord;
    typedef typename DataTypes::Deriv Deriv;
    typedef typename Coord::value_type Real;
    typedef typename type::MatSym<3,Real> MatrixSym;
    typedef typename type::Mat<3,3,Real> Matrix3;
    typedef type::Vec<3,Real>                            Vec3;
    typedef type::Vec<2,Real>		VecCoord2d;
    typedef DataTypes     CouplingTypes ; /// assumes the mechanical object type
    //typedef StdVectorTypes< Vec3, Vec3, Real >     CouplingTypes ; /// assumes the mechanical object type
    typedef component::container::MechanicalObject<CouplingTypes>      MechObject;

    typedef sofa::Index Index;
    typedef core::topology::BaseMeshTopology::Tetra Element;
    typedef core::topology::BaseMeshTopology::SeqTetrahedra VecElement;
    typedef core::objectmodel::Data<VecDeriv>    DataVecDeriv;
    typedef core::objectmodel::Data<VecCoord>    DataVecCoord;
    typedef sofa::core::topology::Topology::Tetrahedron Tetrahedron;
    typedef sofa::core::topology::Topology::Triangle Triangle;
    typedef sofa::core::topology::Topology::TetraID TetraID;
    typedef sofa::core::topology::Topology::Tetra Tetra;
    typedef sofa::core::topology::Topology::Edge Edge;
    typedef sofa::core::topology::BaseMeshTopology::EdgesInTriangle EdgesInTriangle;
    typedef sofa::core::topology::BaseMeshTopology::EdgesInTetrahedron EdgesInTetrahedron;
    typedef sofa::core::topology::BaseMeshTopology::TrianglesInTetrahedron TrianglesInTetrahedron;

public :


    /// data structure stored for each tetrahedron
        class TetrahedronRestInformation
        {
    public:
          Coord shapeVector[4];
          Coord fiberDirections;
          Real contractivity;
          Real relaxationRate;
          Real contractionRate;
          Real volume;
          Matrix3 deformationGradient;
          Matrix3 SPK;
          Real oldEff;
          Matrix3 contractionTensor;
          Real oldEc;
          Real SigmaC;

         //stress calc
          Real CauchyStress_n;
          Real J;
          Matrix3 SecSPK_p;

          Real Td;
          Real APD;
          //Real dEc;
          Real Ec;

        Real tauC,k,Uc;
        Real tauCana;

          //Tetrahedron Point Indices for CUDA
          float tetraIndices[4];
           //f_tetraContractivityParam for CUDA
           type::Vec<3,Real> f_contractivityParams;
           //f_depolarisationTimes for CUDA
           type::Vec<4,Real> f_depolarisationTimes;
           //f_APD for CUDA
           type::Vec<4,Real> f_APD;
          //Tetrahedron Edges for CUDA
          float tetraEdges[6];


      /// Output stream
      inline friend ostream& operator<< ( ostream& os, const TetrahedronRestInformation& /*eri*/ ) {  return os;  }
      /// Input stream
      inline friend istream& operator>> ( istream& in, TetrahedronRestInformation& /*eri*/ ) { return in; }

      TetrahedronRestInformation() {}
    };
    //typedef typename VecCoord::template rebind<>::other tetrahedronRestInfoVector;
    typedef typename VecCoord::template rebind<TetrahedronRestInformation>::other tetrahedronRestInfoVector;

    //Tetrahedron Handler
    class ContractionTetrahedronHandler : public TopologyDataHandler<Tetrahedron,tetrahedronRestInfoVector >
    {
    public:
        typedef typename ContractionForceField<DataTypes>::TetrahedronRestInformation TetrahedronRestInformation;
        ContractionTetrahedronHandler(ContractionForceField<DataTypes>* _ff, TetrahedronData<tetrahedronRestInfoVector >* _data) : TopologyDataHandler<Tetrahedron, tetrahedronRestInfoVector >(_data), ff(_ff) {}

        void applyCreateFunction(unsigned int tetraIndex, TetrahedronRestInformation &teti, const Tetrahedron& ,  const sofa::type::vector< unsigned int > &, const sofa::type::vector< double >&);

    protected:
        ContractionForceField<DataTypes>* ff;
    };



 public :
        core::topology::BaseMeshTopology* _topology;
        VecCoord  _initialPoints;	/// the intial positions of the points

        Data<Real> f_viscosityParam;
        Data<Real> f_heartPeriod;
        Data<bool> f_addElastometry;
        Data<bool> useCoupling;
        Data<bool> useVerdandi;
        Data<bool> DesactivateElastoIfNoElec;
        Data<std::string> m_tagMeshCoupling;
        Data<std::string> m_tagMeshSolver;
        Data<Real> f_elasticModulus;
        Data <unsigned int> _tetraPloted;
        Data<std::map < std::string, sofa::type::vector<double> > > f_graph;
        Data<std::map < std::string, sofa::type::vector<double> > > f_graph2;
        Data<std::map < std::string, sofa::type::vector<double> > > f_graph3;
        bool updateMatrix;
        TetrahedronData<type::vector<Vec3> > f_fiberDirections;
        TetrahedronData<type::vector<Vec3> > f_tetraContractivityParam;
        PointData<type::vector<Real> > f_depolarisationTimes;
        PointData<type::vector<Real> > f_APD;
        //TetrahedronData<type::vector<Real> > f_contractionRate;
        //TetrahedronData<type::vector<Real> > f_relaxationRate;

        // stress calc
        Data<bool> f_calculateStress;
        Data<std::string> f_stressFile;
        std::ofstream StressStream;
        TetrahedronData<type::vector<type::Vec<9,Real> > >  f_2ndSPK_p;
        std::ofstream Jac1;
        std::ofstream Jac2;
        std::ofstream Jac3;
        std::ofstream Jac4;
        std::ofstream Jac5;
        std::ofstream Jac6;

 public:
        std::ofstream elasto;

        double EnergyTotale;
        Real dt;
        Real heartPeriod;
        Real elasticMod;

        Data<Real> m_mean_stress;  // used to compute the current phase

        //std::ofstream energie;
        void updateGraph(int tetraIndex);
        void resetGraph();


        ContractionForceField();

   	virtual ~ContractionForceField();
    virtual void init();
    virtual void reinit();
	virtual void reset();

    //Used for CUDA non atomic implementation
    void initTetraNeighbourhoodPoints();
    void initEdgeNeighbourhoodPoints();
    void initNeighbourhoodEdges();
  
    virtual void addForce(const core::MechanicalParams* mparams /* PARAMS FIRST */, DataVecDeriv& d_f, const DataVecCoord& d_x, const DataVecDeriv& d_v);
    virtual void addDForce(const core::MechanicalParams* mparams /* PARAMS FIRST */, DataVecDeriv& d_df, const DataVecDeriv& d_dx);
    virtual void addKToMatrix(linearalgebra::BaseMatrix *m, SReal kFactor, unsigned int &offset);

    // stress calc
    /// Function handling all the events (if listening=true)
    virtual void handleEvent(sofa::core::objectmodel::Event* event);

        virtual SReal getPotentialEnergy(const core::MechanicalParams*, const DataVecCoord&) const;

        virtual void updateMatrixData();
        type::Vec<3,double> getFiberDirections(const unsigned int tetraIndex);
        type::Vec<3,double> getContractivityParameters(const unsigned int tetraIndex);
        double getdEpsilonC(const unsigned int tetraIndex);
        double getEC(unsigned int tetraIndex);
        double getSigmaC(unsigned int tetraIndex);
        double getE1d(unsigned int tetraIndex);
        void setEC(unsigned int tetraIndex,double value);
        double getECold(unsigned int tetraIndex);
        void setECold(unsigned int tetraIndex,double value);
        double getVolume(const unsigned int tetraIndex);

        virtual sofa::type::vector<double> get_Energies(sofa::type::vector <unsigned int> TetrasIndices);


    virtual void draw(const core::visual::VisualParams*);

    class EdgeInformation
   {
   public:
           /// store the stiffness edge matrix
           Matrix3 DfDx;
            //For CUDA
           float vertices[2];
           /// Output stream
           inline friend ostream& operator<< ( ostream& os, const EdgeInformation& /*eri*/ ) {  return os;  }
           /// Input stream
           inline friend istream& operator>> ( istream& in, EdgeInformation& /*eri*/ ) { return in; }

     EdgeInformation() {}
   };
    typedef typename VecCoord::template rebind<EdgeInformation>::other edgeInfoVector;

  protected:
    // handle topological changes
      virtual void handleTopologyChange();

    /// the array that describes the complete material energy and its derivatives

      TetrahedronData<tetrahedronRestInfoVector > tetrahedronInfo;
      EdgeData<edgeInfoVector> edgeInfo;
      Data<std::string> fileName2;
      MechObject *mechanicalObject;

public:
      ContractionTetrahedronHandler* tetrahedronHandler;


};

} // namespace forcefield

} // namespace component

} // namespace sofa


