#pragma once
#include <sofa/simulation/BaseSimulationExporter.h>
#include <sofa/defaulttype/VecTypes.h>
#include "../copy_new_components/PressureFF.h"
#include "../copy_new_components/utils.h"
#include <fstream>

namespace sofa
{

namespace simulation
{

class TxtExporter : public _basesimulationexporter_::BaseSimulationExporter
{
public:
    SOFA_CLASS(TxtExporter, _basesimulationexporter_::BaseSimulationExporter);

    SingleLink<TxtExporter, component::forcefield::PressureFF<defaulttype::Vec3Types>, BaseLink::FLAG_STOREPATH | BaseLink::FLAG_STRONGLINK> l_ff;
    Data<Real> d_volume;

    TxtExporter();
    ~TxtExporter();
    void doInit() override;
    bool write() override;

private:
    std::ofstream _pressure_file;

};


} // namespace simulation

} // namespace sofa

