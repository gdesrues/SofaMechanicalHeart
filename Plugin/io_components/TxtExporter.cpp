#include "TxtExporter.h"
#include <sofa/core/ObjectFactory.h>


namespace sofa
{

namespace simulation
{


TxtExporter::TxtExporter()
    : l_ff(initLink("ff", "Link to the PressureFF component"))
    , d_volume(initData(&d_volume, "volume", "Link to VolumeEngine.volume"))
{
}


TxtExporter::~TxtExporter()
{
    if(_pressure_file.is_open()) _pressure_file.close();
}


void TxtExporter::doInit() {
    msg_error_when(!l_ff) << "Failed to get the link to the PressureFF component ('ff')";
    _pressure_file.open(d_filename.getValue());
    dump(_pressure_file, "i", "t", "Pv", "Pat", "Par", "Q", "Vol");
}


bool TxtExporter::write() {
    dump(_pressure_file,
         m_stepCounter,
         this->getTime(),
         l_ff->Pv.getValue(),
         l_ff->Pat.getValue(),
         l_ff->Par.getValue(),
         l_ff->Q.getValue(),
         d_volume.getValue());
    return true;
}


int TxtExporterClass = sofa::core::RegisterObject("").add<TxtExporter>();

} // namespace simulation

} // namespace sofa
