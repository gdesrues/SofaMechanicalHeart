#include <iostream>
#include "CardiacVTKLoader.h"
#include <sofa/core/ObjectFactory.h>
#include <sofa/type/Vec.h>
#include "SofaBaseTopology/TetrahedronSetTopologyContainer.h"
#include "SofaBaseTopology/MeshTopology.h"
#include <sofa/core/topology/BaseMeshTopology.h>
#include <sofa/helper/system/FileRepository.h>

namespace sofa
{

    namespace component
    {

        namespace loader
        {

            using namespace sofa::type;
            SOFA_DECL_CLASS(CardiacVTKLoader)


                    int CardiacVTKLoaderClass = core::RegisterObject("Specific mesh loader for different tetrahedron files formats.")
                                                .add< CardiacVTKLoader >()
                                                ;

            CardiacVTKLoader::CardiacVTKLoader()
                : MeshLoader()
                ,m_numberOfZone(initData(&m_numberOfZone,"numberOfZone","Vertices of the mesh loaded"))
                ,m_zoneNames (core::objectmodel::Base::initData(&m_zoneNames, "m_zoneNames", "See zones Name."))
                ,m_zoneSizes (core::objectmodel::Base::initData(&m_zoneSizes, "m_zoneSizes", "See zones Size."))
                ,m_numberOfSurfaceZone(initData(&m_numberOfSurfaceZone,"numberOfSurfaceZone","Vertices of the mesh loaded"))
                ,m_surfaceZoneNames (core::objectmodel::Base::initData(&m_surfaceZoneNames, "m_surfaceZoneNames", "See surface zones Name."))
                ,m_surfaceZoneSizes (core::objectmodel::Base::initData(&m_surfaceZoneSizes, "m_surfaceZoneSizes", "See surface zones Size."))
                ,m_nodeFiber_filename(initData(&m_nodeFiber_filename,"FileNodeFiber","Filename of the fiber par node of the mesh loaded (.bb file)."))
                ,m_nodeFibers(initData(&m_nodeFibers,"nodeFibers","Fiber par node of the mesh loaded."))
                ,m_facetFiber_filename(initData(&m_facetFiber_filename,"FileFacetFiber","Filename of the fiber par facet of the mesh loaded (.tbb file)."))
                ,m_facetFibers(initData(&m_facetFibers,"facetFibers","Fiber par facet of the mesh loaded."))
                ,m_facetBFiber_filename(initData(&m_facetBFiber_filename,"FileFacetBFiber","Filename of the fiber par facet of the mesh loaded, described in barycentric coordinates (.lbb file)."))
                ,m_facetBFibers(initData(&m_facetBFibers,"facetBFibers","Fiber par facet of the mesh loaded, described in barycentric coordinates."))
                ,m_tetraTensor_filename(initData(&m_tetraTensor_filename,"FileTetraTensor","Filename of the tensor for each vertex on a tetra (.ttsr file)"))
                ,m_tetraTensor(initData(&m_tetraTensor,"tetraTensor","tensor for each vertex on a tetra"))
                ,m_contractionParameters_filename(initData(&m_contractionParameters_filename,"FileContractionParameters","Filename of the .txt containing the contraction parameters per zonename"))
                ,m_contractionParameters(initData(&m_contractionParameters, "ContractionParameters", "Parameter for linking to ContractionParameters from other components."))
                ,m_contractionParametersString(initData(&m_contractionParametersString,"InputContractionParameters","contraction parameter at each tetra"))
                ,m_StiffnessParameters_filename(initData(&m_StiffnessParameters_filename,"FileStiffnessParameters","Filename of the .txt containing the stiffness parameters per zonename"))
                ,m_StiffnessParameters(initData(&m_StiffnessParameters,"StiffnessParameters","Parameter for linking to StiffnessParameters from other components."))
                ,m_StiffnessParametersString(initData(&m_StiffnessParametersString,"InputStiffnessParameters","stiffness parameter at each tetra"))
                ,m_ElectroFile(initData(&m_ElectroFile,"ElectroFile","File with precomputed electrophysiology"))
                ,m_VelocityFile(initData(&m_VelocityFile,"velocityFile","File with intial velocities"))
                ,m_velocities(initData(&m_velocities,"velocities","initial velocities"))
                ,m_unit(initData(&m_unit,"unitTime","unit of the time scale use for electrophysiology (s or ms)"))
                ,m_startContraction(initData(&m_startContraction,"startContraction","time at which the contraction starts"))
                ,m_depolarizationTimes(initData(&m_depolarizationTimes,"depoTimes","Times of depolarization per node"))
                ,m_APDTimes(initData(&m_APDTimes,"APDTimes","Times of APD per node"))
                ,m_tetraConductivity_filename(initData(&m_tetraConductivity_filename,"FileTetraConductivity","FileTetraConductivity"))
                ,m_tetraConductivity(initData(&m_tetraConductivity,"tetraConductivity"," Conductivity per tetra for MF electrical wave"))
                ,m_APDTimesZones(initData(&m_APDTimesZones,"APDTimesZones","Times of APD per node"))
                ,m_APDTimesString(initData(&m_APDTimesString,"APDTimesString","Times of APD per node"))
                ,m_APDTimesFile(initData(&m_APDTimesFile,"APDTimesFile","Times of APD per node"))
                ,m_MSinitSurfaceZoneNames(initData(&m_MSinitSurfaceZoneNames,"MSinitSurfaceZoneNames","Input-names of the surface zones for the init pacing of Mitchell Shaeffer (need for coupling)"))
                ,m_cellTypes(initData(&m_cellTypes, "CellTypes", "Type of each cell element"))
                ,createFibers(initData(&createFibers,(bool) false,"createFibers","if the loader must create the Fibers"))
                //,planZone(initData(&planZone,"planZone","plan z=b above which the base is defined, for 2 valves model only"))
                ,outputnodeFibers(initData(&outputnodeFibers,"outputNodeFibers","output Fibers"))
                ,outputtetraFibers(initData(&outputtetraFibers,"outputTetraFibers","output Tetra Fibers"))
                ,angleEpi((initData(&angleEpi,"angleEpi","if the loader must create the Fibers")))
                ,angleEndo((initData(&angleEndo,"angleEndo","if the loader must create the Fibers")))
				,withAHA((initData(&withAHA,(bool) 0,"withAHA","with AHA zones")))
				,TetraZoneName((initData(&TetraZoneName,(std::string)"LVRVTetrazones","TetraZoneName","name of the tetra data ")))
				,AHAzoneName((initData(&AHAzoneName,(std::string)"AHATetraZones","AHAzoneName","name of the tetra data ")))
				,TriZoneName((initData(&TriZoneName,"TriZoneName","name of the endo epi data")))
				,TetraTri((initData(&TetraTri,"TetraTriangle","Tetra or Triangle mesh")))
				//,LVRVBase((initData(&LVRVBase,"LVRVBase","int vector with the number of the zones in LV,RV,Base order for TetraZoneName")))
				,PointZoneName((initData(&PointZoneName,"PointZoneName","PointZoneName")))
            {

                //     addAlias (&m_numberOfZone,"numberOfZone");
                addAlias (&m_numberOfSurfaceZone,"numberOfSurfaceZone");
                addAlias (&m_zones,"zones");
                addAlias (&m_surfaceZones,"surfaceZones");
				addAlias (&m_surfaceZoneNames,"surfaceZoneNames");
				addAlias (&m_pointZones,"pointZones");
				addAlias (&m_pointNames,"pointZoneNames");
				addAlias (&trianglesSurf,"trianglesSurf");
                addAlias (&m_cellTypes, "cellTypes");
            }

            void CardiacVTKLoader::doClearBuffers()
            {
                m_numberOfSurfaceZone.setValue(0);
                m_numberOfPointZone.setValue(0);
                helper::getWriteOnlyAccessor(m_cellTypes).clear();
                helper::getWriteOnlyAccessor(m_zoneNames).clear();
                helper::getWriteOnlyAccessor(m_zoneSizes).clear();
                helper::getWriteOnlyAccessor(m_zones).clear();
                helper::getWriteOnlyAccessor(trianglesSurf).clear();
                helper::getWriteOnlyAccessor(m_surfaceZoneNames).clear();
                helper::getWriteOnlyAccessor(m_surfaceZoneSizes).clear();
                helper::getWriteOnlyAccessor(m_surfaceZones).clear();
                helper::getWriteOnlyAccessor(m_pointNames).clear();
                helper::getWriteOnlyAccessor(m_pointZoneSizes).clear();
                helper::getWriteOnlyAccessor(m_pointZones).clear();
            }



            bool CardiacVTKLoader::doLoad()
            {
                m_internalEngine["filename"].cleanDirty();

                std::cout << "Loading VTK file: " << d_filename << std::endl;

                FILE* file;
                bool fileRead = false;

                // -- Loading file
                const char* filename = d_filename.getFullPath().c_str();

                if ((file = fopen(filename, "r")) == NULL)
                {
                    serr << "Error:  Cannot read file '" << d_filename << "'." << sendl;
                    return false;
                }
                fclose(file);


                std::string sfilename (filename);

                // -- Check file extension.
                std::string extension;
                size_t pointPosition = sfilename.find_last_of (".");

                if (pointPosition != sfilename.npos)
                    extension = sfilename.substr (pointPosition+1);


                // -- Reading file and writing Mesh  (a remplacer par un appel au canLoad ?)
                if (extension == "vtk" || extension == "VTK")
                    fileRead = readVtkFormat (filename);

                else
                {
                    serr << "Warning: CardiacVTKLoader: the file: " << sfilename << " has no extension known. "<< sendl;
                }


                if (!createFibers.getValue()){
                    // -- Reading fibers files
                    if ( m_tetraTensor_filename.getValue() != "" ){
                        std::cout<<"readTetraTensorDirection()"<<std::endl;
                        readTetraTensorDirection();
                    }
                    else{
                        if ( m_nodeFiber_filename.getValue() != "" ){
                            std::cout<<"readFibreDirection()"<<std::endl;
                            readFibreDirection();
                        }


                        if ( m_facetBFiber_filename.getValue() != "" ){
                            std::cout<<"TetraLocalFibreDirection()"<<std::endl;
                            readTetraLocalFibreDirection();
                        }

                        if ( m_facetFiber_filename.getValue() != "" ){
                            std::cout<<"readTetraFibreDirection()"<<std::endl;
                            readTetraFibreDirection();
                        }
                    }
                }
                else createFiberDirection(angleEpi.getValue(),angleEndo.getValue());

                if(m_contractionParameters_filename.getValue() != ""){
                    std::cout<<"readContractionParametersFile()"<<std::endl;
                    readContractionParametersFile();
                }
                else if(m_contractionParametersString.getValue() != "")
                {
                    std::cout<<"readContractionParametersString()"<<std::endl;
                    readContractionParametersString();
                }

                if(m_StiffnessParameters_filename.getValue() != ""){
                    std::cout<<"readStiffnessParameters()"<<std::endl;
                    readStiffnessParametersFile();
                }
                else if(m_StiffnessParametersString.getValue() != "")
                {
                    std::cout<<"readStiffnessParametersString()"<<std::endl;
                    readStiffnessParametersString();
                }


                if(m_ElectroFile.getValue() != ""){
                    std::cout<<"readElectroFile()"<<std::endl;
                    readElectrophysiology();
                }


                if(m_APDTimesFile.getValue()!="")  // New
                {
                    std::cout<<"readAPDFile"<<std::endl;
                    readAPDFile();
                }
                else if(m_APDTimesString.getValue() != "")  // New
                {
                    std::cout<<"readAPDString()"<<std::endl;
                    readAPDString();
                }


                if(m_tetraConductivity_filename.getValue() != "")  // New
                {
                    std::cout<<"readConductivity()"<<std::endl;
                    readConductivity();
                }

                if(m_VelocityFile.getValue() != "")
                    readVelocity();

                if(m_MSinitSurfaceZoneNames.getValue().size()>0) // if mitchell Shaeffer initialization zone  // New
                   getInitMSvertices(); // get the vertices of the init surface zones



                //if(m_ElectroFile.getValue() != "")
                //    readElectrophysiology();
                //if(m_VelocityFile.getValue() != "")
                //    readVelocity();

                //std::cout << "Surface zones names: " << m_surfaceZoneNames << std::endl;
                //std::cout << "Surface zones: " << m_surfaceZones << std::endl;

                return fileRead;
            }


            bool CardiacVTKLoader::readVtkFormat (const char* filename)
            {
                std::ifstream dataFile (filename);
                std::string line;

                // Number of vertices, tetrahedras, cells (area between points, grid points) and triangles.
                unsigned int nbVertices = 0;
                unsigned int nbTetrahedra = 0;
				unsigned int nbCells=0;
				unsigned int nbTriangles=0;
                std::string file_location; // String to store at which file part we are currently
                unsigned int nbHeader=0;
                unsigned int nbPoints=1; // Number of elements to assign to each point
                unsigned int nbToRead; // Number of points to read
                bool cell_data = 0;
                bool point_data = 0;

                // Vectors to store vertices, tetrahedras and triangles
                // d_something, member data herited from MeshLoader
                // Store in the direction pointed for my_positions, the value of d_positions
                type::vector<sofa::type::Vector3>& my_positions = *(d_positions.beginEdit()); // d_positions, vertices of the mesh loaded
                type::vector<Tetrahedron >& my_tetrahedra = *(d_tetrahedra.beginEdit()); // d_tetrahedra, tetrahedra of the mesh loaded
                type::vector<Triangle >& my_triangles = *(d_triangles.beginEdit()); // d_triangles, triangles of the mesh loaded

                // Vector to store the cell types
                type::vector<unsigned int>& my_cellTypes = *(m_cellTypes.beginEdit());  // Initialize array to store the cell types

                // Vector to store surface triangles
				type::vector<unsigned int >& my_trianglesSurf = *(trianglesSurf.beginEdit());

                // Number of volume zones
                unsigned int& my_numberOfZone =  *(m_numberOfZone.beginEdit());
                // Initialization of vectors to store volume zones and names
                type::vector <std::string>& my_zoneNames =  *(m_zoneNames.beginEdit());
                type::vector <unsigned int>& my_zoneSizes =  *(m_zoneSizes.beginEdit());
                type::vector < type::vector <unsigned int> >& my_zones =  *(m_zones.beginEdit());

                // Number of surface zones
                unsigned int& my_numberOfSurfaceZone =  *(m_numberOfSurfaceZone.beginEdit());
                // Initialization of vectors to store surfaces zones and names
                type::vector <std::string>& my_surfaceZoneNames =  *(m_surfaceZoneNames.beginEdit());
                type::vector <unsigned int>& my_surfaceZoneSizes =  *(m_surfaceZoneSizes.beginEdit());
                type::vector < type::vector <unsigned int> >& my_surfaceZones =  *(m_surfaceZones.beginEdit());

                // Number of point zones
				unsigned int& my_numberOfPointZone =  *(m_numberOfPointZone.beginEdit());
                // Initialization of vectors to store the point zones and names
                type::vector <std::string>& my_pointNames =  *(m_pointNames.beginEdit());
                type::vector <unsigned int>& my_pointZoneSizes =  *(m_pointZoneSizes.beginEdit());
                type::vector < type::vector <unsigned int> >& my_pointZones =  *(m_pointZones.beginEdit());

                // Initialize for cell data
                int u; // Store the zone label
                double uf; // Store float information

                type::vector <unsigned int> zonenumber; // Store the different zone numbers
                type::vector <unsigned int> surfzonenumber; // Store the different surface numbers
                type::vector <unsigned int> tetranumber; // Store number of tetrahedras by zone
                type::vector <unsigned int> trinumber; // Store the number of triangles by zone

                // For the point areas
                type::vector <unsigned int> pointzonenumber; // Store the different point zone numbers
                type::vector <unsigned int> pointnumber; // Store the number of points by zone

                // Check if the mesh is formed of tetrahedras or triangles
                if (TetraTri.getValue() == "Tetra")
                { // Tetrahedras
                while(!dataFile.eof())  // Read the file
                {
                    std::getline(dataFile, line);
                    if (line.empty()) continue;
                    std::istringstream ln(line);
                    std::string kw;
                    ln >> kw; // Parse string
                    if (kw == "#") {
                        // Skip line
                        continue;
                    }
                    if (kw.find_first_not_of(' ') == std::string::npos) {
                        // Only white space
                        continue;
                    }
                    // Read header - usually three lines
                    if (nbHeader < 3) {
                        // - Name
                        // - Data type (ASCII)
                        // - Format (DATASET UNSTRUCTURED_GRID)
                        nbHeader += 1;
                        continue;
                    }

                    // Start with the file
                    if (kw == "POINTS") // We are at the beginning of the nodes
                    {
                        file_location = "POINTS";
                        int n; // Number of nodes
                        std::string typestr; // Type of nodes
                        ln >> n >> typestr; // save ln content, first item to n, second to typestr
                        std::cout << "Found " << n << " " << typestr << " points" << std::endl;
                        nbVertices=n; // Get total number of vertices
                        nbPoints=3;
                        nbToRead=nbVertices;
                        double x, y, z;
                        for (unsigned int i=0; i<nbVertices; ++i) // Store vertices
                        {
                            dataFile >> x >> y >> z;
                            my_positions.push_back (type::Vector3(x, y, z)); // Store the vertices in the memory direction pointed by my_positions
                        }
                        d_positions.endEdit(); // Finish to edit the values of d_positions
                    }
                    else if (kw == "CELLS") // We are at the beginning of the CELLS data
                    {
                        file_location = "CELLS";
                        int n, ni; // Store number of cells, and number of elements
                        ln >> n >> ni;
                        std::cout << "Found " << n << " cells" << std::endl;
                        nbCells=n;
                        int num;
                        for (unsigned int i=0; i<nbCells; ++i)
                        {
                            Tetrahedron nodes; // Create tetrahedron object
                            Triangle nodestri; // Create triangle object
                            dataFile >> num; // Get type of cell
                            if (num==4) // It is a tetrahedron
                            {
                                dataFile >> nodes[0] >> nodes[1] >> nodes[2] >> nodes[3]; // Store the nodes in the object
                                my_tetrahedra.push_back (nodes); // Save it
                                // std::cout<<"Tetra = "<<nodes<<std::endl;
							}
                            if (num==3) // It is a triangle
                            {
                                dataFile >> nodestri[0] >> nodestri[1] >> nodestri[2]; // Store the nodes in the object
                                my_triangles.push_back (nodestri);
                                // std::cout<<"Triangle = "<<nodestri<<std::endl;
                            }
                        }
                        d_tetrahedra.endEdit(); // Finish to edit d_tetrahedra
                        d_triangles.endEdit(); // Finish to edit d_triangles
                        // std::cout<<"Triangles = "<<d_triangles<<std::endl;
                        // std::cout<<"Tetrahedras = "<<d_tetrahedra<<std::endl;
                        nbTriangles=d_triangles.getValue().size(); // Get total number of triangles
                        nbTetrahedra=d_tetrahedra.getValue().size(); // Get total number of tetrahedras
                        std::cout<<"Number of triangles="<<nbTriangles<<std::endl;
                        std::cout<<"Number of tetrahedras="<<nbTetrahedra<<std::endl;
                    }

                    else if (kw == "CELL_TYPES") // We are at the beginning of the cell types
                    {
                        // Not very useful right now, maybe we could use this to detect automatically if we are dealing with triangles or tetrahedras
                        file_location = "CELL_TYPES";
                        // std::cout<<"EH CELL TYPES"<<std::endl;
                        int n;
                        ln >> n; // Get number of cell types
                        int type;
                        // Store the cell types
                        for (unsigned int i=0; i<nbCells; ++i)
                        {
                            dataFile >> type; // Get type of cell
                            my_cellTypes.push_back (type);
                        }
                        m_cellTypes.endEdit(); // Finish to edit
                        int nbCellTypes=m_cellTypes.getValue().size(); // Get total number of cell types
                        std::cout<<"Number of cell types= "<<nbCellTypes<<std::endl;
                    }

                    else if (kw == "CELL_DATA") // Check the data associated to the cell
                    {
                        file_location = "CELL_DATA";
                        int n; // Should be equal to the number of cells
                        ln >> n;
                        if (n != nbCells) {
                            std::cout<<"Uooops! Something is wrong with the CELL_DATA! The number of values does not match the number of cells!"<<std::endl;
                        }
                    }

                    else if (kw == "POINT_DATA") // Check the data associated to the points
                    {
                        std::cout<<"Point data!" <<std::endl;
                        file_location = "POINT_DATA";
                        int n; // Should be equal to the number of nodes
                        ln >> n;
                        if (n != nbVertices) {
                            std::cout<<"Uooops! Something is wrong with the POINT_DATA! The number of values does not match the number of vertices!"<<std::endl;
                        }
                    }

                    else if (file_location == "CELL_DATA") // Read data associated to the cell
                    {
                        // std::cout << "Here inside CELL_DATA!" << kw << std::endl;

                        // Check if we are the first cell data or a new one
                        std::string zone_name;
                        std::string typstr;

                        if (kw == "FIELD") {
                            // std::cout << "FIELD! " << kw << std::endl;
                            ln >> kw; // FieldData
                            ln >> kw; // # Field
                            dataFile >> zone_name >> nbPoints >> nbToRead >> typstr;
                            // std::cout << zone_name << nbPoints << nbToRead << typstr << std::endl;
                        }
                        else if (kw == "VECTORS") {
                            ln >> zone_name >> typstr;
                            nbToRead = nbCells;
                            nbPoints = 3;
                        }
                        else if (kw == "SCALARS") {
                            // Parse the zone name + the format
                            // std::cout << "Inside scalars!" << std::endl;
                            ln >> zone_name >> typstr;
                            // The next line is: LOOKUP_TABLE default, after it, the data starts!
                            dataFile>> kw;
                            dataFile>> kw;
                            nbPoints = 1;
                            nbToRead = nbCells;
                        }
                        else {
                            // Assume we are reading the name of the fieldname
                            zone_name = kw;
                            ln >> nbPoints >> nbToRead >> typstr;
                        }

                        std::string TetraZoneNames = TetraZoneName.getValue();
                        // std::cout << TetraZoneNames << std::endl;
                        size_t pos = 0;
                        bool saved = 0;
                        std::string token;
                        // Assume that there can be more than one point zone name
                        std::string delimiter = " ";

                        // Parse the data
                         std::cout << "Zone name: " << zone_name << std::endl;
                        while ((pos = TetraZoneNames.find(delimiter)) != std::string::npos) {
                            token = TetraZoneNames.substr(0, pos);
                             std::cout << token << std::endl;
                            if (zone_name == token)
                            {
                                std::cout << "Zone: " << zone_name << " saved! " << std::endl;
                                saved = 1;
                                for (unsigned int i=0; i<nbCells; ++i)  // Start from the second label
                                {
                                    dataFile>> u;  // Read label

                                    if (my_cellTypes[i] == 10) // Tetrahedra
                                    {
                                        if (cell_data) {
                                            if (tetranumber[i] == 0) {
                                                tetranumber[i] = u; // Overwrite label if the original is 0
                                            }
                                        }
                                        else {
                                            tetranumber.push_back(u);  // Save in the individual tetrahedra labels
                                        }
                                        // Check if we already found this label or is new
                                        bool find=false;
                                        unsigned int z=0;
                                        for (z=0; z < zonenumber.size(); z++){
                                            if (u==zonenumber[z])
                                            {
                                                find =true;
                                                break;  // We already found it...
                                            }
                                        }
                                        if (find==false)
                                        { // It is a new label
                                            zonenumber.push_back(u);  // Store it in the zonenumber
                                            std::stringstream obb;
                                            obb<<u;
                                            std::string name="Zone"+obb.str();
                                            my_zoneNames.push_back(name);
                                            std::cout<<"Found zone: " << name <<std::endl;
                                        }
                                    }

                                    if (my_cellTypes[i] == 5) // Triangle
                                    {
                                        if (cell_data) {
                                            if (trinumber[i] == 0)    {
                                                trinumber[i] = u; // Overwrite label if the original is 0
                                            }
                                        }
                                        else {
                                            trinumber.push_back(u);  // Store for each triangle its label
                                        }
                                        // Check if we already found this label or is new
                                        bool find=false;
                                        unsigned int z=0;
                                        for (z=0; z < surfzonenumber.size(); z++){
                                            if (u==surfzonenumber[z])
                                            {
                                                find =true;
                                                break;  // We already found it...
                                            }
                                        }
                                        if (find==false)
                                        { // It is a new label
                                            surfzonenumber.push_back(u);  // Store it in the surfzonenumber
                                            std::stringstream obb;
                                            obb<<u;
                                            std::string name="SurfZone"+obb.str();
                                            my_surfaceZoneNames.push_back(name);
                                            std::cout<<"Found zone: " << name <<std::endl;
                                        }
                                    }
                                }
                                if (!cell_data) {
                                    cell_data = 1;
                                }
                            }
                            TetraZoneNames.erase(0, pos + delimiter.length());
                        }
                        if (!saved) {
                            std::cout << "Zone: " << zone_name << " not saved " << std::endl;
                            for (unsigned int i=0; i<nbCells; ++i)  // Read the zone but not save it...
                            {
                                dataFile >> u;  // Read next label
                            }
                        }
                    }

                    else if (file_location == "POINT_DATA") // Read data associated to the points
                    {
                        // std::cout << "Here inside POINT_DATA!" << std::endl;
                        // Check if we are the first cell data or a new one
                        std::string zone_name;
                        std::string typstr;

                        if (kw == "FIELD") {
                            ln >> kw; // FieldData
                            ln >> kw; // # Field
                            dataFile >> zone_name >> nbPoints >> nbToRead >> typstr;
                        }
                        else if (kw == "VECTORS") {
                            ln >> zone_name >> typstr;
                            nbToRead = nbVertices;
                            nbPoints = 3;
                        }
                        else if (kw == "SCALARS") {
                            // Parse the zone name + the format
                            ln >> zone_name >> typstr;
                            // std::cout<<"Check: "<<zone_name<<" "<<typstr<<std::endl;
                            // The next line is: LOOKUP_TABLE default, after it, the data starts!
                            dataFile>> kw;
                            dataFile>> kw;
                            nbPoints = 1;
                            nbToRead = nbVertices;
                        }
                        else {
                            // Assume we are reading the name of the fieldname
                            zone_name = kw;
                            ln >> nbPoints >> nbToRead >> typstr;
                        }

                        // -- Read them
                        std::string PointZoneNames = PointZoneName.getValue();
                        // std::cout << PointZoneNames << std::endl;
                        size_t pos = 0;
                        std::string token;
                        bool saved = 0;

                        // Assume that there can be more than one point zone name
                        std::string delimiter = " ";

                        while ((pos = PointZoneNames.find(delimiter)) != std::string::npos) {
                            token = PointZoneNames.substr(0, pos);
                            // std::cout << token << std::endl;
                            // Check if the token matches the current zone_name
                            if (zone_name == token) // Check if we have zones defined by points
                            {
                                std::cout << "Zone: " << zone_name << " saved! " << std::endl;
                                saved = 1;

                                for (unsigned int i=0; i<nbVertices; ++i)
                                {
                                    dataFile>> u;
                                    if (nbPoints == 1) {
                                        if (point_data) {
                                            if (pointnumber[i] == 0) {
                                                pointnumber[i]  = u; // Overwrite label if the original is 0
                                            }
                                        }
                                        else {
                                            pointnumber.push_back(u);
                                        }

                                        bool find=false;
                                        unsigned z=0;
                                        for (z=0; z<pointzonenumber.size();z++){
                                            if(u==pointzonenumber[z]) {
                                                find =true;
                                                break;
                                            }
                                        }
                                        if(find==false){
                                            pointzonenumber.push_back(u);
                                            std::stringstream obb;
                                            obb<<u;
                                            std::string name="PointZone"+obb.str();
                                            my_pointNames.push_back(name);
                                        }
                                    }
                                    if (nbPoints == 3) {
                                        // For the moment just let it pass..
                                        double x, y, z;
                                        dataFile >> x >> y >> z;
                                    }
                                }
                                if (!point_data) {
                                    point_data = 1;
                                }

                            }
                            PointZoneNames.erase(0, pos + delimiter.length());
                        }
                        if (!saved) {
                            std::cout << "Zone: " << zone_name << " not saved " << std::endl;
                            for (unsigned int i=0; i<nbVertices; ++i)  // Read the zone but not save it...
                            {
                                if (nbPoints == 1) {
                                    if (typstr == "double") {
                                        // Some data
                                        dataFile>>uf;
                                    }
                                    else {
                                        // It is a zone
                                        dataFile>>u;
                                    }
                                }
                                if (nbPoints == 3) {
                                    double x, y, z;
                                    dataFile >> x >> y >> z;  // Read next labels
                                }
                            }
                        }
                    }
                }  // Close while (reading of file)

                std::cout << "Resizing the zones..." << std::endl;

                // -- Resize the tetrahedras and triangles zones
                if (nbTetrahedra > 0) {
                    my_zones.resize(zonenumber.size()); // Resize my_zones to the size of zoenumber (all the unique zones found)
                    //std::cout<<zonenumber.size()<<std::endl;

                    //  For each zone in my_zones store the respective tetrahedras
                    for (unsigned int i=0; i<nbTetrahedra; ++i)
                    {
                        //std::cout<<i<<std::endl;
                        for (unsigned int z=0; z<zonenumber.size(); z++){
                        //std::cout<<zonenumber[i]<<" "<<z<<std::endl;
                            if (tetranumber[i]==zonenumber[z]) {
                                //std::cout<<zonenumber[i]<<" "<<z<<std::endl;
                                my_zones[z].push_back(i);
                            }
                        }
                     }

                    // Store size of each zone
                    for (unsigned z=0;z<zonenumber.size();z++){
                        my_zoneSizes.push_back(my_zones[z].size());
                    }

                    //std::cout<<my_zoneSizes[0]<<" " <<my_zoneSizes[1]<<" "<<my_zoneSizes[2]<<std::endl;
                    my_numberOfZone = zonenumber.size();

                    m_zoneNames.endEdit();
                    m_zoneSizes.endEdit();
                    m_zones.endEdit();
                    m_numberOfZone.endEdit();
                }
                // std::cout<<m_zoneNames.getValue()<<std::endl;

                if (nbTriangles > 0) {
                    // For each zone in my_surfaceZones store the respective triangles
                    my_surfaceZones.resize(surfzonenumber.size());
                    for (unsigned int i=0; i<nbTriangles; ++i)
                    {
                        //std::cout<<i<<std::endl;
                        for (unsigned int z=0;z<surfzonenumber.size();z++)
                        {
                            //std::cout<<surfzonenumber[i]<<" "<<z<<std::endl;
                             if (trinumber[i]==surfzonenumber[z])
                             {
                                //std::cout<<surfzonenumber[i]<<" "<<z<<std::endl;
                                my_surfaceZones[z].push_back(i);
                             }

                         }
                     }

                    for (unsigned z=0;z<surfzonenumber.size();z++)
                    {
                        my_surfaceZoneSizes.push_back(my_surfaceZones[z].size());
                    }
                    // std::cout<<"Surface zones: "<<my_surfaceZones<<std::endl;
                    std::cout<<"Surface zones sizes: "<<my_surfaceZoneSizes[0]<<" " <<my_surfaceZoneSizes[1]<<" "<<my_surfaceZoneSizes[2]<<std::endl;
                    my_numberOfSurfaceZone = surfzonenumber.size();

                    m_surfaceZoneNames.endEdit();
                    m_surfaceZoneSizes.endEdit();
                    m_surfaceZones.endEdit();
                    m_numberOfSurfaceZone.endEdit();
                    std::cout<<m_surfaceZoneNames.getValue()<<std::endl;
                }

                // Resize point zones
                my_pointZones.resize(pointzonenumber.size());
                for (unsigned int i=0; i<nbVertices; ++i)
                {
                    // std::cout << pointnumber[i] << std::endl;
                    for (unsigned z=0;z<pointzonenumber.size();z++){
                        if (pointzonenumber[z]==pointnumber[i]) my_pointZones[z].push_back(i);
                    }
                }
                for (unsigned z=0;z<pointzonenumber.size();z++){
                    my_pointZoneSizes.push_back(my_pointZones[z].size());
                }

                my_numberOfPointZone =  pointzonenumber.size();
                m_pointNames.endEdit();
                m_pointZoneSizes.endEdit();
                m_pointZones.endEdit();
                m_numberOfPointZone.endEdit();
                std::cout<<m_pointNames.getValue()<<std::endl;
                std::cout<<m_pointZoneSizes.getValue()<<std::endl;

                if (my_triangles.size() > 0) {  // Surface triangles present in the cells
                    createEdgeSetArray();
                    std::cout<<"createEdgeArray ok"<<std::endl;
                    createEdgeSetArrayFromTriangle();
                    std::cout<<"createEdgeSetArrayFromTriangle ok"<<std::endl;
                    updateNormals();
                    std::cout<<"normals update ok"<<std::endl;
                    updateMesh();
                    std::cout<<"mesh update ok"<<std::endl;
                }
                else {  // No surface triangles in the cells
                    createEdgeSetArray();
                    std::cout<<"createEdgeArray ok"<<std::endl;
                    createTriangleSetArray();
                    std::cout<<"createTriangleArray ok"<<std::endl;
                    //type::vector<Triangle > my_triangles = triangles.getValue();
                    this->updateMesh();
                    std::cout<<"updatemesh ok"<<std::endl;

                    std::cout<<my_triangles.size()<<std::endl;
                    sofa::component::topology::TetrahedronSetTopologyContainer::SPtr tset = sofa::core::objectmodel::New<sofa::component::topology::TetrahedronSetTopologyContainer>();
                    tset->d_tetrahedron.setValue(d_tetrahedra.getValue());
                    tset->init();

                    for (unsigned int i=0; i<my_triangles.size(); ++i)
                    {
                        if (tset->getTetrahedraAroundTriangle(i).size()==1) my_trianglesSurf.push_back(i);
                    }
                    std::cout<<my_trianglesSurf.size()<<std::endl;
                }

                }  // Close tetrahedral case

                // The triangular case should be reviewed...
                else if (TetraTri.getValue() == "Triangle")
                {
                    std::cout<<TetraTri<<std::endl;
                    return 1;
                    while(!dataFile.eof())
                    {
                        std::getline(dataFile, line);
                        if (line.empty()) continue;
                        std::istringstream ln(line);
                        std::string kw;
                        ln >> kw;
                        if (kw == "POINTS")
                        {
                            int n;
                            std::string typestr;
                            ln >> n >> typestr;
                            std::cout << "Found " << n << " " << typestr << " points" << std::endl;
                            nbVertices=n;
                            for (unsigned int i=0; i<nbVertices; ++i)
                            {
                                double x,y,z;
                                dataFile >> x >> y >> z;
                                my_positions.push_back (type::Vector3(x, y, z));
                            }
                            d_positions.endEdit();
                        }
                        else if ((kw == "CELLS")||(kw=="POLYGONS"))
                        {
                            int n, ni;
                            ln >> n >> ni;
                            std::cout << "Found " << n << " cells" << std::endl;
                            nbCells=n;
                            int num;
                            for (unsigned int i=0; i<nbCells; ++i)
                            {
                                Triangle nodestri;
                                dataFile >> num;
                                 if (num==3){
                                    dataFile >> nodestri[0] >> nodestri[1] >> nodestri[2];
                                    my_triangles.push_back(nodestri);
                                }
                            }

                            nbTriangles=my_triangles.size();
                            std::cout<<nbTriangles<<std::endl;
                            d_triangles.endEdit();
                            std::cout<<d_triangles.getValue().size()<<std::endl;
                        }
                        else if (kw == "CELL_TYPES")
                        {
                            int n;
                            ln >> n;
                            // std::cout<<"cell_type= "<<n<<std::endl;
                        }

                        if (kw == TriZoneName.getValue()) /// Read Surface Zones  2= RV_ENDO, 3= ALL_EPI, 1= LV_ENDO, 0 myo// ON POINTS !! Then we convert to triangles
                        {
                            int u;
                            int n,nu;
                            std::string typstr;
                            ln >> n >> nu >> typstr;

                           type::vector <unsigned int> zonenumber;
                           type::vector <unsigned int> tetranumber;
                           dataFile>>u;
                           zonenumber.push_back(u);
                           tetranumber.push_back(u);
                           std::stringstream oss;
                           oss<<u;
                           std::string name="SurfZone"+oss.str();
                           my_surfaceZoneNames.push_back(name);

                            for (unsigned int i=1; i<nbTriangles; ++i)
                            {
                                dataFile>> u;
                            //	std::cout<<u<<std::endl;
                                tetranumber.push_back(u);
                                bool find=false;
                                unsigned int z=0;
                                for (z=0; z<zonenumber.size();z++){
                                    if(u==zonenumber[z]) {
                                        find =true;
                                        break;
                                    }

                                }
                                if(find==false){
                                    zonenumber.push_back(u);
                                    std::stringstream obb;
                                    obb<<u;
                                    name="SurfZone"+obb.str();
                                    my_surfaceZoneNames.push_back(name);
                                    //std::cout<<name<<std::endl;

                                }

                            }
                            my_surfaceZones.resize(zonenumber.size());
                            //std::cout<<zonenumber.size()<<std::endl;
                             for (unsigned int i=0; i<nbTriangles; ++i)
                            {
                                //std::cout<<i<<std::endl;
                                for (unsigned int z=0;z<zonenumber.size();z++){
                                //	std::cout<<zonenumber[i]<<" "<<z<<std::endl;
                                    if (tetranumber[i]==zonenumber[z]) {
                                        //std::cout<<zonenumber[i]<<" "<<z<<std::endl;
                                        my_surfaceZones[z].push_back(i);
                                    }

                                }
                             }

                             for (unsigned z=0;z<zonenumber.size();z++){
                                 my_surfaceZoneSizes.push_back(my_surfaceZones[z].size());
                             }

                            std::cout<<my_surfaceZoneSizes[0]<<" " <<my_surfaceZoneSizes[1]<<" "<<my_surfaceZoneSizes[2]<<std::endl;
                            my_numberOfSurfaceZone =  zonenumber.size();

                            m_surfaceZoneNames.endEdit();
                            m_surfaceZoneSizes.endEdit();
                            m_surfaceZones.endEdit();
                            m_numberOfSurfaceZone.endEdit();
                             std::cout<<m_surfaceZoneNames.getValue()<<std::endl;

                        }

                    }
                createEdgeSetArrayFromTriangle();
                std::cout<<"createEdgeSetArrayFromTriangle ok"<<std::endl;
                updateNormals();
                updateMesh();
                } // End of triangular case
            return true;
            }  // Close function

            void CardiacVTKLoader::createEdgeSetArray()
            {
               type::vector<Edge> & m_edge=*(d_edges.beginEdit());
              // create a temporary map to find redundant edges
              std::map<Edge,unsigned int> edgeMap;

              sofa::type::vector<Tetrahedron> m_tetrahedron = d_tetrahedra.getValue();
             const unsigned int edgesInTetrahedronArray[6][2] = {{0,1}, {0,2}, {0,3}, {1,2}, {1,3}, {2,3}};
              /// create the m_edge array at the same time than it fills the m_edgesInTetrahedron array
              for (unsigned int i = 0; i < m_tetrahedron.size(); ++i)
              {
                const Tetrahedron &t = m_tetrahedron[i];
                for (unsigned int j=0; j<6; ++j)
                {
                  const unsigned int v1 = t[edgesInTetrahedronArray[j][0]];
                  const unsigned int v2 = t[edgesInTetrahedronArray[j][1]];

                  // sort vertices in lexicographic order
                  const Edge e((v1<v2) ? Edge(v1,v2) : Edge(v2,v1));

                      if (edgeMap.find(e)==edgeMap.end())
                      {
                              // edge not in edgeMap so create a new one
                              const int edgeIndex = edgeMap.size();
                              edgeMap[e] = edgeIndex;
                              m_edge.push_back(e);
                      }
                    }
              }
              d_edges.endEdit();
              std::cout<<d_edges.getValue().size()<<std::endl;
            }

            void CardiacVTKLoader::createEdgeSetArrayFromTriangle()
            {

               type::vector<Edge> & m_edge=*(d_edges.beginEdit());
              // create a temporary map to find redundant edges
              std::map<Edge,unsigned int> edgeMap;
              sofa::type::vector<Triangle> m_triangles = d_triangles.getValue();

             const unsigned int edgesInTriangleArray[3][2] = {{0,1}, {1,2}, {2,0}};
              /// create the m_edge array at the same time than it fills the m_edgesInTetrahedron array
              for (unsigned int i = 0; i < m_triangles.size(); ++i)
              {
                const Triangle &t = m_triangles[i];
                for (unsigned int j=0; j<3; ++j)
                {
                  const unsigned int v1 = t[edgesInTriangleArray[j][0]];
                  const unsigned int v2 = t[edgesInTriangleArray[j][1]];

                  // sort vertices in lexicographic order
                  const Edge e((v1<v2) ? Edge(v1,v2) : Edge(v2,v1));

                      if (edgeMap.find(e)==edgeMap.end())
                      {
                              // edge not in edgeMap so create a new one
                              const int edgeIndex = edgeMap.size();
                              edgeMap[e] = edgeIndex;
                              m_edge.push_back(e);
                      }
                    }
              }
			 // std::cout<<"ici"<<std::endl;
              d_edges.endEdit();
              std::cout<<d_edges.getValue().size()<<std::endl;
            }

			void CardiacVTKLoader::createTriangleSetArray(){
                 type::vector<Tetrahedron > m_tetrahedron = d_tetrahedra.getValue();
                 type::vector<Triangle >& my_triangles = *(d_triangles.beginEdit());

				 const unsigned int trianglesInTetrahedronArray[4][3]={{0,1,2},{1,3,2},{0,3,1},{0,2,3}};
				 const Tetrahedron &t = m_tetrahedron[0];
				 for (unsigned int j=0;j<4;j++){
						 const unsigned int v1 = t[trianglesInTetrahedronArray[j][0]];
					     const unsigned int v2 = t[trianglesInTetrahedronArray[j][1]];
						 const unsigned int v3 = t[trianglesInTetrahedronArray[j][2]];

						 Triangle tri(v1,v2,v3);
						 my_triangles.push_back(tri);
				 }


				 for (unsigned int i=1;i<m_tetrahedron.size();i++){
					// std::cout<<i<<std::endl;
					 bool find=false;
					 const Tetrahedron &t = m_tetrahedron[i];
					 for (unsigned int j=0;j<4;j++){
						 const unsigned int v1 = t[trianglesInTetrahedronArray[j][0]];
					     const unsigned int v2 = t[trianglesInTetrahedronArray[j][1]];
						 const unsigned int v3 = t[trianglesInTetrahedronArray[j][2]];


						 for( unsigned int a=0; a<my_triangles.size();a++){
							 //check if triangles already exists
							 Triangle at=my_triangles[a];

							 if((at[0]==v1)&&(at[1]==v2)&&(at[2]==v3)) {find=true; break; }
							else if ((at[0]==v2)&&(at[1]==v3)&&(at[2]==v1)) {find=true; break;}
							else if ((at[0]==v3)&&(at[1]==v1)&&(at[2]==v2)) {find=true; break;}
							else if ((at[0]==v2)&&(at[1]==v1)&&(at[2]==v3)) {find=true; break;}
							else if ((at[0]==v1)&&(at[1]==v3)&&(at[2]==v2)) {find=true; break;}
							else if ((at[0]==v3)&&(at[1]==v2)&&(at[2]==v1)) {find=true; break;}
						 }

						 if (!find){
							// std::cout<<i<<std::endl;
						 Triangle tri(v1,v2,v3);
						  my_triangles.push_back(tri);
						 }
					 }

				 }


                 d_triangles.endEdit();

				}

            void CardiacVTKLoader::updateNormals()
            {
                //  normals.beginEdit();
                type::vector<sofa::type::Vector3> my_positions = d_positions.getValue();
                type::vector< Triangle > m_triangle = d_triangles.getValue();

                //  type::vector<Tetrahedron > m_tetrahedron = d_tetrahedra.getValue();
                type::vector<sofa::type::Vec<3,SReal> >& my_normals = *(d_normals.beginEdit());
                my_normals.resize(my_positions.size());
                for (unsigned int i = 0; i < m_triangle.size() ; i++)
                {
                      const sofa::type::Vec<3,SReal>  v1 = my_positions[m_triangle[i][0]];
                      const sofa::type::Vec<3,SReal>  v2 = my_positions[m_triangle[i][1]];
                      const sofa::type::Vec<3,SReal>  v3 = my_positions[m_triangle[i][2]];
                      sofa::type::Vec<3,SReal> n = cross(v2-v1, v3-v1);

                      n.normalize();
                      my_normals[m_triangle[i][0]] += n;
                      my_normals[m_triangle[i][1]] += n;
                      my_normals[m_triangle[i][2]] += n;


                }
               /*   for (unsigned int i = 0; i < m_tetrahedron.size() ; i++)
                  {
                      const sofa::type::Vec<3,SReal> & v1 = my_positions[m_tetrahedron[i][0]];
                      const sofa::type::Vec<3,SReal> & v2 = my_positions[m_tetrahedron[i][1]];
                      const sofa::type::Vec<3,SReal> & v3 = my_positions[m_tetrahedron[i][2]];
                      const sofa::type::Vec<3,SReal> & v4 = my_positions[m_tetrahedron[i][3]];
                      sofa::type::Vec<3,SReal> n1 = cross(v2-v1, v4-v1);
                              sofa::type::Vec<3,SReal> n2 = cross(v3-v2, v1-v2);
                              sofa::type::Vec<3,SReal> n3 = cross(v4-v3, v2-v3);
                              sofa::type::Vec<3,SReal> n4 = cross(v1-v4, v3-v4);
                      n1.normalize(); n2.normalize(); n3.normalize(); n4.normalize();
                      my_normals[m_tetrahedron[i][0]] += n1;
                      my_normals[m_tetrahedron[i][1]] += n2;
                      my_normals[m_tetrahedron[i][2]] += n3;
                      my_normals[m_tetrahedron[i][3]] += n4;
                  }*/

                      for (unsigned int i = 0; i < my_normals.size(); i++)
                      {
                              my_normals[i].normalize();
                      }
                      d_normals.endEdit();
            }

            void CardiacVTKLoader::createFiberDirection(double angleEpi, double angleEndo)
            {
                nodeFibers.open((outputnodeFibers.getValue()).c_str(),std::ios::out);
                tetraFibers.open((outputtetraFibers.getValue()).c_str(),std::ios::out);

                std::cout<<"Creating Fiber Directions"<<std::endl;
                sofa::component::topology::TetrahedronSetTopologyContainer::SPtr tset = sofa::core::objectmodel::New<sofa::component::topology::TetrahedronSetTopologyContainer>();

                //sofa::component::topology::TetrahedronSetTopologyContainer *tset=new sofa::component::topology::TetrahedronSetTopologyContainer();

                tset->d_tetrahedron.setValue(d_tetrahedra.getValue());
                tset->init();
                /////////////////////
                // Initialisations //
                /////////////////////

                // On recupere les triangles, les edges et les vertex classés par zone
                // Et on pose des iterateurs par dessus
                type::vector<sofa::type::Vector3> my_normals = d_normals.getValue();
                type::vector<sofa::type::Vector3> my_positions = d_positions.getValue();
                type::vector<sofa::type::Vector3> anisotropicDirection;
                type::vector<double> anisotropicAngle;
                type::vector <unsigned int> triangle_set[3] =  {(m_surfaceZones.getValue())[0],(m_surfaceZones.getValue())[1],(m_surfaceZones.getValue())[2]};
                type::vector <unsigned int>::iterator triangle_set_iterator[3] = {triangle_set[0].begin(),triangle_set[1].begin(),triangle_set[2].begin()};
                type::vector <unsigned int> vertex_set[3];
                type::vector <unsigned int>::iterator vertex_set_iterator[3];
                type::vector <unsigned int> edge_set[3];
                type::vector <unsigned int>::iterator edge_set_iterator[3];
                for(int i=0;i<3;i++) {
                    for(;triangle_set_iterator[i]!=triangle_set[i].end();++triangle_set_iterator[i]) {
                        type::fixed_array<unsigned int,3> edgei;
                        edgei=tset->getEdgesInTriangle(*triangle_set_iterator[i]);
                        unsigned int tt=*triangle_set_iterator[i];
                        Triangle tr=tset->getTriangle(tt);
                        for (int j=0;j<3;j++) {
                            vertex_set[i].push_back(tr[j]);
                            edge_set[i].push_back(edgei[j]);
                        }
                    }
                }

                // Déclarations
                unsigned int vertexA,vertexB;
                unsigned int plus_proche_vertex[3];
                unsigned int plus_proche_edge[3];
                unsigned int plus_proche_triangle[3];



                unsigned int vertex_courant;
                unsigned int tetra_courant;
                sofa::type::Vector3 posP,dir,dir2;

                sofa::type::Vector3 M,A,B,C,P,u,normale,AB,AC;
                sofa::type::Vector3 PA,PB,PC,PBPC,PCPA,PAPB,vertical,P1,P2;
                sofa::type::Vector3 axe,circumferential,normaleA,normaleB,normaleC,normale1,normale2;

                double projete[3][3][3];
                double distance,pA,pB,pP1,pP2,angle,norme1,norme2,normeA,normeB,p,p_old;
                double distance_zone[3];
                double distance_min[3][3];
                int plus_proche_zone1,plus_proche_zone2;

                int plus_proche_objet[3];

                // Calcul du grand axe d'inertie du LV
                //computeMassOnVertices();

                /// updateLVlongAxis();
                type::vector <unsigned int> vset=vertex_set[0];
                type::vector <unsigned int>::iterator vsetit=vset.begin();
                double m = 0.0;
                sofa::type::Vector3 LVbarycenter;
                LVbarycenter.clear();
                for(;vsetit!=vset.end();++vsetit) {
                    LVbarycenter += my_positions[*vsetit];
                    m += 1;
                }
                LVbarycenter /= m;
                std::cout<<LVbarycenter<<std::endl;

                /// Get the edges on the borders
                std::vector<Edge> EdgesBo;
                std::pair<unsigned int,unsigned int> edgetri;
                for (unsigned int i=0; i<triangle_set[0].size(); i++){
                    unsigned int tri=(triangle_set[0])[i];
                    type::fixed_array<unsigned int,3> EdgeInTri=tset->getEdgesInTriangle(tri);
                    for(unsigned int u=0; u<3; u++){
                        std::vector<unsigned int> AroundEdge=tset->getTrianglesAroundEdge(EdgeInTri[u]);
                        int number=0;
                        for(unsigned int j=0; j< AroundEdge.size();j++){

                            for (unsigned int k=0; k<triangle_set[0].size();k++){

                                if (AroundEdge[j]==(triangle_set[0])[k]){
                                    number++;
                                    edgetri.first=EdgeInTri[u];
                                    edgetri.second=AroundEdge[j];
                                    break;
                                }
                            }
                        }
                        if (number==1) {
                            Edge newedge;
                            Triangle tr=tset->getTriangle(edgetri.second);
                            Edge EdgesTri=tset->getEdge(edgetri.first);
                            unsigned int k=EdgesTri[0];
                            unsigned int l=EdgesTri[1];
                            if(((tr[0]==k)&&(tr[1]==l))||((tr[1]==k)&&(tr[2]==l))||((tr[2]==k)&&(tr[0]==l))){
                                newedge[0]=l;
                                newedge[1]=k;
                            }
                            else{
                                newedge[0]=k;
                                newedge[1]=l;
                            }
                            EdgesBo.push_back(newedge);

                        }
                    }
                }

                sofa::type::Vector3 holeCenter;
                double n=0.0;
                for(unsigned int i=0;i<EdgesBo.size();i++) {
                    Edge ei=EdgesBo[i];

                    holeCenter += my_positions[ei[0]];
                    ++n;
                }
                holeCenter /= n;
                std::cout<<holeCenter<<std::endl;



                for (unsigned int i = 0; i < 3; i++)
                    axe[i] = LVbarycenter[i] - holeCenter[i];

                //////////////////////////////////////////////////
                // Calcul du sens des fibres pour chaque vertex //
                //////////////////////////////////////////////////

                std::cout<<"Application du modele de fibres synthetiques au maillage..."<<std::endl;
                p_old = -1;
                int cccc = 0;
                int nb_vertex = tset->getNbPoints();

                nodeFibers<<nb_vertex<<" "<<3<<std::endl;
                anisotropicDirection.resize(nb_vertex);
                anisotropicAngle.resize(nb_vertex);
                vertex_courant = 0;
                for(;vertex_courant!=tset->getNbPoints();++vertex_courant) {


                    //////////////////////////////////////////////////////////
                    // Calcul des projetés sur les 2 zones les plus proches //
                    //////////////////////////////////////////////////////////

                    // Le point courant
                    M = my_positions[vertex_courant];
                    // On initialise les distances minimales
                    // En ligne : LV, RV, Epicardium
                    // En colonne : vertex, edge, triangle
                    for(int i=0;i<3;i++) for (int j=0;j<3;j++) distance_min[i][j] = 1e10;

                    // On trouve la distance min aux vertex
                    for(int i=0;i<3;i++) {
                        vertex_set_iterator[i] = vertex_set[i].begin();
                        for(;vertex_set_iterator[i]!=vertex_set[i].end();++vertex_set_iterator[i]) {


                            // Calcul de la distance aux vertex P
                            posP = my_positions[(*vertex_set_iterator[i])];
                            P = posP;
                            sofa::type::Vector3 v = P - M;
                            distance = v.norm();

                            // On met à jour la distance min
                            if(distance<distance_min[i][0]) {
                                distance_min[i][0] = distance;
                                projete[i][0][0] = P[0];
                                projete[i][0][1] = P[1];
                                projete[i][0][2] = P[2];
                                plus_proche_vertex[i] = (*vertex_set_iterator[i]);
                            }

                        }
                    }

                    // On trouve la distance min aux edges [AB]
                    for(int i=0;i<3;i++) {
                        edge_set_iterator[i] = edge_set[i].begin();
                        for(;edge_set_iterator[i]!=edge_set[i].end();++edge_set_iterator[i]) {
                            Edge ei=tset->getEdge(*edge_set_iterator[i]);
                            // Calcul du projeté P du vertex courant M sur la droite (AB)
                            A= my_positions[ei[0]];
                            B= my_positions[ei[1]];

                            if ( (B-A).norm() )
                                u = (B-A) / (B-A).norm();
                            else
                                u[0] = u[1] = u[2] = 0;
                            P = A + u*dot(M-A,u);

                            // On vérifie que P appartienne bien à [AB]
                            if(dot(A-P,B-P) > 0) {
                                distance = 1e10;
                            } else {
                                distance = (P-M).norm();
                            }

                            // On met à jour la distance min
                            if(distance<distance_min[i][1]) {
                                distance_min[i][1] = distance;
                                projete[i][1][0] = P[0];
                                projete[i][1][0] = P[1];
                                projete[i][1][0] = P[2];
                                plus_proche_edge[i] = (*edge_set_iterator[i]);
                            }

                        }
                    }


                    // On trouve la distance min aux triangles (ABC)
                    for(int i=0;i<3;i++) {
                        triangle_set_iterator[i] = triangle_set[i].begin();
                        for(;triangle_set_iterator[i]!=triangle_set[i].end();++triangle_set_iterator[i]) {

                            Triangle tr=tset->getTriangle(*triangle_set_iterator[i]);

                            // Calcul du projeté P du vertex courant M sur le triangle (ABC)
                            A= my_positions[tr[0]];
                            B= my_positions[tr[1]];
                            C= my_positions[tr[2]];
                            AB = B-A; AC = C-A;
                            normale(0) = (AB(1)*AC(2))-(AB(2)*AC(1));
                            normale(1) = (AB(2)*AC(0))-(AB(0)*AC(2));
                            normale(2) = (AB(0)*AC(1))-(AB(1)*AC(0));
                            double aireABC = normale.norm();
                            if(aireABC > 0) {
                                normale /= aireABC;
                                P = M + normale*dot(normale,A-M);
                                PA=A-P;
                                PB=B-P;
                                PC=C-P;

                                // On vérifie que P appartienne bien à (ABC)
                                PAPB(0) = (PA(1)*PB(2))-(PA(2)*PB(1));
                                PAPB(1) = (PA(2)*PB(0))-(PA(0)*PB(2));
                                PAPB(2) = (PA(0)*PB(1))-(PA(1)*PB(0));
                                PBPC(0) = (PB(1)*PC(2))-(PB(2)*PC(1));
                                PBPC(1) = (PB(2)*PC(0))-(PB(0)*PC(2));
                                PBPC(2) = (PB(0)*PC(1))-(PB(1)*PC(0));
                                PCPA(0) = (PC(1)*PA(2))-(PC(2)*PA(1));
                                PCPA(1) = (PC(2)*PA(0))-(PC(0)*PA(2));
                                PCPA(2) = (PC(0)*PA(1))-(PC(1)*PA(0));
                                if(dot(PAPB,PBPC)>0 && dot(PBPC,PCPA)>0 && dot(PCPA,PAPB)>0) {
                                    distance = (P-M).norm();
                                } else {
                                    distance = 1e10;
                                }

                                // On met à jour la distance min
                                if(distance<distance_min[i][2]) {
                                    distance_min[i][2] = distance;
                                    projete[i][2][0] = P[0];
                                    projete[i][2][1] = P[1];
                                    projete[i][2][2] = P[2];
                                    plus_proche_triangle[i] = (*triangle_set_iterator[i]);
                                }

                            }
                        }
                    }

                    // On détermine le point le plus proche sur les 3 de chaque zone surfacique
                    for(int i=0;i<3;i++) {
                        if(distance_min[i][0]<=distance_min[i][1] && distance_min[i][0]<=distance_min[i][2]) {
                            // Le point le plus proche est un vertex
                            distance_zone[i] = distance_min[i][0];
                            plus_proche_objet[i] = 0;
                        }
                        else if(distance_min[i][1]<=distance_min[i][2]) {
                            // Le point le plus proche est sur un edge
                            distance_zone[i] = distance_min[i][1];
                            plus_proche_objet[i] = 1;
                        }
                        else {
                            // Le point le plus proche est sur un triangle
                            distance_zone[i] = distance_min[i][2];
                            plus_proche_objet[i] = 2;
                        }
                    }

                    // On détermine les 2 zones les plus proches sur les 3
                    // WARNING : P1 doit toujours être sur l'épicarde !!!
                    if(distance_zone[0]<=distance_zone[1] && distance_zone[0]<=distance_zone[2]) {
                        // La zone la plus proche est LV
                        if(distance_zone[1]<=distance_zone[2]) {
                            // La zone la deuxième plus proche est RV
                            plus_proche_zone1 = 1;
                            plus_proche_zone2 = 0;
                        } else {
                            // La zone la deuxième plus proche est Epicardium
                            plus_proche_zone1 = 2;
                            plus_proche_zone2 = 0;
                        }
                    }
                    else if(distance_zone[1]<=distance_zone[2]) {
                        // La zone la plus proche est RV
                        if(distance_zone[0]<=distance_zone[2]) {
                            // La zone la deuxième plus proche est LV
                            plus_proche_zone1 = 1;
                            plus_proche_zone2 = 0;
                        } else {
                            // La zone la deuxième plus proche est Epicardium
                            plus_proche_zone1 = 2;
                            plus_proche_zone2 = 1;
                        }
                    } else {
                        // La zone la plus proche est Epicardium
                        if(distance_zone[0]<=distance_zone[1]) {
                            // La zone la deuxième plus proche est LV
                            plus_proche_zone1 = 2;
                            plus_proche_zone2 = 0;
                        } else {
                            // La zone la deuxième plus proche est RV
                            plus_proche_zone1 = 2;
                            plus_proche_zone2 = 1;
                        }
                    }

                    ////////////////////////////////////////
                    // Calcul de l'orientation des fibres //
                    ////////////////////////////////////////

                    // Les projetés
                    P1[0] = projete[plus_proche_zone1][plus_proche_objet[plus_proche_zone1]][0];
                    P1[1] = projete[plus_proche_zone1][plus_proche_objet[plus_proche_zone1]][1];
                    P1[2] = projete[plus_proche_zone1][plus_proche_objet[plus_proche_zone1]][2];
                    P2[0] = projete[plus_proche_zone2][plus_proche_objet[plus_proche_zone2]][0];
                    P2[1] = projete[plus_proche_zone2][plus_proche_objet[plus_proche_zone2]][1];
                    P2[2] = projete[plus_proche_zone2][plus_proche_objet[plus_proche_zone2]][2];
                    // Calcul des normales sur les 2 projetés


                    // Projeté 1
                    if(plus_proche_objet[plus_proche_zone1]== 0){
                            // Si c'est sur un vertex

                            normale1 = my_normals[plus_proche_vertex[plus_proche_zone1]];


                        }
                    else if(plus_proche_objet[plus_proche_zone1]== 1){
                            // Si c'est sur un edge (interpolation des normales aux extrémités)
                            Edge et=tset->getEdge(plus_proche_edge[plus_proche_zone1]);

                            vertexA = et[0];
                            vertexB = et[1];
                            A = my_positions[vertexA];
                            B = my_positions[vertexB];
                            normaleA=my_normals[vertexA];
                            normaleB=my_normals[vertexB];
                            normeA = (B-P1).norm();
                            normeB = (P1-A).norm();
                            if(normeA+normeB>0) {
                                pA = normeA/(normeA+normeB);
                                pB = normeB/(normeA+normeB);
                            } else {
                                pA = 0.5;
                                pB = 0.5;
                            }
                            normale1 = (normaleA*pA + normaleB*pB) ;
                            normale1/= normale1.norm();

                            }
                    else if(plus_proche_objet[plus_proche_zone1]== 2){
                            // Si c'est sur un triangle
                            Triangle tr=tset->getTriangle(plus_proche_triangle[plus_proche_zone1]);
                            A = my_positions[tr[0]];
                            B = my_positions[tr[1]];
                            C = my_positions[tr[2]];

                            normale1 = cross(B-A,C-A);
                            normale1/= normale1.norm();

                        }

                    if(plus_proche_objet[plus_proche_zone2]== 0){
                            // Si c'est sur un vertex

                            normale2 = my_normals[plus_proche_vertex[plus_proche_zone2]];


                        }
                    else if(plus_proche_objet[plus_proche_zone2]== 1){
                            // Si c'est sur un edge (interpolation des normales aux extrémités)
                            Edge et=tset->getEdge(plus_proche_edge[plus_proche_zone2]);
                            vertexA = et[0];
                            vertexB = et[1];
                            A = my_positions[vertexA];
                            B = my_positions[vertexB];
                            normaleA=my_normals[vertexA];
                            normaleB=my_normals[vertexB];
                            normeA = (B-P1).norm();
                            normeB = (P1-A).norm();
                            if(normeA+normeB>0) {
                                pA = normeA/(normeA+normeB);
                                pB = normeB/(normeA+normeB);
                            } else {
                                pA = 0.5;
                                pB = 0.5;
                            }
                            normale2 = (normaleA*pA + normaleB*pB) ;
                            normale2/= normale2.norm();

                            }
                    else if(plus_proche_objet[plus_proche_zone2]== 2){
                            // Si c'est sur un triangle
                            Triangle tr=tset->getTriangle(plus_proche_triangle[plus_proche_zone2]);
                            A = my_positions[tr[0]];
                            B = my_positions[tr[1]];
                            C = my_positions[tr[2]];
                            normale2 = cross(B-A,C-A);
                            normale2/= normale2.norm();

                        }


                    // Les normales doivent pointer dans le même sens
                    if(dot(normale1,normale2)<0) normale2 = -normale2;

                    // On calcule la normale et l'angle de rotation sur le point courant par interpolation
                    norme1 = (P2-M).norm();
                    norme2 = (M-P1).norm();
                    if(norme1+norme2>0) {
                        pP1 = norme1/(norme1+norme2);
                        pP2 = norme2/(norme1+norme2);
                    } else {
                        pP1 = 0.5;
                        pP2 = 0.5;
                    }
                    normale = normale1*pP1 +normale2*pP2;
                    if ( normale.norm() )
                        normale /= (normale).norm();
                    angle = angleEpi*pP1 + angleEndo*pP2;



                    // On calcule la direction principale
                    circumferential(0) = (axe(1)*normale(2))-(axe(2)*normale(1));
                    circumferential(1) = (axe(2)*normale(0))-(axe(0)*normale(2));
                    circumferential(2) = (axe(0)*normale(1))-(axe(1)*normale(0));
                    if ( (circumferential).norm() )
                        circumferential /= (circumferential).norm();
                    vertical(0) = (normale(1)*circumferential(2))-(normale(2)*circumferential(1));
                    vertical(1) = (normale(2)*circumferential(0))-(normale(0)*circumferential(2));
                    vertical(2) = (normale(0)*circumferential(1))-(normale(1)*circumferential(0));
                    if ( (vertical).norm() )
                        vertical /= (vertical).norm();

                    // On applique la rotation
                    //normale *= angle/180.0*3.141592654; // angle from circumferential direction in the plane (circumferential,vertical)
                    angle= -angle/180.0*3.141592654; // angle from circumferential direction in the plane (circumferential,vertical)

                    Matrix3 rot,P,Id,Q;
                    Id.identity();
                    for (int i=0;i<3;i++){
                        for (int j=0;j<3;j++){
                            P[i][j]=normale[i]*normale[j];
                        }
                    }
                    Q.clear();
                    Q[0][1]=-normale(2);
                    Q[0][2]=normale(1);
                    Q[1][0]=normale(2);
                    Q[2][0]=-normale(2);
                    Q[1][2]=-normale(0);
                    Q[2][1]=normale(0);
                    rot=P+(Id-P)*cos(angle)+Q*sin(angle);
                    //  norm=(normale(0),normale(1),normale(2));

                    dir2=rot*circumferential;

                    nodeFibers<<dir2(0)<<" "<<dir2(1)<<" "<<dir2(2)<<std::endl;
                    // On enregistre la nouvelle direction et l'angle du vertex
                    anisotropicDirection[vertex_courant]=dir2;
                    anisotropicAngle[vertex_courant]=angle;
                     cccc++;
                                            // Progression
                                            p = floor((double)cccc/nb_vertex*100.0);
                                            if(p!=p_old) {
                                                std::cerr<<"\r"<<p<<" %           ";
                                                    p_old = p;
                                            }
                }
                nodeFibers.close();

                 /////////////////////////////////////////////////////
                                    // Calcul du sens des fibres pour chaque tétraèdre //
                                    /////////////////////////////////////////////////////

                tetra_courant = 0;
                int Nbtetra=tset->getNbTetrahedra();
                tetraFibers<<Nbtetra<<" 3"<<std::endl;
                for(;tetra_courant!=Nbtetra;++tetra_courant) {

                    dir  = anisotropicDirection[tset->getTetrahedron(tetra_courant)[0]];
                    dir += anisotropicDirection[tset->getTetrahedron(tetra_courant)[1]];
                    dir += anisotropicDirection[tset->getTetrahedron(tetra_courant)[2]];
                    dir += anisotropicDirection[tset->getTetrahedron(tetra_courant)[3]];
                    if (dir.norm()!=0.0) {
                        tetraFibers<<dir(0)/dir.norm()<<" "<<dir(1)/dir.norm()<<" "<<dir(2)/dir.norm()<<std::endl;

                    }
                    else {
                        tetraFibers<<dir(0)<<" "<<dir(1)<<" "<<dir(2)<<std::endl;

                    }

                }
                tetraFibers.close();
                            //delete tset;


            }

            void CardiacVTKLoader::readFibreDirection()
            {

                FILE* file;
                char cmd[1024];


                const char* fileFiber = m_nodeFiber_filename.getFullPath().c_str();
                std::string sfileFiber (fileFiber);

                // -- Check file extension.
                std::string extension;
                size_t pointPosition = sfileFiber.find_last_of (".");

                if (pointPosition != sfileFiber.npos)
                    extension = sfileFiber.substr (pointPosition+1);

                if (extension != "bb")
                {
                    std::cerr << "Error: CardiacVTKLoader: wrong file format '" << m_nodeFiber_filename << "' as fiber file." << std::endl;
                    return;
                }

                // -- Check if file exist:
                if (!sofa::helper::system::DataRepository.findFile(sfileFiber))
                {
                    std::cerr << "Error: CardiacVTKLoader: File '" << m_nodeFiber_filename << "' not found. " << std::endl;
                    return;
                }

                // -- Check if file is readable:
                if ((file = fopen(fileFiber, "r")) == NULL)
                {
                    std::cerr << "Error: CardiacVTKLoader: Cannot read file '" << m_nodeFiber_filename << "'." << std::endl;
                    return;
                }

                // -- Check first line.
                if (!readLine(cmd, sizeof(cmd), file))
                {
                    std::cerr << "Error: CardiacVTKLoader: Cannot read first line in file '" << m_nodeFiber_filename << "'." << std::endl;
                    fclose(file);
                    return;
                }
                fclose(file);


                // -- Reading File

                std::ifstream dataFile (fileFiber);

                type::vector < sofa::type::Vec<3,SReal> >& my_nodeFibers = *(m_nodeFibers.beginEdit());

                // Reading tab size of file:
                unsigned int line, column;

                dataFile >> line >> column;

                //std::cout << "Number of fibres: " << line << std::endl;
                //std::cout << "Number of columns: " << column << std::endl;

                //Test number of fiber....
                //if (line != (position.getValue()).size() - virtualVertex)
                //CERR << "Wrong number of lines" << ENDL;
                //return;
                //}

                if (column != 3)
                {
                    std::cerr << "Wrong number of columns" << std::endl;
                    return;
                }

                for (unsigned int i=0; i<line; i++) // Go over all the lines
                {
                    type::Vector3 fibre;
                    dataFile >> fibre[0] >> fibre[1] >> fibre[2]; // Get the data of the current fibre
                    //std::cout << fibre << std::endl;
                    if ( fibre.norm() == 0.0)
                        std::cerr << "Warning: CardiacVTKLoader::readFibreDirection Fibre N'" << i <<"' is null." << std::endl;

                    fibre.normalize();

                    my_nodeFibers.push_back (fibre);
                }

                m_nodeFibers.endEdit();


            }





            // We assume here that there is no null local fibre direction. To avoid this
            // issue be sure that you generated .lbb file by correctly opening the .tbb
            // file with yav and saving the result as .lbb. A log message should appear
            // confirming that the null fibres have been completed
            void CardiacVTKLoader::readTetraLocalFibreDirection()
            {

                FILE* file;
                char cmd[1024];

                const char* fileFiber = m_facetBFiber_filename.getFullPath().c_str();
                std::string sfileFiber (fileFiber);

                // -- Check file extension.
                std::string extension;
                size_t pointPosition = sfileFiber.find_last_of (".");

                if (pointPosition != sfileFiber.npos)
                    extension = sfileFiber.substr (pointPosition+1);

                if (extension != "lbb")
                {
                    std::cerr << "Error: CardiacVTKLoader: wrong file format '" << m_facetBFiber_filename << "' as tetra local fiber file." << std::endl;
                    return;
                }

                // -- Check if file exist:
                if (!sofa::helper::system::DataRepository.findFile(sfileFiber))
                {
                    std::cerr << "Error: CardiacVTKLoader: File '" << m_facetBFiber_filename << "' not found. " << std::endl;
                    return;
                }

                // -- Check if file is readable:
                if ((file = fopen(fileFiber, "r")) == NULL)
                {
                    std::cerr << "Error: CardiacVTKLoader: Cannot read file '" << m_facetBFiber_filename << "'." << std::endl;
                    return;
                }

                // -- Check first line.
                if (!readLine(cmd, sizeof(cmd), file))
                {
                    std::cerr << "Error: CardiacVTKLoader: Cannot read first line in file '" << m_facetBFiber_filename << "'." << std::endl;
                    fclose(file);
                    return;
                }
                fclose(file);


                // -- Reading File

                std::ifstream dataFile (fileFiber);

                type::vector < sofa::type::Vec<3,SReal> >& my_facetBFibers = *(m_facetBFibers.beginEdit());


                // Reading tab size of file:
                unsigned int line, column;
                dataFile >> line >> column;

                if (column != 3)
                {
                    std::cerr << "Wrong number of columns" << std::endl;
                    return;
                }

                for (unsigned int i=0; i<line; i++)
                {
                    type::Vector3 fibre;
                    dataFile >> fibre[0] >> fibre[1] >> fibre[2];

                    if ( fibre.norm() == 0.0)
                        std::cerr << "Warning: CardiacVTKLoader::readTetraLocalFibreDirection Fibre N'" << i <<"' is null." << std::endl;

                    fibre.normalize();

                    my_facetBFibers.push_back (fibre);
                }

                m_facetBFibers.endEdit();




            }




            void CardiacVTKLoader::readTetraFibreDirection()
            {
                FILE* file;
                char cmd[1024];


                const char* fileFiber = m_facetFiber_filename.getFullPath().c_str();
                std::string sfileFiber (fileFiber);

                // -- Check file extension.
                std::string extension;
                size_t pointPosition = sfileFiber.find_last_of (".");

                if (pointPosition != sfileFiber.npos)
                    extension = sfileFiber.substr (pointPosition+1);

                if (extension != "tbb")
                {
                    std::cerr << "Error: CardiacVTKLoader: wrong file format '" << m_facetFiber_filename << "' as tetra fiber file." << std::endl;
                    return;
                }

                // -- Check if file exist:
                if (!sofa::helper::system::DataRepository.findFile(sfileFiber))
                {
                    std::cerr << "Error: CardiacVTKLoader: File '" << m_facetFiber_filename << "' not found. " << std::endl;
                    return;
                }

                // -- Check if file is readable:
                if ((file = fopen(fileFiber, "r")) == NULL)
                {
                    std::cerr << "Error: CardiacVTKLoader: Cannot read file '" << m_facetFiber_filename << "'." << std::endl;
                    return;
                }

                // -- Check first line.
                if (!readLine(cmd, sizeof(cmd), file))
                {
                    std::cerr << "Error: CardiacVTKLoader: Cannot read first line in file '" << m_facetFiber_filename << "'." << std::endl;
                    fclose(file);
                    return;
                }
                fclose(file);


                // -- Reading File

                std::ifstream dataFile (fileFiber);

                type::vector < sofa::type::Vec<3,SReal> >& my_facetFibers = *(m_facetFibers.beginEdit());

                // Reading tab size of file:
                unsigned int line, column;

                dataFile >> line >> column;

                //Test number of fiber....
                //if (line != (position.getValue()).size() - virtualVertex)
                //CERR << "Wrong number of lines" << ENDL;
                //return;
                //}

                if (column != 3)
                {
                    std::cerr << "Wrong number of columns" << std::endl;
                    return;
                }

                for (unsigned int i=0; i<line; i++)
                {
                    type::Vector3 fibre;
                    dataFile >> fibre[0] >> fibre[1] >> fibre[2];

                    if ( fibre.norm() == 0.0)
                        std::cerr << "Warning: CardiacVTKLoader::readTetraFibreDirection Fibre N'" << i <<"' is null." << std::endl;

                    fibre.normalize();


                    my_facetFibers.push_back (fibre);
                }

                m_facetFibers.endEdit();


            }


            void CardiacVTKLoader::readContractionParametersFile()
            {
                std::cout<<"readContractionParameters()"<<std::endl;
                FILE* file;
                char cmd[1024];


                const char* fileparam = m_contractionParameters_filename.getFullPath().c_str();
                std::string sfileparam (fileparam);

                // -- Check file extension.
                std::string extension;
                size_t pointPosition = sfileparam.find_last_of (".");

                if (pointPosition != sfileparam.npos)
                    extension = sfileparam.substr (pointPosition+1);

                if (extension != "txt")
                {
                    std::cerr << "Error: CardiacVTKLoader: wrong file format '" << m_contractionParameters_filename << "' as contraction parameters file." << std::endl;
                    return;
                }

                // -- Check if file exist:
                if (!sofa::helper::system::DataRepository.findFile(sfileparam))
                {
                    std::cerr << "Error: CardiacVTKLoader: File '" << m_contractionParameters_filename << "' not found. " << std::endl;
                    return;
                }

                // -- Check if file is readable:
                if ((file = fopen(fileparam, "r")) == NULL)
                {
                    std::cerr << "Error: CardiacVTKLoader: Cannot read file '" << m_contractionParameters_filename << "'." << std::endl;
                    return;
                }

                // -- Check first line.
                if (!readLine(cmd, sizeof(cmd), file))
                {
                    std::cerr << "Error: CardiacVTKLoader: Cannot read first line in file '" << m_contractionParameters_filename << "'." << std::endl;
                    fclose(file);
                    return;
                }
                fclose(file);


                // -- Reading File

                std::ifstream dataFile (fileparam);

                // Reading tab size of file:
                /*
                                unsigned int nLines, nColumns;

                                dataFile >> nLines;

                                if(dataFile.fail())
                                {
                                        serr << "Error: CardiacVTKLoader - Contraction Parameters File require the first line to have the format \"numberOfRows numberOfColumns\"." << sendl;

                                }

                                dataFile >> nColumns;

                                if(dataFile.fail())
                                {
                                        serr << "Error: CardiacVTKLoader - Contraction Parameters File require the first line to have the format \"numberOfRows numberOfColumns\"." << sendl;

                                }


                                //Test number of fiber....
                                //if (line != (position.getValue()).size() - virtualVertex)
                                //CERR << "Wrong number of lines" << ENDL;
                                //return;
                                //}
                                // std::cout<<column<<" "<<line<<std::endl;

                                if (nColumns != 5)
                                {
                                        serr << "Warning: CardiacVTKLoader - Contraction Parameters File requires 5 columns." << sendl;

                                }

                                loadContractionParameters(dataFile, nLines);
                                */

                loadContractionParameters(dataFile);

                dataFile.close();

            }

            void CardiacVTKLoader::readContractionParametersString()
            {

                std::string& my_contractionParametersString = *(m_contractionParametersString.beginEdit());

                std::istringstream parametersStream;

                parametersStream.str(my_contractionParametersString);

                m_contractionParametersString.endEdit();

                loadContractionParameters(parametersStream);

            }


            void CardiacVTKLoader::loadContractionParameters(std::istream &data)
            {
                loadContractionParameters(data, m_numberOfZone.getValue());
            }

            void CardiacVTKLoader::loadContractionParameters(std::istream& data, unsigned int nLinesSpecified)
            {

                int numberTetra=(d_tetrahedra.getValue()).size();
                type::vector < sofa::type::Vec<4,SReal> >& my_contractionParam = *(m_contractionParameters.beginEdit());
                my_contractionParam.resize(numberTetra);

                std::string token;
                unsigned int nZones = 0;
                unsigned int nLinesRead = 0;

                data >> token;
                while(data.good() && !data.fail() && nLinesRead < nLinesSpecified)
                {
                    bool eof = data.eof();
                    nLinesRead++;

                    bool fail = data.fail();
                    bool bad = data.bad();

                    if(isZone(token))
                    {
                        nZones++;
                        unsigned int zoneIndex= getZoneIndex(token);

                        type::Vector4 param;
                        for(int i=0; i<4; i++)
                        {
                            data >> token;
                            if(isReal(token))
                            {
                                param[i] = getRealFromToken(token);
                            }
                            else
                            {
                                serr << "Error: CardiacVTKLoader - Each zone must have 4 Contraction Parameters." << sendl;
                                m_contractionParameters.endEdit();

                                return;
                            }
                        }

                        std::vector < unsigned int > tetraZones = m_zones.getValue()[zoneIndex];

                        for(unsigned int i=0; i < tetraZones.size(); i++)
                        {
                            my_contractionParam[tetraZones[i]] = param;
                        }

                        data >> token;

                    }
                    else
                    {
                        serr << "Error: CardiacVTKLoader - \"" << token << "\" in Contraction Parameters is not a defined zone." << sendl;
                        m_contractionParameters.endEdit();

                        return;
                    }
                }

                m_contractionParameters.endEdit();

                if (nZones < m_numberOfZone.getValue())
                {
                    serr << "Error: CardiacVTKLoader: Missing Contraction Parameters for some zones" << sendl;
                    return;
                }

            }


            void CardiacVTKLoader::readConductivity()
            {
                //std::cout<<"readContractionParameters()"<<std::endl;
                FILE* file;
                char cmd[1024];

                const char* fileparam = m_tetraConductivity_filename.getFullPath().c_str();
                std::string sfileparam (fileparam);

                // -- Check file extension.
                std::string extension;
                size_t pointPosition = sfileparam.find_last_of (".");

                if (pointPosition != sfileparam.npos)
                    extension = sfileparam.substr (pointPosition+1);

                if (extension != "txt")
                {
                    std::cerr << "Error: CardiacVTKLoader: wrong file format '" << m_tetraConductivity_filename << "' as conductivity file." << std::endl;
                    return;
                }

                // -- Check if file exist:
                if (!sofa::helper::system::DataRepository.findFile(sfileparam))
                {
                    std::cerr << "Error: CardiacVTKLoader: File '" << m_tetraConductivity_filename << "' not found. " << std::endl;
                    return;
                }

                // -- Check if file is readable:
                if ((file = fopen(fileparam, "r")) == NULL)
                {
                    std::cerr << "Error: CardiacVTKLoader: Cannot read file '" << m_tetraConductivity_filename << "'." << std::endl;
                    return;
                }

                // -- Check first line.
                if (!readLine(cmd, sizeof(cmd), file))
                {
                    std::cerr << "Error: CardiacVTKLoader: Cannot read first line in file '" << m_contractionParameters_filename << "'." << std::endl;
                    fclose(file);
                    return;
                }
                fclose(file);


                // -- Reading File
                std::ifstream dataFile (fileparam);

                int nLinesSpecified=m_numberOfZone.getValue();
                int numberTetra=(d_tetrahedra.getValue()).size();
                type::vector < double >& my_contractionParam = *(m_tetraConductivity.beginEdit());

                my_contractionParam.resize(numberTetra);

                std::string token;
                unsigned int nZones = 0;
                unsigned int nLinesRead = 0;

                std::istream& data=dataFile;
                data >> token;
                while(data.good() && !data.fail() && nLinesRead < nLinesSpecified)
                {
                    bool eof = data.eof();
                    nLinesRead++;

                    bool fail = data.fail();
                    bool bad = data.bad();

                    if(isZone(token))
                    {
                        nZones++;
                        unsigned int zoneIndex= getZoneIndex(token);

                        double param;
                        //for(int i=0; i<4; i++)
                        //{
                            data >> token;
                            if(isReal(token))
                            {
                                    param = getRealFromToken(token);
                            }
                            else
                            {
                                    serr << "Error: CardiacVTKLoader - Each zone must have 1 Conductivity Parameters." << sendl;
                                    m_tetraConductivity.endEdit();
                                    //return;
                            }
                        //}

                        std::vector < unsigned int > tetraZones = m_zones.getValue()[zoneIndex];

                        //my_contractionParamZones[zoneIndex]=param;
                        for(unsigned int i=0; i < tetraZones.size(); i++)
                        {
                                my_contractionParam[tetraZones[i]] = param;
                        }

                        data >> token;
                    }
                    else
                    {
                        serr << "Error: CardiacVTKLoader - \"" << token << "\" in Conductivity Parameters is not a defined zone." << sendl;
                        m_tetraConductivity.endEdit();

                        //return;
                    }
                }
                m_tetraConductivity.endEdit();
                // m_contractionParametersZones.endEdit();
                if (nZones < m_numberOfZone.getValue())
                {
                    serr << "Error: CardiacVTKLoader: Missing Conductivity Parameters for some zones" << sendl;
                    //return;
                }
                dataFile.close();
            }


            void CardiacVTKLoader::readAPDFile()
            {
                //std::cout<<"readAPD()"<<std::endl;
                FILE* file;
                char cmd[1024];

                const char* fileparam = m_APDTimesFile.getFullPath().c_str();
                std::string sfileparam (fileparam);

                // -- Check file extension.
                std::string extension;
                size_t pointPosition = sfileparam.find_last_of (".");

                if (pointPosition != sfileparam.npos)
                    extension = sfileparam.substr (pointPosition+1);

                if (extension != "txt")
                {
                    std::cerr << "Error: MeshTetrahedrisationLoader: wrong file format '" << m_APDTimesFile << "' as APD file." << std::endl;
                    return;
                }

                // -- Check if file exist:
                if (!sofa::helper::system::DataRepository.findFile(sfileparam))
                {
                    std::cerr << "Error: MeshTetrahedrisationLoader: File '" << m_APDTimesFile << "' not found. " << std::endl;
                    return;
                }

                // -- Check if file is readable:
                if ((file = fopen(fileparam, "r")) == NULL)
                {
                    std::cerr << "Error: MeshTetrahedrisationLoader: Cannot read file '" << m_APDTimesFile << "'." << std::endl;
                    return;
                }

                // -- Check first line.
                if (!readLine(cmd, sizeof(cmd), file))
                {
                    std::cerr << "Error: MeshTetrahedrisationLoader: Cannot read first line in file '" << m_APDTimesFile << "'." << std::endl;
                    fclose(file);
                    return;
                }
                fclose(file);
                // -- Reading File
                std::ifstream dataFile (fileparam);

                loadAPD(dataFile);

                dataFile.close();
            }

            void CardiacVTKLoader::readAPDString()
            {
                std::string& my_APDString = *(m_APDTimesString.beginEdit());
                std::istringstream parametersStream;
                parametersStream.str(my_APDString);
                m_APDTimesString.endEdit();
                loadAPD(parametersStream);
            }

            void CardiacVTKLoader::loadAPD(std::istream &data)
            {
                loadAPD(data, m_numberOfZone.getValue());
            }

            void CardiacVTKLoader::loadAPD(std::istream& data, unsigned int nLinesSpecified)
            {
                int numberTetra=(d_tetrahedra.getValue()).size();
                int numberVertices=(d_positions.getValue()).size();
                type::vector < double >& my_APD = *(m_APDTimes.beginEdit());
                type::vector < double >& my_APDZones = *(m_APDTimesZones.beginEdit());
                my_APD.resize(numberVertices);
                my_APDZones.resize(m_numberOfZone.getValue());
                std::string token;
                unsigned int nZones = 0;
                unsigned int nLinesRead = 0;

                data >> token;
                while(data.good() && !data.fail() && nLinesRead < nLinesSpecified)
                {
                    bool eof = data.eof();
                    nLinesRead++;
                    bool fail = data.fail();
                    bool bad = data.bad();

                    if(isZone(token))
                    {
                        nZones++;
                        unsigned int zoneIndex= getZoneIndex(token);

                        double param;
                                data >> token;
                                if(isReal(token))
                                {
                                    param = getRealFromToken(token);
                                }
                                else
                                {
                                    serr << "Error: MeshTetrahedrisationLoader - Each zone must have 1 APD Times" << sendl;
                                    m_APDTimes.endEdit();
                                    return;
                                }


                        std::vector < unsigned int > tetraZones = m_zones.getValue()[zoneIndex];

                        my_APDZones[zoneIndex]=param;
                        for(unsigned int i=0; i < tetraZones.size(); i++)
                        {
                            Tetrahedron t=(d_tetrahedra.getValue())[tetraZones[i]];
                            for (int v=0;v<4;v++){
                                int pos=t[v];
                                my_APD[pos] = param;
                            }
                        }
                        data >> token;
                    }
                    else
                    {
                        serr << "Error: MeshTetrahedrisationLoader - \"" << token << "\" in APD is not a defined zone." << sendl;
                        m_APDTimes.endEdit();
                        return;
                    }
                }
                m_APDTimes.endEdit();
                m_APDTimesZones.endEdit();
                if (nZones < m_numberOfZone.getValue())
                {
                    serr << "Error: MeshTetrahedrisationLoader: Missing APD for some zones" << sendl;
                    return;
                }
            }


            bool CardiacVTKLoader::isZone(std::string token)
            {
                type::vector <std::string> zoneNames=m_zoneNames.getValue();
                for(unsigned int i=0; i < m_numberOfZone.getValue(); i++)
                {
                    if(token == zoneNames[i])
                    {
                        return true;
                    }
                }

                return false;
            }


            bool CardiacVTKLoader::isSurfaceZone(std::string token)
            {
                type::vector <std::string> surfaceZoneNames=m_surfaceZoneNames.getValue();
                for(unsigned int i=0; i < m_numberOfSurfaceZone.getValue(); i++)
                {
                    if(token == surfaceZoneNames[i])
                    {
                        return true;
                    }
                }

                return false;
            }


            bool CardiacVTKLoader::isReal(std::string token)
            {
                std::istringstream tokenStream;
                tokenStream.str(token);

                SReal testReal;
                tokenStream >> testReal;

                if(tokenStream.fail())
                {
                    return false;
                }

                return true;

            }

            SReal CardiacVTKLoader::getRealFromToken(std::string token)
            {
                std::istringstream tokenStream;
                tokenStream.str(token);

                SReal extractedReal;
                tokenStream >> extractedReal;

                if(tokenStream.fail())
                {
                    return -1;
                }

                return extractedReal;
            }

            int CardiacVTKLoader::getZoneIndex(std::string token)
            {
                type::vector <std::string> zoneNames=m_zoneNames.getValue();
                for(unsigned int i=0; i < m_numberOfZone.getValue(); i++)
                {
                    if(token == zoneNames[i])
                    {
                        return i;
                    }
                }
                return -1;
            }

            void CardiacVTKLoader::readStiffnessParametersFile()
            {std::cout<<"readStiffnessParameters()"<<std::endl;

                FILE* file;
                char cmd[1024];


                const char* fileparam = m_StiffnessParameters_filename.getFullPath().c_str();
                std::string sfileparam (fileparam);

                // -- Check file extension.
                std::string extension;
                size_t pointPosition = sfileparam.find_last_of (".");

                if (pointPosition != sfileparam.npos)
                    extension = sfileparam.substr (pointPosition+1);

                if (extension != "txt")
                {
                    serr << "Error: CardiacVTKLoader: wrong file format '" << m_StiffnessParameters_filename << "' as Stiffness parameters file." << sendl;
                    return;
                }

                // -- Check if file exist:
                if (!sofa::helper::system::DataRepository.findFile(sfileparam))
                {
                    serr << "Error: CardiacVTKLoader: File '" << m_StiffnessParameters_filename << "' not found. " << sendl;
                    return;
                }

                // -- Check if file is readable:
                if ((file = fopen(fileparam, "r")) == NULL)
                {
                    serr << "Error: CardiacVTKLoader: Cannot read file '" << m_StiffnessParameters_filename << "'." << sendl;
                    return;
                }

                // -- Check first line.
                if (!readLine(cmd, sizeof(cmd), file))
                {
                    serr << "Error: CardiacVTKLoader: Cannot read first line in file '" << m_StiffnessParameters_filename << "'." << sendl;
                    fclose(file);
                    return;
                }
                fclose(file);


                // -- Reading File

                std::ifstream dataFile (fileparam);

                // Reading tab size of file:
                //unsigned int line, column;

                //dataFile >> line >> column;

                //loadStiffnessParameters(dataFile, line);
                loadStiffnessParameters(dataFile);

                dataFile.close();

            }

            void CardiacVTKLoader::readStiffnessParametersString()
            {
                std::string& my_stiffnessParametersString = *(m_StiffnessParametersString.beginEdit());

                std::istringstream parametersStream;

                parametersStream.str(my_stiffnessParametersString);

                m_StiffnessParametersString.endEdit();

                loadStiffnessParameters(parametersStream);

            }

            void CardiacVTKLoader::loadStiffnessParameters(std::istream& data)
            {
                loadStiffnessParameters(data, m_numberOfZone.getValue());
            }

            void CardiacVTKLoader::loadStiffnessParameters(std::istream& data, unsigned int nLinesSpecified)
            {

                int numberTetra=(d_tetrahedra.getValue()).size();
                type::vector < sofa::type::Vec<10,SReal> >& my_StiffnessParam = *(m_StiffnessParameters.beginEdit());
                my_StiffnessParam.resize(numberTetra);

                std::string token;
                unsigned int nZones = 0;
                unsigned int nLinesRead = 0;

                data >> token;

                while(data.good() && nLinesRead < nLinesSpecified)
                {
                    nLinesRead++;

                    if(isZone(token))
                    {
                        nZones++;
                        unsigned int zoneIndex= getZoneIndex(token);

                        sofa::type::Vec<10, SReal> param;
                        int nParam = 0;
                        data >> token;
                        while(isReal(token) && nParam < 10)
                        {
                            param[nParam] = getRealFromToken(token);
                            data >> token;
                            if(data.fail())
                            {
                                token = "endoffile";
                            }
                            nParam++;
                        }

                        std::vector < unsigned int > tetraZones = m_zones.getValue()[zoneIndex];

                        for(unsigned int i=0; i < tetraZones.size(); i++)
                        {
                            my_StiffnessParam[tetraZones[i]] = param;
                        }

                    }
                    else
                    {
                        serr << "Error: CardiacVTKLoader - \"" << token << "\" in Stiffness Parameters is not a defined zone." << sendl;
                        m_StiffnessParameters.endEdit();

                        return;
                    }
                }

                m_StiffnessParameters.endEdit();

                /* if (nZones < m_numberOfZone.getValue())
                                {
                                    std::cout<<nZones<<std::endl;
                                        serr << "Error: CardiacVTKLoader: Missing Stiffness Parameters for some zones" << sendl;
                                        return;
                                }*/

            }


            void CardiacVTKLoader::getInitMSvertices()
            {
                const sofa::type::vector<std::string>& names =m_surfaceZoneNames.getValue();
                const sofa::type::vector <std::string>& initNames=  m_MSinitSurfaceZoneNames.getValue();
                sofa::type::vector <unsigned int>& initVertices= *(m_MSinitVertices.beginEdit());
                std::cout<<names<<std::endl;
                std::cout<<initNames<<std::endl;

                for (unsigned int a=0;a<initNames.size();a++)
                {
                    if(!isSurfaceZone(initNames[a])){
                        std::cout << "MeshTetrahedrisationLoader::loadSurfaceZone : The init zone " << initNames[a] << " is not a surfaceZone." << std::endl;
                    }
                    else
                    {
                        for (unsigned int i = 0; i<names.size(); ++i )
                        {
                            if (names[i] == initNames[a])
                            {
                                sofa::type::vector <unsigned int> tmp =(m_surfaceZones.getValue())[i]; // current surf zone
                                for (unsigned int j = 0; j<tmp.size(); ++j)
                                {
                                    int tri=tmp[j]; // surface zone are triangles
                                    type::fixed_array<unsigned int,3> nodesIntri=(d_triangles.getValue())[tri];
                                    for (unsigned int node=0; node<3; node++)
                                    {
                                       // if(std::find(initVertices.begin(),initVertices.end(),nodesIntri[node]=initVertices.end())) // vertice not registered yet
                                            initVertices.push_back(nodesIntri[node]);
                                    }
                                    //initVertices.push_back(tmp[j]);
                                }
                            }
                        }
                    }
                }
                std::cout<<" MeshTetrahedrisationLoader::getInitMSvertices : size of sinus vertices list = " <<initVertices.size()<<std::endl;
                //erase duplicates
                std::sort( initVertices.begin(), initVertices.end() );
                initVertices.erase( std::unique( initVertices.begin(), initVertices.end() ), initVertices.end() );

                //std::set<int> s( initVertices.begin(), initVertices.end() );
                //initVertices.assign( s.begin(), s.end() );

                std::cout<<" MeshTetrahedrisationLoader::getInitMSvertices : size of sinus vertices list = " <<initVertices.size()<<std::endl;

                if (initVertices.empty())
                {
                    std::cout << "Error: MeshTetrahedrisationLoader : Initialisation of Mitchell Shaeffer sinus failed." <<std::endl;
                    initVertices.push_back(0);
                }
                m_MSinitVertices.endEdit();
            }


            void CardiacVTKLoader::readVelocity()
            {std::cout<<"readVelocity()"<<std::endl;

                FILE* file;
                char cmd[1024];

                const char* fileparam = m_VelocityFile.getFullPath().c_str();
                std::string sfileparam (fileparam);

                // -- Check file extension.
                std::string extension;
                size_t pointPosition = sfileparam.find_last_of (".");

                if (pointPosition != sfileparam.npos)
                    extension = sfileparam.substr (pointPosition+1);

                if (extension != "txt")
                {
                    std::cerr << "Error: CardiacVTKLoader: wrong file format '" << m_VelocityFile << "' as Velocity file." << std::endl;
                    return;
                }

                // -- Check if file exist:
                if (!sofa::helper::system::DataRepository.findFile(sfileparam))
                {
                    std::cerr << "Error: CardiacVTKLoader: File '" << m_VelocityFile << "' not found. " << std::endl;
                    return;
                }

                // -- Check if file is readable:
                if ((file = fopen(fileparam, "r")) == NULL)
                {
                    std::cerr << "Error: CardiacVTKLoader: Cannot read file '" << m_VelocityFile << "'." << std::endl;
                    return;
                }

                // -- Check first line.
                if (!readLine(cmd, sizeof(cmd), file))
                {
                    std::cerr << "Error: CardiacVTKLoader: Cannot read first line in file '" << m_VelocityFile << "'." << std::endl;
                    fclose(file);
                    return;
                }
                fclose(file);


                // -- Reading File

                std::ifstream dataFile (fileparam);

                type::vector < type::Vec<3,SReal> >& my_velocities = *(m_velocities.beginEdit());


                // Reading tab size of file:
                unsigned int line;

                dataFile >> line;
                my_velocities.resize(line);

                for (unsigned int i=0; i<line; i++)
                {
                    type::Vec<3,SReal> param;

                    for (unsigned int u=0;u<3;u++){
                        dataFile >> param[u];
                    }
                    my_velocities[i]=param;

                }

                m_velocities.endEdit();

            }

            void CardiacVTKLoader::readElectrophysiology()
            {

                FILE* file;
                char cmd[1024];


                const char* fileparam = m_ElectroFile.getFullPath().c_str();
                std::string sfileparam (fileparam);
                std::string unit=m_unit.getValue();
                double startContraction=m_startContraction.getValue();

                // -- Check file extension.
                std::string extension;
                size_t pointPosition = sfileparam.find_last_of (".");

                if (pointPosition != sfileparam.npos)
                    extension = sfileparam.substr (pointPosition+1);

                if (extension != "txt")
                {
                    std::cerr << "Error: CardiacVTKLoader: wrong file format '" << m_ElectroFile << "' as Electro file." << std::endl;
                    return;
                }

                // -- Check if file exist:
                if (!sofa::helper::system::DataRepository.findFile(sfileparam))
                {
                    std::cerr << "Error: CardiacVTKLoader: File '" << m_ElectroFile << "' not found. " << std::endl;
                    return;
                }

                // -- Check if file is readable:
                if ((file = fopen(fileparam, "r")) == NULL)
                {
                    std::cerr << "Error: CardiacVTKLoader: Cannot read file '" << m_ElectroFile << "'." << std::endl;
                    return;
                }

                // -- Check first line.
                if (!readLine(cmd, sizeof(cmd), file))
                {
                    std::cerr << "Error: CardiacVTKLoader: Cannot read first line in file '" << m_ElectroFile << "'." << std::endl;
                    fclose(file);
                    return;
                }
                fclose(file);


                // -- Reading File

                std::ifstream dataFile (fileparam);

                unsigned int line, column;

                dataFile >> line >> column;

                int numberNode=line;
                type::vector < SReal >& my_TD = *(m_depolarizationTimes.beginEdit());
                my_TD.resize(numberNode);
                type::vector < SReal >& my_APD = *(m_APDTimes.beginEdit());
                my_APD.resize(numberNode);
                // Reading tab size of file:
                int scale=0;
                if (unit=="s") scale=1;
                if (unit=="ms") scale=1000;
                std::cout<<scale<<std::endl;

                if(column<2){
                    for (unsigned int i=0; i<my_TD.size(); i++)
                    {
                        SReal TD;
                        dataFile >> TD;
                        my_TD[i]=TD/scale +startContraction ;///just pour essayer
                        my_APD[i]=0.3;
                    }
                }
                else {

                    for (unsigned int i=0; i<my_TD.size(); i++)
                    {
                        SReal APD,TD;
                        dataFile >> TD>>APD;
                        my_TD[i]=TD/scale+startContraction;///just pour essayer
                        my_APD[i]=APD/scale;
                    }

                }

                m_depolarizationTimes.endEdit();
                m_APDTimes.endEdit();

            }




            void CardiacVTKLoader::readTetraTensorDirection()
            {
#ifdef SOFA_HAVE_EIGEN2
                typedef Eigen::SelfAdjointEigenSolver<Eigen::Matrix<double,3,3> >::MatrixType EigenMatrix;
                typedef Eigen::SelfAdjointEigenSolver<Eigen::Matrix<double,3,3> >::RealVectorType CoordEigen;
#endif

                FILE* file;
                char cmd[1024];
                std::ofstream out;
                out.open(m_facetFiber_filename.getFullPath().c_str(),std::ios::out);

                const char* fileTensor = m_tetraTensor_filename.getFullPath().c_str();
                std::string sfileTensor (fileTensor);

                // -- Check file extension.
                std::string extension;
                size_t pointPosition = sfileTensor.find_last_of (".");

                if (pointPosition != sfileTensor.npos)
                    extension = sfileTensor.substr (pointPosition+1);

                if (extension != "ttsr")
                {
                    std::cerr << "Error: CardiacVTKLoader: wrong file format '" << m_tetraTensor_filename << "' as tetra tensor file." << std::endl;
                    return;
                }

                // -- Check if file exist:
                if (!sofa::helper::system::DataRepository.findFile(sfileTensor))
                {
                    std::cerr << "Error: CardiacVTKLoader: File '" << m_tetraTensor_filename << "' not found. " << std::endl;
                    return;
                }

                // -- Check if file is readable:
                if ((file = fopen(fileTensor, "r")) == NULL)
                {
                    std::cerr << "Error: CardiacVTKLoader: Cannot read file '" << m_tetraTensor_filename << "'." << std::endl;
                    return;
                }

                // -- Check first line.
                if (!readLine(cmd, sizeof(cmd), file))
                {
                    std::cerr << "Error: CardiacVTKLoader: Cannot read first line in file '" << m_tetraTensor_filename << "'." << std::endl;
                    fclose(file);
                    return;
                }
                fclose(file);


                // -- Reading File

                std::ifstream dataFile (fileTensor);

                type::vector < sofa::type::Vec<6,SReal> >& my_tetraTensor = *(m_tetraTensor.beginEdit());
                type::vector < sofa::type::Vec<3,SReal> >& my_facetBFibers = *(m_facetBFibers.beginEdit());
                type::vector < sofa::type::Vec<3,SReal> >& my_facetFibers = *(m_facetFibers.beginEdit());
                // Reading tab size of file:
                unsigned int line, column;

                dataFile >> line >> column;
                out<<line<<" 3 "<<std::endl;

                //Test number of fiber....
                //if (line != (position.getValue()).size() - virtualVertex)
                //CERR << "Wrong number of lines" << ENDL;
                //return;
                //}

                if (column != 6)
                {
                    std::cerr << "Wrong number of columns" << std::endl;
                    return;
                }

                for (unsigned int i=0; i<line; i++)
                {
                    type::Vector6 tensor;
                    Mat<3,3,double> tensormat,id;
                    type::Vector3 dirf,coordf;
                    id.identity();
                    dataFile >> tensor[0] >> tensor[1] >> tensor[2]>> tensor[3]>> tensor[4]>> tensor[5];


                    // tensor.normalize();

                    my_tetraTensor.push_back (tensor);

                    tensormat(0,0)=tensor[0];
                    tensormat(0,1)=tensormat(1,0)=tensor[1];
                    tensormat(1,1)=tensormat(1,1)=tensor[2];
                    tensormat(0,2)=tensormat(2,0)=tensor[3];
                    tensormat(1,2)=tensormat(2,1)=tensor[4];
                    tensormat(2,2)=tensor[5];
                    //std::cout<<tensor<<std::endl;

                    if(tensormat!=id){
#ifdef SOFA_HAVE_EIGEN2
                        EigenMatrix meanEigen;
                        for (int i=0;i<3;i++){
                            for(int j=0;j<3;j++){
                                meanEigen(i,j)=tensormat(i,j);
                            }
                        }
                        Eigen::SelfAdjointEigenSolver<EigenMatrix> Vect(meanEigen);

                        EigenMatrix Evect=Vect.eigenvectors();// classe by valeurs propres croissantes, donc on inverse l�rdre puisque (f,s,n) choisi pour f associe a la plus grande

                        dirf[0]=Evect(0,2);
                        dirf[1]=Evect(1,2);
                        dirf[2]=Evect(2,2);

                        //       std::cout<<Evect<<std::endl;
                        //       std::cout<<tensor<<std::endl;
                        //	dirs=Coord(Evect(0,1),Evect(1,1),Evect(2,1));
                        //	dirn=Coord(Evect(0,0),Evect(1,0),Evect(2,0));
#endif
                    }

                    else{
                        dirf[0]=1;
                        dirf[1]=dirf[2]=0;
                        //	dirs=Coord(0,1,0);
                        //	dirn=Coord(0,0,1);
                    }
                    if ( tensor.norm() == 0.0){
                        std::cerr << "Warning: CardiacVTKLoader::readTetraFibreDirection Tensor N'" << i <<"' is null." << std::endl;
                        dirf.clear();
                    }


                    //    coordf=dirf/dirf.norm();
                    my_facetFibers.push_back (dirf);
                    //    std::cout<<"dirf"<<coordf.norm()<<std::endl;
                    out<<dirf<<std::endl;

                    Vec<3,double> u, v, w;
                    type::fixed_array<unsigned int,4> nodesIntetra=(d_tetrahedra.getValue())[i];

                    u=(d_positions.getValue())[nodesIntetra[1]]-(d_positions.getValue())[nodesIntetra[0]];
                    v=(d_positions.getValue())[nodesIntetra[2]]-(d_positions.getValue())[nodesIntetra[0]];
                    w=(d_positions.getValue())[nodesIntetra[3]]-(d_positions.getValue())[nodesIntetra[0]];
                    //u=tinfo.pointP[1]-tinfo.pointP[0];
                    //v=tinfo.pointP[2]-tinfo.pointP[0];
                    //w=tinfo.pointP[3]-tinfo.pointP[0];
                    Mat<3,3,double> base;
                    base[0]=u;
                    base[1]=v;
                    base[2]=w;
                    Mat<3,3,double> invbase;
                    invertMatrix(invbase,base);

                    my_facetBFibers.push_back(invbase*dirf);
                }




                out.close();
                m_tetraTensor.endEdit();
                m_facetFibers.endEdit();
                m_facetBFibers.endEdit();


            }



        } // namespace loader

    } // namespace component

} // namespace sofa
