#pragma once

#include <iostream>
#include <fstream>
#include <sofa/core/loader/MeshLoader.h>
#include <sofa/type/Vec.h>
#include <sofa/type/Mat.h>
#include <SofaBaseTopology/TopologyData.h>
#include <sofa/core/topology/BaseMeshTopology.h>
#include <SofaBaseTopology/TetrahedronSetTopologyContainer.h>
#include <sofa/core/topology/BaseMeshTopology.h>


namespace sofa
{
namespace component
{
namespace loader
{

class CardiacVTKLoader : public sofa::core::loader::MeshLoader
{
public:
    SOFA_CLASS(CardiacVTKLoader, sofa::core::loader::MeshLoader);

    typedef int             neighborID;
    typedef unsigned int    tetrahedronID;
    typedef type::Mat<3,3,double> Matrix3;
    typedef sofa::type::vector < sofa::type::vector <float> >  VecVecF;
    typedef sofa::type::vector < sofa::type::vector <unsigned int> >  VecVecUI;
    typedef type::vector<double> SetParameterArray;
    typedef type::Vec<3,int> vecint;

    CardiacVTKLoader();

    virtual bool doLoad() override;
    virtual void doClearBuffers() override;

    template <class T>
    static bool canCreate ( T*& obj, core::objectmodel::BaseContext* context, core::objectmodel::BaseObjectDescription* arg )
    {
        std::cout << "CardiacVTKLoader::cancreate()" << std::endl;

        //      std::cout << BaseLoader::d_filename << " is not an Gmsh file." << std::endl;

        return BaseLoader::canCreate (obj, context, arg);
    }


protected:

    bool readVtkFormat (const char* filename);


    void readFibreDirection();

    void readTetraLocalFibreDirection();


    // Add labels
    void addLabel(unsigned label, unsigned cellType);

    //void createFiberDirection();
    void readTetraFibreDirection();
    void createFiberDirection(double angleEpi, double angleEndo);
    void readTetraTensorDirection();
    void readContractionParametersFile();
    void readContractionParametersString();
    void readElectrophysiology();
    void createTriangleSetArray();
    void createEdgeSetArrayFromTriangle();
    void createEdgeSetArray();
    void readVelocity();
    void readStiffnessParametersFile();
    void readStiffnessParametersString();
    void updateNormals();
    void loadContractionParameters(std::istream &data);
    void loadContractionParameters(std::istream& data, unsigned nLinesSpecified);

    void loadStiffnessParameters(std::istream &data);
    void loadStiffnessParameters(std::istream& data, unsigned nLinesSpecified);


    bool isZone(std::string token);
    bool isReal(std::string token);
    int getZoneIndex(std::string token);
    SReal getRealFromToken(std::string token);

    // Added by me
    void readConductivity();
    void getInitMSvertices();// init pacing of mitchell Shaeffer electric component (for electro-meca coupling)
    void readAPDFile();
    void readAPDString();
    void loadAPD(std::istream &data);
    void loadAPD(std::istream &data, unsigned nLinesSpecified);
    bool isSurfaceZone(std::string token);


public:
    Data<bool> createFibers;
    //  Data<SReal > planZone;
    sofa::core::objectmodel::DataFileName outputnodeFibers;
    sofa::core::objectmodel::DataFileName outputtetraFibers;
    Data<std::string> TetraTri;
    Data<std::string> m_unit;
    Data<std::string> TetraZoneName;
    Data<std::string> PointZoneName;
    //Data<vecint > LVRVBase;
    //Data<vecint > endoepi;
    Data<bool> withAHA;
    Data<std::string> TriZoneName;
    Data<std::string> AHAzoneName;
    Data<double> m_startContraction;
    Data <double> angleEpi;
    Data <double> angleEndo;
    std::ofstream nodeFibers;
    std::ofstream tetraFibers;


    //  Data <type::vector < Tetrahedron > > neighborTable;
    //  Data <type::vector <unsigned int> > tetrahedraOnBorderList;
    //  Data <type::vector < type::vector <unsigned int> > > edgesOnBorder;
    //  Data <type::vector < type::vector <unsigned int> > > trianglesOnBorder;

    Data <type::vector<unsigned int> > m_cellTypes; // Added by Jaume, to store the cell types

    Data <unsigned int> m_numberOfZone;
    Data <type::vector <std::string> > m_zoneNames;
    Data <type::vector <unsigned int> > m_zoneSizes;
    Data <type::vector < type::vector <unsigned int> > > m_zones;
    Data <type::vector<unsigned int> > trianglesSurf;
    Data <unsigned int> m_numberOfSurfaceZone;
    Data <type::vector <std::string> > m_surfaceZoneNames;
    Data <type::vector <unsigned int> > m_surfaceZoneSizes;
    Data <type::vector < type::vector <unsigned int> > > m_surfaceZones;

    Data <unsigned int> m_numberOfPointZone;
    Data <type::vector <std::string> > m_pointNames;
    Data <type::vector <unsigned int> > m_pointZoneSizes;
    Data <type::vector < type::vector <unsigned int> > > m_pointZones;

    Data <type::vector < type::vector <float> > > m_ambient, m_diffuse, m_specular;
    Data <type::vector <float> > m_shininess;

    Data <type::vector <std::string> > m_MSinitSurfaceZoneNames; // init pacing of mitchell Shaeffer electric component (for electro-meca coupling)
    Data <type::vector <unsigned int> > m_MSinitVertices;
    //Data for Fiber

    sofa::core::objectmodel::DataFileName m_nodeFiber_filename;
    Data <type::vector < sofa::type::Vec<3,SReal> > > m_nodeFibers;

    sofa::core::objectmodel::DataFileName m_facetFiber_filename;
    Data <type::vector < sofa::type::Vec<3,SReal> > > m_facetFibers;

    sofa::core::objectmodel::DataFileName m_facetBFiber_filename;
    Data <type::vector < sofa::type::Vec<3,SReal> > > m_facetBFibers;

    sofa::core::objectmodel::DataFileName m_tetraTensor_filename;
    Data <type::vector < sofa::type::Vec<6,SReal> > > m_tetraTensor;

    // Contraction parameters
    sofa::core::objectmodel::DataFileName m_contractionParameters_filename;
    Data <type::vector < sofa::type::Vec<4,SReal> > > m_contractionParameters;// for sigma0, k0, Katp, krs
    Data<std::string> m_contractionParametersString;

    // Conductivity parameters
    sofa::core::objectmodel::DataFileName m_tetraConductivity_filename;
    Data <type::vector < double > > m_tetraConductivity;// for electrical

    // Stiffness parameters
    sofa::core::objectmodel::DataFileName m_StiffnessParameters_filename;
    Data <type::vector < sofa::type::Vec<10,SReal> > > m_StiffnessParameters;
    Data<std::string> m_StiffnessParametersString;

    // APD and electro file
    sofa::core::objectmodel::DataFileName m_ElectroFile;
    Data <type::vector < SReal > > m_depolarizationTimes;
    Data <type::vector < SReal > > m_APDTimes;
    Data <type::vector < SReal > > m_APDTimesZones;
    Data<std::string> m_APDTimesString;
    sofa::core::objectmodel::DataFileName m_APDTimesFile;

    // Velocity file
    sofa::core::objectmodel::DataFileName m_VelocityFile;
    Data <type::vector < sofa::type::Vec<3,SReal> > > m_velocities;


};


} // namespace loader
} // namespace component
} // namespace sofa