#include "HeartMeshLoader.h"
#include <sofa/core/ObjectFactory.h>

namespace sofa {

namespace component {

namespace loader {


/*
 * Mesh loader intended to load the heart tetrahedral mesh
 * Several point and cell data must be present in the mesh:
 *      PointData:
 *          - Ventricles: 0 for LV and 1 for RV
 *          - Endos_Epi: 0 for endo LV, 1 endo RV, 2 for epi, 3 for myocardium (interior points)
 *      CellData:
 *          - Volume_Areas: AHA regions, LV is 1-17, RV is 18-29
 *          - K0: Bulk modulus of MR model
 *          - c1: MR parameter
 *          - c2: MR parameter
 *          - fibers: Direction of the fiber in each tetra
 */


HeartMeshLoader::HeartMeshLoader()
    : c_aha_zones(initData(&c_aha_zones, "c_aha_zones", "Cell data in mesh (Volume_Areas)"))
    , c_bulk(initData(&c_bulk, "c_bulk", "Cell data in mesh (bulk)"))
    , c_c1(initData(&c_c1, "c_c1", "Cell data in mesh (c1)"))
    , c_c2(initData(&c_c2, "c_c2", "Cell data in mesh (c2)"))
    , c_fibers(initData(&c_fibers, "c_fibers", "Direction of the fiber in each tetra (fibers)"))
    , c_K0(initData(&c_K0, "c_K0", ""))
    , c_sigma(initData(&c_sigma, "c_sigma", "Cell data in mesh (sigma)"))
    , c_Katp(initData(&c_Katp, "c_Katp", "Cell data in mesh (Katp)"))
    , c_Krs(initData(&c_Krs, "c_Krs", "Cell data in mesh (Krs)")), p_ventricles(initData(&p_ventricles, "p_ventricles", "Point data in mesh (Ventricles)"))
    , p_endos_epi(initData(&p_endos_epi, "p_endos_epi", "Point data in mesh (Endos_Epi)"))
    , p_depol(initData(&p_depol, "p_depol", "Point data in mesh (depol)"))
    , p_apd(initData(&p_apd, "p_apd", "Point data in mesh (apd)"))

    , StiffnessParameters(initData(&StiffnessParameters, "StiffnessParameters", "DEBUG StiffnessParameters (compat old loader)"))
    , facetFibers(initData(&facetFibers, "facetFibers", "DEBUG facetFibers (compat old loader)"))
    , ContractionParameters(initData(&ContractionParameters, "ContractionParameters", "DEBUG ContractionParameters (compat old loader)"))
    , m_zones(initData(&m_zones, "zones", "DEBUG m_zones (compat old loader)"))
{
}


bool HeartMeshLoader::doLoad() {
    bool parent_doLoad = MeshVTKLoader::doLoad();
    if (!parent_doLoad) return parent_doLoad;

    m_internalEngine["filename"].cleanDirty();



    // Load cell data
    // AHA regions
    c_aha_zones.setValue(get_cell_data("Volume_Areas"));

    // Mooney-Rivlin
    c_bulk.setValue(get_cell_data("bulk"));
    c_c1.setValue(get_cell_data("c1"));
    c_c2.setValue(get_cell_data("c2"));

    // Contraction
    c_K0.setValue(get_cell_data("K0"));  // max stiffness
    c_sigma.setValue(get_cell_data("sigma"));  // contractility
    c_Katp.setValue(get_cell_data("Katp"));
    c_Krs.setValue(get_cell_data("Krs"));

    // Fibers
    c_fibers.setValue(get_3d_cell_data("fibers"));

    // Load point data
    // Input labels
    p_ventricles.setValue(get_point_data("Ventricles"));
    p_endos_epi.setValue(get_point_data("Endos_Epi"));

    // Electro
    p_depol.setValue(get_point_data("depol"));
    p_apd.setValue(get_point_data("apd"));

    // Compat old loader
    make_compat();

    return parent_doLoad;
}


VecReal HeartMeshLoader::get_cell_data(std::string name) {
    msg_info() << "Trying to load '" << name << "' cell data";

    core::objectmodel::BaseData *cell_data_ = findData(name);

    if ((!cell_data_) or (cell_data_->getValueString().size() == 0)) {
        msg_error() << "Cell data '" << name << "' not found in mesh";
        return VecReal();
    }

    VecReal cell_data = parse_1d_data(cell_data_->getValueString());
    msg_info() << "Found cell data '" << name << "' (" << cell_data.size() << ")";

    helper::WriteAccessor<Data<type::vector<Tetrahedron>>> x(d_tetrahedra);
    if (cell_data.size() != x.size())
        msg_warning() << "Found " << x.size() << " tetras but '" << name
                      << "' cell data has " << cell_data.size() << " elements";

    return cell_data;
}

VecDeriv HeartMeshLoader::get_3d_cell_data(std::string name) {
    msg_info() << "Trying to load '" << name << "' cell data";

    core::objectmodel::BaseData *cell_data_ = findData(name);

    if ((!cell_data_) or (cell_data_->getValueString().size() == 0)) {
        msg_error() << "Cell data '" << name << "' not found in mesh";
        return VecDeriv();
    }

    VecReal cdata = parse_1d_data(cell_data_->getValueString());
    VecDeriv cell_data;
    for (int j=0; j<cdata.size(); j+=3)
        cell_data.push_back({cdata[j], cdata[j+1], cdata[j+2]});

    msg_info() << "Found cell data '" << name << "' (" << cell_data.size() << ")";

    helper::WriteAccessor<Data<type::vector<Tetrahedron>>> x(d_tetrahedra);
    if (cell_data.size() != x.size())
        msg_warning() << "Found " << x.size() << " tetras but '" << name
                      << "' cell data has " << cell_data.size() << " elements";

    return cell_data;
}


VecReal HeartMeshLoader::get_point_data(std::string name) {
    msg_info() << "Trying to load '" << name << "' point data";

    core::objectmodel::BaseData *point_data_ = findData(name);

    if (!point_data_) {
        msg_error() << "Point data '" << name << "' not found in mesh";
        return VecReal();
    }

    VecReal point_data = parse_1d_data(point_data_->getValueString());

    msg_info() << "Found point data '" << name << "' (" << point_data.size() << ")";

    helper::WriteAccessor<Data<type::vector<type::Vec3>>> x(d_positions);
    if (point_data.size() != x.size())
        msg_warning() << "Found " << x.size() << " points but '" << name
                      << "' point data has " << point_data.size() << " elements";

    return point_data;
}




//// --- Compat old loader ---

void HeartMeshLoader::make_compat() {
    helper::ReadAccessor<Data<type::vector<type::Vec3>>> points(d_positions);
    helper::ReadAccessor<Data<type::vector<Tetrahedron>>> tetras(d_tetrahedra);

    helper::ReadAccessor<Data<type::vector<Triangle>>> triangles(d_triangles);

//    msg_error() << "nb tetras: " << tetras.size();
//    msg_error() << "nb triangles: " << triangles.size();
//    msg_error() << "nb points: " << points.size();


    // Topology zones
    helper::WriteAccessor<Data<type::vector<std::string>>> zoneNames(m_zoneNames);
    for(int i=1; i<30; i++) zoneNames.push_back("Zone"+std::to_string(i));

    helper::ReadAccessor<Data<VecReal>> aha(c_aha_zones);
    helper::WriteAccessor<Data<type::vector<type::vector<unsigned int>>>> zones(m_zones);
    for(int i=1; i<30; i++) {
        type::vector<unsigned int> tetras_in_zone;
        for(int j=0; j<tetras.size(); j++) if(aha[j] == i) tetras_in_zone.push_back(j);
        zones.push_back(tetras_in_zone);

    }
//    Data<type::vector<std::string>> m_zoneNames;  // Zonei where i=0..29
//    Data<type::vector<type::vector<int>>> m_zones;  // list of tetras indices per aha region
//    Data<type::vector<std::string>> m_surfaceZoneNames;  // SurfaceZonei where i=30..33
//    Data<type::vector<type::vector<int>>> m_surfaceZones;  // list of triangles indices per Endos_Epi region



    // From Mooney-Rivlin
    helper::ReadAccessor<Data<VecReal>> bulk(c_bulk);
    helper::ReadAccessor<Data<VecReal>> c1(c_c1);
    helper::ReadAccessor<Data<VecReal>> c2(c_c2);
    helper::WriteAccessor<Data<type::vector<type::Vec<10, Real>>>> stiff(StiffnessParameters);
    stiff.clear(); stiff.resize(tetras.size());
    for(int i=0; i<tetras.size(); i++){
        stiff[i][0] = c1[i];
        stiff[i][1] = c2[i];
        stiff[i][2] = bulk[i];
    }

    // From ContractionForceField
    helper::ReadAccessor<Data<VecCoord>> fibers(c_fibers);
    helper::WriteAccessor<Data<type::vector<type::Vec<3, Real>>>> _fibers(facetFibers);
    _fibers.clear(); _fibers.resize(tetras.size());
    for(int i=0; i<tetras.size(); i++){
        _fibers[i][0] = fibers[i][0];
        _fibers[i][1] = fibers[i][1];
        _fibers[i][2] = fibers[i][2];
    }

    helper::ReadAccessor<Data<VecReal>> K0(c_K0);
    helper::ReadAccessor<Data<VecReal>> sigma(c_sigma);
    helper::ReadAccessor<Data<VecReal>> Katp(c_Katp);
    helper::ReadAccessor<Data<VecReal>> Krs(c_Krs);
    helper::WriteAccessor<Data<type::vector<type::Vec<4, Real>>>> cont(ContractionParameters);
    cont.clear(); cont.resize(tetras.size());
    for(int i=0; i<tetras.size(); i++){
        cont[i][0] = sigma[i];
        cont[i][2] = Katp[i];
        cont[i][3] = Krs[i];
        cont[i][1] = K0[i];
    }

}



//// Links in BaseConstraintForceField and PressureConstraintForceField
//Data<VecStr> m_zoneNames;  // Zonei where i=0..29
//Data<type::vector<VecInt>> m_zones;  // list of tetras indices per aha region
//Data<VecStr> m_surfaceZoneNames;  // SurfaceZonei where i=30..33
//Data<type::vector<VecInt>> m_surfaceZones;  // list of triangles indices per Endos_Epi region
//
////    // Links in scene graph
////    // From PressureConstraintForceField
////    trianglesSurf
////    pointZoneNames
////    surfaceZoneNames
////    surfaceZones
////    pointZones
//



//void HeartMeshLoader::draw(const core::visual::VisualParams* /*vparams*/)
//{
//}



int HeartMeshLoaderClass = sofa::core::RegisterObject("This class does nothing").add<HeartMeshLoader>();


} // loader

} // namespace component

} // namespace sofa
