#pragma once
#include <SofaLoader/MeshVTKLoader.h>


namespace sofa
{

typedef typename defaulttype::Vec3Types::VecCoord VecCoord;
typedef typename defaulttype::Vec3Types::VecDeriv VecDeriv;
typedef typename defaulttype::Vec3Types::Coord Coord;
typedef typename defaulttype::Vec3Types::Deriv Deriv;
typedef typename Coord::value_type Real;
typedef typename type::vector<Real> VecReal;
typedef typename std::string String;
typedef typename std::vector<String> VecStr;
typedef typename std::vector<unsigned int> VecInt;

namespace component
{

namespace loader
{

class HeartMeshLoader : public MeshVTKLoader
{
public:
    SOFA_CLASS(HeartMeshLoader, MeshVTKLoader);

    Data<VecReal> c_aha_zones;  // AHA regions
    Data<VecReal> c_bulk, c_c1, c_c2;  // Mooney-Rivlin parameters
    Data<VecReal> p_ventricles, p_endos_epi;  // input labels
    Data<VecCoord> c_fibers;  // fiber direction in each tetra
    Data<VecReal> c_K0, c_sigma, c_Katp, c_Krs;  // contraction parameters
    Data<VecReal> p_depol, p_apd;  // electrophysiology input

    HeartMeshLoader();
    bool doLoad() override;
    VecReal get_cell_data(std::string name);
    VecDeriv get_3d_cell_data(std::string name);
    VecReal get_point_data(std::string name);

    VecReal parse_1d_data(std::string s) {
        std::string sep = " ";
        size_t last(0), next(0);
        VecReal vals;
        while ((next = s.find(sep, last)) != std::string::npos) {
            vals.push_back(std::stod(s.substr(last, next - last)));
            last = next + 1;
        }
        vals.push_back(std::stod(s.substr(last)));
        return vals;
    }




    // --- Compat old loader ---

    void make_compat();

    // Links in BaseConstraintForceField and PressureConstraintForceField
    Data<type::vector<std::string>> m_zoneNames;  // Zonei where i=0..29
    Data<type::vector<type::vector<unsigned int>>> m_zones;  // list of tetras indices per aha region
    Data<type::vector<std::string>> m_surfaceZoneNames;  // SurfaceZonei where i=30..33
    Data<type::vector<type::vector<unsigned int>>> m_surfaceZones;  // list of triangles indices per Endos_Epi region

//    // Links in scene graph
//    // From PressureConstraintForceField
//    trianglesSurf
//    pointZoneNames
//    surfaceZoneNames
//    surfaceZones
//    pointZones

    // From Mooney-Rivlin
    Data<type::vector<type::Vec<10, Real>>> StiffnessParameters;

    // From ContractionForceField
    Data<type::vector<type::Vec<3, Real>>> facetFibers;
    Data<type::vector<type::Vec<4, Real>>> ContractionParameters;

};



} // namespace loader

} // namespace component

} // namespace sofa

