#include <sofa/config.h>


namespace sofa
{

	namespace component
	{

		void initExternalModule()
		{
		}

		const char* getModuleName()
		{
			return "Epione Mechanical Heart plugin";
		}

		const char* getModuleVersion()
		{
			return "2.0";
		}

		const char* getModuleLicense()
		{
			return "LGPL";
		}

		const char* getModuleDescription()
		{
			return "This plugin contain all Epione Mechanical Heart SOFA components.";
		}

		const char* getModuleComponentList()
		{
			return "BaseConstraintForceField, ContractionCouplingForceField, ContractionForceField, ContractionInitialization, MRForceField, PressureConstraintForceField, ProjectivePressureConstraint";
		}

	}

}
