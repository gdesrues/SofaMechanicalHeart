#include "PressureFF.h"
#include <sofa/core/ObjectFactory.h>
#include <sofa/defaulttype/VecTypes.h>
#include <sofa/core/visual/VisualParams.h>
#include <sofa/type/RGBAColor.h>
#include <SofaBaseTopology/TopologyData.inl>


namespace sofa
{

namespace component
{

namespace forcefield
{

using namespace sofa::defaulttype;


/*
 * ForceField used to compute the forces applied on the endocardium
 * due to ventricular pressure.
 */


template <class DataTypes>
PressureFF<DataTypes>::PressureFF()
    : updateMatrix(true)
    , l_topology(initLink("topology", "Link to TriangleSetTopologyContainer"))
    , holes(initData(&holes, "edges_on_border", "Link from VolumeEngine"))
    , d_dV(initData(&d_dV, "dV", "Volume derivative (from VolumeEngine)"))
    , d_pv_0(initData(&d_pv_0, "pv_0", "Initial ventricular pressure (Pa)"))
    , d_showForces(initData(&d_showForces, "showForces", "Visual param"))
    , Pv(initData(&Pv, "Pv", "Ventricular Pressure (Pa)"))
    , phase(initData(&phase, "phase", "link to PhaseManager.phase"))
    , Par(initData(&Par, "Par", "Arterial Pressure (Pa)"))
    , par_inf(initData(&par_inf, "par_inf", "Initial Arterial Pressure (linked from Par.par_inf)"))
    , Pat(initData(&Pat, "Pat", "Atrial Pressure (Pa)"))
    , d_Kat(initData(&d_Kat, "Kat", "Heamodynamic coefficient (atrial valves)"))
    , d_Kar(initData(&d_Kar, "Kar", "Heamodynamic coefficient (arterial valves)"))
    , d_KisoC(initData(&d_KisoC, "KisoC", "Kiso for isovol contraction"))
    , d_KisoR(initData(&d_KisoR, "KisoR", "Kiso for isovol cont"))
    , d_Zc(initData(&d_Zc, "Zc", "Windkessel param (Zc)"))
    , d_Tau(initData(&d_Tau, "Tau", "Windkessel param (Tau)"))
    , d_L(initData(&d_L, "L", "Windkessel param (L)"))
    , d_Rp(initData(&d_Rp, "Rp", "Windkessel param (Rp)"))
    , g_press(initData(&g_press, "graph_press", "Graph of pressures"))
    , edgeInfo(initData(&edgeInfo, "edgeInfo", "Data to handle topology on edges"))
{
    g_press.setWidget("graph");
}

template <class DataTypes>
void PressureFF<DataTypes>::init()
{
    msg_error_when(!l_topology) << "Link to topology failed (topology)";

    Pv.setValue(d_pv_0.getValue());
    Q_previous = 0;
}


template <class DataTypes>
void PressureFF<DataTypes>::update_internals(DataVecDeriv& _f, const DataVecCoord& _x, const DataVecDeriv& _v) {
    SOFA_UNUSED(_f);

    helper::ReadAccessor<DataVecCoord> x(_x);
    helper::ReadAccessor<DataVecDeriv> v(_v);
    helper::ReadAccessor<DataVecDeriv> dV(d_dV);

    Real Gs = 0;
    for(unsigned int i=0; i<x.size(); i++){
        Gs += dV[i] * v[i];
    }
    Q_previous = Q.getValue();
    Q = -Gs;
    msg_info() << "Q : " << Q;


    // Update graph
    Graph& graph = *(g_press.beginEdit());
    VecReal& g_pv = graph["Pv (Pa)"]; g_pv.push_back(Pv.getValue());
    VecReal& g_pat = graph["Pat"]; g_pat.push_back(Pat.getValue());
    VecReal& g_par = graph["Par"]; g_par.push_back(Par.getValue());
    g_press.endEdit();


    Real time = this->getTime();
    switch(phase.getValue()) {
        case engine::PhaseManager::FILLING:

            D = d_Kat.getValue();
            Fd = d_Kat.getValue() * Pat.getValue() - Gs;

            msg_info() << time << "s: Filling";
            break;

        case engine::PhaseManager::ISOCONTRACTION:
            D = d_KisoC.getValue();
            Fd = d_KisoC.getValue() * Pat.getValue() - Gs;

            msg_info() << time << "s: Isovolumetric Contraction";
            break;

        case engine::PhaseManager::ISORELAXATION:
            D = d_KisoR.getValue();
            Fd = d_KisoR.getValue() * Pat.getValue() - Gs;

            msg_info() << time << "s: Isovolumetric Relaxation";
            break;

        case engine::PhaseManager::EJECTION:

            int windkessel = 4;  // DEBUG

            Real d1, d0, P;
            Real dt = this->getContext()->getDt();

            // Haemodynamic params
            Real Kar = d_Kar.getValue();
            Real Kiso = d_KisoC.getValue();
            Real Pat_ = Pat.getValue();
            Real Par_ = Par.getValue();
            Real Par_Inf = par_inf.getValue();
            Real Q_ = Q.getValue();
            Real dQ = (Q_ - Q_previous) / dt;

            // Windkessel params
            Real Zc = d_Zc.getValue();
            Real Tau = d_Tau.getValue();
            Real L = d_L.getValue();
            Real Rp = d_Rp.getValue();

            if(windkessel==4)
            {
                d1=Kar/(Kar-Kiso)*(1.0+Tau/dt);
                d0=Rp+(1.0+Tau/dt)*(1.0/(Kar-Kiso)+Zc)+L/dt+L*Tau/(dt*dt);
                P=(Pat_*(1.0+Tau/dt)*(Kiso/(Kar-Kiso))+Tau/dt*Par_+Par_Inf)-((Tau*Zc+L)/dt+L*Tau/(dt*dt))*Q_-L*Tau/dt*dQ;
            }
            else if (windkessel==2)
            {
                d1=Kar/(Kar-Kiso)*(1.0+Tau/dt);
                d0=Rp+(1.0+Tau/dt)/(Kar-Kiso);
                P=(Pat_*(1.0+Tau/dt)*(Kiso/(Kar-Kiso))+Tau/dt*Par_+Par_Inf);
            }
            else if (windkessel==3)
            {
                d1=Kar/(Kar-Kiso)*(1.0+Tau/dt);
                d0=Rp+(1.0+Tau/dt)*(1.0/(Kar-Kiso)+Zc);
                P=(Pat_*(1.0+Tau/dt)*(Kiso/(Kar-Kiso))+Tau/dt*Par_+Par_Inf)-Tau*Zc*Q_/dt;
            }
            else if (windkessel==0)
            {
                d1=Kar;
                d0=1.0;
                P=((Kar-Kiso)*Par_+Kiso*Pat_);
            }

            D = d1 / d0;
            Fd = P / d0 - Gs;

            msg_info() << time << "s: Ejection";

            break;
    } // end switch(phase.getValue())
}


template <class DataTypes>
void PressureFF<DataTypes>::addForce(const core::MechanicalParams* mparams, DataVecDeriv& _f, const DataVecCoord& _x, const DataVecDeriv& _v) {
    SOFA_UNUSED(mparams);

    update_internals(_f, _x, _v);

    helper::WriteAccessor<DataVecDeriv> f(_f);
    helper::ReadAccessor<DataVecDeriv> dV(d_dV);
    Real press = Pv.getValue();

    for(unsigned long int i=0; i<f.size(); i++)
        f[i] += dV[i] * press;

    updateMatrix = true;
}


template <class DataTypes>
void PressureFF<DataTypes>::draw(const core::visual::VisualParams* vparams)
{
    if (!d_showForces.getValue())
        return;

    const VecCoord& x = this->mstate->read(core::ConstVecCoordId::position())->getValue();
    helper::ReadAccessor<DataVecDeriv> dV(d_dV);
//    vparams->drawTool()->disableLighting();
    const sofa::type::RGBAColor& color = sofa::type::RGBAColor::red();
    Real press = Pv.getValue();

    std::vector<type::Vector3> lines;
    for(unsigned long int i=0; i<x.size(); i++) {
        lines.push_back(x[i]);
        lines.push_back(x[i] + dV[i] * press);
    }
    vparams->drawTool()->drawLines(lines, 3, color);
}






template <class DataTypes>
void PressureFF<DataTypes>::updateMatrixData()
{
    // std::cout<< Pv<<std::endl;
    unsigned int a,b;
    Coord dp;
    const VecDeriv& x = this->mstate->read(core::ConstVecCoordId::position())->getValue();
    const std::vector< Triangle> &triangles=l_topology->getTriangles() ;
    unsigned int nbEdges=l_topology->getNbEdges();
    const type::vector< Edge> &edgeArray=l_topology->getEdges() ;





    edgeInfoVector& edgeInf = *(edgeInfo.beginEdit());
    edgeInf.resize(nbEdges);
    EdgeInformation *einfo;


//    msg_error() << "nbEdges: " << nbEdges;
//    msg_error() << "nb triangles: " << triangles.size();
//    msg_error() << "nb holes: " << holes.getValue().size();
//    msg_error() << "updateMatrix: " << updateMatrix;



    if (updateMatrix) {

        msg_error_when(!holes.isSet()) << "Boundary edges not linked from VolumeEngine";

        for(unsigned int l=0; l<nbEdges; l++ ) {
            edgeInf[l].DfDx.clear();
        }

        const int vertexSigneIndex[3][3][2]={{{5,5},{2,-1},{1,1}},{{2,1},{5,5},{0,-1}},{{1,-1},{0,1},{5,5}}};


        for (unsigned int i=0; i<triangles.size(); i++){
            EdgesInTriangle te=l_topology->getEdgesInTriangle(i);

            // Triangle tri=l_topology->getTriangle(SurfaceTriangles[i]);
            const Triangle &ta= triangles[i];

            for (int j=0; j<3;j++){
                einfo= &edgeInf[te[j]];
                EdgeID ei =te[j];
                Edge e=l_topology->getEdge(ei);
                if (e[0]==ta[0]) {
                    a=0;
                    if (e[1]==ta[1])b=1;
                    else b=2;
                }
                if (e[0]==ta[1]) {
                    a=1;
                    if (e[1]==ta[0])b=0;
                    else b=2;
                }
                if (e[0]==ta[2]) {
                    a=2;
                    if (e[1]==ta[1])b=1;
                    else b=0;
                }


                type::Matrix3 &edgeDfDx = einfo->DfDx;
                type::Matrix3 d2J;
                dp = x[ta[vertexSigneIndex[a][b][0]]]*vertexSigneIndex[a][b][1]/6.0;

                d2J[0][0] = 0;
                d2J[0][1] = -dp[2];
                d2J[0][2] = dp[1];
                d2J[1][0] = dp[2];
                d2J[1][1] = 0;
                d2J[1][2] = -dp[0];
                d2J[2][0] = -dp[1];
                d2J[2][1] = dp[0];
                d2J[2][2] = 0;


                edgeDfDx -= d2J * Pv.getValue();
            }
        }


        helper::ReadAccessor<Data<Holes>> _holes(holes);
        VecCoord centroid; centroid.resize(_holes.size());
        for(unsigned int u=0; u<_holes.size(); u++) // for each hole, compute the centroid
        {
            std::vector<Edge> hole = _holes[u];
            centroid[u].clear();

            for(unsigned int k=0; k<hole.size(); k++){
                Edge ei = hole[k];
                centroid[u] += x[ei[0]];
            }
            // centroid coords
            centroid[u] /= hole.size();
        }


        if (_holes.size()>0){
            for(unsigned int u=0; u<_holes.size(); u++){
                std::vector<Edge> hole=_holes[u];
                for (unsigned int i=0; i<hole.size();i++){
                    Edge ea=hole[i];
                    Edge eb=hole[(i+1)%hole.size()];
                    EdgeID eia =l_topology->getEdgeIndex(ea[0],ea[1]);
                    einfo= &edgeInf[eia];
                    Matrix3 &edgeDfDx = einfo->DfDx;

                    type::MatNoInit<3,3,Real> d2J;
                    //dp =( x[ea[0]]-centroid[u])/6.0+(x[ea[0]]-x[eb[1]])/(hole.size()*6.0);
                    dp =( x[ea[0]]-centroid[u])/6.0;
                    d2J[0][0] = 0;
                    d2J[0][1] = -dp[2];
                    d2J[0][2] = dp[1];
                    d2J[1][0] = dp[2];
                    d2J[1][1] = 0;
                    d2J[1][2] = -dp[0];
                    d2J[2][0] = -dp[1];
                    d2J[2][1] = dp[0];
                    d2J[2][2] = 0;

                    edgeDfDx -= d2J * Pv.getValue();

                }
            }
        }


        updateMatrix = false;
    }
}


template <class DataTypes>
void PressureFF<DataTypes>::addDForce(const core::MechanicalParams* mparams /* PARAMS FIRST */, DataVecDeriv& d_df, const DataVecDeriv& d_dx)
{

    unsigned int l=0;
    unsigned int nbEdges=l_topology->getNbEdges();
    const type::vector<core::topology::BaseMeshTopology::Edge> &edgeArray=l_topology->getEdges();

    edgeInfoVector& edgeInf = *(edgeInfo.beginEdit());
    EdgeInformation *einfo;

    this->updateMatrixData();

    /// performs matrix vector computation
    unsigned int v0,v1;
    Deriv deltax;	Deriv dv0,dv1;
    const VecDeriv dx=d_dx.getValue();
    VecDeriv& df=*(d_df.beginEdit());
    double kfactor = mparams->kFactor();

    for(l=0; l<nbEdges; l++ )
    {
        einfo=&edgeInf[l];
        v0=edgeArray[l][0];
        v1=edgeArray[l][1];

        deltax= dx[v0] - dx[v1];
        dv0 = einfo->DfDx * deltax;



        // do the transpose multiply:
        dv1[0] = (Real)(deltax[0]*einfo->DfDx[0][0] + deltax[1]*einfo->DfDx[1][0] + deltax[2]*einfo->DfDx[2][0]);
        dv1[1] = (Real)(deltax[0]*einfo->DfDx[0][1] + deltax[1]*einfo->DfDx[1][1] + deltax[2]*einfo->DfDx[2][1]);
        dv1[2] = (Real)(deltax[0]*einfo->DfDx[0][2] + deltax[1]*einfo->DfDx[1][2] + deltax[2]*einfo->DfDx[2][2]);
        // add forces
        df[v0] += dv1*kfactor;
        df[v1] -= dv0*kfactor;

    }
    d_df.endEdit();
    edgeInfo.endEdit();

}

template <class DataTypes>
void PressureFF<DataTypes>::addKToMatrix(linearalgebra::BaseMatrix *m, SReal kFactor, unsigned int &offset) {

    unsigned int l=0;
    unsigned int nbEdges=l_topology->getNbEdges();
    const type::vector<core::topology::Topology::Edge> &edgeArray=l_topology->getEdges();

    edgeInfoVector& edgeInf = *(edgeInfo.beginEdit());
    EdgeInformation *einfo;

    this->updateMatrixData();

    /// performs matrix vector computation
    unsigned int v0,v1;

    for(l=0; l<nbEdges; l++ )
    {
        einfo=&edgeInf[l];
        v0=offset + edgeArray[l][0]*3;
        v1=offset + edgeArray[l][1]*3;

        for (int L=0;L<3;L++) {
            for (int C=0;C<3;C++) {
                double v = einfo->DfDx[L][C] * kFactor;
                m->add(v0+C,v0+L, v);
                m->add(v0+C,v1+L,-v);
                m->add(v1+L,v0+C,-v);
                m->add(v1+L,v1+C, v);
            }
        }
    }

    edgeInfo.endEdit();
}







int PressureFFClass = core::RegisterObject("Endocardium Pressure")
        .add< PressureFF<Vec3Types> >();



} // forcefield

} // namespace component

} // namespace sofa
