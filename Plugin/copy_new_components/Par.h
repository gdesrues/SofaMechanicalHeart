#pragma once
#include <sofa/core/objectmodel/BaseObject.h>
#include <sofa/core/objectmodel/Event.h>
#include <sofa/defaulttype/VecTypes.h>
#include "PressureFF.h"
#include "utils.h"

namespace sofa
{

namespace component
{


class Par : public sofa::core::objectmodel::BaseObject
{
public:
    SOFA_CLASS(Par, sofa::core::objectmodel::BaseObject);

    Data<Real> pressure;
    Data<Real> par_0, par_inf;
    Data<int> phase;

    SingleLink<Par, forcefield::PressureFF<defaulttype::Vec3Types>, BaseLink::FLAG_STOREPATH | BaseLink::FLAG_STRONGLINK> l_vent;

    Par();
    void init() override;
    void compute_par();
    void handleEvent(sofa::core::objectmodel::Event *event);

};


} // namespace component

} // namespace sofa

