#pragma once
#include <sofa/core/DataEngine.h>
#include "utils.h"

namespace sofa
{

namespace component
{

namespace engine
{


class PhaseManager : public sofa::core::DataEngine
{
public:
    SOFA_CLASS(PhaseManager, sofa::core::DataEngine);

    // Cardiac cycle phases
    enum Phase : unsigned int {
        FILLING = 0,
        ISOCONTRACTION = 1,
        EJECTION = 2,
        ISORELAXATION=3
    };

    Data<Real> Pv, Pat, Par;
    Data<int> current_phase;

    PhaseManager();
    void init() override;
    void doUpdate() override;

};

} // namespace engine

} // namespace component

} // namespace sofa

