#include "VolumeEngine.h"
#include <sofa/core/ObjectFactory.h>
#include <SofaBaseTopology/CommonAlgorithms.h>


namespace sofa
{

namespace component
{

namespace engine
{

using namespace sofa::defaulttype;

/*
 * Engine used to compute the endocardium volume and its derivative
 * with respect to nodal position.
 *
 * Input: d_position (MechanicalObject.position)
 * Outputs: d_volume, d_dV
 */


template <class DataTypes>
VolumeEngine<DataTypes>::VolumeEngine()
    : l_topology(initLink("topology", "Link to topology"))
    , d_position(initData(&d_position, "position", "input positions"))
    , d_volume(initData(&d_volume, "volume", "Volume of the ventricle"))
    , d_dV(initData(&d_dV, "dV", "Derivative of the volume with respect to endocardium vertices"))
    , d_show_boundary_edges(initData(&d_show_boundary_edges, false, "show_boundary_edges", ""))
    , g_volume(initData(&g_volume, "graph", ""))
    , holes(initData(&holes, "edges_on_border", ""))
{
    g_volume.setWidget("graph");
}

template <class DataTypes>
VolumeEngine<DataTypes>::~VolumeEngine()
{
    this->f_listening.setValue(true);
}

template <class DataTypes>
void VolumeEngine<DataTypes>::init()
{
    msg_error_when(!l_topology) << "Link to topology failed (topology)";

    addInput(&d_position);

    get_boundary_edges();

    addOutput(&d_volume);
    addOutput(&d_dV);
}



template <class DataTypes>
void VolumeEngine<DataTypes>::get_boundary_edges() {
    l_topology->createElementsOnBorder();
    type::vector<EdgeID> edges = l_topology->getEdgesOnBorder();

    msg_error_when(edges.size() == 0) << "edges array is empty";
//    msg_info() << "edges.size(): " << edges.size();

    VecBool used; used.resize(edges.size());
    helper::WriteAccessor<Data<Holes>> _holes(holes);
    _holes.clear();

    int n(0);
    while(!all_used(used) and n<5) {
        n++;
        Hole hole;

        // Ensure first edge is well oriented
        int idx = get_first(used);
        Edge edge = l_topology->getEdge(edges[idx]);
        TrianglesAroundEdge tae = l_topology->getTrianglesAroundEdge(idx);
        Triangle tri = l_topology->getTriangle(tae[0]);

        for(PointID x : tri) {
            if(x == edge[0]) break;
            else if(x == edge[1]) {edge = Edge(edge[1], edge[0]); break;}
        }

        int next = edge[1];
        hole.push_back(edge);
        used[idx] = true;

        for(int k=0; k<edges.size(); k++) {
            for (int i = 0; i < edges.size(); i++) {
                if (!used[i]) {
                    Edge e = l_topology->getEdge(edges[i]);
                    if ((e[0] == next) or (e[1] == next)) {
                        if (e[1] == next) e = Edge(e[1], e[0]);
                        used[i] = true;
                        hole.push_back(e);
                        next = e[1];
                    }
                }
            }
        }
        _holes.push_back(hole);
    }

    int nb_nnused(0); for(bool x:used) if(!x) nb_nnused++;
    msg_warning_when(nb_nnused>0) << "Boundary edges detection may have failed, found "
        << nb_nnused << " edges not used";

    msg_info() << "Found " << _holes.size() << " hole(s)";
}


template <class DataTypes>
void VolumeEngine<DataTypes>::compute_volume() {
    helper::ReadAccessor< Data<VecCoord> > x(d_position);
    Real vol = 0;

    // loop over all triangles and add their contribution
    for(Triangle t : l_topology->getTriangleArray())
        vol += topology::tripleProduct(x[t[0]], x[t[1]], x[t[2]]);

    // Compute the hole(s) contribution
    for(Hole hole : holes.getValue()) {
        Coord center;
        for(Edge e : hole) center += x[e[0]];
        center /= hole.size();
        for(Edge e : hole)
            vol += topology::tripleProduct(x[e[0]], center, x[e[1]]);
    }

    d_volume.setValue(std::abs(vol) / 6);
//    msg_info() << "Volume: " << d_volume.getValue() * 1e6 << " mL";
}


template <class DataTypes>
void VolumeEngine<DataTypes>::compute_volume_derivative() {
    helper::ReadAccessor <Data<VecCoord>> x(d_position);
    VecDeriv dvol; dvol.resize(x.size());

    for(PointID i=0; i<x.size(); i++)
        for(TriangleID idx : l_topology->getTrianglesAroundVertex(i)) {
            Triangle t = l_topology->getTriangle(idx);
            dvol[i] -= area(x[t[0]], x[t[1]], x[t[2]]) / 6;
        }

    for(Hole hole : holes.getValue()) {
        Coord center;
        for(Edge e : hole) center += x[e[0]];
        center /= hole.size();
        for(Edge e : hole) {
            Deriv val = area(x[e[0]], x[e[1]], center) / 6;
            dvol[e[0]] += val;
            dvol[e[1]] += val;
        }
    }

    d_dV.setValue(dvol);
//    msg_info() << "Volume derivative: " << d_dV.getValue();
}


template <class DataTypes>
void VolumeEngine<DataTypes>::doUpdate()
{
    compute_volume();
    Graph& graph = *(g_volume.beginEdit());
    VecReal& gv = graph["Volume (mL)"];
    gv.push_back(d_volume.getValue() * 1e6);
    g_volume.endEdit();

    compute_volume_derivative();
}


template <class DataTypes>
void VolumeEngine<DataTypes>::draw(const core::visual::VisualParams* vparams)
{
    if (!d_show_boundary_edges.getValue())
        return;

    helper::ReadAccessor< Data<VecCoord> > x(d_position);

    std::vector<type::Vector3> lines;
    for(Hole hole : holes.getValue())
        for(Edge e : hole) {
            lines.push_back((type::Vector3) x[e[0]]);
            lines.push_back((type::Vector3) x[e[1]]);
        }

    vparams->drawTool()->drawLines(lines, 6, type::RGBAColor::white());
}


int VolumeEngineClass = sofa::core::RegisterObject("Compute volume and its derivative")
        .add< VolumeEngine<Vec3Types> >();


} // engine

} // namespace component

} // namespace sofa
