#include "Pat.h"
#include <sofa/core/ObjectFactory.h>
#include <sofa/simulation/AnimateBeginEvent.h>

namespace sofa
{

namespace component
{


Pat::Pat()
    : pat_0(initData(&pat_0, "pat_0", "Initial atrial pressure (Pa)"))
    , pat_max(initData(&pat_max, "pat_max", "Max atrial pressure (Pa)"))
    , heart_period(initData(&heart_period, "heart_period", "Heart period (s)"))
    , t1(initData(&t1, "t1", "First (start) time for sigmoids (see implementation)"))
    , t2(initData(&t2, "t2", "Second (middle) time for sigmoids (see implementation)"))
    , t3(initData(&t3, "t3", "Third (last) time for sigmoids (see implementation)"))
    , alpha1(initData(&alpha1, "alpha1", "First coefficient for sigmoids (see implementation)"))
    , alpha2(initData(&alpha2, "alpha2", "Second coefficient for sigmoids (see implementation)"))
    , pressure(initData(&pressure, "pressure", "Atrial pressure (Pa)"))
{
    this->f_listening.setValue(true);
}


void Pat::init()
{
    pressure.setValue(pat_0.getValue());
}


// Quick fix, best would be to compute only if needed
void Pat::handleEvent(sofa::core::objectmodel::Event *event){
    if(simulation::AnimateBeginEvent::checkEventType(event)) compute_pat();
    BaseObject::handleEvent(event);
}


void Pat::compute_pat()
{
    Real t = this->getTime();
//    msg_info() << "Time: " << t;
    Real hp = heart_period.getValue();
    Real offset = int(t / hp) * hp;

    Real pat_new = pressure.getValue();
    Real _t1 = offset + t1.getValue();
    Real _t2 = offset + t2.getValue();
    Real _t3 = offset + t3.getValue();
    Real T1 = 0.5 * (_t1 + _t2);
    Real T2 = 0.5 * (_t2 + _t3);

    Real _pat_0 = pat_0.getValue();
    Real _pat_max = pat_max.getValue();
    Real _alpha1 = alpha1.getValue();
    Real _alpha2 = alpha2.getValue();

    if(t <= _t2 and t >= _t1)
        // first sigmoid, increase pressure
        pat_new = _pat_0 * (1 + ( _pat_max / _pat_0 - 1 )
                / ( 1 + exp(-_alpha1 * (t - T1)) ));
    else if(t > _t2 and t <= _t1+hp)
        // second, decrease pressure
        pat_new = _pat_0 * (1 + ( _pat_max / _pat_0 - 1 )
                / ( 1 + exp(_alpha2 * (t - T2)) ));

    pressure.setValue(pat_new);
//    msg_info() << "New value: " << pressure.getValue();
}


int PatClass = sofa::core::RegisterObject("Computes the atrial pressure with sigmoids").add<Pat>();


} // namespace component

} // namespace sofa
