#include "PhaseManager.h"
#include <sofa/core/ObjectFactory.h>


namespace sofa
{

namespace component
{

namespace engine
{


PhaseManager::PhaseManager()
    : Pv(initData(&Pv, "Pv", "Ventricular pressure"))
    , Pat(initData(&Pat, "Pat", "Atrial pressure"))
    , Par(initData(&Par, "Par", "Arterial pressure"))
    , current_phase(initData(&current_phase, "phase", "Current phase"))
{
}


void PhaseManager::init()
{
    addInput(&Pv);
    addInput(&Pat);
    addInput(&Par);

    addOutput(&current_phase);
}


void PhaseManager::doUpdate()
{
    switch (current_phase.getValue()) {
        case FILLING:
            if(Pv.getValue() > Pat.getValue())
            {
                // FILLING: blood from atria, volume increases, almost constant pressure
                // The pressure increases due to the electrical activation and muscle contraction
                // When above atrial pressure, tricupid valves close
                current_phase.setValue(ISOCONTRACTION);
            }
            break;

        case ISOCONTRACTION:
            if(Pv.getValue() > Par.getValue())
            {
                // ISOCONTRACTION: All valves are closed, the pressure increases rapidly, constant volume
                current_phase.setValue(EJECTION);
            }
            break;

        case EJECTION:
            if(Pv.getValue() < Par.getValue())
            {
                // EJECTION: volume ejects the blood through the aorta
                // Ventricular pressure decreases and the aortic valves close under the aortic pressure
                // All valves are closed, the pressure decreases rapidly
                current_phase.setValue(ISORELAXATION);
            }
            break;

        case ISORELAXATION:
            if(Pv.getValue() <= Pat.getValue())
            {
                // ISORELAXATION:
                // When the ventricular pressure falls under the atrial pressure, the atrial valves open
                // The ventricle receives blood from atria, the volume is increasing
                current_phase.setValue(FILLING);
            }
            break;
    }
}


int PhaseManagerClass = sofa::core::RegisterObject("Computes the next phase of the ventricle cardiac cycle")
        .add<PhaseManager>();

} // namespace engine

} // namespace component

} // namespace sofa
