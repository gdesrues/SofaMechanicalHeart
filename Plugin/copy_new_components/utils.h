#pragma once
#include <string>


// .setRequired(true); on a Data in constructor
namespace sofa {

    typedef typename defaulttype::Vec3Types::VecCoord VecCoord;
    typedef typename defaulttype::Vec3Types::VecDeriv VecDeriv;
    typedef typename defaulttype::Vec3Types::Coord Coord;
    typedef typename defaulttype::Vec3Types::Deriv Deriv;
    typedef typename Coord::value_type Real;
    typedef typename type::vector<Real> VecReal;
    typedef typename std::string String;
    typedef typename core::objectmodel::Data<std::map<String, VecReal>> DataGraph;

    /*
     * Helper to write to files
     */
    template <class... Ts>
    void sdump(std::ostream& out, std::string sep, Ts&&... args){
        std::stringstream o;
        ((o << args << sep), ...);
        std::string s = o.str();
        s = s.substr(0, s.size() - sep.size());
        out << s << std::endl;
    }

    template <class... Ts>
    void dump(std::ostream& out, Ts&&... args){
        std::string sep = ",";
        sdump(out, sep, args...);
    }
}


// virtual void addForce(const MechanicalParams* mparams, DataVecDeriv& _f, const DataVecCoord& _x, const DataVecDeriv& _v) { }
// virtual void addDForce(const MechanicalParams* mparams, DataVecDeriv& _df, const DataVecDeriv& _dx) { }
// virtual SReal getPotentialEnergy(const MechanicalParams* mparams, const DataVecCoord& _x) const {return 0;}