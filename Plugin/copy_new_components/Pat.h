#pragma once
#include <sofa/core/objectmodel/BaseObject.h>
#include <sofa/core/objectmodel/Event.h>
#include "utils.h"

namespace sofa
{

namespace component
{



class Pat : public sofa::core::objectmodel::BaseObject
{
public:
    SOFA_CLASS(Pat, sofa::core::objectmodel::BaseObject);

    Data<Real> pat_0, pat_max, heart_period;
    Data<Real> t1, t2, t3, alpha1, alpha2;
    Data<Real> pressure;

    Pat();
    void init() override;
    void compute_pat();
    void handleEvent(sofa::core::objectmodel::Event *event);

};


} // namespace component

} // namespace sofa

