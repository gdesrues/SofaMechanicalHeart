#include "Par.h"
#include <sofa/core/ObjectFactory.h>
#include <sofa/simulation/AnimateBeginEvent.h>
#include "PhaseManager.h"
#include <sofa/helper/StringUtils.h>


namespace sofa
{

namespace component
{


Par::Par()
    : pressure(initData(&pressure, "pressure", "Arterial pressure (Pa)"))
    , phase(initData(&phase, "phase", "Link to PhaseManager.phase"))
    , l_vent(initLink("ff", "Link to the PressureFF component"))
    , par_0(initData(&par_0, "par_0", "initial aortic pressure"))
    , par_inf(initData(&par_inf, "par_inf", "asymptotic limit of aortic pressure"))
{
    this->f_listening.setValue(true);
}


void Par::init() {
    msg_error_when(!l_vent) << "Could not find the link to the PressureFF component ('ff')";
    pressure.setValue(par_0.getValue());
}


void Par::compute_par() {
    int windkessel = 4;

    Real dt = this->getContext()->getDt();
    Real Q = l_vent->Q.getValue();
    Real Tau = l_vent->d_Tau.getValue();
    Real Pv = l_vent->Pv.getValue();
    Real Pat = l_vent->Pat.getValue();
    Real Kar = l_vent->d_Kar.getValue();
    Real Kiso = l_vent->d_KisoC.getValue();
    Real par = pressure.getValue();
    Real par_asymp = par_inf.getValue();

    Real Parnew;

    if(phase.getValue() == engine::PhaseManager::EJECTION) {
        Parnew = (Q - Pv * Kar + Pat * Kiso) / (Kiso - Kar);
        if (windkessel == 0) Parnew = par;
    } else {

//        msg_info() << "Tau: " << Tau <<
//            ", and par: " << par <<
//            ", and dt: " << dt <<
//            ", and par_asymp: " << par_asymp;

        Parnew = (Tau * par + dt * par_asymp) / (dt + Tau);
        if (windkessel == 0) Parnew = par;
    }

    pressure.setValue(Parnew);
}


void Par::handleEvent(sofa::core::objectmodel::Event *event){
    if(simulation::AnimateBeginEvent::checkEventType(event)) compute_par();
    BaseObject::handleEvent(event);
}


int ParClass = sofa::core::RegisterObject("Compute the arterial pressure with WindKessel model").add<Par>();


} // namespace component

} // namespace sofa
