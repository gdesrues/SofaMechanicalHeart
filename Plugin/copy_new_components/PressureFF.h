#pragma once
#include <sofa/core/behavior/ForceField.h>
#include <sofa/core/MechanicalParams.h>
#include "PhaseManager.h"
#include <sofa/helper/map.h>
#include <SofaBaseTopology/TriangleSetTopologyContainer.h>






#include <SofaBaseMechanics/MechanicalObject.h>

#include <sofa/type/fixed_array.h>
#include <sofa/type/vector.h>
#include <sofa/type/Vec.h>
#include <sofa/type/Mat.h>

#include <SofaBaseTopology/TopologyData.h>
#include <sofa/core/topology/BaseMeshTopology.h>

#include <SofaBaseTopology/TetrahedronSetTopologyContainer.h>

#include <sofa/core/topology/BaseMeshTopology.h>
#include <string>

#include <map>

#include <sofa/core/behavior/OdeSolver.h>
#include <sofa/core/visual/VisualModel.h> /// really important for graphs
#include <sofa/helper/map.h>







namespace sofa
{

namespace component
{

namespace forcefield
{

template<class DataTypes>
class PressureFF : public core::behavior::ForceField<DataTypes>
{
public:
    SOFA_CLASS(SOFA_TEMPLATE(PressureFF, DataTypes), SOFA_TEMPLATE(core::behavior::ForceField, DataTypes));

    typedef typename DataTypes::VecCoord VecCoord;
    typedef typename DataTypes::VecDeriv VecDeriv;
    typedef typename DataTypes::Coord    Coord   ;
    typedef typename DataTypes::Deriv    Deriv   ;
    typedef typename Coord::value_type   Real    ;
    typedef type::Mat<3,3,Real> Matrix3;


    typedef sofa::core::topology::Topology::Triangle Triangle;
    typedef sofa::core::topology::Topology::TriangleID TriangleID;
    typedef sofa::core::topology::Topology::Edge Edge;
    typedef sofa::core::topology::Topology::EdgeID EdgeID;
    typedef sofa::core::topology::BaseMeshTopology::EdgesInTriangle EdgesInTriangle;
    typedef typename type::vector<Edge> Hole;
    typedef typename type::vector<Hole> Holes;

    typedef Data<VecCoord> DataVecCoord;
    typedef Data<VecDeriv> DataVecDeriv;
    using Index = sofa::Index;
    typedef typename type::vector<Real> VecReal;
    typedef typename std::map<std::string, VecReal> Graph;

    Data<Graph> g_press;

    DataVecDeriv d_dV;
    Data<Real> d_pv_0, Pv, Par, par_inf, Pat;
    Data<bool> d_showForces;
    Data<int> phase;

    Data<Real> d_Kat, d_Kar, d_KisoC, d_KisoR;
    Data<Real> d_Zc, d_Tau, d_L, d_Rp;

    Real D, Fd;
    Real Q_previous;

    Data<Real> Q;

    SingleLink<PressureFF, topology::TriangleSetTopologyContainer, BaseLink::FLAG_STOREPATH | BaseLink::FLAG_STRONGLINK> l_topology;

    PressureFF();
    void init() override;
    void draw(const core::visual::VisualParams*) override;
    void update_internals(DataVecDeriv& _f, const DataVecCoord& _x, const DataVecDeriv& _v);

    void addForce(const core::MechanicalParams* mparams, DataVecDeriv& _f, const DataVecCoord& _x, const DataVecDeriv& _v);
    void addDForce(const core::MechanicalParams* mparams, DataVecDeriv& df, const DataVecDeriv& dx );
    SReal getPotentialEnergy(const core::MechanicalParams* /*mparams*/, const DataVecCoord& x) const {return 0;}
    void addKToMatrix(linearalgebra::BaseMatrix *m, SReal kFactor, unsigned int &offset);
    void updateMatrixData();


    class EdgeInformation
    {
    public:
        /// store the stiffness edge matrix
        type::Matrix3 DfDx;

        /// Output stream
        inline friend std::ostream& operator<< ( std::ostream& os, const EdgeInformation& /*eri*/ ) {  return os;  }
        /// Input stream
        inline friend std::istream& operator>> ( std::istream& in, EdgeInformation& /*eri*/ ) { return in; }

        EdgeInformation() { }
    };
    typedef typename VecCoord::template rebind<EdgeInformation>::other edgeInfoVector;
    topology::EdgeData<edgeInfoVector> edgeInfo;
//    topology::EdgeData<type::vector<EdgeInformation>> edgeInfo;

    bool updateMatrix;



    Data<Holes> holes;

};

} // namespace forcefield

} // namespace component

} // namespace sofa

