#pragma once
#include <sofa/core/DataEngine.h>

#include <SofaBaseTopology/TriangleSetTopologyContainer.h>
#include <SofaBaseTopology/TriangleSetGeometryAlgorithms.h>
#include <sofa/core/visual/VisualParams.h>
#include <sofa/helper/map.h>

namespace sofa
{

namespace component
{

namespace engine
{

template<class DataTypes>
class VolumeEngine : public sofa::core::DataEngine
{
public:
    SOFA_CLASS(SOFA_TEMPLATE(VolumeEngine, DataTypes), sofa::core::DataEngine);

    typedef typename DataTypes::VecCoord VecCoord;
    typedef typename DataTypes::VecDeriv VecDeriv;
    typedef typename DataTypes::Coord    Coord   ;
    typedef typename DataTypes::Deriv    Deriv   ;
    typedef typename Coord::value_type   Real    ;
    typedef typename type::vector<bool> VecBool;

    typedef topology::TriangleSetTopologyContainer::PointID                      PointID;
    typedef topology::TriangleSetTopologyContainer::EdgeID                       EdgeID;
    typedef topology::TriangleSetTopologyContainer::TriangleID                   TriangleID;
    typedef topology::TriangleSetTopologyContainer::Edge                         Edge;
    typedef topology::TriangleSetTopologyContainer::Triangle                     Triangle;
    typedef topology::TriangleSetTopologyContainer::SeqTriangles                 SeqTriangles;
    typedef topology::TriangleSetTopologyContainer::EdgesInTriangle              EdgesInTriangle;
    typedef topology::TriangleSetTopologyContainer::TrianglesAroundVertex        TrianglesAroundVertex;
    typedef topology::TriangleSetTopologyContainer::TrianglesAroundEdge          TrianglesAroundEdge;
    typedef sofa::type::vector<TriangleID>                                       VecTriangleID;

    typedef typename type::vector<Edge> Hole;
    typedef typename type::vector<Hole> Holes;


    typedef typename type::vector<Real> VecReal;
    typedef typename std::map<std::string, VecReal> Graph;



    SingleLink<VolumeEngine, topology::TriangleSetTopologyContainer, BaseLink::FLAG_STOREPATH | BaseLink::FLAG_STRONGLINK> l_topology;

    Data<VecCoord> d_position;
    Data<Real> d_volume;
    Data<VecDeriv> d_dV;
    Data<bool> d_show_boundary_edges;
    Data<Holes> holes;

    Data<Graph> g_volume;


    VolumeEngine();
    ~VolumeEngine() override;
    void init() override;
    void doUpdate() override;
    void get_boundary_edges();
    void compute_volume();
    void compute_volume_derivative();
    void draw(const core::visual::VisualParams* vparams) override;



    bool all_used(VecBool &x) const {
        for(bool y : x) if(!y) return false;
        return true;
    }

    int get_first(VecBool &x) const {
        for(int i=0; i<x.size(); i++) if(!x[i]) return i;
        return -1;
    }

    Deriv area(const Coord &a, const Coord &b, const Coord &c) {
        return cross(b-a, c-a) * 0.5;
    }
};

} // namespace engine

} // namespace component

} // namespace sofa

