#include "MRForceField.h"

#include <SofaBaseMechanics/MechanicalObject.h>
#include <sofa/core/ObjectFactory.h>
#include <fstream>
#include <iostream>
#include <GL/gl.h>
#include <SofaBaseTopology/TopologyData.inl>
#include <sofa/core/behavior/ForceField.inl>
#include <algorithm>
#include <iterator>
#include <sofa/core/visual/VisualParams.h>
#include <sofa/helper/AdvancedTimer.h>

namespace sofa
{
    namespace component
    {
        namespace forcefield
        {
			using namespace sofa::type;
			using namespace	sofa::component::topology;
			using namespace core::topology;
			using core::topology::BaseMeshTopology;

			typedef BaseMeshTopology::Tetra				Tetra;
			typedef BaseMeshTopology::EdgesInTetrahedron		EdgesInTetrahedron;

			typedef Tetra			Tetrahedron;
			typedef EdgesInTetrahedron		EdgesInTetrahedron;




			template< class DataTypes>
			void MRForceField<DataTypes>::MREdgeHandler::applyCreateFunction(unsigned int, EdgeInformation &ei,
				const Edge& ,  const type::vector< unsigned int > &,
				const type::vector< double >&)
			{
				if (ff) {

					unsigned int u,v;
					/// set to zero the stiffness matrix
					for (u=0;u<3;++u) {
						for (v=0;v<3;++v) {
							ei.DfDx[u][v]=0;

						}
					}

				}
			}


			template< class DataTypes>
			void MRForceField<DataTypes>::MRTetrahedronHandler::applyCreateFunction(unsigned int tetrahedronIndex,
				TetrahedronRestInformation &tinfo,const Tetrahedron& , const type::vector< unsigned int > &,
				const type::vector< double >&)
			{
				if (ff) {
					const vector< Tetrahedron > &tetrahedronArray=ff->_topology->getTetrahedra() ;
					const std::vector< Edge> &edgeArray=ff->_topology->getEdges() ;
					unsigned int j,m,n;
					int k,l;
					typename DataTypes::Real volume;
#ifdef SOFA_HAVE_EIGEN2
					typedef typename Eigen::SelfAdjointEigenSolver<Eigen::Matrix<Real,3,3> >::MatrixType EigenMatrix;
					typedef typename Eigen::SelfAdjointEigenSolver<Eigen::Matrix<Real,3,3> >::RealVectorType CoordEigen;
#endif

                    const typename DataTypes::VecCoord restPosition = ff->mstate->read(core::ConstVecCoordId::restPosition())->getValue();
					///describe the indices of the 4 tetrahedron vertices
					const Tetrahedron &t= tetrahedronArray[tetrahedronIndex];
					BaseMeshTopology::EdgesInTetrahedron te=ff->_topology->getEdgesInTetrahedron(tetrahedronIndex);

					//store point indices
					tinfo.tetraIndices[0] = (float)t[0];
					tinfo.tetraIndices[1] = (float)t[1];
					tinfo.tetraIndices[2] = (float)t[2];
					tinfo.tetraIndices[3] = (float)t[3];
					//store edges
					tinfo.tetraEdges[0] = (float)te[0];
					tinfo.tetraEdges[1] = (float)te[1];
					tinfo.tetraEdges[2] = (float)te[2];
					tinfo.tetraEdges[3] = (float)te[3];
					tinfo.tetraEdges[4] = (float)te[4];
					tinfo.tetraEdges[5] = (float)te[5];
					//store parameters
					tinfo.f_parameters = ff->getParameters(tetrahedronIndex);

					// store the point position

					for(j=0;j<4;++j)
                        tinfo.pointP[j]=(restPosition)[t[j]];
					/// compute 6 times the rest volume
					volume=dot(cross(tinfo.pointP[2]-tinfo.pointP[0],tinfo.pointP[3]-tinfo.pointP[0]),tinfo.pointP[1]-tinfo.pointP[0]);
					/// store the rest volume
					tinfo.volScale =(Real)(1.0/volume);
					tinfo.restVolume = fabs(volume/6);

					// store shape vectors at the rest configuration
					for(j=0;j<4;++j) {
						if (!(j%2))
							tinfo.shapeVector[j]=-cross(tinfo.pointP[(j+2)%4] - tinfo.pointP[(j+1)%4],tinfo.pointP[(j+3)%4] -tinfo.pointP[(j+1)%4])/ volume;
						else
							tinfo.shapeVector[j]=cross(tinfo.pointP[(j+2)%4] - tinfo.pointP[(j+1)%4],tinfo.pointP[(j+3)%4] - tinfo.pointP[(j+1)%4])/ volume;;
					}

                    ///stress calc
                    for(m=0;m<3; m++){
                        for(n=0;n<3;++n){
                            tinfo.SecSPK_p[m][n]=0;
                        }
                    }
                    ///end stress calc

					for(j=0;j<6;++j) {
						Edge e=ff->_topology->getLocalEdgesInTetrahedron(j);
						k=e[0];
						l=e[1];
						if (edgeArray[te[j]][0]!=t[k]) {
							k=e[1];
							l=e[0];
						}
						for(m=0;m<3;++m){
							for(n=0;n<3;++n){
								tinfo.DicrossDj[j][m][n]=tinfo.shapeVector[k][m]*tinfo.shapeVector[l][n];
							}
						}

					}





					/// get the parameters
					Vec3 param=ff->getParameters(tetrahedronIndex);
					tinfo.k0=param[2];
					tinfo.c1=param[0];
					tinfo.c2=param[1];



					//we want to precomput Lij=2BDicrossDj(B) or Lij=C(DicrossDj^TD^T+djdotDDi)
					Matrix3 id;
					id.identity();

                    tinfo.specialcoeff=1;

					for(j=0;j<6;++j) {

						Edge e=ff->_topology->getLocalEdgesInTetrahedron(j);
						k=e[0];
						l=e[1];
						if (edgeArray[te[j]][0]!=t[k]) {
							k=e[1];
							l=e[0];
						}

						tinfo.Lij[j]=(tinfo.DicrossDj[j]*2.0 - id*dot(tinfo.shapeVector[l],tinfo.shapeVector[k])
							- tinfo.DicrossDj[j].transposed());
					}


				}//end if(ff)

            }

			template<class DataTypes>
			Vec<3,double>  MRForceField<DataTypes>::getParameters(const unsigned int tetraIndex)
			{

				Vec3 fibers;
				fibers.clear();

				if ((f_parameterSet.getValue()).size()==1){

                    Vec3 param=f_parameterSet.getValue()[0];


                    fibers[0] =param[0];
                    fibers[1] =param[1];
                    fibers[2] =param[2];

				}
				else{
                    Vec3 param=(f_parameterSet.getValue())[tetraIndex];

                    fibers[0] =param[0];
                    fibers[1] =param[1];
                    fibers[2] =param[2];


				}


				return fibers;
			}



			template< class DataTypes> MRForceField<DataTypes>::MRForceField()
				: _topology(0)
				, _initialPoints(0)
				, updateMatrix(true)
				, _meshSaved( false)
				, f_stiffnessMatrixRegularizationWeight(initData(&f_stiffnessMatrixRegularizationWeight,false,"matrixRegularization","Regularization of the Stiffness Matrix (true or false)"))
                , f_parameterSet(initData(&f_parameterSet,"ParameterSet","The global parameters specifying the Mooney-rivlin material"))
				, tetrahedronInfo(initData(&tetrahedronInfo,"tetrahedronInfo","Data to handle topology on tetrahedra"))
				, edgeInfo(initData(&edgeInfo,"edgeInfo","Data to handle topology on edges"))
                , fileName(initData(&fileName,"file","File where to store the Jacobian"))
				, useVerdandi(initData(&useVerdandi,(bool)0,"useVerdandi","useVerdandi"))
                , f_depolarisationTimes(initData(&f_depolarisationTimes,"depolarisationTimes","depolarisationTimes at each node"))
                , f_APD(initData(&f_APD,"APD","APD at each node"))
                , f_heartPeriod(initData(&f_heartPeriod,"heartPeriod","heart Period"))
                , activeRelaxation(initData(&activeRelaxation,(bool)0,"activeRelaxation","Use or not an active relaxation triggering after the end of depolarisation"))
                , MaxPeakActiveRelaxation(initData(&MaxPeakActiveRelaxation,(double)5.0,"MaxPeakActiveRelaxation","Multiplicative factor in front of each c1 c2 I in active realxation"))
                , f_calculateStress(initData(&f_calculateStress, (bool)0, "calculateStress", "set to 1 for calculating the cauchy stress"))
                , f_2ndSPK_p(initData(&f_2ndSPK_p, "MRStressPK", "Second Piola-Kirshoff stress from MR part (= passive part)"))
			{
				edgeHandler = new MREdgeHandler(this, &edgeInfo);
				tetrahedronHandler = new MRTetrahedronHandler(this, &tetrahedronInfo);
			}

			template< class DataTypes> void MRForceField<DataTypes>::handleTopologyChange()
			{
			}

			template <class DataTypes> MRForceField<DataTypes>::~MRForceField()
			{
				if(edgeHandler) delete edgeHandler;
				if(tetrahedronHandler) delete tetrahedronHandler;
				Jac.close();
            }



			template <class DataTypes>
			void MRForceField<DataTypes>::init()
			{
				Jac.open((fileName.getValue()).c_str(),std::ios::out);
				//  cerr << "initializing MRForceField" <<endl;
				this->Inherited::init();
				_topology = this->getContext()->getMeshTopology();

				if (!_topology->getNbTetrahedra())
				{
					cerr << "ERROR(MRForceField): object must have a Tetrahedral Set Topology.\n";
					return;
				}



				/// prepare to store info in the triangle array
				edgeInfo.createTopologyHandler(_topology);
//				edgeInfo.registerTopologicalData();

				tetrahedronInfo.createTopologyHandler(_topology);
//				tetrahedronInfo.registerTopologicalData();

				tetrahedronRestInfoVector& tetrahedronInf = *(tetrahedronInfo.beginEdit());
				tetrahedronInf.resize(_topology->getNbTetrahedra());

				edgeInfoVector& edgeInf = *(edgeInfo.beginEdit());
				edgeInf.resize(_topology->getNbEdges());
				edgeInfo.endEdit();

				// get restPosition
				if (_initialPoints.size() == 0)
				{
                    const VecCoord& p = this->mstate->read(core::ConstVecCoordId::restPosition())->getValue();
					_initialPoints=p;
				}
				int i;

				/// initialize the data structure associate with each tetrahedron
				for (int i=0; i<_topology->getNbEdges(); i++)
				{
					edgeInf[i].vertices[0] = (float) _topology->getEdge(i)[0];
					edgeInf[i].vertices[1] = (float) _topology->getEdge(i)[1];
				}

				// Real Vtotal=0;
				/// initialize the data structure associated with each tetrahedron
				for (i=0;i<_topology->getNbTetrahedra();++i)
				{

					tetrahedronHandler->applyCreateFunction(i, tetrahedronInf[i],
						_topology->getTetrahedron(i),  (const vector< unsigned int > )0,
						(const vector< double >)0);
					//Vtotal+=tetrahedronInf[i].restVolume;
				}

				tetrahedronInfo.endEdit();
				/// FOR CUDA
				/// Save the neighbourhood for points (in case of CudaTypes and non atomic)
				this->initTetraNeighbourhoodPoints();
				this->initEdgeNeighbourhoodPoints();
				this->initNeighbourhoodEdges();

                ///stress calc
                if (f_calculateStress.getValue())
                    cout << "(MRForceField): calculating Cauchy Stress" <<endl;
                else
                    cout << "(MRForceField): Not calculating Cauchy Stress" <<endl;

                type::vector<Vec<9,Real> > & my__2ndSPK_p = *(f_2ndSPK_p.beginEdit());
                for (i=0;i<_topology->getNbTetrahedra();++i)
                {
                    my__2ndSPK_p.push_back(Vec<9,Real>(0,0,0,0,0,0,0,0,0));
                }
                f_2ndSPK_p.endEdit();
                ///end stress calc
			}

			template <class DataTypes>
			void MRForceField<DataTypes>::reinit()
			{

			}


			template <class DataTypes>
			void MRForceField<DataTypes>::reset()
			{
				
			}


            template <class DataTypes>
            void MRForceField<DataTypes>::initTetraNeighbourhoodPoints(){}

            template <class DataTypes>
            void MRForceField<DataTypes>::initEdgeNeighbourhoodPoints(){}

            template <class DataTypes>
            void MRForceField<DataTypes>::initNeighbourhoodEdges(){}

            template <class DataTypes>
            SReal MRForceField<DataTypes>::getPotentialEnergy(const core::MechanicalParams*, const DataVecCoord& d_x) const
            {
                    const VecCoord& x = d_x.getValue();

                    unsigned int nbTetrahedra=_topology->getNbTetrahedra();
                    Real energy;
                    const tetrahedronRestInfoVector& tetrahedronInf = tetrahedronInfo.getValue();

                    unsigned int i=0,j=0,k=0,l=0;
                    const TetrahedronRestInformation *tetInfo;
                    Coord dp[3],x0,sv;

                    Real J;
                    Matrix3 deformationGradient;

                    Real functionf1,functionf2,functiong1,functiong2;
                    energy=0;
                    for(i=0; i<nbTetrahedra; i++ )
                    {
                            tetInfo=&tetrahedronInf[i];
                            const Tetrahedron &ta= _topology->getTetrahedron(i);
                            x0=x[ta[0]];

                            // compute the deformation gradient
                            // deformation gradient = sum of tensor product between vertex position and shape vector
                            // optimize by using displacement with first vertex
                            dp[0]=x[ta[1]]-x0;
                            sv=tetInfo->shapeVector[1];
                            for (k=0;k<3;++k) {
                                for (l=0;l<3;++l) {
                                    deformationGradient[k][l]=dp[0][k]*sv[l];
                                }
                            }
                            for (j=1;j<3;++j) {
                                dp[j]=x[ta[j+1]]-x0;
                                sv=tetInfo->shapeVector[j+1];
                                for (k=0;k<3;++k) {
                                    for (l=0;l<3;++l) {
                                        deformationGradient[k][l]+=dp[j][k]*sv[l];
                                    }
                                }
                            }




                            /// compute the CauchyGreen matrix
                            MatrixSym C;
                            for (k=0;k<3;++k) {
                                for (l=k;l<3;++l) {
                                    C(k,l)=(deformationGradient(0,k)*deformationGradient(0,l)+
                                                    deformationGradient(1,k)*deformationGradient(1,l)+
                                                    deformationGradient(2,k)*deformationGradient(2,l));

                                }
                            }

                            Coord areaVec = cross( dp[1], dp[2] );


                            J = dot( areaVec, dp[0] ) * tetInfo->volScale;



                            Real I1=trace(C);
                            Real I1square=(Real)(C[0]*C[0] + C[2]*C[2]+ C[5]*C[5]+2*(C[1]*C[1] + C[3]*C[3] + C[4]*C[4]));
                            Real I2=(Real)((pow(I1,(Real)2)- I1square)/2.0);
                            functionf1=(Real)(pow(J*J,(Real)(-1.0/3.0)));
                            functionf2=(Real)(pow(J*J,(Real)(-2.0/3.0)));

                            functiong1=(Real)tetInfo->specialcoeff*tetInfo->c1*I1;
                            functiong2=(Real)tetInfo->specialcoeff*tetInfo->c2*I2;

                            energy+=(functionf1*functiong1+functionf2*functiong2+tetInfo->specialcoeff*tetInfo->k0/2.0*(J-1)*(J-1)-3.0*tetInfo->specialcoeff*tetInfo->c1-3.0*tetInfo->specialcoeff*tetInfo->c2)*tetInfo->restVolume;

                    }

               // return energy;
return 0;

            }





            template <class DataTypes>
            double MRForceField<DataTypes>::get_PassiveEnergy(sofa::type::vector <unsigned int> TetrasIndices)
            {
                    Real energy;
                    const VecDeriv& x = this->mstate->read(core::ConstVecCoordId::position())->getValue();
                    const tetrahedronRestInfoVector& tetrahedronInf = tetrahedronInfo.getValue();

                    unsigned int i=0,j=0,k=0,l=0;
                    const TetrahedronRestInformation *tetInfo;
                    Coord dp[3],x0,sv;

                    Real J;
                    Matrix3 deformationGradient;
                    Real functionf1,functionf2,functiong1,functiong2;

                    energy=0;
                    for (unsigned int v = 0; v<TetrasIndices.size(); v++ )
                    {

                            i=TetrasIndices[v];

                            tetInfo=&tetrahedronInf[i];
                            const Tetrahedron &ta= _topology->getTetrahedron(i);
                            x0=x[ta[0]];

                            // compute the deformation gradient
                            // deformation gradient = sum of tensor product between vertex position and shape vector
                            // optimize by using displacement with first vertex
                            dp[0]=x[ta[1]]-x0;
                            sv=tetInfo->shapeVector[1];
                            for (k=0;k<3;++k) {
                                for (l=0;l<3;++l) {
                                    deformationGradient[k][l]=dp[0][k]*sv[l];
                                }
                            }
                            for (j=1;j<3;++j) {
                                dp[j]=x[ta[j+1]]-x0;
                                sv=tetInfo->shapeVector[j+1];
                                for (k=0;k<3;++k) {
                                    for (l=0;l<3;++l) {
                                        deformationGradient[k][l]+=dp[j][k]*sv[l];
                                    }
                                }
                            }




                            /// compute the CauchyGreen matrix
                            MatrixSym C;
                            for (k=0;k<3;++k) {
                                for (l=k;l<3;++l) {
                                    C(k,l)=(deformationGradient(0,k)*deformationGradient(0,l)+
                                                    deformationGradient(1,k)*deformationGradient(1,l)+
                                                    deformationGradient(2,k)*deformationGradient(2,l));

                                }
                            }

                            Coord areaVec = cross( dp[1], dp[2] );


                            J = dot( areaVec, dp[0] ) * tetInfo->volScale;



                            Real I1=trace(C);
                            Real I1square=(Real)(C[0]*C[0] + C[2]*C[2]+ C[5]*C[5]+2*(C[1]*C[1] + C[3]*C[3] + C[4]*C[4]));
                            Real I2=(Real)((pow(I1,(Real)2)- I1square)/2.0);
                            functionf1=(Real)(pow(J*J,(Real)(-1.0/3.0)));
                            functionf2=(Real)(pow(J*J,(Real)(-2.0/3.0)));

                            functiong1=(Real)tetInfo->specialcoeff*tetInfo->c1*I1;
                            functiong2=(Real)tetInfo->specialcoeff*tetInfo->c2*I2;

                            energy+=(functionf1*functiong1+functionf2*functiong2+tetInfo->specialcoeff*tetInfo->k0/2.0*(J-1)*(J-1)-3.0*tetInfo->specialcoeff*tetInfo->c1-3.0*tetInfo->specialcoeff*tetInfo->c2)*tetInfo->restVolume;

                    }

               return energy;


            }





            template <class DataTypes>
            void MRForceField<DataTypes>::addForce(const core::MechanicalParams* /* PARAMS FIRST */, DataVecDeriv& d_f, const DataVecCoord& d_x, const DataVecDeriv& )
            {
                sofa::helper::AdvancedTimer::stepBegin("addForceMR");


                unsigned int i=0,j=0,k=0,l=0;
                unsigned int nbTetrahedra=_topology->getNbTetrahedra();

                tetrahedronRestInfoVector& tetrahedronInf = *(tetrahedronInfo.beginEdit());
                VecDeriv &f=*(d_f.beginEdit());
                VecCoord x=d_x.getValue();

                TetrahedronRestInformation *tetInfo;

                /// stress calc
                type::vector<Vec<9,Real> > & my__2ndSPK_p = *(f_2ndSPK_p.beginEdit());

                assert(this->mstate);

                Coord dp[3],x0,sv;
               // myposition=x;
                Jtotal=0;
//                double EnergyTotale=0;
                double minJ=10;
                double maxJ=0;
  int indmax,indmin;
                for(i=0; i<nbTetrahedra; i++ )
                {
                    tetInfo=&tetrahedronInf[i];
                    const Tetrahedron &ta= _topology->getTetrahedron(i);

                    /// get the parameters
                if(useVerdandi.getValue()){
                    Vec3 param=getParameters(i);
                    tetInfo->k0=param[2];
                    tetInfo->c1=param[0];
                    tetInfo->c2=param[1];
                }
                    x0=x[ta[0]];

                    // compute the deformation gradient
                    // deformation gradient = sum of tensor product between vertex position and shape vector
                    // optimize by using displacement with first vertex
                    dp[0]=x[ta[1]]-x0;
                    sv=tetInfo->shapeVector[1];
                    for (k=0;k<3;++k) {
                        for (l=0;l<3;++l) {
                            tetInfo->deformationGradient[k][l]=dp[0][k]*sv[l];
                        }
                    }
                    for (j=1;j<3;++j) {
                        dp[j]=x[ta[j+1]]-x0;
                        sv=tetInfo->shapeVector[j+1];
                        for (k=0;k<3;++k) {
                            for (l=0;l<3;++l) {
                                tetInfo->deformationGradient[k][l]+=dp[j][k]*sv[l];
                            }
                        }
                    }




                    /// compute the CauchyGreen matrix
                    MatrixSym C;
                    for (k=0;k<3;++k) {
                        for (l=k;l<3;++l) {
                            C(k,l)=(tetInfo->deformationGradient(0,k)*tetInfo->deformationGradient(0,l)+
                                            tetInfo->deformationGradient(1,k)*tetInfo->deformationGradient(1,l)+
                                            tetInfo->deformationGradient(2,k)*tetInfo->deformationGradient(2,l));

                        }
                    }

                    Coord areaVec = cross( dp[1], dp[2] );


                    tetInfo->J = dot( areaVec, dp[0] ) * tetInfo->volScale;
                    Jtotal+=tetInfo->J;


                    if(fabs(tetInfo->J)<minJ){
                        minJ=fabs(tetInfo->J);
                        indmin=i;
                    }
                    if(fabs(tetInfo->J)>maxJ){
                        maxJ=fabs(tetInfo->J);
                        indmax=i;
                    }
                    // we have to compute dJ
                    tetInfo->dJ[0] = cross( x[ta[3]]-x[ta[1]], x[ta[2]]-x[ta[1]] ) * tetInfo->volScale;
                    tetInfo->dJ[1] = areaVec * tetInfo->volScale;
                    tetInfo->dJ[2] = cross( dp[2], dp[0] ) * tetInfo->volScale;
                    tetInfo->dJ[3] = cross( dp[0], dp[1] ) * tetInfo->volScale;


                    if(activeRelaxation.getValue()){

                        double simuTime = this->getContext()->getTime();
                        int k=floor(simuTime/f_heartPeriod.getValue());
                        simuTime=simuTime-k*f_heartPeriod.getValue();

                        double APD=0;
                        double Td=0;

                        for (int u=0;u<4;u++){
                            ///value at the barycenter
                            APD+=(f_APD.getValue())[tetInfo->tetraIndices[u]]/4.0;
                            Td+=(f_depolarisationTimes.getValue())[tetInfo->tetraIndices[u]]/4.0;
                        }

                        double Tr=Td+APD;
                        double Interval=0.2*APD;
                        double alpha=7.0/Interval;

/*
                        if((simuTime>=(Tr))&&(simuTime<(Tr+Interval))){
                            tetInfo->specialcoeff=1.0*(1.0+(MaxPeakActiveRelaxation.getValue()/1.0-1.0)/(1.0+exp(-100*(simuTime-(Tr+Interval/2)))));
                        }
                        else if((simuTime>=(Tr+Interval))&&(simuTime<(Tr+APD))){
                            tetInfo->specialcoeff=1.0*(1.0+(MaxPeakActiveRelaxation.getValue()/1.0-1.0)/(1.0+exp(25*(simuTime-(Tr+3*Interval)))));
                        }
                        else{
                            tetInfo->specialcoeff=1.0;
                        }
*/

                    }


                   Real I1=trace(C);
                    Real I1square=(Real)(C[0]*C[0] + C[2]*C[2]+ C[5]*C[5]+2*(C[1]*C[1] + C[3]*C[3] + C[4]*C[4]));
                    Real I2=(Real)((pow(I1,(Real)2)- I1square)/2.0);
                    tetInfo->functionf1=(Real)(pow(tetInfo->J*tetInfo->J,(Real)(-1.0/3.0)));
                    tetInfo->functionf2=(Real)(pow(tetInfo->J*tetInfo->J,(Real)(-2.0/3.0)));

                    tetInfo->functionf1prime=(Real)(pow(tetInfo->J*tetInfo->J,(Real)(-1.0/3.0))/tetInfo->J)*(Real)(-2.0/3.0);
                    tetInfo->functionf2prime=(Real)(pow(tetInfo->J*tetInfo->J,(Real)(-2.0/3.0))/tetInfo->J)*(Real)(-4.0/3.0);
                    tetInfo->functiong1=(Real)tetInfo->specialcoeff*tetInfo->c1*I1;
                    tetInfo->functiong2=(Real)tetInfo->specialcoeff*tetInfo->c2*I2;
                 //  tetInfo->Energy=tetInfo->functionf1*tetInfo->functiong1+tetInfo->functionf2*tetInfo->functiong2+tetInfo->k0/2.0*(tetInfo->J-1)*(tetInfo->J-1)-3.0*tetInfo->c1-3.0*tetInfo->c2;
                    //EnergyTotale+=tetInfo->Energy*tetInfo->restVolume;
                 //   EnergyTotale+=tetInfo->Energy*tetInfo->restVolume;
                    tetInfo->functionf3prime=(Real)tetInfo->specialcoeff*tetInfo->k0*log(tetInfo->J)/tetInfo->J;

                    MatrixSym id;
                    id.identity();
                    tetInfo->SPK1=id*2.0*tetInfo->specialcoeff*tetInfo->c1;
                    tetInfo->SPK2=(id*I1-C)*2.0*tetInfo->specialcoeff*tetInfo->c2;
                    tetInfo->sumfS=tetInfo->SPK1*tetInfo->functionf1+tetInfo->SPK2*tetInfo->functionf2;
                    tetInfo->sumDfg=tetInfo->functionf1prime*tetInfo->functiong1+tetInfo->functionf2prime*tetInfo->functiong2
                            +tetInfo->functionf3prime;

                    /// stress calc
                    if(f_calculateStress.getValue())
                    {

                        Matrix3 inverseDeformationGradient;
                        inverseDeformationGradient.invert(tetInfo->deformationGradient);

                        //second piola kirshoff stress tensor of the passive part (see Marchesseau thesis, p115)
                        tetInfo->SecSPK_p=tetInfo->sumfS+tetInfo->sumDfg*tetInfo->J*inverseDeformationGradient*inverseDeformationGradient.transposed();

                        // copying into f_2ndSPK_p for ContractionForceField
                        my__2ndSPK_p.at(i)=Vec<9,Real>(tetInfo->SecSPK_p(0,0),tetInfo->SecSPK_p(0,1),tetInfo->SecSPK_p(0,2),
                                        tetInfo->SecSPK_p(1,0), tetInfo->SecSPK_p(1,1), tetInfo->SecSPK_p(1,2),
                                        tetInfo->SecSPK_p(2,0), tetInfo->SecSPK_p(2,1), tetInfo->SecSPK_p(2,1));

                    }
                    /// end stress calc

                    for(l=0;l<4;++l){
                        f[ta[l]]-=(tetInfo->deformationGradient*(tetInfo->sumfS*tetInfo->shapeVector[l])+tetInfo->dJ[l]*tetInfo->sumDfg)*tetInfo->restVolume;
                    }



                }// end of for i


                if(activeRelaxation.getValue()){
                tetInfo=&tetrahedronInf[10000];

                double simuTime = this->getContext()->getTime();
                double APD=0;
                double Td=0;

                for (int u=0;u<4;u++){
                    ///value at the barycenter
                    APD+=(f_APD.getValue())[tetInfo->tetraIndices[u]]/4.0;
                    Td+=(f_depolarisationTimes.getValue())[tetInfo->tetraIndices[u]]/4.0;
                }

                double Tr=Td+APD;
                double Interval=0.5*APD;

                double write=0;



                    if((simuTime>=(Tr-Interval))&&(simuTime<Tr)){
                        write=1000;
                    }
                    else if((simuTime>=Tr)&&(simuTime<(Tr+Interval))){
                        write=2000;
                    }


                Jtotal/=nbTetrahedra;
                Jac<<minJ<<" "<<indmin<<" "<<Jtotal<<" "<<maxJ<<" "<<indmax<<" "<<APD<<" "<<Td<<" "<<Tr<<" "<<simuTime<<" "<<write<<" "<<tetInfo->specialcoeff<<std::endl;

                }
                else{
                    Jtotal/=nbTetrahedra;
                    Jac<<minJ<<" "<<indmin<<" "<<Jtotal<<" "<<maxJ<<" "<<indmax<<std::endl;
                }









                /// indicates that the next call to addDForce will need to update the stiffness matrix
                updateMatrix=true;
                tetrahedronInfo.endEdit();
                d_f.endEdit();
                f_2ndSPK_p.endEdit();
                sofa::helper::AdvancedTimer::stepEnd("addForceMR");

            }

            template <class DataTypes>
            void MRForceField<DataTypes>::updateGraph(double en)
            {


               std::map < std::string, sofa::type::vector<double> >& graph = *(f_graph.beginEdit());
               sofa::type::vector<double>& graph_energy = graph["Energy"];
               graph_energy.push_back(en);//+1 pour un meilleur affichage
               //std::cout<<EnergyTotale<<std::endl;
               f_graph.endEdit();

            }

            template <class DataTypes>
                    void MRForceField<DataTypes>::updateMatrixData()
            {
                /// if the  matrix needs to be updated
                if (updateMatrix) {
                unsigned int i=0,j=0,k=0,l=0;
                unsigned int nbEdges=_topology->getNbEdges();
                const vector< Edge> &edgeArray=_topology->getEdges() ;

                Real h;
                edgeInfoVector& edgeInf = *(edgeInfo.beginEdit());
                tetrahedronRestInfoVector& tetrahedronInf = *(tetrahedronInfo.beginEdit());

                const VecDeriv& x = this->mstate->read(core::ConstVecCoordId::position())->getValue();
                EdgeInformation *einfo;


                    unsigned int m,n;
                    TetrahedronRestInformation *tetInfo;
                    unsigned int nbTetrahedra=_topology->getNbTetrahedra();
                    const std::vector< Tetrahedron> &tetrahedronArray=_topology->getTetrahedra() ;
                    const unsigned int vertexVertexIndex[4][4][2]={{{5,5},{3,2},{1,3},{2,1}},{{2,3},{5,5},{3,0},{0,2}},
                                                                   {{3,1},{0,3},{5,5},{1,0}},{{1,2},{2,0},{0,1},{5,5}}};

                    Coord dp;
                    Matrix3 Lam, M, Theta, Pi, R,N;

                    for(l=0; l<nbEdges; l++ ) {
                        edgeInf[l].DfDx.clear();
                    }
                    for(i=0; i<nbTetrahedra; i++ )
                    {
                        tetInfo=&tetrahedronInf[i];
                        /// get the parameters
                        if(useVerdandi.getValue()){
                        Vec3 param=getParameters(i);
                            tetInfo->k0=param[2];
                            tetInfo->c1=param[0];
                            tetInfo->c2=param[1];
                         }


                        Matrix3 &df=tetInfo->deformationGradient;
                        Matrix3 Tdf=df.transposed();
                        BaseMeshTopology::EdgesInTetrahedron te=_topology->getEdgesInTetrahedron(i);

                        if (f_stiffnessMatrixRegularizationWeight.getValue()) {
                            if(tetInfo->J>=1) h=0;
                            else if (tetInfo->J <=0 ) h=1;
                            else h=(1-tetInfo->J);
                        }
                        else h=0;
                        MatrixSym sumDfS;
                        Real sumD2fg;
                        Real functionf1snde=(Real)(pow(tetInfo->J*tetInfo->J,(Real)(-4.0/3.0)))*(Real)(10.0/9.0);
                        Real functionf2snde=(Real)(28.0*pow(tetInfo->J*tetInfo->J,(Real)(-5.0/3.0))/9.0);
                       Real functionf3snde=(Real)tetInfo->specialcoeff*tetInfo->k0*(1-log(tetInfo->J))/(tetInfo->J*tetInfo->J);

                       sumDfS=tetInfo->functionf1prime*tetInfo->SPK1+tetInfo->functionf2prime*tetInfo->SPK2;
                       sumD2fg=functionf1snde*tetInfo->functiong1+functionf2snde*tetInfo->functiong2+functionf3snde;
                        /// describe the jth vertex index of tetra no i
                        const Tetrahedron &ta= tetrahedronArray[i];

                        for(j=0;j<6;j++) {
                            einfo= &edgeInf[te[j]];
                            Edge e=_topology->getLocalEdgesInTetrahedron(j);

                            k=e[0];
                            l=e[1];
                            if (edgeArray[te[j]][0]!=ta[k]) {
                                k=e[1];
                                l=e[0];
                            }
                            Matrix3 &edgeDfDx = einfo->DfDx;


                            //second derivative of J:
                            MatNoInit<3,3,Real> d2J;
                            dp = (x[ta[vertexVertexIndex[k][l][0]]] - x[ta[vertexVertexIndex[k][l][1]]])  * tetInfo->volScale;

                            d2J[0][0] = 0;
                            d2J[0][1] = -dp[2];
                            d2J[0][2] = dp[1];
                            d2J[1][0] = dp[2];
                            d2J[1][1] = 0;
                            d2J[1][2] = -dp[0];
                            d2J[2][0] = -dp[1];
                            d2J[2][1] = dp[0];
                            d2J[2][2] = 0;


                            Coord dJl=tetInfo->dJ[l];
                            Coord dJk=tetInfo->dJ[k];
                            Coord svl=tetInfo->shapeVector[l];
                            Coord svk=tetInfo->shapeVector[k];

                            N=(df*tetInfo->Lij[j]*2.0*tetInfo->specialcoeff*tetInfo->c2*Tdf).transposed()*tetInfo->functionf2;


                            Coord FF[4];
                            for(m=0;m<4;++m) {
                                FF[m]=df*(sumDfS*tetInfo->shapeVector[m]);
                                }
                            //Now M
                            Real productDfSD;
                            Coord vectSD=tetInfo->sumfS*svk;

                            productDfSD=dot(vectSD,svl);

                            M[0][1]=M[0][2]=M[1][0]=M[1][2]=M[2][0]=M[2][1]=0;

                            M[0][0]=M[1][1]=M[2][2]=(Real)productDfSD;

                            ///Then R Lambda et Theta
                            R.clear();

                            for(m=0;m<3;++m){
                                for(n=0;n<3;++n){
                                    R[m][n]=dJl[m]*dJk[n]*(1-h)*sumD2fg;
                                    if(m==n) {
                                        R[m][n]+=dot(dJl,dJk)*sumD2fg*h;

                                    }
                                    Lam[m][n]=dJl[m]*FF[k][n];
                                    Theta[m][n]=dJk[n]*FF[l][m];
                               }
                            }

                            ///Then Pi
                            Pi=d2J*tetInfo->sumDfg;



                            edgeDfDx += (Lam+ M+ Theta+ Pi+ R+N)*tetInfo->restVolume;

                        }

                    }//end of for i
                    updateMatrix=false;
                    edgeInfo.endEdit();
                    tetrahedronInfo.endEdit();
                }// end of if
            }

            template<class DataTypes>
            void MRForceField<DataTypes>::addDForce (const core::MechanicalParams* mparams /* PARAMS FIRST */, DataVecDeriv& d_df, const DataVecDeriv& d_dx){

                sofa::helper::AdvancedTimer::stepBegin("addDForceMR");
                VecDeriv& df = *d_df.beginEdit();
                const VecDeriv& dx = d_dx.getValue();
                double kFactor = mparams->kFactor();

                unsigned int l=0;
                unsigned int nbEdges=_topology->getNbEdges();
                const vector< Edge> &edgeArray=_topology->getEdges();

                edgeInfoVector& edgeInf = *(edgeInfo.beginEdit());
                EdgeInformation *einfo;

                this->updateMatrixData();

                /// performs matrix vector computation
                unsigned int v0,v1;
                Deriv deltax;	Deriv dv0,dv1;

                for(l=0; l<nbEdges; l++ )
                {
                        einfo=&edgeInf[l];
                        v0=edgeArray[l][0];
                        v1=edgeArray[l][1];

                        deltax= dx[v0] - dx[v1];
                        dv0 = einfo->DfDx * deltax;

                        dv1[0] = (Real)(deltax[0]*einfo->DfDx[0][0] + deltax[1]*einfo->DfDx[1][0] + deltax[2]*einfo->DfDx[2][0]);
                        dv1[1] = (Real)(deltax[0]*einfo->DfDx[0][1] + deltax[1]*einfo->DfDx[1][1] + deltax[2]*einfo->DfDx[2][1]);
                        dv1[2] = (Real)(deltax[0]*einfo->DfDx[0][2] + deltax[1]*einfo->DfDx[1][2] + deltax[2]*einfo->DfDx[2][2]);
                        // add forces
                        df[v0] += dv1 * kFactor;
                        df[v1] -= dv0 * kFactor;
                }

                edgeInfo.endEdit();
                d_df.endEdit();
                sofa::helper::AdvancedTimer::stepEnd("addDForceMR");

            }

        template <class DataTypes>
        void MRForceField<DataTypes>::addKToMatrix(linearalgebra::BaseMatrix *m, SReal kFactor, unsigned int &offset) {
                unsigned int l=0;
                unsigned int nbEdges=_topology->getNbEdges();
                const vector< Edge> &edgeArray=_topology->getEdges();

                edgeInfoVector& edgeInf = *(edgeInfo.beginEdit());
                EdgeInformation *einfo;

                this->updateMatrixData();

                /// performs matrix vector computation
                unsigned int v0,v1;

                for(l=0; l<nbEdges; l++ )
                {
                        einfo=&edgeInf[l];
                        v0=offset + edgeArray[l][0]*3;
                        v1=offset + edgeArray[l][1]*3;

                        for (int L=0;L<3;L++) {
                          for (int C=0;C<3;C++) {
                            double v = einfo->DfDx[L][C] * kFactor;
                            m->add(v0+C,v0+L, v);
                            m->add(v0+C,v1+L,-v);
                            m->add(v1+L,v0+C,-v);
                            m->add(v1+L,v1+C, v);
                          }
                        }
                }

                edgeInfo.endEdit();
        }



            template<class DataTypes>
            Mat<3,3,double> MRForceField<DataTypes>::getPhi(int TetrahedronIndex)
            {
                tetrahedronRestInfoVector& tetrahedronInf = *(tetrahedronInfo.beginEdit());
                TetrahedronRestInformation *tetInfo;
                tetInfo=&tetrahedronInf[TetrahedronIndex];
                return tetInfo->deformationGradient;
            }



            template<class DataTypes>
            void MRForceField<DataTypes>::draw(const core::visual::VisualParams* vparams)
            {
                    //	unsigned int i;
                    if (!vparams->displayFlags().getShowForceFields()) return;
                    if (!this->mstate) return;

                    if (vparams->displayFlags().getShowWireFrame())
                            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

                    if (vparams->displayFlags().getShowWireFrame())
                            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

            }




        } // namespace forcefield
    } // namespace Components
} // namespace Sofa


