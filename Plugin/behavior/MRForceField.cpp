#include "MRForceField.h"
#include "MRForceField.inl"


#include <SofaBaseMechanics/MechanicalObject.h>
#include <sofa/core/ObjectFactory.h>

#include <sofa/core/behavior/ForceField.inl>
#include <SofaBaseTopology/TopologyData.inl>

#include <string.h>
#include <iostream>

namespace sofa
{

namespace component
{

namespace forcefield
{

using namespace sofa::defaulttype;
using namespace	sofa::component::topology;
using namespace core::topology;

using std::cerr;
using std::cout;
using std::endl;
using std::string;


//////////****************To register in the factory******************

SOFA_DECL_CLASS(MRForceField)

// Register in the Factory
int MRForceFieldClass = core::RegisterObject("MR's law in Tetrahedral finite elements")
#ifndef SOFA_FLOAT
.add< MRForceField<sofa::defaulttype::Vec3dTypes> >()
#endif
#ifndef SOFA_DOUBLE
.add< MRForceField<Vec3fTypes> >()
#endif
;

#ifndef SOFA_FLOAT
template class MRForceField<Vec3dTypes>;
#endif
#ifndef SOFA_DOUBLE
template class MRForceField<Vec3fTypes>;
#endif

} // namespace forcefield

} // namespace component

} // namespace sofa


