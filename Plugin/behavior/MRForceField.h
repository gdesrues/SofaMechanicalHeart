#pragma once


#include <sofa/core/behavior/ForceField.h>
#include <SofaBaseMechanics/MechanicalObject.h>
#include <sofa/core/visual/VisualModel.h>
#include <sofa/type/Vec.h>
#include <sofa/type/Mat.h>
#include <sofa/type/MatSym.h>
#include <sofa/core/topology/BaseMeshTopology.h>
#include <SofaBaseTopology/TopologyData.h>
#include <string>
#include <map>
#include <sofa/helper/map.h>

namespace sofa
{

namespace component
{

namespace forcefield
{
using namespace std;
using namespace sofa::type;
using namespace sofa::component::topology;



//***************** Tetrahedron FEM code for MR elastic models using MJED method************************//

/** Compute Finite Element forces based on tetrahedral elements.
*/
template<class DataTypes>
class MRForceField : public core::behavior::ForceField<DataTypes>
{
  public:
    SOFA_CLASS(SOFA_TEMPLATE(MRForceField, DataTypes), SOFA_TEMPLATE(core::behavior::ForceField, DataTypes));

    typedef core::behavior::ForceField<DataTypes> Inherited;
    typedef typename DataTypes::VecCoord VecCoord;
    typedef typename DataTypes::VecDeriv VecDeriv;
    typedef typename DataTypes::Coord Coord;
    typedef typename DataTypes::Deriv Deriv;
    typedef typename Coord::value_type Real;
    typedef typename type::MatSym<3,Real> MatrixSym;
    typedef typename type::Mat<3,3,Real> Matrix3;
    typedef typename type::Mat<6,6,Real> Matrix6;
    typedef typename type::Vec6d Vec6;
    typedef typename type::Vec3d Vec3;


    typedef core::objectmodel::Data<VecDeriv>    DataVecDeriv;
    typedef core::objectmodel::Data<VecCoord>    DataVecCoord;

    typedef type::vector<Real> SetParameterArray;

    
    typedef core::topology::BaseMeshTopology::Tetra Element;
    typedef core::topology::BaseMeshTopology::SeqTetrahedra VecElement;

    typedef sofa::core::topology::Topology::Tetrahedron Tetrahedron;
    typedef sofa::core::topology::Topology::TetraID TetraID;
    typedef sofa::core::topology::Topology::Tetra Tetra;
    typedef sofa::core::topology::Topology::Edge Edge;
    typedef sofa::core::topology::BaseMeshTopology::EdgesInTriangle EdgesInTriangle;
    typedef sofa::core::topology::BaseMeshTopology::EdgesInTetrahedron EdgesInTetrahedron;
    typedef sofa::core::topology::BaseMeshTopology::TrianglesInTetrahedron TrianglesInTetrahedron;

public :







    /// data structure stored for each tetrahedron
    class TetrahedronRestInformation
    {
        public:

        //Matrix3 DirectionMatrix;
        //Matrix3 inverseDirection;
        /// Material parameters
        Real c1,c2,k0;
        Real Energy;
        /// shape vector at the rest configuration
        Coord shapeVector[4];
        Coord pointP[4];
        Matrix3 DicrossDj[6];
        Matrix3 Lij[6];
        Real J;
        /// rest volume
        Real restVolume;
        /// current tetrahedron volume
        Real volScale;
        Real volume;
        MatrixSym sumfS;
        Real sumDfg;
        double specialcoeff;

        /// volume/ restVolume
        /// derivatives of J
        Coord dJ[4];
        /// deformation gradient = gradPhi
        Matrix3 deformationGradient;
        // Matrix3 inverseDeformationGradient;

        MatrixSym SPK1,SPK2;
        Real functionf1,functionf2,functionf1prime,functionf2prime,functionf3prime,functiong1,functiong2;

        // stress calc
        Matrix3  SecSPK_p;

        //Tetrahedron Point Indices for CUDA
        float tetraIndices[4];
        //Tetrahedron Edges for CUDA
        float tetraEdges[6];
        //f_parameterSet info for CUDA
        Vec<3,Real> f_parameters;

        /// Output stream
        inline friend ostream& operator<< ( ostream& os, const TetrahedronRestInformation& /*eri*/ ) {  return os;  }
        /// Input stream
        inline friend istream& operator>> ( istream& in, TetrahedronRestInformation& /*eri*/ ) { return in; }

        TetrahedronRestInformation() {}
    };
typedef typename VecCoord::template rebind<TetrahedronRestInformation>::other tetrahedronRestInfoVector;


   /// data structure stored for each edge
   class EdgeInformation
   {
   public:
           /// store the stiffness edge matrix
           Matrix3 DfDx;
           float vertices[2];
           /// Output stream
           inline friend ostream& operator<< ( ostream& os, const EdgeInformation& /*eri*/ ) {  return os;  }
           /// Input stream
           inline friend istream& operator>> ( istream& in, EdgeInformation& /*eri*/ ) { return in; }

     EdgeInformation() {}
   };
   typedef typename VecCoord::template rebind<EdgeInformation>::other edgeInfoVector;

public:
   TetrahedronData<type::vector<Vec<3,Real> > >   f_parameterSet;



   //Vertex Handler
   class MREdgeHandler : public TopologyDataHandler<Edge,sofa::type::vector<EdgeInformation> >
   {
   public:
       typedef typename MRForceField<DataTypes>::EdgeInformation EdgeInformation;
       MREdgeHandler(MRForceField<DataTypes>* _ff, EdgeData<sofa::type::vector<EdgeInformation> >* _data) : TopologyDataHandler<Edge, sofa::type::vector<EdgeInformation> >(_data), ff(_ff) {}       

       void applyCreateFunction(unsigned int edgeIndex,
                                EdgeInformation &ei,
                                const Edge& ,  const sofa::type::vector< unsigned int > &,
                                const sofa::type::vector< double >&);
   protected:
       MRForceField<DataTypes>* ff;
   };



   //Tetrahedron Handler
   class MRTetrahedronHandler : public TopologyDataHandler<Tetrahedron,sofa::type::vector<TetrahedronRestInformation> >
   {
   public:
       typedef typename MRForceField<DataTypes>::TetrahedronRestInformation TetrahedronRestInformation;
       MRTetrahedronHandler(MRForceField<DataTypes>* _ff, TetrahedronData<sofa::type::vector<TetrahedronRestInformation> >* _data) : TopologyDataHandler<Tetrahedron, sofa::type::vector<TetrahedronRestInformation> >(_data), ff(_ff) {}

       void applyCreateFunction(unsigned int tetraIndex, TetrahedronRestInformation &teti, const Tetrahedron& ,  const sofa::type::vector< unsigned int > &, const sofa::type::vector< double >&);

   protected:
       MRForceField<DataTypes>* ff;
   };



 protected :
   int numberVertices;
   core::topology::BaseMeshTopology* _topology;
   VecCoord  _initialPoints;	/// the intial positions of the points
   bool updateMatrix;
   bool  _meshSaved ;
   Real Jtotal;
   double EnergyTotale;
    Data<std::map < std::string, sofa::type::vector<double> > > f_graph;
   Data<bool> f_stiffnessMatrixRegularizationWeight;
    Data<std::string> m_tagMeshSolver;
   bool stiffnessMatrixRegularizationWeight;
Data<std::string> fileName;
std::ofstream Jac;
Data<bool> useVerdandi;

    PointData<type::vector<Real> > f_depolarisationTimes;
    PointData<type::vector<Real> > f_APD;
    Data<bool> activeRelaxation;
    Data<double> MaxPeakActiveRelaxation;
    Data<Real> f_heartPeriod;

    // stress calc
    Data<bool> f_calculateStress;
    TetrahedronData<type::vector<Vec<9,Real> > >  f_2ndSPK_p;

   //TetrahedronData<type::vector<Vec<3,Real> > >   f_parameterSet;

 public:

 void updateGraph();

        void setnumbervertices(const int number)
        { numberVertices=number; }


   MRForceField();

   virtual   ~MRForceField();


  //  virtual void parse(core::objectmodel::BaseObjectDescription* arg);

    virtual void init();
	virtual void reinit();
	virtual void reset();

    void updateGraph(double en);
    //Used for CUDA non atomic implementation
    void initTetraNeighbourhoodPoints();
    void initEdgeNeighbourhoodPoints();
    void initNeighbourhoodEdges();

        //virtual void reinit();
    virtual void addForce(const core::MechanicalParams* mparams /* PARAMS FIRST */, DataVecDeriv& d_f, const DataVecCoord& d_x, const DataVecDeriv& d_v);
    virtual void addDForce(const core::MechanicalParams* mparams /* PARAMS FIRST */, DataVecDeriv& d_df, const DataVecDeriv& d_dx);

    virtual void addKToMatrix(linearalgebra::BaseMatrix *m, SReal kFactor, unsigned int &offset);

    virtual SReal getPotentialEnergy(const core::MechanicalParams*, const DataVecCoord&) const;
    virtual double get_PassiveEnergy(sofa::type::vector <unsigned int> TetrasIndices);

    virtual void updateMatrixData();

  virtual void draw(const core::visual::VisualParams*);

    Vec<3,double>  getParameters(const unsigned int tetraIndex);

        Mat<3,3,double> getPhi( int tetrahedronIndex);


  protected:

    // handle topological changes
          virtual void handleTopologyChange();

    /// the array that describes the complete material energy and its derivatives

        TetrahedronData<type::vector<TetrahedronRestInformation> > tetrahedronInfo;
        EdgeData<type::vector<EdgeInformation> > edgeInfo;
        void testDerivatives();
        void saveMesh( const char *filename );

        VecCoord myposition;
      //  sofa::component::odesolver::EulerImplicitSolver *eul;
      //  sofa::component::odesolver::VariationnalSolver *var;
public:
    MREdgeHandler* edgeHandler;
    MRTetrahedronHandler* tetrahedronHandler;


};

} // namespace forcefield

} // namespace component

} // namespace sofa



