#include "PressureConstraintForceField.h"

#include <SofaBaseMechanics/MechanicalObject.h>
#include <sofa/core/ObjectFactory.h>
#include <fstream>
#include <iostream>
#include <GL/gl.h>
#include <SofaBaseTopology/TopologyData.inl>
#include <sofa/core/behavior/ForceField.inl>
#include <algorithm>
#include <iterator>
#include <json.h>
#include <sofa/helper/AdvancedTimer.h>
#include <sofa/simulation/Node.h>
#include <sofa/simulation/Simulation.h>
#include <sofa/core/visual/VisualParams.h>



namespace sofa
{
namespace component
{
namespace forcefield
{
using namespace sofa::defaulttype;
using namespace sofa::component::topology;
using namespace core::topology;
using core::topology::BaseMeshTopology;



    template <class DataTypes> PressureConstraintForceField<DataTypes>::~PressureConstraintForceField()
    {
        pressure.close();
        _pr.close();
    }

    template <class DataTypes> PressureConstraintForceField<DataTypes>::PressureConstraintForceField()
    : f_Kiso(initData(&f_Kiso,"Kiso", "parameter K isovolumic"))
    , f_hp(initData(&f_hp,"heartPeriod","heart period"))
    , f_atriumParam(initData(&f_atriumParam,"atriumParam","Kat, Pat0, Patm, alpha1, alpha2, tof, tm, tc"))
    , f_aorticParam(initData(&f_aorticParam,"aorticParam","Kar, Par0, Pve,tau=Rp*C,Rp,Zc,L"))
    , f_windkessel(initData(&f_windkessel,"windkessel","which model of windkessel (2,3,4)?"))
    , f_Pv0(initData(&f_Pv0,(Real)1000.,"Pv0", "minimal Pressure ventricule"))
    , updateMatrix(true)
    , _topology(0)
    , _initialPoints(0)
    , f_loader(initData(&f_loader,"loadername","give ATET3D or VTK"))
    , m_tagMeshSolver(initData(&m_tagMeshSolver, std::string("solver"),"tagSolver","Tag of the Solver Object"))
    , fileName(initData(&fileName,"file","File name to register pressures"))
    , f_SurfaceZone(initData(&f_SurfaceZone,"SurfaceZone","List of triangles on the surface"))
    , f_graphPressure( initData(&f_graphPressure,"graphPressure","Pressures per iteration") )
    , f_graphVolume( initData(&f_graphVolume,"graphVolume","Volume per iteration") )
    , f_graphFlow( initData(&f_graphFlow,"graphFlow","Flow per iteration") )
    , useProjection(initData(&useProjection,"useProjection","useProjection"))
    , DisableFirstAtriumContraction(initData(&DisableFirstAtriumContraction,(bool)0,"DisableFirstAtriumContraction","DisableFirstAtriumContraction"))
    , edgeInfo(initData(&edgeInfo,"edgeInfo","Data to handle topology on edges"))
    , volume(initData(&volume, "volume", "Volume.", false))
    , pressurePv(initData(&pressurePv, "pressurePv", "Pressure Pv", false))
    , pressurePat(initData(&pressurePat, "pressurePat", "Pressure Pat", false))
    , pressurePar(initData(&pressurePar, "pressurePar", "Pressure Par", false))
    , flowQ(initData(&flowQ, "flowQ", "Flow Q", false))
    , useVerdandi(initData(&useVerdandi,(bool)0,"useVerdandi","useVerdandi"))
    , displayName(initData(&displayName,"displayName","ONLY used for the Cardiac GUI: name displayed in the GUI"))
    , m_loaderSurfaceZoneNames(initData(&m_loaderSurfaceZoneNames,"loaderZoneNames","name of the surface zone from the loader"))
    , m_loaderSurfaceZones(initData(&m_loaderSurfaceZones,"loaderZones","loaderZones"))
    , m_loaderTriangleSurf(initData(&m_loaderTriangleSurf,"trianglesSurf","list of surface triangles"))
    , m_loaderPointZoneNames(initData(&m_loaderPointZoneNames,"pointZoneName","list of point zone"))
    , m_loaderPointZones(initData(&m_loaderPointZones,"pointZones","list of points"))
    , m_whatZone(initData(&m_whatZone,"ZoneType","Triangles or Points"))
    //                , m_edgesOnBorder(initData(&m_edgesOnBorder, "EdgesOnBorder", "List of holes. A hole is an orderred list of point id pairs"))
    , m_boundaryEdgesPath(initData(&m_boundaryEdgesPath, "BoundaryEdgesPath", "Path to the json file containing holes as orderred list of point id pairs"))
    , m_boundaryEdgesKey(initData(&m_boundaryEdgesKey, "BoundaryEdgesKey", "Key (LV|RV) of the ventricle to retrieve edges in json file"))
    , m_tolerance(initData(&m_tolerance, (Real)0, "tolerance", "Tolerance for the cardiac phase detection"))
//     , m_loader_link(initLink("loader_link", "Single link to the CardiacVTKLoader"))
    , m_filePressure(initData(&m_filePressure, "file2", "File to minitor data inside the components"))
    {
        f_graphFlow.setWidget("graph");
        f_graphVolume.setWidget("graph");
        f_graphPressure.setWidget("graph");
    }


      template <class DataTypes> void PressureConstraintForceField<DataTypes>::init()
      {

        msg_info() << "Init PressureConstraintForceField";

//        msg_error_when(!contraction_forcefield) << "contraction_forcefield not found";
//        msg_error_when(!tetra_topology) << "tetra_topology not found";
//         msg_error_when(!m_loader_link) << "Loader link not found";
//         assert(m_loader_link);
msg_error() << "GAETAN: loader link";


        pressure.open((fileName.getValue()).c_str(),std::ios::out);


        if(m_filePressure.isSet())
        {
            if(_pr) _pr.close();  // in case of reinit
            _pr.open(m_filePressure.getValue());
            dump(_pr, "time", "tol");  // "pp", "Pv", "dPv", "Pat", "Par", "btwStartAndQRS", "increasingPv", "secDer", "V", "strain");
        }

        std::vector<Real> atriumParam=f_atriumParam.getValue();
        KKiso=f_Kiso.getValue();
        Pat0=atriumParam[1];
        Pat=Pat0;
        Kiso=KKiso[0];
        windkessel=f_windkessel.getValue();
        // ATRIUM PARAMETERS : Kat (linear atrium coeff - flow/pressure law), Pat0 (initial atrium pressure),
        //Patm (middle atrium pressure = max), alpha1 (first sigmoid coeff), alpha2 (second
        // sigmoid coeff), tof (initial time of atrium contraction ??),
        //tm (middle time of atrium contraction), tc (LV contraction time = end of atrium contraction)
        Kat=atriumParam[0];
        Patm=atriumParam[2];
        alpha1=atriumParam[3];
        alpha2=atriumParam[4];
        tm=atriumParam[6];
        tof=atriumParam[5];
        tc=atriumParam[7];
        std::vector<Real> aorticParam=f_aorticParam.getValue();
        //AORTIC PARAETERS : Kar (linear aorta coeff - flow/pressure
        //law), Par0 (initial aortic pressure), Pve (asymptotic limit of aortic pressure),
        //tau=Rp*C (Windkessel charact. time - for models 2/3/4), Rp (Windkessel
        //peripheral resistance) - for models 2/3/4, Zc (Wind. characteristic impedance
        //- for models 3/4), beta=L/Zc (with L=arterial inertance,- for model 4).
        Kar=aorticParam[0];
        Par=aorticParam[1];
        Zc=aorticParam[5];
        Tau=aorticParam[3];
        L=aorticParam[6];
        Rp=aorticParam[4];
        Pve=aorticParam[2];
        Q=0;


        Pv0=f_Pv0.getValue();
        pp2 = Pv0;

        //t1=(tof+tm)/2.0;
        // t2=(tc+tm)/2.0;
        Pv=Pv0;
        phase=1; //on commence par un remplissage

        msg_info() << "Suppose that first phase is filling";
        _phase_manager = new PhasesManager(PhasesManager::FILLING);

        cerr << "initializing PressureConstraintForceField" <<endl;

        this->Inherited::init();
        this->getContext()->get(_topology, sofa::core::objectmodel::BaseContext::SearchUp);
        std::cout<<_topology->getName()<<std::endl;
        const typename DataTypes::VecCoord restPosition = this->mstate->read(core::ConstVecCoordId::restPosition())->getValue();
        const typename DataTypes::VecDeriv restVitesse = this->mstate->read(sofa::core::ConstVecDerivId::velocity())->getValue();

        edgeInfoVector& edgeInf = *(edgeInfo.beginEdit());
        edgeInf.resize(_topology->getNbEdges());
        std::cout<<_topology->getNbEdges()<<std::endl;
        edgeInfo.endEdit();

        // get restPosition
        if (_initialPoints.size() == 0)
        {
          const VecCoord& p = this->mstate->read(core::ConstVecCoordId::restPosition())->getValue();
          _initialPoints=p;
        }
        std::cout<<_initialPoints.size()<<" points "<<std::endl;
        dV.resize(_initialPoints.size());
        vold.resize((restVitesse).size());
        const std::string sol=m_tagMeshSolver.getValue();

        /// initialize the data structure associate with each tetrahedron
        for (int i=0; i<_topology->getNbEdges(); i++)
        {
          edgeInf[i].vertices[0] = (float) _topology->getEdge(i)[0];
          edgeInf[i].vertices[1] = (float) _topology->getEdge(i)[1];
        }

        /// get the triangles and the vertices on the surface zone, what if I do not have the surface defined?
        sofa::type::vector<std::string> names;
        sofa::type::vector<std::string> surf;
        sofa::type::vector<sofa::type::vector <unsigned int> > loaderzones;
        surf=f_SurfaceZone.getValue();

        std::cout<< surf.size() << " Size of surf" << std::endl;

        if (m_whatZone.getValue() == "Triangles")
        {
          names = m_loaderSurfaceZoneNames.getValue();
          loaderzones=m_loaderSurfaceZones.getValue();
          std::cout<<"surface zone :" <<names<<std::endl;
          // std::cout<<"loader zones :" <<loaderzones<<std::endl; //  Here surface zones are ok...

          BOOST_ASSERT( loaderzones.size() >= names.size() );
          for(unsigned int u=0;u<surf.size();u++)
          {
            for (unsigned int i = 0; i<names.size(); i++ )
            {
              if (names[i] == surf[u])
              {
                sofa::type::vector <unsigned int> tmp = loaderzones[i];
                // std::cout<<"Choosen zone :" <<tmp<<std::endl; // Ok
                for (unsigned int j = 0; j<tmp.size(); ++j)
                {
                  TriangleID tri =tmp [j];
                  SurfaceTriangles.push_back(tri);
                }
              }
            }
          }

          BOOST_ASSERT(SurfaceTriangles.size() > 0);
          std::cout<<SurfaceTriangles.size()<<" triangles"<<std::endl;
          TriangleID tri=SurfaceTriangles[0];
          Triangle trian=_topology->getTriangle(tri);

          BOOST_ASSERT(trian.size() > 0);
          SurfaceVertices.push_back(trian[0]);
          for(unsigned int i=0; i<SurfaceTriangles.size();i++)
          {
            TriangleID tri=SurfaceTriangles[i];
            // std::cout<<"ID: "<<tri<<std::endl; // The ids of the vertex do not match the ones from the loader...
            Triangle trian=_topology->getTriangle(tri);
            // std::cout<<"Triangles: "<<trian<<std::endl; // The ids of the vertex do not match the ones from the loader...
            for (unsigned int p = 0; p<3; ++p)
            {
              bool find = false;
              BOOST_ASSERT(trian.size() >= 3);

              unsigned int vertex = trian [p];
              for (unsigned int k =0; k<SurfaceVertices.size(); k++)
              {
                if ( vertex == SurfaceVertices[k])
                {
                  find = true;
                  break;
                }
              }
              if (!find)
              SurfaceVertices.push_back(vertex);
            }
          }

          std::cout<<SurfaceVertices.size()<<" vertices"<<std::endl;
          /// Associate a vertex with its surface triangles
        }

        else if (m_whatZone.getValue() == "Points")
        {
          names = m_loaderPointZoneNames.getValue();
          loaderzones=m_loaderPointZones.getValue();
          std::cout<<"point zone :" <<names<<std::endl;

          BOOST_ASSERT( loaderzones.size() >= names.size() );
          for(unsigned int u=0;u<surf.size();u++)
          {
            for (unsigned int i = 0; i<names.size(); i++ )
            {
              if (names[i] == surf[u])
              {
                sofa::type::vector <unsigned int> tmp2 = loaderzones[i];
                for (unsigned int j = 0; j<tmp2.size(); j++)
                {
                  unsigned int tri =tmp2 [j];
                  SurfaceVertices.push_back(tri);
                }
              }
            }
          }
          std::cout<<SurfaceVertices.size()<<" vertices"<<std::endl;

          const sofa::type::vector <unsigned int> tmp = m_loaderTriangleSurf.getValue();
          std::cout<<tmp.size()<<std::endl;
          for (unsigned int j = 0; j<tmp.size(); j++)
          {
            int tri =tmp [j];
            Triangle trian=_topology->getTriangle(tri);
            int cpt=0;
            for (unsigned int p = 0; p<3; p++)
            {
              BOOST_ASSERT(trian.size() >= 3);
              unsigned int vertex = trian [p];
              for (unsigned int k =0; k<SurfaceVertices.size(); k++)
              {
                if ( vertex == SurfaceVertices[k])
                {
                  cpt++;
                  break;
                }
              }
            }
            if (cpt>1) SurfaceTriangles.push_back(tri);
          }
          std::cout<<SurfaceTriangles.size()<<" bb triangles on the surface"<<std::endl;
        }

        else  /// Neither points ot triangles
        {
          std::cout<<f_loader.getValue()<<std::endl;
          if (f_loader.getValue()=="VTK")
          {
            this->getContext()->get(_vtkLoader, sofa::core::objectmodel::BaseContext::SearchRoot);
            if (!_vtkLoader)
            std::cout << "Error: PressureConstraintForceField is not able to acces mesh loader!" << std::endl;
            names = _vtkLoader->m_surfaceZoneNames.getValue();
            surf=f_SurfaceZone.getValue(); // zone
            loaderzones=_vtkLoader->m_surfaceZones.getValue();
            std::cout<<"surface zone :" <<names<<std::endl;
          }
//           if (f_loader.getValue()=="ATET3D")
//           {
//             this->getContext()->get(_meshLoader, sofa::core::objectmodel::BaseContext::SearchRoot);
//             if (!_meshLoader)
//             std::cout << "Error: PressureConstraintForceField is not able to acces mesh loader!" << std::endl;
//
//             names = _meshLoader->m_surfaceZoneNames.getValue();
//             surf=f_SurfaceZone.getValue(); // zone
//             loaderzones=_meshLoader->m_surfaceZones.getValue();
//             //names = _meshLoader->m_surfaceZoneNames.getValue();
//             //surf=f_SurfaceZone.getValue();
//             //loaderzones=_meshLoader->m_surfaceZones.getValue();
//           }

          BOOST_ASSERT( loaderzones.size() >= names.size() );
          for(unsigned int u=0;u<surf.size();u++)
          {
            for (unsigned int i = 0; i<names.size(); i++ )
            {
              if (names[i] == surf[u])
              {
                sofa::type::vector <unsigned int> tmp = loaderzones[i];
                for (unsigned int j = 0; j<tmp.size(); ++j)
                {
                  TriangleID tri =tmp [j];
                  SurfaceTriangles.push_back(tri);
                  // std::cout<<"Triangle = "<<tri<<std::endl; // Print indices of triangle
                }
              }
            }
          }
          BOOST_ASSERT(SurfaceTriangles.size() > 0);
          std::cout<<SurfaceTriangles.size()<<" triangles"<<std::endl;
          TriangleID tri=SurfaceTriangles[0];
          Triangle trian=_topology->getTriangle(tri);

          BOOST_ASSERT(trian.size() > 0);
          SurfaceVertices.push_back(trian[0]);
          for(unsigned int i=0; i<SurfaceTriangles.size();i++)
          {
            TriangleID tri=SurfaceTriangles[i];
            Triangle trian=_topology->getTriangle(tri);
            // std::cout<<"Triangles: "<<trian<<std::endl; // The ids of the vertex do not match the ones from the loader...
            for (unsigned int p = 0; p<3; ++p)
            {
              bool find = false;
              BOOST_ASSERT(trian.size() >= 3);

              unsigned int vertex = trian [p];
              for (unsigned int k =0; k<SurfaceVertices.size(); k++)
              {
                if ( vertex == SurfaceVertices[k])
                {
                  find = true;
                  break;
                }
              }
              if (!find)
              SurfaceVertices.push_back(vertex);
            }
          }
          std::cout<<SurfaceVertices.size()<<" vertices"<<std::endl;
          /// Associate a vertex with its surface triangles
        }  /// END of the options to get the surface triangles

        for (unsigned int i=0; i<SurfaceVertices.size();i++){
          vector<TriangleID> TrianglesAroundVertex=_topology->getTrianglesAroundVertex(SurfaceVertices[i]);

          //	std::cout<<TrianglesAroundVertex.size()<<std::endl;
          vold[SurfaceVertices[i]]=(restVitesse)[SurfaceVertices[i]];

          for (unsigned int k=0; k<TrianglesAroundVertex.size();k++){
            for (unsigned int j=0; j<SurfaceTriangles.size();j++){
              if (TrianglesAroundVertex[k]==SurfaceTriangles[j]){
                std::pair<TriangleID,int> pairTri;
                pairTri.first=SurfaceTriangles[j];
                pairTri.second=SurfaceVertices[i];
                SurfaceTrianglesPair.push_back(pairTri);
                //	std::cout<<i<<" vertice"<<pairTri.first<<" first "<<pairTri.second<<" second"<<std::endl;
                break;
              }
            }
          }

        }
        std::cout<<"pairing done"<<std::endl;


        bool COMPUTE_ANYWAY = true; // if an error occurs, compute edges on border
        if(m_boundaryEdgesKey.isSet() and m_boundaryEdgesPath.isSet()) {
            COMPUTE_ANYWAY = false;
            typedef std::vector<unsigned int> TEdge;
            typedef std::vector<TEdge> THole;
            typedef std::vector<THole> TVentHoles;
            typedef std::map<std::string, TVentHoles> TMap;

            //                    msg_info() << "Starting to cast Edges on Border!";
            EdgesOnBorder.clear();

            try {
                std::ifstream i(m_boundaryEdgesPath.getValue());
                helper::json j;
                i >> j;

                std::string key = m_boundaryEdgesKey.getValue();
                TMap data = j.get<TMap>();
                msg_info() << "Found "<<data[key].size()<<" holes for "<<key;
                for(THole couples: data[key]) {
                std::vector<Edge> hole;
                for(TEdge edge: couples) hole.push_back(Edge(edge[0], edge[1]));
                EdgesOnBorder.push_back(hole);
                }

                msg_info() << "Hole 1: First edge: " << EdgesOnBorder[0][0];
                msg_info() << "Hole 1: Second edge: " << EdgesOnBorder[0][EdgesOnBorder[0].size()-1];
            }  catch (const std::exception& e) {
                msg_error() << "Cannot read Edges on Border from json file: " << m_boundaryEdgesPath.getValue();// << ". Error is: " << e.what();
                COMPUTE_ANYWAY = true;
            }
        }
        if (COMPUTE_ANYWAY) { // means that retreiving egdes from json file has failed
          /// Get the edges on the borders
          std::vector<Edge> EdgesBo;
          Coord pointP[3];
          std::pair<EdgeID,TriangleID> edgetri;
          for (unsigned int i=0; i<SurfaceTriangles.size(); i++){
            TriangleID tri=SurfaceTriangles[i];
            EdgesInTriangle EdgeInTri=_topology->getEdgesInTriangle(tri);

            for(unsigned int u=0; u<3; u++){
              BOOST_ASSERT(EdgeInTri.size() >= 3);
              std::vector<TriangleID> AroundEdge=_topology->getTrianglesAroundEdge(EdgeInTri[u]);

              int number=0;
              for(unsigned int j=0; j< AroundEdge.size();j++){

                for (unsigned int k=0; k<SurfaceTriangles.size();k++){

                  if (AroundEdge[j]==SurfaceTriangles[k]){
                    number++;
                    edgetri.first=EdgeInTri[u];
                    edgetri.second=AroundEdge[j];
                    break;
                  }
                }
              }
              if (number==1) {
                Edge newedge;
                Triangle tr=_topology->getTriangle(edgetri.second);
                Edge EdgesTri=_topology->getEdge(edgetri.first);
                BOOST_ASSERT(EdgesTri.size() >= 2);
                unsigned int k=EdgesTri[0];
                unsigned int l=EdgesTri[1];
                BOOST_ASSERT(tr.size() >= 3);
                BOOST_ASSERT(newedge.size() >= 2);
                if(((tr[0]==k)&&(tr[1]==l))||((tr[1]==k)&&(tr[2]==l))||((tr[2]==k)&&(tr[0]==l))){
                  newedge[0]=l;
                  newedge[1]=k;
                }
                else{
                  newedge[0]=k;
                  newedge[1]=l;
                }
                EdgesBo.push_back(newedge);

              }
            }
          }


          /// Get the edges attached to each hole;
          if(EdgesBo.size()>0)
          {
            std::cout<<"Get edges attached to a hole"<<std::endl;
            std::vector<Edge> Hole;
            Hole.push_back(EdgesBo[0]);
            try
            {
              int ite = 0;

              while (Hole[Hole.size()-1][1]!=Hole[0][0])
              {
                ite++;
                for (unsigned int i=0;i<EdgesBo.size();i++)
                {

                  Edge ei=EdgesBo[i];
                  if(ei[0]==Hole[Hole.size()-1][1]){
                    Hole.push_back(ei);
                    i=0;
                  }

                }
                msg_info_when(ite == 1000) << "More than 1000 iterations !";
                msg_info_when(ite == 10000) << "More than 10000 iterations !";

                if(ite == 1000){
                  for(int ii=0; ii<Hole.size(); ++ii)
                  msg_warning() << Hole[ii];
                }
              }
            }

            catch (std::bad_alloc& ba)
            {
              std::cout << "PressureConstraintForceField: ended up iterating infinitely on one triangle while looking for edges attached to each hole for the mesh zone "
              << surf[0] << ". Hole vector size " << Hole.size() << '\n' << "try to define " << surf[0] << " surface zone as manifold.";
            }

            EdgesOnBorder.push_back(Hole);
            if (Hole.size()<EdgesBo.size()){
              std::vector<Edge> Hole2;
              std::cout<<Hole.size()<<" "<<EdgesBo.size()<<std::endl;
              for (unsigned int i=0; i<EdgesBo.size();i++){
                int number=0;
                for (unsigned int j=0;j<Hole.size();j++){
                  if (EdgesBo[i][0]==Hole[j][0]){
                    number++;
                    break;
                  }
                }
                if(number==0) Hole2.push_back(EdgesBo[i]);
              }
              EdgesOnBorder.push_back(Hole2);
            }


          }
        }



        //calcul old volume (before deformation)
        std::cout<<"Calculate volume before def."<<std::endl;
        V=0;
        Coord pointP[3];
        for (unsigned int i=0; i<SurfaceTriangles.size(); i++){

          Triangle tri=_topology->getTriangle(SurfaceTriangles[i]);
          for(int j=0;j<3;++j) pointP[j]=(restPosition)[tri[j]];
          Real Vi=dot(cross(pointP[1],pointP[2]),pointP[0]);
          V+=Vi/6.0;


        }

        if (EdgesOnBorder.size()>0){

          for(unsigned int u=0; u<EdgesOnBorder.size(); u++){
            std::vector<Edge> Hole=EdgesOnBorder[u];
            Coord centroid;
            centroid.clear();

            for(unsigned int k=0; k<Hole.size(); k++){
              Edge ei=Hole[k];
              centroid += (restPosition)[ei[0]];
            }
            centroid/=Hole.size();
            std::cout<<centroid<<std::endl;
            for(unsigned int k=0; k<Hole.size(); k++){
              Edge ei=Hole[k];
              Real vi=dot(cross((restPosition)[ei[1]],centroid),(restPosition)[ei[0]])/6.0;
              V+=vi;
              //std::cout<<vi<<std::endl;
            }
          }
          //  xg.resize(EdgesOnBorder.size());
          dvcentroid.resize(EdgesOnBorder.size());
        }

        std::cout<<"Volume :" <<V<<std::endl;
        V_initial = V;
        Fd=0;
        D=1e99;
        pp=Pv0;
        volume.setValue(V);
        pressurePat.setValue(Pat);
        pressurePv.setValue(Pv);
        pressurePar.setValue(Par);
        flowQ.setValue(Q);

        this->initEdgeNeighbourhoodPoints();
      }


      template <class DataTypes>
      void PressureConstraintForceField<DataTypes>::reinit()
      {
        if(pressure.is_open()){
          pressure.close();
        }
        pressure.open((fileName.getValue()).c_str(),std::ios::out);


        std::vector<Real> atriumParam=f_atriumParam.getValue();
        KKiso=f_Kiso.getValue();
        Pat0=atriumParam[1];
        Pat=Pat0;
        Kiso=KKiso[0];
        windkessel=f_windkessel.getValue();
        Kat=atriumParam[0];
        Patm=atriumParam[2];
        alpha1=atriumParam[3];
        alpha2=atriumParam[4];
        tm=atriumParam[6];
        tof=atriumParam[5];
        tc=atriumParam[7];
        std::vector<Real> aorticParam=f_aorticParam.getValue();
        Kar=aorticParam[0];
        Par=aorticParam[1];
        Zc=aorticParam[5];
        Tau=aorticParam[3];
        L=aorticParam[6];
        Rp=aorticParam[4];
        Pve=aorticParam[2];
        Q=0;


        Pv0=f_Pv0.getValue();
        //t1=(tof+tm)/2.0;
        // t2=(tc+tm)/2.0;
        Pv=Pv0;
        phase=1; //on commence par un remplissage

        cerr << "reset PressureConstraintForceField" <<endl;


        V=0;
        Coord pointP[3];

        const typename DataTypes::VecCoord restPosition = this->mstate->read(core::ConstVecCoordId::restPosition())->getValue();
        //const typename DataTypes::VecDeriv restVitesse = this->mstate->read(sofa::core::ConstVecDerivId::velocity())->getValue();

        std::cout << SurfaceTriangles.size() << std::endl;
        for (unsigned int i=0; i<SurfaceTriangles.size(); i++){

          Triangle tri=_topology->getTriangle(SurfaceTriangles[i]);
          for(int j=0;j<3;++j) pointP[j]=(restPosition)[tri[j]];
          Real Vi=dot(cross(pointP[1],pointP[2]),pointP[0]);
          V+=Vi/6.0;


        }

        if (EdgesOnBorder.size()>0){

          for(unsigned int u=0; u<EdgesOnBorder.size(); u++){
            std::vector<Edge> Hole=EdgesOnBorder[u];
            Coord centroid;
            centroid.clear();

            for(unsigned int k=0; k<Hole.size(); k++){
              Edge ei=Hole[k];
              centroid += (restPosition)[ei[0]];
            }
            centroid/=Hole.size();
            for(unsigned int k=0; k<Hole.size(); k++){
              Edge ei=Hole[k];
              Real vi=dot(cross((restPosition)[ei[1]],centroid),(restPosition)[ei[0]])/6.0;
              V+=vi;
              //std::cout<<vi<<std::endl;
            }
          }
          //  xg.resize(EdgesOnBorder.size());
          dvcentroid.resize(EdgesOnBorder.size());
        }

        std::cout<<V<<std::endl;
        Fd=0;
        D=1e99;
        pp=Pv0;
        volume.setValue(V);
        pressurePat.setValue(Pat);
        pressurePv.setValue(Pv);
        pressurePar.setValue(Par);
        flowQ.setValue(Q);
      }


      template <class DataTypes>
      void PressureConstraintForceField<DataTypes>::reset()
      {
        if(pressure.is_open()){
          pressure.close();
        }
        pressure.open((fileName.getValue()).c_str(),std::ios::out);


        std::vector<Real> atriumParam=f_atriumParam.getValue();
        KKiso=f_Kiso.getValue();
        Pat0=atriumParam[1];
        Pat=Pat0;
        Kiso=KKiso[0];
        windkessel=f_windkessel.getValue();
        Kat=atriumParam[0];
        Patm=atriumParam[2];
        alpha1=atriumParam[3];
        alpha2=atriumParam[4];
        tm=atriumParam[6];
        tof=atriumParam[5];
        tc=atriumParam[7];
        std::vector<Real> aorticParam=f_aorticParam.getValue();
        Kar=aorticParam[0];
        Par=aorticParam[1];
        Zc=aorticParam[5];
        Tau=aorticParam[3];
        L=aorticParam[6];
        Rp=aorticParam[4];
        Pve=aorticParam[2];
        Q=0;


        Pv0=f_Pv0.getValue();
        //t1=(tof+tm)/2.0;
        // t2=(tc+tm)/2.0;
        Pv=Pv0;
        phase=1; //on commence par un remplissage
        cerr << "reset PressureConstraintForceField" <<endl;

        V=0;
        Coord pointP[3];

        const typename DataTypes::VecCoord restPosition = this->mstate->read(core::ConstVecCoordId::restPosition())->getValue();
        //const typename DataTypes::VecDeriv restVitesse = this->mstate->read(sofa::core::ConstVecDerivId::velocity())->getValue();

        for (unsigned int i=0; i<SurfaceTriangles.size(); i++){
          Triangle tri=_topology->getTriangle(SurfaceTriangles[i]);
          for(int j=0;j<3;++j) pointP[j]=(restPosition)[tri[j]];
          Real Vi=dot(cross(pointP[1],pointP[2]),pointP[0]);
          V+=Vi/6.0;


        }

        if (EdgesOnBorder.size()>0){

          for(unsigned int u=0; u<EdgesOnBorder.size(); u++){
            std::vector<Edge> Hole=EdgesOnBorder[u];
            Coord centroid;
            centroid.clear();

            for(unsigned int k=0; k<Hole.size(); k++){
              Edge ei=Hole[k];
              centroid += (restPosition)[ei[0]];
            }
            centroid/=Hole.size();
            for(unsigned int k=0; k<Hole.size(); k++){
              Edge ei=Hole[k];
              Real vi=dot(cross((restPosition)[ei[1]],centroid),(restPosition)[ei[0]])/6.0;
              V+=vi;
              //std::cout<<vi<<std::endl;
            }
          }
          //  xg.resize(EdgesOnBorder.size());
          dvcentroid.resize(EdgesOnBorder.size());
        }

        std::cout<<V<<std::endl;

        Fd=0;
        D=1e99;
        pp=Pv0;
        volume.setValue(V);
        pressurePat.setValue(Pat);
        pressurePv.setValue(Pv);
        pressurePar.setValue(Par);
        flowQ.setValue(Q);

      }


      template <class DataTypes>
      void PressureConstraintForceField<DataTypes>::initEdgeNeighbourhoodPoints(){}


      template <class DataTypes> void PressureConstraintForceField<DataTypes>::handleTopologyChange()
      {

      }

      template <class DataTypes>
      SReal PressureConstraintForceField<DataTypes>::getPotentialEnergy(const core::MechanicalParams*, const DataVecCoord& d_x) const
      {
        const VecCoord& x = d_x.getValue();
        double energy=0;

        for (unsigned int i=0; i<SurfaceVertices.size();i++){
          if(!useProjection.getValue()) energy-=dot(dV[SurfaceVertices[i]],x[SurfaceVertices[i]])*Fd/D;
          else energy-=dot(dV[SurfaceVertices[i]],x[SurfaceVertices[i]])*Pv;

        }


        //  return energy;
        return 0;
      }

      template <class DataTypes>
      SReal PressureConstraintForceField<DataTypes>::getPotentialEnergy2() const
      {
        const VecDeriv& x = this->mstate->read(core::ConstVecCoordId::position())->getValue();

        //const VecCoord& x = d_x.getValue();
        double energy=0;

        for (unsigned int i=0; i<SurfaceVertices.size();i++){
          if(!useProjection.getValue()) energy-=dot(dV[SurfaceVertices[i]],x[SurfaceVertices[i]])*Fd/D;
          else energy-=dot(dV[SurfaceVertices[i]],x[SurfaceVertices[i]])*Pv;

        }


        return energy;
        //  return 0;
      }


    template <class DataTypes>
    void PressureConstraintForceField<DataTypes>::NanVolume()
    {
        msg_error() << "Volume is Nan, simulation crashed";
//        std::cout << "Volume is Nan, simulation crashed" << std::endl;
        std::ofstream Jac2;
        std::string fileNameString = (fileName.getValue()).c_str();
        char *fileNameChar = new char[fileNameString.length() + 1];
        std::strcpy(fileNameChar, fileNameString.c_str());
        char* name = strcat(fileNameChar,"_ERROR_NAN");
        Jac2.open(name,std::ios::out);
        Jac2 << "NAN spotted 1";
        Jac2.close();
        delete[] fileNameChar;
        std::exit(EXIT_FAILURE);
    }




    template <class DataTypes>
    void PressureConstraintForceField<DataTypes>::addForce(const core::MechanicalParams* /* PARAMS FIRST */, DataVecDeriv& d_f, const DataVecCoord& d_x, const DataVecDeriv& d_v )
    {
        sofa::helper::AdvancedTimer::stepBegin("addForce_PressureConstraintFF");

        double simuTime = this->getContext()->getTime();
        double dt = this->getContext()->getDt();
        int HP = f_hp.getValue()/this->getContext()->getDt();
        int steps = simuTime / this->getContext()->getDt();

        VecDeriv &f = *(d_f.beginEdit());
        VecDeriv v = d_v.getValue();
        VecCoord x = d_x.getValue();

        Coord pointP[3];
        Deriv pop[3];
        Real V1=0;
        Real Parnew=0;
        int numberCycles = int(simuTime / f_hp.getValue()) + 1;


        if(useVerdandi.getValue())
        {
          std::vector<Real> aorticParam = f_aorticParam.getValue();
          Zc=aorticParam[5];
          Tau=aorticParam[3];
          L=aorticParam[6];
          Rp=aorticParam[4];
        }




        ///
        /// Computation of the new volume, and flow
        ///

        if(EdgesOnBorder.size() > 0) centroid.resize(EdgesOnBorder.size());

        // Computation of volume for interior points
        for (unsigned int i=0; i<SurfaceTriangles.size(); i++){
            Triangle tri = _topology->getTriangle(SurfaceTriangles[i]);
            for(int j=0; j<3; ++j)
            {
                pointP[j] = x[tri[j]];
                if(j>0) pop[j] = pointP[j] - pointP[0];
            }
            Real Vi = dot(cross(pointP[1],pointP[2]),pointP[0]);
            V1 += Vi/6.0;
        }
        if(V1!=V1) this->NanVolume();


        // Computation of volume for border points, hole centroids
        if(EdgesOnBorder.size() > 0)
        {
            for(unsigned int u=0; u<EdgesOnBorder.size(); u++) // for each hole, compute the centroid
            {
                std::vector<Edge> Hole = EdgesOnBorder[u];
                centroid[u].clear();
                dvcentroid[u].clear();

                for(unsigned int k=0; k<Hole.size(); k++){
                  Edge ei = Hole[k];
                  centroid[u] += x[ei[0]];
                }
                // centroid coords
                centroid[u] /= Hole.size();

                for(unsigned int k=0; k<Hole.size(); k++)
                {
                    Edge ei = Hole[k];
                    Real vi = dot(cross(x[ei[1]], centroid[u]), x[ei[0]]) / 6.0;
                    // volume
                    V1 += vi;
                    dvcentroid[u] += cross(x[ei[0]], x[ei[1]]) / Hole.size();
                }
            }
        }
        if(V1!=V1) this->NanVolume();
        V = V1;

        ///
        /// Calcul of G(t-dt).vitesse(t)
        ///
        Real Gdv = 0;
        for (unsigned int i=0; i<SurfaceVertices.size();i++){
          Gdv += dV[SurfaceVertices[i]] * v[SurfaceVertices[i]];
        }

        // therefore we obtain Q(t)=-G(t-dt).v(t)
        Real Qnew = -Gdv;
        Real dQ = (Qnew - Q) / dt;
        Q = Qnew;

        if (!useProjection.getValue()) Pv = (Fd+Q+GV)/D;



        VecCoord Dvo;
        Dvo.resize(x.size());
        dV = Dvo;

        ///
        /// Computation of the derivative of the volume with respect to the nodal position, for the nodes on the surface
        /// See Apendix C in Stephanie's thesis
        ///

        // computation of dV for interior points
        for (unsigned int i=0; i<SurfaceTrianglesPair.size(); i++)
        {
            unsigned int vertex = SurfaceTrianglesPair[i].second;
            TriangleID TriID = SurfaceTrianglesPair[i].first;
            Triangle Tri = _topology->getTriangle(TriID);
            Vec3 areaVector = cross(x[Tri[0]], x[Tri[1]]) + cross(x[Tri[1]], x[Tri[2]]) + cross(x[Tri[2]], x[Tri[0]]);
            dV[vertex] += areaVector / 6.0;
        }

        // computation of dV for border points
        if(EdgesOnBorder.size() > 0)
        {
            for(unsigned int u=0; u<EdgesOnBorder.size(); u++)
            {
                std::vector<Edge> Hole = EdgesOnBorder[u];
                for (unsigned int i=0; i<Hole.size();i++)
                {
                    Edge ei = Hole[i];
                    dV[ei[0]] += dvcentroid[u]/12.0;
                    dV[ei[1]] += dvcentroid[u]/12.0;
                    dV[ei[0]] += (cross(x[ei[1]],centroid[u])+cross(x[ei[0]],x[ei[1]]))/6.0;
                    dV[ei[1]] += (cross(x[ei[0]],x[ei[1]])+cross(centroid[u],x[ei[0]]))/6.0;
                }
            }
        }


        ///
        /// Computation of G(t).vitesse(t)
        ///
        GV = 0;
        for (unsigned int i=0; i<SurfaceVertices.size(); i++){
          GV += dV[SurfaceVertices[i]] * v[SurfaceVertices[i]];
        }




        /// Calcul of Par(t)
        if(pp>=Par){ // if ejection phase at time t-dt
          Parnew=(Q-Pv*Kar+Pat*Kiso)/(Kiso-Kar);
          if(windkessel==0) Parnew=Par;
        } else if (pp<Par) {
          Parnew=(Tau*Par+dt*Pve)/(dt+Tau);
          if (windkessel==0) Parnew=Par;
        }

        Par = Parnew;

        double AtriumContraction=0;
        double IsoPhase=0;
        double EjectionPhase=0;
        double RealPhase = 0;


        /// Computation of Pat(t+dt)
        Real Patnew=Pat0;
        double Tm=tm+(numberCycles-1)*f_hp.getValue(); // //middle time ??
        double Tof=tof+(numberCycles-1)*f_hp.getValue(); // offset time ?? (beginning)
        double Tc=tc+(numberCycles-1)*f_hp.getValue(); // contraction time ??
        double T1=(Tm+Tof)/2;
        double T2=(Tc+Tm)/2;

        if(DisableFirstAtriumContraction.getValue() && (numberCycles==1))
        {
            Patnew = Pat0;
            AtriumContraction = 0;
        }
        else
        {
            if(m_use_escalier_Pat.getValue())
            {
                Patnew = ((simuTime+dt >= Tof) && (simuTime+dt <= Tc)) ? Patm : Pat0;
                msg_info() << "Updating Patnew: " << Patnew;
            }
            else
                {
                if((simuTime+dt <= Tm) && (simuTime+dt >= Tof))
                // first sigmoid, augment pressure
                {
                    Patnew = Pat0 * (1.0+(Patm/Pat0-1.0)/(1.0+exp(-alpha1*(simuTime+dt-T1))));
                    AtriumContraction = 1;
                }
                else if((simuTime+dt > Tm) && (simuTime+dt <= Tof+f_hp.getValue()))
                // second, decrease pressure
                {
                    Patnew = Pat0 * (1.0+(Patm/Pat0-1.0)/(1.0+exp(alpha2*(simuTime+dt-T2))));
                    AtriumContraction = 2;
                }
                else
                {
                    Patnew = Pat0;
                    AtriumContraction = 0;
                }
            }
        }


    Pat=Patnew;

    /// get D and Fd fonction of the phase we are in at time t

    //  Real tol = m_tolerance.getValue();
    //  Real hp = f_hp.getValue();
    Real time = this->getTime();
    //  Real qrs = m_loader_link->m_startContraction.getValue();
    //  bool btwStartAndQRS = ((fmod(time, hp) > 0) && (fmod(time, hp) <= qrs));

//    PhasesManager::Phase old_phase = _phase_manager->getCurrentPhase();
    PhasesManager::Phase current_phase = getPhase();




    switch(current_phase) {
    // See page 42 of Stephanie's thesis
    case PhasesManager::FILLING:
        D = Kat;
        Fd = Kat * Patnew - GV;

        phase = 1;
        RealPhase = 0;

        msg_info() << time << " Filling";
        break;



    case PhasesManager::ISOCONTRACTION:
        Kiso = KKiso[0];
        IsoPhase = 1;
        RealPhase = 1;

        msg_info() << time << " Isovolumetric Contraction";

        D = Kiso;
        Fd = Kiso*Patnew - GV;
        break;



    case PhasesManager::ISORELAXATION:
        Kiso=KKiso[1];
        IsoPhase=2;
        RealPhase = 3;

        msg_info() << time << " Isovolumetric Relaxation";

        D = Kiso;
        Fd = Kiso*Patnew - GV;
        break;



    case PhasesManager::EJECTION:
        EjectionPhase=1;
        phase=2;
        RealPhase = 2;

        msg_info() << time << " Ejection";


        Real d1, d0, P;
        if(windkessel==4)
        {
            d1=Kar/(Kar-Kiso)*(1.0+Tau/dt);
            d0=Rp+(1.0+Tau/dt)*(1.0/(Kar-Kiso)+Zc)+L/dt+L*Tau/(dt*dt);
            P=(Patnew*(1.0+Tau/dt)*(Kiso/(Kar-Kiso))+Tau/dt*Par+Pve)-((Tau*Zc+L)/dt+L*Tau/(dt*dt))*Q-L*Tau/dt*dQ;
        }
        else if (windkessel==2)
        {
            d1=Kar/(Kar-Kiso)*(1.0+Tau/dt);
            d0=Rp+(1.0+Tau/dt)/(Kar-Kiso);
            P=(Patnew*(1.0+Tau/dt)*(Kiso/(Kar-Kiso))+Tau/dt*Par+Pve);
        }
        else if (windkessel==3)
        {
            d1=Kar/(Kar-Kiso)*(1.0+Tau/dt);
            d0=Rp+(1.0+Tau/dt)*(1.0/(Kar-Kiso)+Zc);
            P=(Patnew*(1.0+Tau/dt)*(Kiso/(Kar-Kiso))+Tau/dt*Par+Pve)-Tau*Zc*Q/dt;
        }
        else if (windkessel==0)
        {
            Parnew=Par;
            d1=Kar;
            d0=1.0;
            P=((Kar-Kiso)*Parnew+Kiso*Patnew);
        }

        D = d1/d0;
        Fd = P/d0 - GV;

        break;
    } // end switch(current_phase)





//  /****************  filling  **********************/
//  if ((Pv <= Pat) || btwStartAndQRS)
//  {
//    D = Kat;
//    Fd = Kat * Patnew - GV;

//    phase = 1;
//    RealPhase = 0;

//    msg_info() << time << " Filling";
//  }


//  /****************  iso  **********************/
//  else if ((Pv > Pat+tol) && (Pv < Par))
//  {
//    if (phase == 1)
//    {
//        Kiso = KKiso[0];
//        IsoPhase = 1;
//        RealPhase = 1;

//        msg_info() << time << " Isovolumetric Contraction";
//    }
//    else if (phase==2)
//    {
//        Kiso=KKiso[1];
//        IsoPhase=2;
//        RealPhase = 3;

//        msg_info() << time << " Isovolumetric Relaxation";
//    }

//    D=Kiso;
//    Fd=Kiso*Patnew-GV;
//  }


//  /****************  ejection  **********************/

//  else if (Pv >= Par)
//  {
//    EjectionPhase=1;
//    phase=2;
//    RealPhase = 2;

//    msg_info() << time << " Ejection";


//    Real d1,d0,P;
//    if(windkessel==4){


//      d1=Kar/(Kar-Kiso)*(1.0+Tau/dt);
//      d0=Rp+(1.0+Tau/dt)*(1.0/(Kar-Kiso)+Zc)+L/dt+L*Tau/(dt*dt);
//      P=(Patnew*(1.0+Tau/dt)*(Kiso/(Kar-Kiso))+Tau/dt*Par+Pve)-((Tau*Zc+L)/dt+L*Tau/(dt*dt))*Q-L*Tau/dt*dQ;


//    }
//    else if (windkessel==2){

//      d1=Kar/(Kar-Kiso)*(1.0+Tau/dt);
//      d0=Rp+(1.0+Tau/dt)/(Kar-Kiso);
//      P=(Patnew*(1.0+Tau/dt)*(Kiso/(Kar-Kiso))+Tau/dt*Par+Pve);

//    }
//    else if (windkessel==3){

//      d1=Kar/(Kar-Kiso)*(1.0+Tau/dt);
//      d0=Rp+(1.0+Tau/dt)*(1.0/(Kar-Kiso)+Zc);
//      P=(Patnew*(1.0+Tau/dt)*(Kiso/(Kar-Kiso))+Tau/dt*Par+Pve)-Tau*Zc*Q/dt;

//    }
//    else if (windkessel==0){

//      Parnew=Par;
//      d1=Kar;
//      d0=1.0;
//      P=((Kar-Kiso)*Parnew+Kiso*Patnew);

//    }

//    D=(d1/d0);
//    Fd=(P/d0)-GV;

//  }


        /// TODO: dump the header and get it in python
        // dump(pressure, "V", "Pat", "Pv", "Par", "Q", "simuTime", "pp", "AtriumContraction", "IsoPhase", "EjectionPhase", "RealPhase");


        dump(pressure, V, Pat, Pv, Par, Q, simuTime, pp, AtriumContraction, IsoPhase, EjectionPhase, RealPhase);
//        pressure<<V<<" "<<Pat<<" "<<Pv<<" "<<Par<<" "<<Q<<" "<<simuTime<<" "<<pp<<" "<<AtriumContraction<<" "<<IsoPhase<<" "<<EjectionPhase<<" "<<RealPhase<<std::endl;  // Write pressure file
        volume.setValue(V);
        pressurePat.setValue(Pat);
        pressurePv.setValue(Pv);
        pressurePar.setValue(Par);
        flowQ.setValue(Q);


        /// Actually add force
        for (unsigned int i=0; i<SurfaceVertices.size(); i++)
        {
            if(!useProjection.getValue())
                f[SurfaceVertices[i]] += dV[SurfaceVertices[i]]*Fd/D;
            else
                f[SurfaceVertices[i]] += dV[SurfaceVertices[i]]*Pv;

            vold[SurfaceVertices[i]] = v[SurfaceVertices[i]];
        }

        pp2 = pp;
        pp = Pv;

        updateMatrix=true;
        d_f.endEdit();

        sofa::helper::AdvancedTimer::stepEnd("addForce_PressureConstraintFF");
    }





    template <class DataTypes>
    Vec<3,double> PressureConstraintForceField<DataTypes>::getG(const unsigned int nodeIndex)
    {
      return dV[nodeIndex];
    }
    template <class DataTypes>
    void PressureConstraintForceField<DataTypes>::setG(const unsigned int nodeIndex,Vec<3,double> vec)
    {
      dV[nodeIndex]=vec;
    }

    template <class DataTypes>
    Vec<9,double> PressureConstraintForceField<DataTypes>::getVariables()
    {
      Vec<9,double> vec;
      vec[0]=Q;
      vec[1]=V;
      vec[2]=pp;
      vec[3]=Pv;
      vec[4]=Par;
      vec[5]=D;
      vec[6]=Fd;
      vec[7]=GV;
      vec[8]=Pat;
      return vec;
    }

    template <class DataTypes>
    void PressureConstraintForceField<DataTypes>::setVariables(Real  q_,Real v_,Real Pvold_,Real Pvnew_,Real Par_,Real D_,Real FD_,Real GV_,Real Pat_)
    {
      Q=q_;
      V=v_;
      pp=Pvold_;
      Pv=Pvnew_;
      Par=Par_;
      D=D_;
      Fd=FD_;
      GV=GV_;
      Pat=Pat_;
    }

    template <class DataTypes>
    void PressureConstraintForceField<DataTypes>::updateGraph()
    {

      std::map < std::string, sofa::type::vector<double> >& graphPressure = *(f_graphPressure.beginEdit());
      sofa::type::vector<double>& graph_Pv = graphPressure["Pv"];
      sofa::type::vector<double>& graph_Pat = graphPressure["Pat"];
      sofa::type::vector<double>& graph_Par = graphPressure["Par"];
      graph_Pv.push_back(Pv);
      graph_Pat.push_back(Pat);
      graph_Par.push_back(Par);


      f_graphPressure.endEdit();

      std::map < std::string, sofa::type::vector<double> >& graphVolume = *(f_graphVolume.beginEdit());
      sofa::type::vector<double>& graphV = graphVolume["Volume"];
      graphV.push_back(V);


      f_graphVolume.endEdit();

      std::map < std::string, sofa::type::vector<double> >& graphFlow = *(f_graphFlow.beginEdit());
      sofa::type::vector<double>& graphF = graphFlow["Flow"];
      graphF.push_back(Q+1);//hack for representation of values around 0
      f_graphFlow.endEdit();

    }


    template <class DataTypes>
    void PressureConstraintForceField<DataTypes>::updateMatrixData()
    {
        sofa::helper::AdvancedTimer::stepBegin("updateMatrixData_PressureConstraintFF");

      // std::cout<< Pv<<std::endl;
      unsigned int a,b;
      Coord dp;
      const VecDeriv& x = this->mstate->read(core::ConstVecCoordId::position())->getValue();
      const std::vector< Triangle> &tetrahedronArray=_topology->getTriangles() ;
      unsigned int nbEdges=_topology->getNbEdges();
      const vector< Edge> &edgeArray=_topology->getEdges() ;
      edgeInfoVector& edgeInf = *(edgeInfo.beginEdit());
      EdgeInformation *einfo;
      if (updateMatrix) {

        for(unsigned int l=0; l<nbEdges; l++ ) {
          edgeInf[l].DfDx.clear();
        }

        const int vertexSigneIndex[3][3][2]={{{5,5},{2,-1},{1,1}},{{2,1},{5,5},{0,-1}},{{1,-1},{0,1},{5,5}}};


        for (unsigned int i=0; i<SurfaceTriangles.size(); i++){
          EdgesInTriangle te=_topology->getEdgesInTriangle(SurfaceTriangles[i]);

          // Triangle tri=_topology->getTriangle(SurfaceTriangles[i]);
          const Triangle &ta= tetrahedronArray[SurfaceTriangles[i]];

          for (int j=0; j<3;j++){
            einfo= &edgeInf[te[j]];
            EdgeID ei =te[j];
            Edge e=_topology->getEdge(ei);
            if (e[0]==ta[0]) {
              a=0;
              if (e[1]==ta[1])b=1;
              else b=2;
            }
            if (e[0]==ta[1]) {
              a=1;
              if (e[1]==ta[0])b=0;
              else b=2;
            }
            if (e[0]==ta[2]) {
              a=2;
              if (e[1]==ta[1])b=1;
              else b=0;
            }


            Matrix3 &edgeDfDx = einfo->DfDx;
            Matrix3 d2J;
            dp = x[ta[vertexSigneIndex[a][b][0]]]*vertexSigneIndex[a][b][1]/6.0;

            d2J[0][0] = 0;
            d2J[0][1] = -dp[2];
            d2J[0][2] = dp[1];
            d2J[1][0] = dp[2];
            d2J[1][1] = 0;
            d2J[1][2] = -dp[0];
            d2J[2][0] = -dp[1];
            d2J[2][1] = dp[0];
            d2J[2][2] = 0;
            if(!useProjection.getValue())   edgeDfDx-=d2J*Fd/D;
            else edgeDfDx-=d2J*Pv;


          }



        }

        if (EdgesOnBorder.size()>0){
          for(unsigned int u=0; u<EdgesOnBorder.size(); u++){
            std::vector<Edge> Hole=EdgesOnBorder[u];
            for (unsigned int i=0; i<Hole.size();i++){
              Edge ea=Hole[i];
              Edge eb=Hole[(i+1)%Hole.size()];
              EdgeID eia =_topology->getEdgeIndex(ea[0],ea[1]);
              einfo= &edgeInf[eia];
              Matrix3 &edgeDfDx = einfo->DfDx;

              MatNoInit<3,3,Real> d2J;
              //dp =( x[ea[0]]-centroid[u])/6.0+(x[ea[0]]-x[eb[1]])/(Hole.size()*6.0);
              dp =( x[ea[0]]-centroid[u])/6.0;
              d2J[0][0] = 0;
              d2J[0][1] = -dp[2];
              d2J[0][2] = dp[1];
              d2J[1][0] = dp[2];
              d2J[1][1] = 0;
              d2J[1][2] = -dp[0];
              d2J[2][0] = -dp[1];
              d2J[2][1] = dp[0];
              d2J[2][2] = 0;

              if(!useProjection.getValue())edgeDfDx-=d2J*Fd/D;
              else edgeDfDx-=d2J*Pv;

              //if(i==0)  //std::cout<<edgeDfDx<<std::endl;


            }
          }
        }


        updateMatrix=false;
      }

      sofa::helper::AdvancedTimer::stepEnd("updateMatrixData_PressureConstraintFF");
    }


    template <class DataTypes>
    void PressureConstraintForceField<DataTypes>::addDForce(const core::MechanicalParams* mparams /* PARAMS FIRST */, DataVecDeriv& d_df, const DataVecDeriv& d_dx)
    {

      sofa::helper::AdvancedTimer::stepBegin("addDForcePressureConstraintFF");

      unsigned int l=0;
      unsigned int nbEdges=_topology->getNbEdges();
      const vector< Edge> &edgeArray=_topology->getEdges();

      edgeInfoVector& edgeInf = *(edgeInfo.beginEdit());
      EdgeInformation *einfo;

      this->updateMatrixData();

      /// performs matrix vector computation
      unsigned int v0,v1;
      Deriv deltax;	Deriv dv0,dv1;
      const VecDeriv dx=d_dx.getValue();
      VecDeriv& df=*(d_df.beginEdit());
      double kfactor = mparams->kFactor();

      for(l=0; l<nbEdges; l++ )
      {
        einfo=&edgeInf[l];
        v0=edgeArray[l][0];
        v1=edgeArray[l][1];

        deltax= dx[v0] - dx[v1];
        dv0 = einfo->DfDx * deltax;



        // do the transpose multiply:
        dv1[0] = (Real)(deltax[0]*einfo->DfDx[0][0] + deltax[1]*einfo->DfDx[1][0] + deltax[2]*einfo->DfDx[2][0]);
        dv1[1] = (Real)(deltax[0]*einfo->DfDx[0][1] + deltax[1]*einfo->DfDx[1][1] + deltax[2]*einfo->DfDx[2][1]);
        dv1[2] = (Real)(deltax[0]*einfo->DfDx[0][2] + deltax[1]*einfo->DfDx[1][2] + deltax[2]*einfo->DfDx[2][2]);
        // add forces
        df[v0] += dv1*kfactor;
        df[v1] -= dv0*kfactor;

      }
      d_df.endEdit();
      edgeInfo.endEdit();

      sofa::helper::AdvancedTimer::stepEnd("addDForcePressureConstraintFF");
    }

    template <class DataTypes>
    void PressureConstraintForceField<DataTypes>::addKToMatrix(linearalgebra::BaseMatrix *m, SReal kFactor, unsigned int &offset) {
        sofa::helper::AdvancedTimer::stepBegin("addKToMatrix_PressureConstraintFF");

      unsigned int l=0;
      unsigned int nbEdges=_topology->getNbEdges();
      const vector< Edge> &edgeArray=_topology->getEdges();

      edgeInfoVector& edgeInf = *(edgeInfo.beginEdit());
      EdgeInformation *einfo;

      this->updateMatrixData();

      /// performs matrix vector computation
      unsigned int v0,v1;

      for(l=0; l<nbEdges; l++ )
      {
        einfo=&edgeInf[l];
        v0=offset + edgeArray[l][0]*3;
        v1=offset + edgeArray[l][1]*3;

        for (int L=0;L<3;L++) {
          for (int C=0;C<3;C++) {
            double v = einfo->DfDx[L][C] * kFactor;
            m->add(v0+C,v0+L, v);
            m->add(v0+C,v1+L,-v);
            m->add(v1+L,v0+C,-v);
            m->add(v1+L,v1+C, v);
          }
        }
      }

      edgeInfo.endEdit();

      sofa::helper::AdvancedTimer::stepEnd("addKToMatrix_PressureConstraintFF");
    }

    template <class DataTypes>
    void PressureConstraintForceField<DataTypes>::addBToMatrix(linearalgebra::BaseMatrix *m, SReal kFactor, unsigned int &offset) {

    }



    template<class DataTypes>
    void PressureConstraintForceField<DataTypes>::draw(const core::visual::VisualParams* vparams)
    {
      //	unsigned int i;
      if (!vparams->displayFlags().getShowForceFields()) return;
      if (!this->mstate) return;

      if (vparams->displayFlags().getShowWireFrame())
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

      if (vparams->displayFlags().getShowWireFrame())
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    }





    template<class DataTypes>
    PhasesManager::Phase PressureConstraintForceField<DataTypes>::getPhase()
    {
//        Real dt = this->getContext()->getDt();
//        Real tol = m_tolerance.getValue();
//        Real hp = f_hp.getValue();
//        Real time = this->getTime();
//        Real qrs = m_loader_link->m_startContraction.getValue();


//        bool btwStartAndQRS = ((fmod(time, hp) >= 0) && (fmod(time, hp) < qrs));
//        bool increasingPv = (pp <= Pv);

//        bool after_qrs = ((fmod(time, hp) > qrs) and (fmod(time, hp) < qrs+0.1));  // between QRS and 100 ms after

//        bool posInflect = ((pp2-2*pp+Pv)/(dt*dt) > 0);
//        float secDer = (pp2-2*pp+Pv)/(dt*dt);
//        float dPv = (Pv - pp) / dt;

//        std::cout << "time " << time << std::endl;
//        std::cout << "qrs " << qrs << std::endl;
//        std::cout << "dt " << dt << std::endl;
//        std::cout << "tol: " << tol << std::endl;

//        std::cout << "get strain" << std::endl;

//        Real strain = 0; // contraction_forcefield->mesh_mean_strain.getValue(); // m_strain->m_test_strain.getValue();
//        std::cout << "strain: " << strain << std::endl;

//        // Strain
//        if(time > 0)
//        {
//            for(int i=0; i<87973; i++)
//                strain += contraction_forcefield->getEC(i);
//            strain /= 87973;
//        }
//        std::cout << "strain : " << strain << std::endl;


//        if(m_filePressure.isSet()) {
//            msg_error_when(!_pr || _pr.fail()) << "Cannot write pressure to file";
//            dump(_pr, this->getTime(), tol*Pv);
//        }

        return _phase_manager->next(Pv, Pat, Par);
    }



} // namespace forcefield
} // namespace Components
} // namespace Sofa





PhasesManager::PhasesManager(PhasesManager::Phase initPhase)
{
    _current_phase = initPhase;
}

PhasesManager::Phase PhasesManager::next(float Pv, float Pat, float Par)
{

    switch (_current_phase) {
        case FILLING:
            if(Pv > Pat)
            {
                // FILLING: blood from atria, volume increases, almost constant pressure
                // The pressure increases due to the electrical activation and muscle contraction
                // When above atrial pressure, tricupid valves close
                _current_phase = ISOCONTRACTION;
            }
            break;

        case ISOCONTRACTION:
            if(Pv > Par)
            {
                // ISOCONTRACTION: All valves are closed, the pressure increases rapidly, constant volume
                _current_phase = EJECTION;
            }
            break;

        case EJECTION:
            if(Pv < Par)
            {
                // EJECTION: volume ejects the blood through the aorta
                // Ventricular pressure decreases and the aortic valves close under the aortic pressure
                // All valves are closed, the pressure decreases rapidly
                _current_phase = ISORELAXATION;
            }
            break;

        case ISORELAXATION:
            if(Pv <= Pat)
            {
                // ISORELAXATION:
                // When the ventricular pressure falls under the atrial pressure, the atrial valves open
                // The ventricle receives blood from atria, the volume is increasing
                _current_phase = FILLING;
            }
            break;
    }

//    std::cout << "returning _current_phase " << _current_phase << std::endl;
    return _current_phase;
}