#pragma once

#include <sofa/core/behavior/ProjectiveConstraintSet.h>
#include <sofa/core/MechanicalParams.h>
#include <sofa/core/MechanicalParams.h>
//#include <sofa/core/BaseMapping.h>
#include <SofaBaseMechanics/SubsetMapping.h>
#include <SofaBaseLinearSolver/CGLinearSolver.h>
#include "../copy_new_components/PressureFF.h"
//#include "PCG.h"


namespace sofa
{

namespace component
{

namespace behavior
{


template<class DataTypes>
class HemoConstraint : public core::behavior::ProjectiveConstraintSet<DataTypes>
{
public:
    SOFA_CLASS(SOFA_TEMPLATE(HemoConstraint, DataTypes), SOFA_TEMPLATE(core::behavior::ProjectiveConstraintSet, DataTypes));

    typedef typename DataTypes::Real Real;
    typedef typename DataTypes::VecCoord VecCoord;
    typedef typename DataTypes::VecDeriv VecDeriv;
    typedef typename DataTypes::MatrixDeriv MatrixDeriv;
    typedef typename DataTypes::Coord Coord;
    typedef typename DataTypes::Deriv Deriv;
    typedef Data<VecCoord> DataVecCoord;
    typedef Data<VecDeriv> DataVecDeriv;
    typedef Data<MatrixDeriv> DataMatrixDeriv;
    typedef linearsolver::GraphScatteredMatrix Matrix;
    typedef linearsolver::GraphScatteredVector Vector;
    typedef typename VecCoord::template rebind<sofa::Index>::other IndexArray;


//    SingleLink<HemoConstraint, forcefield::PressureFF<defaulttype::Vec3Types>, BaseLink::FLAG_STOREPATH | BaseLink::FLAG_STRONGLINK> l_ff;
    MultiLink<HemoConstraint, forcefield::PressureFF<defaulttype::Vec3Types>, BaseLink::FLAG_STOREPATH | BaseLink::FLAG_STRONGLINK> l_forcefields;

//    SingleLink<HemoConstraint, linearsolver::PCG<Matrix, Vector>, BaseLink::FLAG_STOREPATH | BaseLink::FLAG_STRONGLINK> l_pcg;
//    SingleLink<HemoConstraint, linearsolver::ShewchukPCGLinearSolver<Matrix, Vector>, BaseLink::FLAG_STOREPATH | BaseLink::FLAG_STRONGLINK> l_pcg;
    SingleLink<HemoConstraint, linearsolver::CGLinearSolver<Matrix, Vector>, BaseLink::FLAG_STOREPATH | BaseLink::FLAG_STRONGLINK> l_solver;

    SingleLink<HemoConstraint, component::mapping::SubsetMapping<defaulttype::Vec3Types, defaulttype::Vec3Types>, BaseLink::FLAG_STOREPATH | BaseLink::FLAG_STRONGLINK> l_mapping;


    int number;
    type::Mat<2,2,Real> D,B;
    VecDeriv G[2];
    VecDeriv A[2];
    type::Vec<2,Real> Fd, oldPv, newPv;
	VecDeriv vold;

    IndexArray m_indices;  // List of size `endo_mesh` with `tetra_mesh` indices


    HemoConstraint();
    void init();
    void bwdInit();
    void solveKAB();
    void updatePv(VecCoord& deltav);

    /// Project dx to constrained space (dx models an acceleration).
    virtual void projectResponse(const core::MechanicalParams* mparams, DataVecDeriv& _dx) { }
    /// Project v to constrained space (v models a velocity).
    virtual void projectVelocity(const core::MechanicalParams* mparams, DataVecDeriv& _v);
    /// Project x to constrained space (x models a position).
    virtual void projectPosition(const core::MechanicalParams* mparams, DataVecCoord& _x) { }
    /// Project c to constrained space (c models a constraint).
    virtual void projectJacobianMatrix(const core::MechanicalParams* mparams, DataMatrixDeriv& _cData) { }

    /// Project the global Mechanical Matrix to constrained space using offset parameter
    void applyConstraint(const core::MechanicalParams* /*mparams*/, const sofa::core::behavior::MultiMatrixAccessor* /*matrix*/) override
    {
        msg_warning() << "applyConstraint(mparams, matrix) not implemented. Necessary?";
    }
};

} // namespace behavior

} // namespace components

} // namespace sofa
