#include "ProjectivePressureConstraint.inl"
#include <sofa/core/ObjectFactory.h>


namespace sofa
{

    namespace component
    {

        namespace projectiveconstraintset
        {
  

            SOFA_DECL_CLASS(ProjectivePressureConstraint)

                int ProjectivePressureConstraintClass = core::RegisterObject("Correction the pressure force field for the heart ventricles")

#ifndef SOFA_FLOAT
                .add< ProjectivePressureConstraint<Vec3dTypes> >()
                //.add< ProjectivePressureConstraint<Vec2dTypes> >()
                //.add< ProjectivePressureConstraint<Vec1dTypes> >()
#endif
#ifndef SOFA_DOUBLE
                .add< ProjectivePressureConstraint<Vec3fTypes> >()
                //.add< ProjectivePressureConstraint<Vec2fTypes> >()
                //.add< ProjectivePressureConstraint<Vec1fTypes> >()
#endif
                ;

#ifndef SOFA_FLOAT
            template class ProjectivePressureConstraint<Vec3dTypes>;
            //template class ProjectivePressureConstraint<Vec2dTypes>;
            //template class ProjectivePressureConstraint<Vec1dTypes>;
#endif
#ifndef SOFA_DOUBLE
            template class ProjectivePressureConstraint<Vec3fTypes>;
            //template class ProjectivePressureConstraint<Vec2fTypes>;
            //template class ProjectivePressureConstraint<Vec1fTypes>;
#endif

        }
    }
}
