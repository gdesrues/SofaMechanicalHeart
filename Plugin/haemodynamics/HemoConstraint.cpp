#include "HemoConstraint.h"
#include <sofa/core/ObjectFactory.h>
#include <sofa/defaulttype/VecTypes.h>
#include <SofaBaseLinearSolver/MatrixLinearSolver.h>

namespace sofa
{

namespace component
{

namespace behavior
{


template<class DataTypes>
HemoConstraint<DataTypes>::HemoConstraint()
    : l_forcefields(initLink("ff", "MultiLink the the PressureFF components"))
    , l_solver(initLink("solver", "Link to the linear solver component"))
    , l_mapping(initLink("mapping", "Link to the (subset) mapping component"))
{
}


template<class DataTypes>
void HemoConstraint<DataTypes>::init() {
    core::behavior::ProjectiveConstraintSet<DataTypes>::init();
    msg_error_when(l_forcefields.size()==0) << "Could not find the link to the the PressureFF components (ff)";
    msg_error_when(!l_solver) << "Could not find the link to the the linear solver component (solver)";
    msg_error_when(!l_mapping) << "Could not find the link to the the mapping component (mapping)";

    number = l_forcefields.size();

    // try to get indices with the subset mapping
    const linearalgebra::BaseMatrix* mat = l_mapping->getJ();

    msg_error() << "rowSize: " << mat->rowSize();
    msg_error() << "colSize: " << mat->colSize();



}


template<class DataTypes>
void HemoConstraint<DataTypes>::bwdInit() {
    m_indices = l_mapping->f_indices.getValue();
//    msg_error() << "indices: " << m_indices.size();
}



template<class DataTypes>
void HemoConstraint<DataTypes>::projectVelocity(const core::MechanicalParams *mparams, DataVecDeriv &_v) {
	helper::WriteAccessor<DataVecDeriv> v(_v);
	if (this->getContext()->getTime() == 0) {
    	vold = _v.getValue();
        oldPv[0] = l_forcefields[0]->d_pv_0.getValue();

        return;
    }





	unsigned int numDOFs = this->mstate->getSize();
    VecDeriv deltav; deltav.resize(numDOFs);
	for (unsigned int j=0; j<numDOFs; j++)   {
		deltav[j] = v[j] - vold[j];
	}


//    type::vector<VecDeriv> ff_G; ff_G.resize(number);
//    for(int k=0; k<number; k++) ff_G[k] = l_forcefields[k]->d_dV.getValue();
//
//    core::VecDerivId A_id = core::VecDerivId::dx();
//    core::VecDerivId G_id = core::VecDerivId::force();
//
//    helper::WriteAccessor<DataVecDeriv> A(this->mstate->write(A_id));
//    helper::WriteAccessor<DataVecDeriv> G(this->mstate->write(G_id));
//    A.clear(); A.resize(v.size());
//    G.clear(); G.resize(v.size());
//
//    for(int k=0; k<number; k++)
////        for (unsigned int j=0; j<v.size(); j++)
//            G[k] = ff_G[k];
//
//
////    msg_info() << "S mat before: " << l_solver->getSystemBaseMatrix();
////    l_solver->setSystemMBKMatrix(mparams);
////    msg_info() << "S mat: " << l_solver->getSystemBaseMatrix();
//
//    l_solver->setSystemLHVector(A_id);
//    l_solver->setSystemRHVector(G_id);
////    l_solver->solveSystem();
//
//    Real B = 0;
//    for (unsigned int j=0; j<v.size(); j++)
//        B += ff_G[j] * A[j];
//
//    // We got A, G and B ready to use


    this->solveKAB();
    this->updatePv(deltav);

    helper::WriteAccessor<DataVecCoord> x(this->mstate->write(core::VecCoordId::position()));

    Real dt = this->getContext()->getDt();

    Coord x_before = x[10];
    Deriv v_before = v[10];

    for (unsigned int j=0; j<numDOFs; j++) {
        Coord dP = A[0][j] * (newPv[0] - oldPv[0]);
        if(j==10) msg_error() << "dP 10: " << dP;
        v[j] += dP;
        x[j] += dP * dt;
    }

    Coord x_after = x[10];
    Deriv v_after = v[10];
    msg_error() << "x before: " << x_before;
    msg_error() << "x after: " << x_after;
    msg_error() << "v before: " << v_before;
    msg_error() << "v after: " << v_after;


    oldPv = newPv;
    vold = _v.getValue();
}


template <class DataTypes>
void HemoConstraint<DataTypes>::solveKAB()
{
    unsigned int numDOFs = this->mstate->getSize();

    core::VecDerivId AiD[2];
    core::VecDerivId GiD[2];

    AiD[0] = core::VecDerivId::dx();
    GiD[0] = core::VecDerivId::force();
    DataVecDeriv & dataA = *this->mstate->write(AiD[0]);
    DataVecDeriv & dataForce = *this->mstate->write(GiD[0]);
    VecDeriv & dx = *dataA.beginEdit();
    VecDeriv& force = *dataForce.beginEdit();
    dx.clear();
    dx.resize(numDOFs);
    force.clear();
    force.resize(numDOFs);

//    msg_error() << "numDOFs: " << numDOFs;
//    msg_error() << "ff nb: " << l_forcefields[0]->d_dV.getValue().size();

    for (Index j=0; j<m_indices.size(); j++) {
        force[m_indices[j]] = l_forcefields[0]->d_dV.getValue()[j];
    }
    dataForce.endEdit();
    dataA.endEdit();

    G[0]=dataForce.getValue();

    /// setSystemLHVector
    /// Set the initial estimate of the linear system left-hand term vector, from the values contained in the (Mechanical/Physical)State objects
    /// This vector will be replaced by the solution of the system once solveSystem is called
    l_solver->setSystemLHVector(AiD[0]);

    /// setSystemRHVector
    /// Set the linear system right-hand term vector, from the values contained in the (Mechanical/Physical)State objects
    l_solver->setSystemRHVector(GiD[0]);

    /// solveSystem
    /// Solve the system as constructed using the previous methods
    l_solver->solveSystem();// returns AiD
    A[0]=dataA.getValue();
    A[1].clear();
    G[1].clear();

    if (number==2){
        AiD[1]=core::VecDerivId::dx();
        DataVecDeriv & dataA2 = *this->mstate->write(AiD[1]);
        VecDeriv & dx2 = *dataA2.beginEdit();
        GiD[1]=core::VecDerivId::force();
        DataVecDeriv & dataForce2 = *this->mstate->write(GiD[1]);
        VecDeriv& force2 = *dataForce2.beginEdit();
        dx2.clear();
        dx2.resize(numDOFs);
        force2.clear();
        force2.resize(numDOFs);

        for (Index j=0; j<m_indices.size(); j++) {
            force2[m_indices[j]] = l_forcefields[1]->d_dV.getValue()[j];
        }
        dataForce2.endEdit();
        dataA2.endEdit();

        G[1]=dataForce2.getValue();
        l_solver->setSystemLHVector(AiD[1]);
        l_solver->setSystemRHVector(GiD[1]);
        l_solver->solveSystem();// returns AiD

        A[1]=dataA2.getValue();
    }


    B.clear();
    for (int i=0; i<number; i++)
        for (int j=0; j<number; j++)
            for (unsigned int m=0; m<numDOFs; m++)
                B[i][j] += G[i][m] * A[j][m];
}






template <class DataTypes>
void HemoConstraint<DataTypes>::updatePv(VecCoord& deltav)
{
    const unsigned int ventricles_number = number;

    // Define G\Delta\tilde{s}^{t+dt}
    type::Vec<2, Real> Gdeltav;



    for(unsigned int v=0; v<ventricles_number; ++v)
        for (unsigned int j=0; j< this->mstate->getSize(); j++)
            Gdeltav[v] += G[v][j] * deltav[j];

    msg_error() << "Gdeltav: " << Gdeltav;
    msg_error() << "oldPv: " << oldPv;

    for(unsigned int v=0; v<ventricles_number; ++v)
    {
        msg_error() << "l_forcefields[v]->Fd: " << l_forcefields[v]->Fd;
        msg_error() << "l_forcefields[v]->D: " << l_forcefields[v]->D;

        Fd[v] = l_forcefields[v]->Fd;
        D[v][v] = l_forcefields[v]->D;
    }

    /// Solve P_v^{t+dt}
    // invert for one ventricle
    if (ventricles_number == 1)
    {
        Real inv=1/(B[0][0]+D[0][0]);
        newPv[0]=inv*(Fd[0]+B[0][0]*oldPv[0]-Gdeltav[0]);
    }
        // for 2 ventricles (all in one)
    else if(ventricles_number == 2)
    {
        type::Mat<2,2,Real> mat = B + D;
        type::Mat<2,2,Real> matinvert;

        matinvert.invert(mat);
        newPv = matinvert*(Fd + B * oldPv - Gdeltav);
    }

    // Update Pv in PressureConstraintForceField
    for(unsigned int v=0; v<ventricles_number; ++v)
        l_forcefields[v]->Pv = newPv[v];

    msg_info() << "In updatePv, LV newPv = " << newPv[0];
}





int HemoConstraintClass = core::RegisterObject("Projective constraint")
        .add< HemoConstraint<defaulttype::Vec3Types> >();


} // namespace behavior

} // namespace components

} // namespace sofa
