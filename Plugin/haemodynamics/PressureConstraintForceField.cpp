#include "PressureConstraintForceField.h"
#include "PressureConstraintForceField.inl"

#include <SofaBaseMechanics/MechanicalObject.h>
#include <sofa/core/ObjectFactory.h>

#include <sofa/core/behavior/ForceField.inl>
#include <SofaBaseTopology/TopologyData.inl>

#include <string.h>
#include <iostream>

namespace sofa
{

namespace component
{

namespace forcefield
{

using namespace sofa::defaulttype;
using namespace	sofa::component::topology;
using namespace core::topology;

using std::cerr;
using std::cout;
using std::endl;
using std::string;


//////////****************To register in the factory******************

SOFA_DECL_CLASS(PressureConstraintForceField)

// Register in the Factory
int PressureConstraintForceFieldClass = core::RegisterObject("PressureConstraint's law in Tetrahedral finite elements")
#ifndef SOFA_FLOAT
.add< PressureConstraintForceField<sofa::defaulttype::Vec3dTypes> >()
#endif
#ifndef SOFA_DOUBLE
.add< PressureConstraintForceField<Vec3fTypes> >()
#endif
;

#ifndef SOFA_FLOAT
template class PressureConstraintForceField<Vec3dTypes>;
#endif
#ifndef SOFA_DOUBLE
template class PressureConstraintForceField<Vec3fTypes>;
#endif

} // namespace forcefield

} // namespace component

} // namespace sofa
