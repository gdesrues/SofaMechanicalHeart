#pragma once


//This code aims at apply the pressure constraint (4 valves model) on the heart, using a correction after applying the pressure
// from previous time step.


#include <sofa/core/behavior/ProjectiveConstraintSet.h>
#include <SofaBaseTopology/TopologySubsetData.h>
#include <sofa/core/behavior/MechanicalState.h>
#include <sofa/core/behavior/OdeSolver.h>
#include <sofa/core/behavior/LinearSolver.h>

#include <sofa/type/Mat.h>
#include <sofa/type/Vec.h>
#include <sofa/type/fixed_array.h>
#include <sofa/type/vector.h>

#include "PressureConstraintForceField.h"
#include <SofaBaseLinearSolver/CGLinearSolver.h>


namespace sofa
{

namespace component
{

namespace projectiveconstraintset
{


using namespace sofa::helper;
using namespace sofa::defaulttype;
using namespace sofa::component::topology;
using namespace sofa::component::container;
using namespace sofa::core::objectmodel;



/** Project given particles on a line/segment defined by others particles.
*/
template <class DataTypes>
class ProjectivePressureConstraint : public core::behavior::ProjectiveConstraintSet<DataTypes>
{
public:
    SOFA_CLASS(SOFA_TEMPLATE(ProjectivePressureConstraint,DataTypes), SOFA_TEMPLATE(core::behavior::ProjectiveConstraintSet, DataTypes));

    typedef typename DataTypes::VecCoord VecCoord;
    typedef typename DataTypes::VecDeriv VecDeriv;
    typedef typename DataTypes::MatrixDeriv MatrixDeriv;
    typedef typename DataTypes::Coord Coord;
    typedef typename DataTypes::Deriv Deriv;
    typedef typename DataTypes::Real Real;
    typedef Data<VecCoord> DataVecCoord;
    typedef core::VecDerivId vecid;
    typedef sofa::component::linearsolver::GraphScatteredMatrix ScatMat;
    typedef sofa::component::linearsolver::GraphScatteredVector ScatVec;
    typedef Data<VecDeriv> DataVecDeriv;
    typedef Data<MatrixDeriv> DataMatrixDeriv;
    typedef type::Vec<3,Real> Vec3;
    typedef type::Mat<3,3,Real> Matrix3;
    typedef DataTypes MechanicalTypes ;
    //typedef StdVectorTypes< Vec3, Vec3, Real >     MechanicalTypes ;



public:
    ProjectivePressureConstraint();
    virtual ~ProjectivePressureConstraint();

    // fonction that make the link with the forcefield and the linear solver
    virtual void init();

    virtual void reinit();

    virtual void reset();

    // fonction that calculate Pvnew and give it to the forcefield
    void updatePv(VecCoord& deltax);

    // fonction that fill in the vectorId G to create the RH and then solve with the linear solver
    void solveKAB();

    // fonction that update the velocity : newvel-=A(Pvnew-Pvold)
    // for that, we need to call solveKAB and then updatePv

    void setvold(type::Vec<3,double> voldi, int i);
    type::Vec<3,double> getvold(int i);
    type::Vec<3,double> getpaproj();
    void setpaproj(int a, int b, int c);

    virtual void projectResponse(const core::MechanicalParams* mparams /* PARAMS FIRST */, DataVecDeriv& resData){}
    virtual void projectVelocity(const core::MechanicalParams* mparams /* PARAMS FIRST */, DataVecDeriv& vData);
    virtual void projectPosition(const core::MechanicalParams* mparams /* PARAMS FIRST */, DataVecCoord& xData){}
    virtual void projectJacobianMatrix(const core::MechanicalParams* /*mparams*/, DataMatrixDeriv& cData){}

    virtual sofa::type::vector<double> get_PressionEnergy();

    virtual void draw();


    std::vector<sofa::component::forcefield::PressureConstraintForceField<MechanicalTypes>*> forcefield;
    // sofa::core::behavior::LinearSolver * linearsolver;
    sofa::component::linearsolver::CGLinearSolver<ScatMat,ScatVec> *linearsolver;


public :
    Data<bool> optimiser;

    Data<std::string> m_tagMeshMechanics;
    Data<std::string> m_tagMeshSolver;
    Data< type::vector< std::string > >  FFNames;

    int number;
    int cpt;
    type::Mat<2,2,Real> D,B;
    type::Vec<2,Real> Fd, oldPv, newPv;

    VecDeriv G[2];
    VecDeriv A[2];
    VecDeriv vold;

    unsigned int numDOFs;
    Real dt;

    Data<type::vector<int> > phaseNew;
    Data<int> updateStep;
    type::vector<int> phaseOld;

    Data<std::string> m_filePressure;


//    void writeFile();


    // Quick helper to write to txt files
    template <class... Ts>
    void dump(std::ostream& out, Ts&&... args){
        std::stringstream o;
        ((o << args << ' '), ...);
        out << o.str() << std::endl;
    }


private:
    std::ofstream _pr;

};

} // namespace projectiveconstraintset

} // namespace component

} // namespace sofa

