#pragma once

#include <sofa/core/behavior/ForceField.h>
#include <SofaBaseMechanics/MechanicalObject.h>

#include <sofa/type/fixed_array.h>
#include <sofa/type/vector.h>
#include <sofa/type/Vec.h>
#include <sofa/type/Mat.h>

#include <SofaBaseTopology/TopologyData.h>
#include <sofa/core/topology/BaseMeshTopology.h>

#include <SofaBaseTopology/TetrahedronSetTopologyContainer.h>

#include <sofa/core/topology/BaseMeshTopology.h>
#include <string>
//#include "../../CardiacMeshTools/src/MeshTetrahedrisationLoader.h"
#include "../io_components/HeartMeshLoader.h"
//#include "../../MechanicalHeart/src/ContractionForceField.h"
//#include <SofaLoader/MeshVTKLoader.h>


//#include <SofaImplicitOdeSolver/EulerImplicitSolver.h>


#include <map>
#include <sofa/helper/map.h>

#include <sofa/core/behavior/OdeSolver.h>
#include <sofa/core/visual/VisualModel.h>
#include <sofa/helper/map.h>



class PhasesManager
{
public:
    // Cardiac cycle phases
    enum Phase : unsigned int {
        FILLING = 0,
        ISOCONTRACTION = 1,
        EJECTION = 2,
        ISORELAXATION=3
    };

    PhasesManager(PhasesManager::Phase initPhase);
//    PhasesManager(float Pv, float Pat, float Par, int phase);
    Phase next(float Pv, float Pat, float Par);
    Phase getCurrentPhase() {return _current_phase;}

private:
    Phase _current_phase;
};




namespace sofa
{

  namespace component
  {


    namespace forcefield
    {
      using namespace std;
      using namespace sofa::defaulttype;
      using namespace sofa::type;
      using namespace sofa::component::topology;
      using namespace sofa::component::container;

      template<class DataTypes>
      class PressureConstraintForceField : public core::behavior::ForceField<DataTypes>
      {
      public:
        SOFA_CLASS(SOFA_TEMPLATE(PressureConstraintForceField, DataTypes), SOFA_TEMPLATE(core::behavior::ForceField, DataTypes));

        typedef core::behavior::ForceField<DataTypes> Inherited;
        typedef typename DataTypes::VecCoord VecCoord;
        typedef typename DataTypes::VecDeriv VecDeriv;
        typedef typename DataTypes::Coord    Coord   ;
        typedef typename DataTypes::Deriv    Deriv   ;
        typedef typename Coord::value_type   Real    ;
        typedef typename type::Mat<3,3,Real> Matrix3;
        typedef Vec<3,Real>                  Vec3;
        typedef vector<Vec3>		       VecCoord3d;
        typedef Vec<2,Real>                  Vec2;
        typedef StdVectorTypes< Vec3, Vec3, Real >     MechanicalTypes ; /// assumes the mechanical object type
        typedef MechanicalObject<MechanicalTypes>      MechObject;
        typedef core::topology::BaseMeshTopology::Tetra Element;
        typedef core::topology::BaseMeshTopology::SeqTetrahedra VecElement;
        typedef sofa::core::topology::Topology::Tetrahedron Tetrahedron;
        typedef sofa::core::topology::Topology::TriangleID TriangleID;
        typedef sofa::core::topology::Topology::EdgeID EdgeID;
        typedef sofa::core::topology::Topology::Tetra Tetra;
        typedef sofa::core::topology::Topology::Edge Edge;
        typedef sofa::core::topology::BaseMeshTopology::EdgesInTriangle EdgesInTriangle;
        typedef sofa::core::topology::BaseMeshTopology::EdgesInTetrahedron EdgesInTetrahedron;
        typedef sofa::core::topology::BaseMeshTopology::TrianglesInTetrahedron TrianglesInTetrahedron;

        typedef core::objectmodel::Data<VecDeriv>    DataVecDeriv;
        typedef core::objectmodel::Data<VecCoord>    DataVecCoord;
        typedef type::vector<Real> SetParameterArray;

      public:

        class EdgeInformation
        {
        public:
          /// store the stiffness edge matrix
          Matrix3 DfDx;
          //Vertex Indices for CUDA
          float vertices[2];

          /// Output stream
          inline friend ostream& operator<< ( ostream& os, const EdgeInformation& /*eri*/ ) {  return os;  }
          /// Input stream
          inline friend istream& operator>> ( istream& in, EdgeInformation& /*eri*/ ) { return in; }

          EdgeInformation() {}
        };
        typedef typename VecCoord::template rebind<EdgeInformation>::other edgeInfoVector;

      protected:
        VecDeriv dV;
        std::vector<Coord> dvcentroid;


        std::vector<unsigned int>  SurfaceVertices;
        std::vector<TriangleID> SurfaceTriangles;
        std::vector<std::pair<TriangleID,unsigned int> > SurfaceTrianglesPair;
        std::vector<std::vector<Edge> > EdgesOnBorder;
        VecCoord myposition;
        Real Q;
        Real dQold;
        Real ParOld;
        Real pp, pp2;  // Pv(t-dt), Pv(t-2dt)
        Real V;
        Real GV;

      public:
        Real Kat,Kiso,Kar;
        Real Pat0,Patm,alpha1,alpha2,to,tof,tc,t1,t2,tm;
        Real Par0,Zc,Tau,L,Rp,Pve;
        int windkessel;
        std::vector<Real> KKiso;

      protected:
        MechanicalObject<MechanicalTypes> *mechanicalObject;
        sofa::component::topology::TriangleSetTopologyContainer* _topology;
        VecCoord  _initialPoints;///< the intial positions of the points
        VecDeriv vold;
        std::vector<Coord> xg;
        std::vector<Coord> centroid;
        std::ofstream pressure;
        int phase;
      public:
        Data<std::string> f_loader;
        Data <type::vector <std::string> > m_loaderSurfaceZoneNames;
        Data <type::vector < type::vector <unsigned int> > > m_loaderSurfaceZones;
        Data <type::vector <std::string> > m_loaderPointZoneNames;
        Data <type::vector < type::vector <unsigned int> > > m_loaderPointZones;
        Data <type::vector <unsigned int> > m_loaderTriangleSurf;
        Data<std::string> m_tagMeshSolver;
        Data<SetParameterArray > f_atriumParam;
        Data<SetParameterArray > f_aorticParam;
        Data<Real> f_Pv0,f_hp;
        Data<std::string> fileName;
        Data<SetParameterArray> f_Kiso;
        Data<int> f_windkessel;
        Data <type::vector <std::string> > f_SurfaceZone;
        bool updateMatrix;
        Data<bool> useProjection;
        Data<bool> useVerdandi;
        Data<bool> DisableFirstAtriumContraction;
        Data<std::string> m_whatZone;
        Data<std::map < std::string, sofa::type::vector<double> > > f_graphPressure;
        Data<std::map < std::string, sofa::type::vector<double> > > f_graphVolume;
        Data<std::map < std::string, sofa::type::vector<double> > > f_graphFlow;

        Data<Real> volume,pressurePv,pressurePat,pressurePar,flowQ;

        Data<std::string> displayName;

        // Initialize boundary edges from external json file
        Data<std::string> m_boundaryEdgesPath;  // json path
        Data<std::string> m_boundaryEdgesKey;  // ventricle key

        // Tolerance for the cardiac phase detection
        Data<Real> m_tolerance;

        // SL to the HeartMeshLoader>
//        SingleLink<PressureConstraintForceField, loader::HeartMeshLoader, BaseLink::FLAG_NONE> m_loader_link;

        // Get the current cardiac phase
        PhasesManager::Phase getPhase();

        // Reference to the ventricle phase manager
        PhasesManager* _phase_manager;

//        // Old ventricular pressure  --> pp
//        Real _old_pv;

        // use escalier (true) or sigmoids (false) to model atrial pressure
        Data<bool> m_use_escalier_Pat;


      public:
//        sofa::component::loader::MeshTetrahedrisationLoader* _meshLoader;
        sofa::component::loader::HeartMeshLoader* _vtkLoader;
//        sofa::component::loader::MeshVTKLoader* _vtkLoader;

        Real D,Fd,Pat,Par,Pv0,Pv;
        void updateGraph();
        void testDerivatives();

        PressureConstraintForceField();
        void setG(const unsigned int i,Vec<3,double> vec);
        Vec<3,double> getG(const unsigned int i);

        Vec<9,double> getVariables();


        void setVariables(Real  q_,Real v_,Real Pvold_,Real Pvnew_,Real Par_,Real D_,Real FD_,Real GV_,Real Pat_);

        virtual ~PressureConstraintForceField();

        virtual void init();

        //Used for CUDA non atomic implementation
        void initEdgeNeighbourhoodPoints();

        virtual void reinit();

        virtual void reset();

        virtual void addForce(const core::MechanicalParams* mparams /* PARAMS FIRST */, DataVecDeriv& d_f, const DataVecCoord& d_x, const DataVecDeriv& d_v);
        virtual void addDForce(const core::MechanicalParams* mparams /* PARAMS FIRST */, DataVecDeriv& d_df, const DataVecDeriv& d_dx);

        virtual void addKToMatrix(linearalgebra::BaseMatrix *m, SReal kFactor, unsigned int &offset);
        virtual void addBToMatrix(linearalgebra::BaseMatrix *m, SReal bFactor, unsigned int &offset);
        virtual SReal getPotentialEnergy(const core::MechanicalParams*, const DataVecCoord&) const;
        virtual SReal getPotentialEnergy2() const;

        virtual void updateMatrixData();

        // handle topological changes
        virtual void handleTopologyChange();
        EdgeData<edgeInfoVector> edgeInfo;

        void NanVolume();
        virtual void draw(const core::visual::VisualParams*);



//        SingleLink<PressureConstraintForceField, ContractionForceField<defaulttype::Vec3dTypes>, BaseLink::FLAG_NONE> contraction_forcefield;
        SingleLink<PressureConstraintForceField, core::topology::BaseMeshTopology, BaseLink::FLAG_NONE> tetra_topology;
//        SingleLink<PressureConstraintForceField, misc::StrainMonitor, BaseLink::FLAG_NONE> m_strain;

        Data<std::string> m_filePressure;

        // Quick helper to write to txt files
        template <class... Ts>
        void dump(std::ostream& out, Ts&&... args){
            std::stringstream o;
            ((o << args << ' '), ...);
            out << o.str() << std::endl;
        }

      private:
          std::ofstream _pr;
          Real V_initial;


      };

    } //namespace forcefield

  } // namespace Components

} // namespace Sofa
