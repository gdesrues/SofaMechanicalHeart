#include "ProjectivePressureConstraint.h"
#include <sofa/core/behavior/ProjectiveConstraintSet.inl>
#include <sofa/helper/AdvancedTimer.h>


namespace sofa
{

namespace component
{

namespace projectiveconstraintset
{
    using namespace core::topology;
    using namespace sofa::defaulttype;
    using namespace sofa::type;
    using namespace sofa::core::behavior;


    template <class DataTypes>
    ProjectivePressureConstraint<DataTypes>::ProjectivePressureConstraint()
        : core::behavior::ProjectiveConstraintSet<DataTypes>(NULL)
        , optimiser(initData(&optimiser,"optimiser", "if the correction should be optimized"))
        , m_tagMeshMechanics(initData(&m_tagMeshMechanics,std::string("meca"),"tagMeca","tagMeca"))
        , FFNames(initData(&FFNames,"FFNames","Names of the pressure forcefields"))
        , phaseNew(initData(&phaseNew,"phaseNew","phaseNew"))
        , m_tagMeshSolver(initData(&m_tagMeshSolver, std::string("solver"),"tagSolver","Tag of the Solver Object"))
        , updateStep(initData(&updateStep,"updateSteps","the number of steps after which the projection should be updated"))
        , m_filePressure(initData(&m_filePressure, "file", "File to minitor the new pressure computed for both ventricles"))
    {
    }


    template <class DataTypes>
    ProjectivePressureConstraint<DataTypes>::~ProjectivePressureConstraint()
    {
        _pr.close();
    }



    template <class DataTypes>
    void ProjectivePressureConstraint<DataTypes>::init()
    {

        std::cout<<"Initializing ProjectivePressureConstraint"<<std::endl;
        this->core::behavior::ProjectiveConstraintSet<DataTypes>::init();


        number=0;
        std::vector<std::string> Names=FFNames.getValue();
        int taille=Names.size();


        /// get the linear solver
        Tag mechanicalTag(m_tagMeshMechanics.getValue());

        this->getContext()->get(linearsolver, mechanicalTag,sofa::core::objectmodel::BaseContext::SearchUp);
        if (linearsolver==NULL) {
            serr << "ERROR(ProjectivePressureConstraint): cannot find the linear solver ."<<sendl;
            return;
        }

        /// get the forcefields
        for (int u=0;u<taille;u++){
            sofa::component::forcefield::PressureConstraintForceField<MechanicalTypes>   *f;
            this->getContext()->get(f, Names[u]);

            if (f==NULL) {
                serr << "ERROR(ProjectivePressureConstraint): cannot find forcefield ."<<sendl;
                return;
            }
            else if (f!=NULL) {number++;
            forcefield.push_back(f);
            }

        }


        numDOFs = this->mstate->getSize();
        dt=this->getContext()->getDt();

        oldPv[0]=forcefield[0]->Pv0;
        oldPv[1]=0;
        if(number==1){std::cout << "WARNING: ProjectivePressureConstraint: only one forcefield ."<<std::endl;}
        if (number==2) oldPv[1]=forcefield[1]->Pv0;
        if (number!=taille) {std::cout << "WARNING: forcefield missing"<<std::endl;}

        cpt=0;
        phaseOld.resize(2);


        if(m_filePressure.isSet())
        {
            if(_pr) _pr.close();  // in case of reinit
            _pr.open(m_filePressure.getValue());
            dump(_pr, "time", "Pv_R", "Pv_L");
        }
    }


    template <class DataTypes>
    void ProjectivePressureConstraint<DataTypes>::reinit()
    {
        init();
    }

    template <class DataTypes>
    void ProjectivePressureConstraint<DataTypes>::reset()
    {
        init();
    }

    template <class DataTypes>
    void ProjectivePressureConstraint<DataTypes>::solveKAB()
    {
        vecid AiD[2];
        vecid GiD[2];

        AiD[0] = core::VecDerivId::dx();
        GiD[0] = core::VecDerivId::force();
        DataVecDeriv & dataA = *this->mstate->write(AiD[0]);
        DataVecDeriv & dataForce = *this->mstate->write(GiD[0]);
        VecDeriv & dx = *dataA.beginEdit();
        VecDeriv& force = *dataForce.beginEdit();
        dx.clear();
        dx.resize(numDOFs);
        force.clear();
        force.resize(numDOFs);
        for (unsigned int j=0; j< numDOFs; j++){
            force[j] = forcefield[0]->getG(j);
        }
        dataForce.endEdit();
        dataA.endEdit();

        G[0]=dataForce.getValue();

        /// setSystemLHVector
        /// Set the initial estimate of the linear system left-hand term vector, from the values contained in the (Mechanical/Physical)State objects
        /// This vector will be replaced by the solution of the system once solveSystem is called
        linearsolver->setSystemLHVector(AiD[0]);

        /// setSystemRHVector
        /// Set the linear system right-hand term vector, from the values contained in the (Mechanical/Physical)State objects
        linearsolver->setSystemRHVector(GiD[0]);

        /// solveSystem
        /// Solve the system as constructed using the previous methods
        linearsolver->solveSystem();// returns AiD
        A[0]=dataA.getValue();
        A[1].clear();
        G[1].clear();

        if (number==2){
            AiD[1]=core::VecDerivId::dx();
            DataVecDeriv & dataA2 = *this->mstate->write(AiD[1]);
            VecDeriv & dx2 = *dataA2.beginEdit();
            GiD[1]=core::VecDerivId::force();
            DataVecDeriv & dataForce2 = *this->mstate->write(GiD[1]);
            VecDeriv& force2 = *dataForce2.beginEdit();
            dx2.clear();
            dx2.resize(numDOFs);
            force2.clear();
            force2.resize(numDOFs);

            for (unsigned int j=0; j< numDOFs; j++){
                 force2[j] = forcefield[1]->getG(j);
             }
            dataForce2.endEdit();
            dataA2.endEdit();

            G[1]=dataForce2.getValue();
            linearsolver->setSystemLHVector(AiD[1]);
            linearsolver->setSystemRHVector(GiD[1]);
            linearsolver->solveSystem();// returns AiD

            A[1]=dataA2.getValue();
        }


        B.clear();
        for (int i=0; i<number; i++)
            for (int j=0; j<number; j++)
                for (unsigned int m=0; m<numDOFs; m++)
                     B[i][j] += G[i][m] * A[j][m];
    }






    template <class DataTypes>
    void ProjectivePressureConstraint<DataTypes>::updatePv(VecCoord& deltav)
    {
        const unsigned int ventricles_number = number;

        // Define G\Delta\tilde{s}^{t+dt}
        Vec<2, Real> Gdeltav;

//        msg_error() << Gdeltav;
//        msg_error() << oldPv;

        for (unsigned int j=0; j< numDOFs; j++)
            for(unsigned int v=0; v<ventricles_number; ++v)
                Gdeltav[v] += G[v][j] * deltav[j];

        for(unsigned int v=0; v<ventricles_number; ++v)
        {
            Fd[v] = forcefield[v]->Fd;
            D[v][v] = forcefield[v]->D;
        }

        /// Solve P_v^{t+dt}
        // invert for one ventricle
        if (ventricles_number == 1)
        {
            Real inv=1/(B[0][0]+D[0][0]);
            newPv[0]=inv*(Fd[0]+B[0][0]*oldPv[0]-Gdeltav[0]);
        }
        // for 2 ventricles (all in one)
        else if(ventricles_number == 2)
        {
            Mat<2,2,Real> mat = B + D;
            Mat<2,2,Real> matinvert;

            matinvert.invert(mat);
            newPv = matinvert*(Fd + B * oldPv - Gdeltav);
        }

        // Update Pv in PressureConstraintForceField
        for(unsigned int v=0; v<ventricles_number; ++v)
            forcefield[v]->Pv = newPv[v];

        msg_info() << "In updatePv, LV newPv = " << newPv[1];
    }






    template <class DataTypes>
    void ProjectivePressureConstraint<DataTypes>::projectVelocity(const core::MechanicalParams* /*mparams*/, DataVecDeriv& vData)
    {
        sofa::helper::AdvancedTimer::stepBegin("projectVelocity_ProjectivePressureConstraint");

        msg_info() << "In projectVelocity ! " << this->getTime();
//        std::cout<<"ProjectivePressureConstraint::projectVelocity: cpt= "<<cpt<<" phaseOld= "<<phaseOld<<std::endl;

        type::vector<int> &phaseN=*phaseNew.beginEdit();
        phaseN.resize(2);

        if(cpt==0){
            VecDeriv &vnew=*vData.beginEdit();
            vold=vnew;
            vData.endEdit();

            phaseOld[0] = 3;
            phaseOld[1] = 3;
        }

        if(this->getContext()->getTime()>0){
            VecDeriv &vnew=*vData.beginEdit();

            oldPv[0]=forcefield[0]->Pv;


            Real tol = forcefield[0]->m_tolerance.getValue();



            if(number==2) oldPv[1] = forcefield[1]->Pv;

            if(oldPv[0] <= forcefield[0]->Pat + tol*oldPv[0]){
                phaseN[0] = 0;  // Filling from atrium
            }

            if(oldPv[0] >= forcefield[0]->Par){
                phaseN[0] = 2;  // Ejection

            }
            if((oldPv[0] > forcefield[0]->Pat + tol*oldPv[0]) && (oldPv[0] < forcefield[0]->Par)){
                phaseN[0] = 1;  // Isovolumique

            }
            phaseN[1]=3;

            if(number==2){
                if(oldPv[1] <= forcefield[1]->Pat + tol*oldPv[1]){
                    phaseN[1] = 0;
                }

                if(oldPv[1] >= forcefield[1]->Par){
                    phaseN[1] = 2;

                }
                if((oldPv[1] > forcefield[1]->Pat + tol*oldPv[1]) && (oldPv[1] < forcefield[1]->Par)){
                    phaseN[1] = 1;
                }
            }

            VecDeriv deltav;
            deltav.resize(numDOFs);

            DataVecCoord & dataX = *this->mstate->write(core::VecCoordId::position());
            const VecCoord& xold = this->mstate->read(core::ConstVecCoordId::position())->getValue();

            VecCoord& xnew =*dataX.beginEdit();
            for (unsigned int j=0; j<numDOFs; j++)   {
                deltav[j] = vnew[j] - vold[j];
            }

            if(optimiser.getValue()){
                if ((phaseN!=phaseOld)||(cpt%updateStep.getValue()==0)){
                     this->solveKAB();
                }
            }
            if(!optimiser.getValue())this->solveKAB();

            this->updatePv(deltav);


            // Correct in some stupid cases where "filling but not filling because K evolved"
            bool reupdate = false;
            if((phaseN[0]==0) && (forcefield[0]->Pv > forcefield[0]->Pat + tol*oldPv[0]))
            {
                forcefield[0]->Fd = forcefield[0]->Fd-(forcefield[0]->Kat)*(forcefield[0]->Pat)+(forcefield[0]->Kiso)*(forcefield[0]->Pat);
                forcefield[0]->D = forcefield[0]->Kiso;

                msg_info() << "We are in this weird if condition reupdate";

                reupdate=true;
            }
            if(number==2){
                if((phaseN[1]==0) && (forcefield[1]->Pv > forcefield[1]->Pat + tol*oldPv[1]))
                {
                    forcefield[1]->Fd = forcefield[1]->Fd-(forcefield[1]->Kat)*(forcefield[1]->Pat)+(forcefield[1]->Kiso)*(forcefield[1]->Pat);
                    forcefield[1]->D = forcefield[1]->Kiso;

                    reupdate=true;
                }
            }

            if(reupdate)
                this->updatePv(deltav);

            /// Correct the velocity
            for (unsigned int j=0;j<numDOFs;j++)  {
                vnew[j]+=A[0][j]*(newPv[0]-oldPv[0]);
                xnew[j]=xold[j]+A[0][j]*(newPv[0]-oldPv[0])*dt;
                if(number==2){
                    vnew[j]+=A[1][j]*(newPv[1]-oldPv[1]);
                    xnew[j]+=A[1][j]*(newPv[1]-oldPv[1])*dt;
                }
            }

            vData.endEdit();
            dataX.endEdit();
            vold=vnew;
            phaseOld=phaseN;
        }

        phaseNew.endEdit();
        cpt++;




        if(m_filePressure.isSet()) {
            msg_error_when(!_pr || _pr.fail()) << "Cannot write pressure to file";
            dump(_pr, this->getTime(), newPv[0], newPv[1]);

//            msg_info() << "New LV Pressure is " << newPv[0];
//            msg_info() << "New RV Pressure is " << newPv[1];
        }

        sofa::helper::AdvancedTimer::stepEnd("projectVelocity_ProjectivePressureConstraint");
    }






    template <class DataTypes>
    sofa::type::vector<double> ProjectivePressureConstraint<DataTypes>::get_PressionEnergy()
    {
        sofa::helper::AdvancedTimer::stepBegin("get_PressionEnergy_ProjectivePressureConstraint");

        sofa::type::vector<double> energy;
        energy.resize(2);
        energy[0]=0;
        energy[1]=0;

        if (newPv[0]==0)
            return energy;

        const VecCoord& x = this->mstate->read(core::ConstVecCoordId::position())->getValue();

        for (unsigned int j=0; j< numDOFs; j++){
            energy[0]-=dot(G[0][j],x[j])*newPv[0];
            if(number==2) energy[1]-=dot(G[1][j],x[j])*newPv[1];
        }

        energy[0]=forcefield[0]->getPotentialEnergy2();
        energy[1]=forcefield[1]->getPotentialEnergy2();

        sofa::helper::AdvancedTimer::stepEnd("get_PressionEnergy_ProjectivePressureConstraint");
        return energy;
    }




    template <class DataTypes>
    void ProjectivePressureConstraint<DataTypes>::setvold(Vec<3,double> voldi,int i)
    {
        vold[i]=voldi;
    }

    template <class DataTypes>
    Vec<3,double> ProjectivePressureConstraint<DataTypes>::getvold(int i)
    {
        Vec<3,double> vec;
        vec=vold[i];
        return  vec;
    }
    template <class DataTypes>
    Vec<3,double> ProjectivePressureConstraint<DataTypes>::getpaproj()
    {
        Vec<3,double> vec;
        vec[0]=cpt;
        vec[1]=phaseOld[0];
        vec[2]=phaseOld[1];
        return vec;
    }
    template <class DataTypes>
    void ProjectivePressureConstraint<DataTypes>::setpaproj(int cpt_, int phase1, int phase2)
    {
        cpt=cpt_;
        phaseOld[0]=phase1;
        phaseOld[1]=phase2;

    }



    template <class DataTypes>
    void ProjectivePressureConstraint<DataTypes>::draw()
    {


    }

}
}
}
