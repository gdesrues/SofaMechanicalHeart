#include "BaseConstraintForceField.h"

#include <SofaBaseMechanics/MechanicalObject.h>
#include <sofa/core/ObjectFactory.h>
#include <fstream>
#include <iostream>
#include <GL/gl.h>
#include <SofaBaseTopology/TopologyData.inl>
#include <sofa/core/behavior/ForceField.inl>
#include <sofa/core/visual/VisualParams.h> 
#include <algorithm>
#include <iterator>
#include <sofa/helper/AdvancedTimer.h>

namespace sofa
{
    namespace component
    {
        namespace forcefield
        {
            using namespace sofa::type;
            using namespace sofa::component::topology;
            using namespace core::topology;
            using core::topology::BaseMeshTopology;



            template <class DataTypes> BaseConstraintForceField<DataTypes>::~BaseConstraintForceField()
            {

            }

            template <class DataTypes> BaseConstraintForceField<DataTypes>::BaseConstraintForceField()
                : f_normal(initData(&f_normal,"normal", "vec3d of the normal direction of the spring"))
                , f_Zone(initData(&f_Zone,"Zone","List of tetra on a base"))
                , f_kn(initData(&f_kn,"kn","stiffness in the normal direction"))
                ,f_kparallel(initData(&f_kparallel,"kp","stiffness in the parallel direction"))
                ,f_useForce(initData(&f_useForce,"useForce","if a force has to be added"))
                ,temporaryForce(initData(&temporaryForce,(bool)0,"temporaryForce","temporaryForce"))
                ,temporaryTimes(initData(&temporaryTimes,"temporaryTimes", "vec3d of the 3 times of force"))
                , f_hp(initData(&f_hp,"heartPeriod","heart period"))
                , f_loader(initData(&f_loader,"loadername","loader name"))
                , m_tagMeshSolver(initData(&m_tagMeshSolver, std::string("solver"),"tagSolver","Tag of the Solver Object"))
            {
            }

            template <class DataTypes> void BaseConstraintForceField<DataTypes>::init()
            {


                if(temporaryForce.getValue())
                {std::cout << "initializing BaseConstraintForceField with TemporaryForce" << std::endl;}
                else
                {std::cerr << "initializing BaseConstraintForceField" <<std::endl;}



                this->Inherited::init();
                _topology = this->getContext()->getMeshTopology();


                // get restPosition
                if (_initialPoints.size() == 0)
                {
                    const VecCoord& p = this->mstate->read(core::ConstVecCoordId::restPosition())->getValue();
                    _initialPoints=p;
                }

                Vec3d normal=f_normal.getValue();

                sofa::type::vector<std::string> names;
                sofa::type::vector<std::string> zone;
                sofa::type::vector<sofa::type::vector <unsigned int> > loaderzones;
                std::cout<<f_loader.getValue()<<std::endl;
                if (f_loader.getValue()=="VTK"){


                    this->getContext()->get(_vtkLoader, sofa::core::objectmodel::BaseContext::SearchRoot);
                    if (!_vtkLoader)
                        std::cout << "Error: BaseConstraintForceField is not able to acces mesh loader!" << std::endl;

                    names = _vtkLoader->m_zoneNames.getValue();
                    zone=f_Zone.getValue();
                    loaderzones=_vtkLoader->m_zones.getValue();

                }


                zone.resize(1); zone[0] = "Zone22";  // hardcoded because not working with Data :(

                msg_info() << "End loader";



                for(unsigned int u=0;u<zone.size();u++){
                    for (unsigned int i = 0; i<names.size(); ++i )
                    {
                        msg_info() << zone[u] << " " << names[i];
                        if (names[i] == zone[u])
                        {
                            msg_info() << names[i];

                            sofa::type::vector <unsigned int> tmp = loaderzones[i];
                            for (unsigned int j = 0; j<tmp.size(); ++j)
                            {
                                TetraID tri =tmp[j];
                                BaseTetra.push_back(tri);
                            }
                        }
                    }
                }
                TetraID tri=BaseTetra[0];
                Tetra trian=_topology->getTetra(tri);
                SurfaceVertices.push_back(trian[0]);
		std::cout<<BaseTetra.size()<<std::endl;
                for(unsigned int i=0; i<BaseTetra.size();i++){
                    TetraID tri=BaseTetra[i];
                    Tetra trian=_topology->getTetra(tri);
                    for (unsigned int p = 0; p<4; ++p){
                        bool find = false;
                        unsigned int vertex = trian [p];

                        for (unsigned int k =0; k<SurfaceVertices.size(); ++k){

                            if ( vertex == SurfaceVertices[k])
                            {
                                find = true;
                                break;
                            }
                        }

                        if (!find)
                            SurfaceVertices.push_back(vertex);
                    }
                }

                Matrix3 id;
                id.identity();
                Matrix3 ncrossn;


                for(int n=0;n<3;n++){
                    for(int m=0;m<3;m++){
                        ncrossn[m][n]=normal[n]*normal[m];
                    }
                }

                matK=(f_kn.getValue()-f_kparallel.getValue())*ncrossn+f_kparallel.getValue()*id;


                this->initSurfaceVertices();
            }


            template <class DataTypes>
            void BaseConstraintForceField<DataTypes>::initSurfaceVertices(){}

            template <class DataTypes> void BaseConstraintForceField<DataTypes>::handleTopologyChange()
            //THIS HAS TO BE RE-IMPLEMENTED TO HANDLE TOPOLOGY

            {

            }


            template <class DataTypes>
                    SReal BaseConstraintForceField<DataTypes>::getPotentialEnergy(const core::MechanicalParams*, const DataVecCoord& _x) const
            {
                const VecCoord& x = _x.getValue();
                unsigned int v0;
                Deriv deltax,dv;

                Real energy=0;
                unsigned int l=0;
                for(l=0; l<SurfaceVertices.size(); l++ )
                {
                    v0=SurfaceVertices[l];
                    deltax= x[v0] -_initialPoints[v0];

                    // do the transpose multiply:
                    dv[0] = (Real)(deltax[0]*matK[0][0] + deltax[1]*matK[1][0] + deltax[2]*matK[2][0]);
                    dv[1] = (Real)(deltax[0]*matK[0][1] + deltax[1]*matK[1][1] + deltax[2]*matK[2][1]);
                    dv[2] = (Real)(deltax[0]*matK[0][2] + deltax[1]*matK[1][2] + deltax[2]*matK[2][2]);
                    // add forces
                    energy += dot(deltax,dv)/2.0;

                }

                return energy;
            }

            template <class DataTypes>
                    sofa::type::vector<double> BaseConstraintForceField<DataTypes>::getPotentialEnergy()
                {
                //Only one "zone" to transfer from there
                const VecCoord& x = this->mstate->read(core::ConstVecCoordId::position())->getValue();


                sofa::type::vector<double> energy;
                energy.resize(2);
                energy[0]=0;

                unsigned int v0;
                Deriv deltax,dv;
                Real energ=0;
                unsigned int l=0;
                for(l=0; l<SurfaceVertices.size(); l++ )
                {
                    v0=SurfaceVertices[l];
                    deltax= x[v0] -_initialPoints[v0];

                    // do the transpose multiply:
                    dv[0] = (Real)(deltax[0]*matK[0][0] + deltax[1]*matK[1][0] + deltax[2]*matK[2][0]);
                    dv[1] = (Real)(deltax[0]*matK[0][1] + deltax[1]*matK[1][1] + deltax[2]*matK[2][1]);
                    dv[2] = (Real)(deltax[0]*matK[0][2] + deltax[1]*matK[1][2] + deltax[2]*matK[2][2]);
                    // add forces
                   energ += dot(deltax,dv)/2.0;

                }
                energy[0]=energ;
                return energy;
            }


            template <class DataTypes>
             void BaseConstraintForceField<DataTypes>::addForce(const core::MechanicalParams* mparams /* PARAMS FIRST */, DataVecDeriv& d_f, const DataVecCoord& d_x, const DataVecDeriv& /*d_v*/)
            {
                sofa::helper::AdvancedTimer::stepBegin("addForceBaseConstraintFF");            
                if(f_useForce.getValue()){
                unsigned int l=0;

                /// performs matrix vector computation
                unsigned int v0;
                Deriv deltax,dv;

                const VecCoord x=d_x.getValue();
                VecCoord& f=*(d_f.beginEdit());

                double kfactor = mparams->kFactor();
                double bfactor = 1.0;

                if(temporaryForce.getValue())
                {
                    Vec3d times=temporaryTimes.getValue();
                    double simuTime = this->getContext()->getTime();
                    int numberCycles=1;
                    numberCycles=int(simuTime/f_hp.getValue())+1;
                    double Tm=times[1]+(numberCycles-1)*f_hp.getValue(); // //middle time ??
                    double Tof=times[0]+(numberCycles-1)*f_hp.getValue(); // offset time ?? (beginning)
                    double Tc=times[2]+(numberCycles-1)*f_hp.getValue(); // contraction time ??
                    double T1=(Tm+Tof)/2;
                    double T2=(Tc+Tm)/2;

                    if((simuTime<=Tm)&&(simuTime>=Tof)){ // first sigmoid, augment force
                    bfactor=1.0/(1.0+exp(-(7/(Tm-Tof))*(simuTime-T1)));
                    }
                    else if((simuTime>Tm)&&(simuTime<=Tc)){ // second, decrease force
                    bfactor=1.0/(1.0+exp((7/(Tc-Tm))*(simuTime-T2)));
                    bfactor=0.0;
                    }
                    else{
                    bfactor=0.0;
                    }

//                    cout << "bfactor " << bfactor << endl;
                }

                for(l=0; l<SurfaceVertices.size(); l++ )
                {
                    v0=SurfaceVertices[l];
                    deltax= x[v0] -_initialPoints[v0];

                    // do the transpose multiply:
                    dv[0] = (Real)(deltax[0]*matK[0][0] + deltax[1]*matK[1][0] + deltax[2]*matK[2][0]);
                    dv[1] = (Real)(deltax[0]*matK[0][1] + deltax[1]*matK[1][1] + deltax[2]*matK[2][1]);
                    dv[2] = (Real)(deltax[0]*matK[0][2] + deltax[1]*matK[1][2] + deltax[2]*matK[2][2]);
                    // add forces
                    f[v0] += dv*bfactor*kfactor;

                }


                d_f.endEdit();
                sofa::helper::AdvancedTimer::stepEnd("addForceBaseConstraintFF");
            }

            }

            template <class DataTypes>
                    void BaseConstraintForceField<DataTypes>::addDForce(const core::MechanicalParams* mparams /* PARAMS FIRST */, DataVecDeriv& d_df, const DataVecDeriv& d_dx)
            {
                sofa::helper::AdvancedTimer::stepBegin("addDForceBaseConstraintFF");
                unsigned int l=0;

                /// performs matrix vector computation
                unsigned int v0;
                Deriv deltadx,dv;

                const VecDeriv dx=d_dx.getValue();
                VecDeriv& df=*(d_df.beginEdit());
                double kfactor = mparams->kFactor();

                double bfactor = 1.0;

                if(temporaryForce.getValue())
                {
                    Vec3d times=temporaryTimes.getValue();
                    double simuTime = this->getContext()->getTime();
                    int numberCycles=1;
                    numberCycles=int(simuTime/f_hp.getValue())+1;
                    double Tm=times[1]+(numberCycles-1)*f_hp.getValue(); // //middle time ??
                    double Tof=times[0]+(numberCycles-1)*f_hp.getValue(); // offset time ?? (beginning)
                    double Tc=times[2]+(numberCycles-1)*f_hp.getValue(); // contraction time ??
                    double T1=(Tm+Tof)/2;
                    double T2=(Tc+Tm)/2;

                    if((simuTime<=Tm)&&(simuTime>=Tof)){ // first sigmoid, augment force
                    bfactor=1.0/(1.0+exp(-(7/(Tm-Tof))*(simuTime-T1)));
                    }
                    else if((simuTime>Tm)&&(simuTime<=Tc)){ // second, decrease force
                    bfactor=1.0/(1.0+exp((7/(Tc-Tm))*(simuTime-T2)));
                    bfactor=0.0;
                    }
                    else{
                    bfactor=0.0;
                    }

                }


                for(l=0; l<SurfaceVertices.size(); l++ )
                {
                    v0=SurfaceVertices[l];
                    deltadx= dx[v0];

                    // do the transpose multiply:
                    dv[0] = (Real)(deltadx[0]*matK[0][0] + deltadx[1]*matK[1][0] + deltadx[2]*matK[2][0]);
                    dv[1] = (Real)(deltadx[0]*matK[0][1] + deltadx[1]*matK[1][1] + deltadx[2]*matK[2][1]);
                    dv[2] = (Real)(deltadx[0]*matK[0][2] + deltadx[1]*matK[1][2] + deltadx[2]*matK[2][2]);
                    // add forces
                    df[v0] -= dv*bfactor*kfactor;

                }


                d_df.endEdit();
                sofa::helper::AdvancedTimer::stepEnd("addDForceBaseConstraintFF");
            }



            template<class DataTypes>
            void BaseConstraintForceField<DataTypes>::draw(const core::visual::VisualParams* vparams)
            {
                    //	unsigned int i;
                    if (!vparams->displayFlags().getShowForceFields()) return;
                    if (!this->mstate) return;

                    if (vparams->displayFlags().getShowWireFrame())
                            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

                    if (vparams->displayFlags().getShowWireFrame())
                            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

            }




        } // namespace forcefield
    } // namespace Components
} // namespace Sofa

