#include "AtFF.h"
#include "AtFF.inl"


#include <SofaBaseMechanics/MechanicalObject.h>
#include <sofa/core/ObjectFactory.h>

#include <sofa/core/behavior/ForceField.inl>
#include <SofaBaseTopology/TopologyData.inl>

#include <string.h>
#include <iostream>

namespace sofa
{

namespace component
{

namespace forcefield
{

using namespace sofa::defaulttype;
using namespace	sofa::component::topology;
using namespace core::topology;

using std::cerr;
using std::cout;
using std::endl;
using std::string;


//////////****************To register in the factory******************

SOFA_DECL_CLASS(AtFF)

// Register in the Factory
int AtFFClass = core::RegisterObject("Spring Constraint on the base for heart simulation")
#ifndef SOFA_FLOAT
.add< AtFF<sofa::defaulttype::Vec3dTypes> >()
#endif
#ifndef SOFA_DOUBLE
.add< AtFF<Vec3fTypes> >()
#endif
;

#ifndef SOFA_FLOAT
template class AtFF<Vec3dTypes>;
#endif
#ifndef SOFA_DOUBLE
template class AtFF<Vec3fTypes>;
#endif

} // namespace forcefield

} // namespace component

} // namespace sofa
