#pragma once

#include <sofa/core/visual/VisualModel.h>
#include <sofa/core/behavior/ForceField.h>
#include <SofaBaseMechanics/MechanicalObject.h>
#include <sofa/type/fixed_array.h>
#include <sofa/type/vector.h>
#include <sofa/type/Vec.h>
#include <sofa/type/Mat.h>
#include <SofaBaseTopology/TopologyData.h>
#include <sofa/core/topology/BaseMeshTopology.h>
#include <SofaBaseTopology/TetrahedronSetTopologyContainer.h>
#include <sofa/core/topology/BaseMeshTopology.h>
#include "../io_components/HeartMeshLoader.h"
#include <string>


namespace sofa
{

namespace component
{


namespace forcefield
{
using namespace std;
using namespace sofa::type;
using namespace sofa::defaulttype;
using namespace sofa::component::topology;
using namespace sofa::component::container;

template<class DataTypes>
class BaseConstraintForceField : public core::behavior::ForceField<DataTypes>
{
 public:
  SOFA_CLASS(SOFA_TEMPLATE(BaseConstraintForceField, DataTypes), SOFA_TEMPLATE(core::behavior::ForceField, DataTypes));

  typedef core::behavior::ForceField<DataTypes> Inherited;
  typedef typename DataTypes::VecCoord VecCoord;
  typedef typename DataTypes::VecDeriv VecDeriv;
  typedef typename DataTypes::Coord    Coord   ;
  typedef typename DataTypes::Deriv    Deriv   ;
  typedef typename Coord::value_type   Real    ;
  typedef typename type::Mat<3,3,Real> Matrix3;
  typedef Vec<3,Real>                  Vec3;
  typedef vector<Vec3>		       VecCoord3d;
  typedef Vec<2,Real>                  Vec2;
  typedef StdVectorTypes< Vec3, Vec3, Real >     MechanicalTypes ; /// assumes the mechanical object type
  typedef MechanicalObject<MechanicalTypes>      MechObject;
  
  typedef core::topology::BaseMeshTopology::Tetra Element;
  typedef core::topology::BaseMeshTopology::SeqTetrahedra VecElement;
  typedef core::objectmodel::Data<VecDeriv>    DataVecDeriv;
  typedef core::objectmodel::Data<VecCoord>    DataVecCoord;
  typedef sofa::core::topology::Topology::TriangleID TriangleID;
  typedef sofa::core::topology::Topology::TetraID TetraID;
  typedef sofa::core::topology::Topology::EdgeID EdgeID;
  typedef sofa::core::topology::Topology::Tetrahedron Tetrahedron;
  typedef sofa::core::topology::Topology::Tetra Tetra;
  typedef type::vector<Real> SetParameterArray;
public:


protected:

        std::vector<TriangleID> BaseTetra;
        std::vector<unsigned int> SurfaceVertices;

        MechanicalObject<MechanicalTypes> *mechanicalObject;
        sofa::core::topology::BaseMeshTopology* _topology;
        VecCoord  _initialPoints;///< the intial positions of the points

public:

        Data<bool> f_useForce;
        Matrix3 matK;
        Data<Vec3d> f_normal;
        Data<Real> f_kn;
        Data<bool> temporaryForce;
        Data<Vec3d> temporaryTimes;
        Data<Real> f_kparallel;
        Data<Real> f_hp;
        Data <type::vector <std::string> > f_Zone;
        Data<std::string> m_tagMeshSolver;
        Data<std::string> f_loader;
public:

        BaseConstraintForceField();

        virtual ~BaseConstraintForceField();

        virtual void init();

        //Used for CUDA non atomic implementation
        void initSurfaceVertices();
        

        //virtual void reinit();

        virtual void addForce(const core::MechanicalParams* mparams /* PARAMS FIRST */, DataVecDeriv& d_f, const DataVecCoord& d_x, const DataVecDeriv& d_v);
        virtual void addDForce(const core::MechanicalParams* mparams /* PARAMS FIRST */, DataVecDeriv& d_df, const DataVecDeriv& d_dx);

        virtual SReal getPotentialEnergy(const core::MechanicalParams*, const DataVecCoord&) const;
        virtual sofa::type::vector<double> getPotentialEnergy();


        // handle topological changes
       virtual void handleTopologyChange();

     //  sofa::component::odesolver::EulerImplicitSolver *eul;
     //  sofa::component::odesolver::VariationnalSolver *var;
       virtual void draw(const core::visual::VisualParams*);
       sofa::component::loader::HeartMeshLoader* _vtkLoader;

};

} //namespace forcefield

} // namespace Components

} // namespace Sofa

