# SofaMechanicalHeart


## Setup

1. Compile SOFA with the following plugins: SofaMechanicalHeart, SofaPython3. See
[this](https://gitlab.inria.fr/gdesrues1/sofaintro) for
compiling SOFA and [this](https://sofapython3.readthedocs.io/en/latest/) to install
SofaPython.
2. Create a virtual env `python -m venv venv`
3. Add the following env variables: `export SOFA_ROOT=path-to/sofa/v21.12/build` 
and `export PYTHONPATH=$SOFA_ROOT/lib/python3/site-packages:$PYTHONPATH` to the `venv/bin/activate` script
4. Activate `source venv/bin/activate`
5. Try: `python -c "import Sofa"`. If Sofa is not found, check your paths
6. In pycharm, you can add PYTHONPATH in the interpreter settings and environment variables
in the run configurations.